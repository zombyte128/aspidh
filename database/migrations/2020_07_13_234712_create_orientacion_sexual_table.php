<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrientacionSexualTable extends Migration
{
    
    public function up()
    {
        Schema::create('orientacion_sexual', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_orientacion_sexual');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orientacion_sexual');
    }
}
