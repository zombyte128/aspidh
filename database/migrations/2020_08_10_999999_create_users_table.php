<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->unsignedBigInteger('pais_id')->nullable();
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
            $table->unsignedBigInteger('departamento_id')->nullable();
            $table->foreign('departamento_id')->references('id')->on('departamentos')->onDelete('cascade');
            $table->unsignedBigInteger('ciudad_id')->nullable();
            $table->foreign('ciudad_id')->references('id')->on('ciudades')->onDelete('cascade');
            $table->unsignedBigInteger('tipo_genero_id')->nullable();
            $table->foreign('tipo_genero_id')->references('id')->on('tipo_generos')->onDelete('cascade');
            $table->unsignedBigInteger('orientacion_sexual_id')->nullable();
            $table->foreign('orientacion_sexual_id')->references('id')->on('orientacion_sexual')->onDelete('cascade');
            $table->string('nombres_segun_dui')->nullable();
            $table->string('apellidos_segun_dui')->nullable();
            $table->string('nombre_social')->nullable();//conocido por? en caso de transexual?...digo yo jajaja
            $table->date('fecha_nacimiento')->nullable();
            $table->string('edad')->nullable();
            $table->string('documento_identificacion')->unique()->nullable();//En caso de no poseer documento de identificación agregar una casilla en la que el usuario-admin pueda colocar el motivo 
            $table->string('email')->unique()->nullable();
            $table->string('numero_telefono')->nullable();
            $table->text('direccion_completa')->nullable();
            $table->string('redes_social_facebook')->nullable();
            $table->string('redes_social_twitter')->nullable();
            $table->string('redes_social_instagram')->nullable();
            $table->string('nombres_emergencia')->nullable();
            $table->string('telefono_emergencia')->nullable();
            $table->string('direccion_emergencia')->nullable();
            $table->string('redes_sociales_emergencia')->nullable();
            //$table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('contrasena')->nullable();
            $table->string('username')->unique()->nullable();;
            /** Educacion */
            $table->string('estudia_actualmente')->nullable();//¿Estudia actualmente? Sí / No 
            $table->string('razon_estudio')->nullable();//porque no estudia
            $table->string('ultimo_grado_estudio_aprobado')->nullable();//1° a 3° / 4° a 6° / 7° a 9° / Bachillerato / Universitario / Maestría / Doctorado  
            $table->string('lugar_estudio_primario')->nullable();//Público / Privado / A distancia 
            $table->string('lugar_estudio_universitario')->nullable();//Público / Privado / A distancia
            /** Acceso a salud */
            $table->string('afiliado_seguro_social')->nullable();//si o no
            $table->string('utiliza_servcios_de_salud')->nullable();//si o no
            $table->string('centro_medico_asiste')->nullable();//Hospital Público / Hospital Privado / Clínica comunal / Unidad de Salud
            $table->string('beneficiario_programa_salud_gubernamental')->nullable();//si o no...cual?
            $table->string('cual_programa_salud')->nullable();//cual? programa de saud gubernamental
            /** Trabajo y empleo */
            $table->string('cuenta_con_empleo_formal')->nullable();//si o no
            $table->string('sector_trabajo')->nullable();//: Privado / Gobierno / ONG / Informal / Propio
            $table->string('negacion_trabajo_por_identidad_genero_orientacion_sexual')->nullable();//si o no
            $table->string('beneficiario_programa_acceso_trabajo_gubernamental')->nullable();//si o no...cual?
            $table->string('cual_programa_trabajo')->nullable();//cual? programa de trabajo gubernamental
            /** Acceso a la vivienda */
            $table->string('vive_actualmente')->nullable();//: Propio / Alquilado / Vivienda familiar / Otro 
            $table->string('con_quien_vive')->nullable();//Solo-a / Familia nuclear / Amigos/as / Pareja / Otros
            $table->string('ingresos_financieros_mensual_aproximado')->nullable();//Menos de $250 / $251 a $500 / $501 a $1,000 / $1,000 a $1,500 / Más de $1,500 
            $table->string('solicitud_credito_para_vivienda')->nullable();//si o no
            $table->string('credito_otorgado')->nullable();//Razon del creito otorgado
            $table->string('justificacion_credito')->nullable();//si o no
            $table->string('beneficiario_programa_acceso_vivienda_gubernamental')->nullable();//si o no...cual?
            $table->string('cual_programa_vivienda')->nullable();//cual? programa de vivienda

            /** PARTICIPACIÓN POLÍTICA Y RELIGIÓN */
            $table->string('ejecicio_derecho_voto')->nullable();//si o no
            $table->string('problemas_al_derecho_voto')->nullable();// si o no..cuales?
            $table->string('cual_problema_voto')->nullable();// Si tuvo problemas de voto la razon
            $table->string('participa_en_politica')->nullable();//si o no
            $table->string('practica_alguna_religion')->nullable();//si o no
            $table->string('tipo_religion')->nullable();// Católica / Evangélica / Ninguna / Otra
            /** VULNERACIÓN DE DERECHOS  */
            $table->string('discriminacion_agrecion_centro_estudio')->nullable();//si o no
            $table->string('discriminacion_agrecion_trabajo')->nullable();//si o no
            $table->string('discriminacion_agrecion_centro_salud')->nullable();//si o no
            $table->string('por_quien_hecho_de_agresion_discriminacion')->nullable();//Familiares / Amigos / Pareja / Iglesia / Trabajo / PNC / Militares / Médicos
            $table->string('otro_por_quien_discrimanacion')->nullable();//otros pregunta de arriba
            $table->string('tipo_agresion_frecuente')->nullable();//Física / Psicológica / Económica 
            $table->string('otro_tipo_agresion')->nullable();//otro pregunta de arriba
            $table->string('conocimientos_mecanimos_defensa_de_derechos')->nullable();//si o no
            $table->rememberToken(); 
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
