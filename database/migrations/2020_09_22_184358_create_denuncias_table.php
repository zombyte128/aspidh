<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDenunciasTable extends Migration
{
    public function up() 
    {
        Schema::create('denuncias', function (Blueprint $table) {
            $table->id();
            /** Info del usuario ( hereda info de la tabla users) */
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            /** informacion del hecho */
            $table->string('quien_registra_denuncia')->nullable();
            $table->date('fecha_del_suceso')->nullable();
            $table->string('donde_ocurrio')->nullable();
            $table->text('breve_relato_del_hecho_consecuencias')->nullable();
            $table->string('tipo_principal_violacion_abuso')->nullable();
            $table->unsignedBigInteger('pais_id')->nullable();
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
            $table->unsignedBigInteger('departamento_id')->nullable();
            $table->foreign('departamento_id')->references('id')->on('departamentos')->onDelete('cascade');
            $table->unsignedBigInteger('ciudad_id')->nullable();
            $table->foreign('ciudad_id')->references('id')->on('ciudades')->onDelete('cascade');
            $table->text('direccion_completa')->nullable();//donde ocurrieron los hechos
            $table->string('tipo_secundario_violacion_abuso')->nullable();
            /** Información del/os autor/es del hecho */
            $table->string('indique_cantidad_agresores')->nullable();
            $table->string('forma_de_agrecion')->nullable();//Verbal,Golpe físico,Arma blanca,Arma de fuego,Vehículo,Otros detallar
            $table->string('informacion_del_autor_del_hecho')->nullable();
            /********* acciones realizadas *********/
            $table->string('realizo_una_denuncia_formal')->nullable();//si,no,no sabe o no contesta
            //si contesta que si realizo la denuncia formal tiene que indicar en este selected o campo el lugar donde realizo la denuncia
            $table->string('lugar_donde_realizo_la_denuncia_formal')->nullable();
            $table->date('fecha_cuando_se_reaizo_la_denuncia_formal')->nullable();
            $table->string('documentacion_respaldatoria')->nullable();
            // foto de cualquiera de estos documentos ////
            $table->string('foto_dui_frente')->nullable();
            $table->string('foto_dui_atras')->nullable();
            $table->string('documento_dui')->nullable();
            $table->string('foto_nit_frente')->nullable();
            $table->string('foto_nit_atras')->nullable();
            $table->string('documento_nit')->nullable();
            $table->string('foto_acta')->nullable();
            $table->string('documento_acta')->nullable();
            $table->string('foto_denuncia')->nullable();
            $table->string('documento_denuncia')->nullable();
            $table->string('foto_otro')->nullable();
            $table->string('foto_otro2')->nullable();
            $table->string('documento_otro')->nullable();
            /** estado de la denuncia */
            $table->string('estado_de_la_denuncia')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('denuncias');
    }
}
