<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'nombres_segun_dui' => 'monica gabriela',
            'apellidos_segun_dui' => 'morales santos',
            'email' => 'superadmin@aspidh.com',
            'password' => bcrypt('123456'),
            'contrasena' => '123456',
        ]);

        DB::table('users')->insert([
            'role_id' => '2',
            'nombres_segun_dui' => 'fundación',
            'apellidos_segun_dui' => 'aspidh',
            'email' => 'admin@aspidh.com',
            'password' => bcrypt('123456'),
            'contrasena' => '123456',
        ]);
    }
}
