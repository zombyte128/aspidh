<?php

use Illuminate\Database\Seeder;

class CiudadesNicaraguaSeeder extends Seeder
{
    
    public function run()
    {

        ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Boaco ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '37',
            'nombre_ciudad' => 'Boaco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '37',
            'nombre_ciudad' => 'Camoapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '37',
            'nombre_ciudad' => 'San José de Los Remates',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '37',
            'nombre_ciudad' => 'San Lorenzo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '37',
            'nombre_ciudad' => 'Santa Lucía',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '37',
            'nombre_ciudad' => 'Teustepe',
        ]);

   ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Carazo ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'Diriamba',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'Dolores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'El Rosario',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'Jinotepe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'La conquista',
            ]);
            
        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'La Paz de Oriente',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'San Marcos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '38',
            'nombre_ciudad' => 'Santa Teresa',
        ]);

        ///////////////////////////////////////////////////////

///////////////// Ciudades departamento Chinandega ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Chichigalpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Chinandega',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Cinco Pinos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Corinto',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'El Realejo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'El viejo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Posoltega',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Puerto Morazán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'San Francisco del Norte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'San Pedro del Norte',
        ]);
        
        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Santo Tomás del Norte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Somotillo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '39',
            'nombre_ciudad' => 'Villanueva',
        ]);

        ///////////////////////////////////////////////////////

///////////////// Ciudades departamento Chontales ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'Acoyapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'Comalapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'Cuapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'El Coral',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'Juigalpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'La Libertad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'San Pedro de Lóvago',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'Santo Domingo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'Santo Tomás',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '40',
            'nombre_ciudad' => 'Villa Sandino',
        ]);

        //////////////////////////////////////////////////////

///////////////// Ciudades departamento Costa Caribe Norte ////////////////////

        DB::table('ciudades')->insert([
                    'departamento_id' => '41',
                    'nombre_ciudad' => 'Bonanza',
        ]);

        DB::table('ciudades')->insert([
                'departamento_id' => '41',
                'nombre_ciudad' => 'Mulukukú',
        ]);

        DB::table('ciudades')->insert([
                'departamento_id' => '41',
                'nombre_ciudad' => 'Prinzapolka',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '41',
            'nombre_ciudad' => 'Puerto Cabezas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '41',
            'nombre_ciudad' => 'Puerto Cabezas',
        ]);

        DB::table('ciudades')->insert([
                'departamento_id' => '41',
                'nombre_ciudad' => 'Rosita',
        ]);

        DB::table('ciudades')->insert([
                'departamento_id' => '41',
                'nombre_ciudad' => 'Siuna',
        ]);

        DB::table('ciudades')->insert([
                'departamento_id' => '41',
                'nombre_ciudad' => 'Waslala',
        ]);

        DB::table('ciudades')->insert([
                'departamento_id' => '41',
                'nombre_ciudad' => 'Waspán',
        ]);

    //////////////////////////////////////////////////////

///////////////// Ciudades departamento Costa Caribe Sur ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Bluefields',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Desembocadura de Río Grande',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'El Ayote',
        ]);
        
        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'El Rama',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'El Tortuguero',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Isla del Maíz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Kukra Hill',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'La Cruz de Río Grande',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Laguna de Perlas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Muelle de los Bueyes',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Nueva Guinea',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '42',
            'nombre_ciudad' => 'Paiwas',
        ]);

    //////////////////////////////////////////////////////

///////////////// Ciudades departamento Estelí ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '43',
            'nombre_ciudad' => 'Condega',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '43',
            'nombre_ciudad' => 'Estelí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '43',
            'nombre_ciudad' => 'La Trinidad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '43',
            'nombre_ciudad' => 'Pueblo Nuevo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '43',
            'nombre_ciudad' => 'San Juan de Limay',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '43',
            'nombre_ciudad' => 'San Nicolás',
        ]);

    //////////////////////////////////////////////////////

///////////////// Ciudades departamento Granada ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '44',
            'nombre_ciudad' => 'Diriá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '44',
            'nombre_ciudad' => 'Diriomo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '44',
            'nombre_ciudad' => 'Granada',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '44',
            'nombre_ciudad' => 'El Cuá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '44',
            'nombre_ciudad' => 'Jinotega',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '44',
            'nombre_ciudad' => 'Granada',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '44',
            'nombre_ciudad' => 'Nandaime',
        ]);


            //////////////////////////////////////////////////////

///////////////// Ciudades departamento Jinotega ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'El Cuá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'Jinotega',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'La Concordia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'San José de Bocay',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'San Rafael del Norte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'San Sebastián de Yalí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'Santa María de Pantasma',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '45',
            'nombre_ciudad' => 'Wiwilí de Jinotega',
        ]);

   //////////////////////////////////////////////////////

///////////////// Ciudades departamento León ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'Achuapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'El Jicaral',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'El Sauce',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'La Paz Centro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'Larreynaga',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'León',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'Nagarote',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'Quezalguaque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'Santa Rosa del Peñón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '46',
            'nombre_ciudad' => 'Telica',
        ]);

   //////////////////////////////////////////////////////

///////////////// Ciudades departamento Madriz ////////////////////


        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'Las Sabanas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'Palacagüina',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'San José de Cusmapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'San Juan de Río Coco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'San Lucas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'Somoto',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'Telpaneca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'Totogalpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '47',
            'nombre_ciudad' => 'Yalagüina',
        ]);

   //////////////////////////////////////////////////////

///////////////// Ciudades departamento Managua ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'Ciudad Sandino',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'El Crucero',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'Managua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'Mateare',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'San Francisco Libre',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'San Rafael del Sur',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'Ticuantepe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'Tipitapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '48',
            'nombre_ciudad' => 'Villa El Carmen',
        ]);

    //////////////////////////////////////////////////////

///////////////// Ciudades departamento Masaya ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'Catarina',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'La Concepción',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'Masatepe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'Masaya',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'Nandasmo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'Nindirí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'Niquinohomo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'San Juan de Oriente',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '49',
            'nombre_ciudad' => 'Tisma',
        ]);

    //////////////////////////////////////////////////////

///////////////// Ciudades departamento Matagalpa ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Ciudad Darío',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'El Tuma - La Dalia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Esquipulas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Matagalpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Matiguás',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Muy Muy',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Rancho Grande',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Río Blanco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'San Dionisio',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'San Isidro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'San Ramón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Sébaco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '50',
            'nombre_ciudad' => 'Terrabona',
        ]);

   //////////////////////////////////////////////////////

///////////////// Ciudades departamento Nueva Segovia ////////////////////


        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Ciudad Antigua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Dipito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'El Jícaro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Jalapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Macuelizo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Mozonte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Murra',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Ocotal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Quilalí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'San Fernando',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Santa María',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '51',
            'nombre_ciudad' => 'Wiwilí',
        ]);

    //////////////////////////////////////////////////////

///////////////// Ciudades departamento Río San Juan ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '52',
            'nombre_ciudad' => 'El Almendro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '52',
            'nombre_ciudad' => 'El Castillo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '52',
            'nombre_ciudad' => 'Morrito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '52',
            'nombre_ciudad' => 'San Carlos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '52',
            'nombre_ciudad' => 'San Juan del Norte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '52',
            'nombre_ciudad' => 'San Miguelito',
        ]);

    //////////////////////////////////////////////////////

///////////////// Ciudades departamento Rivas ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Altagracia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Belén',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Buenos Aires',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Cárdenas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Moyogalpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Potosí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Rivas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'San Jorge',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'San Juan del Sur',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '53',
            'nombre_ciudad' => 'Tola',
        ]);


    } 


}
