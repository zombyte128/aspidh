<?php

use Illuminate\Database\Seeder;

class CiudadesHondurasSeeder extends Seeder
{
   
    public function run()
    {
    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Atlántida ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'La Ceiba',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'El Porvenir',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'Tela',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'Jutiapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'La Masica',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'San Francisco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'Arizona',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '67',
            'nombre_ciudad' => 'Esparta',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Colón ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Trujillo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Balfate',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Iriona',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Limón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Sabá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Santa Fe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Santa Rosa de Aguán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Sonaguera',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Tocoa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '68',
            'nombre_ciudad' => 'Bonito Oriental',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Comayagua ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Comayagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Ajuterique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'El Rosario',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Esquías',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Humuya',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'La libertad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Lejamani',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Meambar',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Minas de Oro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Ojos de Agua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'San Jerónimo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'San José de Comayagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'San José del Potrero',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'San Luis',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'San Sebastián',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Siguatepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Villa de San Antonio',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Las Lajas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '69',
            'nombre_ciudad' => 'Taulabé',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Copán ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Santa Rosa de Copán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Cabañas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Concepción',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Copán Ruinas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Corquín',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Cucuyagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Dolores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Dulce Nombre',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'El Paraíso',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Florida',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'La Jigua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'La Unión',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Nueva Arcadia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'San Agustín',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'San Antonio',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'San Jerónimo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'San José',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'San Juan de Opoa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'San Nicolás',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'San Pedro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Santa Rita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Trinidad de Copán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '70',
            'nombre_ciudad' => 'Veracruz',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Cortés ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'San Pedro Sula',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'Choloma',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'Omoa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'Pimienta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'Potrerillos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'Puerto Cortés',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'San Antonio de Cortés',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'San Francisco de Yojoa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'San Manuel',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'Santa Cruz de Yojoa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'Villanueva',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '71',
            'nombre_ciudad' => 'La Lima',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Choluteca ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Choluteca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Apacilagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Concepción de María',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Duyure',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'El Corpus',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'El Triunfo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Marcovia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Morolica',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Namasigue',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Orocuina',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Pespire',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'San Antonio de Flores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'San Isidro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'San José',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'San Marcos de Colón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '72',
            'nombre_ciudad' => 'Santa Ana de Yusguare',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento El Paraíso ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Yuscarán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Alauca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Danlí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'El Paraíso',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Güinope',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Jacaleapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Liure',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Morocelí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Oropolí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Potrerillos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'San Antonio de Flores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'San Lucas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'San Matías',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Soledad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Teupasenti',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Texiguat',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Vado Ancho',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Yauyupe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '73',
            'nombre_ciudad' => 'Trojes',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Francisco Morazán ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Comayagüela',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Tegucigalpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Alubarén',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Cedros',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Curarén',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'El Porvenir',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Guaimaca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'La Libertad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'La Venta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Lepaterique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Maraita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Marale',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Nueva Armenia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Ojojona',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Orica',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Reitoca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Sabanagrande',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'San Antonio de Oriente',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'San Buenaventura',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'San Ignacio',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'San Juan de Flores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'San Miguelito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Santa Ana',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Santa Lucía',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Talanga',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Tatumbla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Valle de Ángeles',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Villa de San Francisco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '74',
            'nombre_ciudad' => 'Vallecillo',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Gracias a Dios ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '75',
            'nombre_ciudad' => 'Puerto Lempira',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '75',
            'nombre_ciudad' => 'Brus Laguna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '75',
            'nombre_ciudad' => 'Ahuas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '75',
            'nombre_ciudad' => 'Juan Francisco Bulnes',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '75',
            'nombre_ciudad' => 'Ramón Villeda Morales',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '75',
            'nombre_ciudad' => 'Wampusirpe',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Intibucá ////////////////////


        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'La Esperanza',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Camasca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Colomoncagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Concepción',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Dolores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Intibucá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Jesús de Otoro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Magdalena',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Masaguara',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'San Antonio',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'San Isidro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'San Juan',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'San Marcos de la Sierra',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'San Miguel Guancapla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Santa Lucía',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'Yamaranguila',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '76',
            'nombre_ciudad' => 'San Francisco de Opalaca',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Islas de la Bahía ////////////////////


        DB::table('ciudades')->insert([
            'departamento_id' => '77',
            'nombre_ciudad' => 'Roatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '77',
            'nombre_ciudad' => 'Guanaja',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '77',
            'nombre_ciudad' => 'José Santos Guardiola',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '77',
            'nombre_ciudad' => 'Utila',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento La Paz ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'La Paz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Aguanqueterique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Cabañas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Cane',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Chinacla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Guajiquiro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Lauterique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Marcala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Mercedes de Oriente',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Opatoro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'San Antonio del Norte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'San José',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'San Juan',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'San Pedro de Tutule',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Santa Ana',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Santa Elena',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Santa María',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Santiago de Puringla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '78',
            'nombre_ciudad' => 'Yarula',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Lempira ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Gracias',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Belén',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Candelaria',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Cololaca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Erandique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Gualcince',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Guarita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'La Campa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'La Iguala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Las Flores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'La Unión',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'La Virtud',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Lepaera',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Mapulaca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Piraera',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'San Andrés',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'San Francisco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'San Juan Guarita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'San Manuel Colohete',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'San Rafael',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'San Sebastián',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Santa Cruz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Talgua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Tambla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Tomalá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Valladolid',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'Virginia',
        ]);
        
        DB::table('ciudades')->insert([
            'departamento_id' => '79',
            'nombre_ciudad' => 'San Marcos de Caiquín',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Ocotepeque ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Ocotepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Belén Gualcho',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Concepción',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Dolores Merendón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Fraternidad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'La Encarnación',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'La Labor',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Lucerna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Mercedes',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'San Fernando',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'San Francisco del Valle',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'San Jorge',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'San Marcos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Santa Fe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Sensenti',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '80',
            'nombre_ciudad' => 'Sinuapa',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Olancho ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Juticalpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Campamento',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Catacamas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Concordia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Dulce Nombre de Culmí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'El Rosario',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Esquipulas del Norte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Gualaco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Guarizama',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Guata',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Guayape',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Jano',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'La Unión',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Mangulile',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Manto',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Salamá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'San Esteban',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'San Francisco de Becerra',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'San Francisco de la Paz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Santa María del Real',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Silca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Yocón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '81',
            'nombre_ciudad' => 'Patuca',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Santa Bárbara ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Santa Bárbara',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Arada',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Atima',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Azacualpa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Ceguaca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Concepción del Norte',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Concepción del Sur',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Chinda',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'El Níspero',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Gualala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Ilama',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Las Vegas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Macuelizo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Naranjito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Nuevo Celilac',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Nueva Frontera',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Petoa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Protección',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Quimistán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'San Francisco de Ojuera',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'San José de Colinas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'San Luis',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'San Marcos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'San Nicolás',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'San Pedro Zacapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'San Vicente Centenario',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Santa Rita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '82',
            'nombre_ciudad' => 'Trinidad',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Valle ////////////////////


        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'Nacaome',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'Alianza',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'Amapala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'Aramecina',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'Caridad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'Goascorán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'Langue',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'San Francisco de Coray',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '83',
            'nombre_ciudad' => 'San Lorenzo',
        ]);

    ////////////////////////////////////////////////////////

///////////////// Ciudades departamento Yoro ////////////////////


        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Yoro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Arenal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'El Negrito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'El Progreso',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Jocón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Morazán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Olanchito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Santa Rita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Sulaco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Victoria',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '84',
            'nombre_ciudad' => 'Yorito',
        ]);//

    }
}
