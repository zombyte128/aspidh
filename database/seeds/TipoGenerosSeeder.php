<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoGenerosSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipo_generos')->insert([
            'nombre_tipo_genero' => 'Cisgénero'
        ]);

        DB::table('tipo_generos')->insert([
            'nombre_tipo_genero' => 'Transgénero'
        ]);

        DB::table('tipo_generos')->insert([
            'nombre_tipo_genero' => 'Transexual'
        ]);

        DB::table('tipo_generos')->insert([
            'nombre_tipo_genero' => 'No Binario'
        ]);
    }
}
