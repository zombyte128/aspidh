<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiudadesTableSeeder extends Seeder
{
    public function run()
    {
 /////////////// Municipios de san salvador ////////////////////////       
        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'San Salvador',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Aguilares',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Apopa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Ayutuxtepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Ciudad Delgado',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Cuscatancingo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'El Paisnal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Guazapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Ilopango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Mejicanos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Nejapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Panchimalco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Rosario de Mora',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'San Marcos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'San Martín',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Santiago Texacuangos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Santo Tomás',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Soyapango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '1',
            'nombre_ciudad' => 'Tonacatepeque',
        ]);

///////////////////////////////////////////////////////

///////////////// Municipios de la libertad ////////////////////

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Santa Tecla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Antiguo Cuscatlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Chiltiupán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Ciudad Arce',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Colón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Comasagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Huizúcar',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Jicalapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Jayaque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Nuevo Cuscatlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Puerto de la Libertad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Quezaltepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Sacacoyo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'San José Villanueva',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'San Matías',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'San Pablo Tacachico',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Talnique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Tamanique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Teotepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Tepecoyo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '2',
            'nombre_ciudad' => 'Zaragoza',
        ]);

//////////////////////////////////////////////////////

////////////////// Municipios de Santa Ana ///////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Santa Ana',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Candelaria de la Frontera',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Chalchuapa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Coatepeque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'El Congo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'El Porvenir',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Masahuat',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Metapán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'San Antonio Pajonal',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'San Sebastián Salitrillo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Santa Rosa Guachipilín',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Santiago de la Frontera',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '3',
    'nombre_ciudad' => 'Texistepeque',
]);

///////////////////////////////////////////////////

////// municipios de san miguel ///////////
DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'San Miguel',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Chapeltique',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Carolina',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Chinameca',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Chirilagua',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Ciudad Barrios',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Comacarán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'El Tránsito',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Lolotique',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Moncagua',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Nueva Guadalupe',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Nuevo Edén de San Juan',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Quelepa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'San Antonio del Mosco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'San Gerardo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'San Jorge',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'San Luis de La Reina',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'San Rafael Oriente',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Sesori',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '4',
    'nombre_ciudad' => 'Uluazapa',
]);

////////////////////////////////////////////////////////////

/////////// Municipios de la paz /////////////////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Zacatecoluca',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Cuyultitán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'El Rosario de paz',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Jerusalén',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Mercedes La Ceiba',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Olocuilta',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Paraíso de Osorio',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Antonio Masahuat',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Emigdio',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Francisco Chinameca',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Juan Nonualco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Juan Talpa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Juan Tepezontes',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Luis La Herradura',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Luis Talpa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Miguel Tepezontes',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Pedro Masahuat',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Pedro Nonualco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'San Rafael Obrajuelo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Santa María Ostuma',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Santiago Nonualco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '5',
    'nombre_ciudad' => 'Tapalhauca',
]);

//////////////////////////////////////////////////////////////////

//////// Municipios de la union ///////////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'La Unión',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Anamorós',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Bolívar',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Concepción de Oriente',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Conchagua',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'El Carmen',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'El Sauce',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Intipucá',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Lislique',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Meanguera del Golfo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Nueva Esparta',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Pasaquina',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Polorós',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'San Alejo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'San José La Fuente',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Santa Rosa de Lima',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Yayantique',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '6',
    'nombre_ciudad' => 'Yucuaiquín',
]);

///////////////////////////////////////////////////////////////

///////////////////// Municios de sonsonate /////////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Sonsonate',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Acajutla',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Armenia',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Caluco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Cuisnahuat',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Izalco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Juayúa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Nahulingo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Nahuizalco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Salcoatitán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'San Antonio del Monte',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'San Julián',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Santa Catarina Masahuat',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Municipio de Santa Isabel Ishuatán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Santo Domingo de Guzmán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Metalío',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '7',
    'nombre_ciudad' => 'Sonzacate',
]);

//////////////////////////////////////////////////////////////////////////

////////////////////// Munucipios de Ahuachapan ////////////////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Ahuachapán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Apaneca',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Atiquizaya',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Concepción de Ataco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'El Refugio',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Guaymango',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Jujutla',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'San Francisco Menéndez',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'San Lorenzo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'San Pedro Puxtla',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Tacuba',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '8',
    'nombre_ciudad' => 'Turín',
]);

////////////////////////////////////////////////////

///////// Municipios de chalatenango ////////

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Chalatenango',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Agua Caliente',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Arcatao',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Azacualpa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Citalá',
]);


DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Comalapa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Concepción Quezaltepeque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Dulce Nombre de María',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'El Carrizal',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'El Paraíso',
]);


DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'La Laguna',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'La Palma',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'La Reina',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Las Vueltas',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Nombre de Jesús',
]);


DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Nueva Concepción',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Nueva Trinidad',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Ojos de Agua',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Potonico',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Antonio de La Cruz',
]);


DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Antonio Los Ranchos',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Fernando',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Francisco Lempa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Francisco Morazán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Ignacio',
]);


DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Isidro Labrador',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San José Cancasque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San José Las Flores',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Luis del Carmen',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Miguel de Mercedes',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'San Rafael',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Santa Rita',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '9',
    'nombre_ciudad' => 'Tejutla',
]);

////////////////////////////////////////////////////////////

////////////////// Municipios de cabañas //////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Sensuntepeque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Cinquera',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Dolores',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Guacotecti',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Ilobasco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Jutiapa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'San Isidro',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Tejutepeque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '10',
    'nombre_ciudad' => 'Victoria',
]);

////////////////////////////////////////////

///////// Municipios de cuscatlan ////////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Cojutepeque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Candelaria',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'El Carmen',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'El Rosario',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Monte San Juan',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Oratorio de Concepción',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'San Bartolomé Perulapía',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'San Cristóbal',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'San José Guayabal',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'San Pedro Perulapán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'San Rafael Cedros',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'San Ramón',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Santa Cruz Analquito',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Santa Cruz Michapa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Suchitoto',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '11',
    'nombre_ciudad' => 'Tenancingo',
]);

/////////////////////////////////////////////////////////////

//////////// Municipios de San vicente /////////////////////

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'San Vicente',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'Apastepeque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'Guadalupe',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'San Cayetano Istepeque',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'San Esteban Catarina',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'San Ildefonso',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'San Lorenzo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'San Sebastián',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'Santa Clara',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'Santo Domingo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'Tecoluca',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'Tepetitán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '12',
    'nombre_ciudad' => 'Verapaz',
]);

//////////////////////////////////////////////////

//////////////////// Municipio de morazan ///////////

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'San Francisco Gotera',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Arambala',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Cacaopera',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Chilanga',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Corinto',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Delicias de Concepción',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'El Divisadero',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'El Rosario',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Gualococti',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Guatajiagua',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Joateca',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Jocoaitique',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Jocoro',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Lolotiquillo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Meanguera',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Osicala',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Perquín',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'San Carlos',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'San Fernando',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'San Isidro',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'San Simón',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Sensembra',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Sociedad',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Torola',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Yamabal',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '13',
    'nombre_ciudad' => 'Yoloaiquín',
]);

///////////////////////////////////////////////

//////////////////// Municipios de usulutan ///////////////////


DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Usulután',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Alegría',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Berlín',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'California',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Concepción Batres',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'El Triunfo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Ereguayquín',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Estanzuelas',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Jiquilisco',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Jucuapa',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Jucuarán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Mercedes Umaña',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Nueva Granada',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Ozatlán',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Puerto El Triunfo',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'San Agustín',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'San Buenaventura',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'San Dionisio',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'San Francisco Javier',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Santa Elena',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Santa María',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Santiago de María',
]);

DB::table('ciudades')->insert([
    'departamento_id' => '14',
    'nombre_ciudad' => 'Tecapán',
]);

    }
}
