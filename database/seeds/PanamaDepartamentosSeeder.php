<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PanamaDepartamentosSeeder extends Seeder
{
    
    public function run()
    {

        DB::table('departamentos')->insert([
            'pais_id' => '5',
            'nombre_departamento' => 'Coclé',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '5',
            'nombre_departamento' => 'Colón',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '5',
            'nombre_departamento' => 'Chiriquí',
        ]);
        
        DB::table('departamentos')->insert([
            'pais_id' => '5',
            'nombre_departamento' => 'Los Santos',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '5',
            'nombre_departamento' => 'Panamá',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '5',
            'nombre_departamento' => 'Veraguas',
        ]);
    }
}