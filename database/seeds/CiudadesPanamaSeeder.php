<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiudadesPanamaSeeder extends Seeder
{
    
    public function run()
    {
        // Coclé

        DB::table('ciudades')->insert([
            'departamento_id' => '61',
            'nombre_ciudad' => 'Aguadulce',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '61',
            'nombre_ciudad' => 'Antón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '61',
            'nombre_ciudad' => 'La Pintada',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '61',
            'nombre_ciudad' => 'Natá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '61',
            'nombre_ciudad' => 'Olá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '61',
            'nombre_ciudad' => 'Penonomé',
        ]);
        
        // Colón

        DB::table('ciudades')->insert([
            'departamento_id' => '62',
            'nombre_ciudad' => 'Colón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '62',
            'nombre_ciudad' => 'Chagres',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '62',
            'nombre_ciudad' => 'Donoso',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '62',
            'nombre_ciudad' => 'Portobelo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '62',
            'nombre_ciudad' => 'Santa Isabel',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '62',
            'nombre_ciudad' => 'Omar Torrijos Herrera',
        ]);

        // Chiriquí

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Alanje',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Barú',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Boquerón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Boquete',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Bugaba',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'David',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Dolega',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Gualaca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Remedios',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Renacimiento',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'San Félix',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'San Lorenzo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Tierras Altas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '63',
            'nombre_ciudad' => 'Tolé',
        ]);

        // LOS SANTOS
        
        DB::table('ciudades')->insert([
            'departamento_id' => '64',
            'nombre_ciudad' => 'Guararé',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '64',
            'nombre_ciudad' => 'Las Tablas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '64',
            'nombre_ciudad' => 'Los Santos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '64',
            'nombre_ciudad' => 'Macaracas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '64',
            'nombre_ciudad' => 'Pedasí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '64',
            'nombre_ciudad' => 'Pocrí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '64',
            'nombre_ciudad' => 'Tonosí',
        ]);

        // PANAMÁ

        DB::table('ciudades')->insert([
            'departamento_id' => '65',
            'nombre_ciudad' => 'Balboa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '65',
            'nombre_ciudad' => 'Chepo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '65',
            'nombre_ciudad' => 'Chimán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '65',
            'nombre_ciudad' => 'Panamá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '65',
            'nombre_ciudad' => 'San Miguelito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '65',
            'nombre_ciudad' => 'Taboga',
        ]);

        // VERAGUAS

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Atalaya',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Calobre',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Cañazas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'La Mesa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Las Palmas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Mariato',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Montijo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Río de Jesús',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'San Francisco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Santa Fe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Santiago',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '66',
            'nombre_ciudad' => 'Soná',
        ]);
    }
}
