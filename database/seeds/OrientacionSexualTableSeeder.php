<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrientacionSexualTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('orientacion_sexual')->insert([
            'nombre_orientacion_sexual' => 'Heterosexual'
        ]);

        DB::table('orientacion_sexual')->insert([
            'nombre_orientacion_sexual' => 'Lesbiana'
        ]);

        DB::table('orientacion_sexual')->insert([
            'nombre_orientacion_sexual' => 'Gay'
        ]);

        DB::table('orientacion_sexual')->insert([
            'nombre_orientacion_sexual' => 'Bisexual'
        ]);
    }
}
