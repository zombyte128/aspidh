<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(PaisesTableSeeder::class);
        /** el salvador */
        $this->call(DepartamentosTableSeeder::class);
        $this->call(CiudadesTableSeeder::class);
        /** aqui termina las del el salvador */
        // $this->call(GuatemalaSeeder::class);
        // $this->call(CiudadesGuatemalaSeeder::class);
        // $this->call(DepartamentosNicaraguaSeeder::class);
        // $this->call(CiudadesNicaraguaSeeder::class);
        // $this->call(DepartamentosCostaRicaSeeder::class);
        // $this->call(CiudadesCostaRicaSeeder::class);
        // $this->call(PanamaDepartamentosSeeder::class);
        // $this->call(CiudadesPanama::class);
        // $this->call(DepartamentosHonduras::class);
        $this->call(RoleTableSeeder::class);
        $this->call(TipoGenerosSeeder::class);
        $this->call(OrientacionSexualTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
