<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaisesTableSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('paises')->insert([
            'nombre_pais' => 'El Salvador'
        ]);

        DB::table('paises')->insert([
            'nombre_pais' => 'Guatemala'
        ]);

        DB::table('paises')->insert([
            'nombre_pais' => 'Nicaragua'
        ]);

        DB::table('paises')->insert([
            'nombre_pais' => 'Costa Rica'
        ]);


        DB::table('paises')->insert([
            'nombre_pais' => 'Panamá'
        ]);

        DB::table('paises')->insert([
            'nombre_pais' => 'Honduras'
        ]);
    }
}
