<?php

use Illuminate\Database\Seeder;

class DepartamentosNicaraguaSeeder extends Seeder
{
   
    
    public function run()
    {
        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Boaco',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Carazo',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Chinandega',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Chontales',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Costa Caribe Norte',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Costa Caribe Sur',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Estelí',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Granada',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Jinotega',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'León',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Madriz',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Managua',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Masaya',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Matagalpa',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Nueva Segovia',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Río San Juan',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '3',
            'nombre_departamento' => 'Rivas',
        ]);
        //dd
    }
}
