<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiudadesCostaRicaSeeder extends Seeder
{

    public function run()
    {
        // ALAJUELA

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Alajuela',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Atenas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Grecia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Guatuso',
        ]);
        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Los Chiles',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Naranjo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Orotina',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Palmares',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Poás',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Río Cuarto',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'San Carlos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'San Mateo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'San Ramón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Sarchí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Upala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '54',
            'nombre_ciudad' => 'Zarcero',
        ]);

        // Cartago

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'Alvarado',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'Cartago',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'El Guarco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'Jiménez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'La Unión',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'Oreamuno',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'Paraíso',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '55',
            'nombre_ciudad' => 'Turrialba',
        ]);

        // GUANACASTE
        
        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Abangares',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Bagaces',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Cañas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Carrillo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Hojancha',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'La Cruz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Liberia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Nandayure',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Nicoya',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Santa Cruz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '56',
            'nombre_ciudad' => 'Tilarán',
        ]);

        // HEREDIA

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'Barva',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'Belén',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'Heredia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'Flores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'San Isidro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'San Pablo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'San Rafael',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'Santa Bárbara',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'Santo Domingo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '57',
            'nombre_ciudad' => 'Sarapiquí',
        ]);

        // LIMÓN

        DB::table('ciudades')->insert([
            'departamento_id' => '58',
            'nombre_ciudad' => 'Guácimo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '58',
            'nombre_ciudad' => 'Matina',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '58',
            'nombre_ciudad' => 'Limón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '58',
            'nombre_ciudad' => 'Pococí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '58',
            'nombre_ciudad' => 'Siquirres',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '58',
            'nombre_ciudad' => 'Talamanca',
        ]);

        // PUNTARENAS

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Buenos Aires',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Corredores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Coto Brus',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Esparza',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Garabito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Golfito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Montes de Oro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Osa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Puntarenas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Parrita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '59',
            'nombre_ciudad' => 'Quepos',
        ]);

        // SAN JOSÉ

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Acosta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Alajuelita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Aserrí',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Curridabat',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Desamparados',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Dota',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Escazú',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Goicoechea',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'León Cortés',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Montes de Oca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Mora',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Moravia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Pérez Zeledón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Puriscal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Santa Ana',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'San José',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Tarrazú',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Tibás',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Turrubares',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '60',
            'nombre_ciudad' => 'Vázquez de Coronado',
        ]);

    }
}
