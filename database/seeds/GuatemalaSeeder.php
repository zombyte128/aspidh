<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GuatemalaSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Alta Verapaz',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Baja Verapaz',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Chimaltenango',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Chiquimula',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'El Progreso',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Escuintla',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Guatemala',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Huehuetenango',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Izabal',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Jalapa',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Jutiapa',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Petén',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Quetzaltenango',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Quiché',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Retalhuleu',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Sacatepéquez',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'San Marcos',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Santa Rosa',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Sololá',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Suchitepéquez',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Totonicapán',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '2',
            'nombre_departamento' => 'Zacapa',
        ]);
    }
}
