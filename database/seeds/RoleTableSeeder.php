<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
            'nombre_rol' => 'Super Administrador',
        ]);

        DB::table('roles')->insert([
            'nombre_rol' => 'Administrador',
        ]);

        DB::table('roles')->insert([
            'nombre_rol' => 'Usuario',
        ]);

        DB::table('roles')->insert([
            'nombre_rol' => 'Donante',
        ]);
    }
}
