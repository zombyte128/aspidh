<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CiudadesGuatemalaSeeder extends Seeder
{
    public function run()
    {
        // ALTA VERAPAZ

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Chisec',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Coban',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Tactic',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'San Pedro Carchá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Fray Bartolomé de las Casas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'San Agustín Lanquín',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Santa María Cahabón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Chahal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Senahú',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Tamahú',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Tucurú',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Santa Cruz Verapaz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Panzós',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Raxruhá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'San Juan Chamelco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'Santa Catalina la Tinta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '15',
            'nombre_ciudad' => 'San Cristobal Verapaz',
        ]);

        // BAJA VERAPAZ 

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'Santa Cruz El Chol',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'Cubulco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'Granados',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'Purulha',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'Rabinal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'Salama',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'San Jerónimo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '16',
            'nombre_ciudad' => 'San Miguel Chicaj',
        ]);

        // CHIMALTENANGO

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Acatenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Chimaltenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'El Tejar',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Parramos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Patzicía',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Patzún',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Pochuta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'San Andrés Itzapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'San José Poaquil',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'San Juan Comalapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'San Martín Jilotepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Santa Apolonia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Santa Cruz Balanyá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Tecpán Guatemala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Yepocapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '17',
            'nombre_ciudad' => 'Zaragoza',
        ]);

        // CHIQUIMULA

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'Camotán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'Chiquimula',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'Esquipulas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'Ipala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'Jocotán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'Olopa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'Quetzaltepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'San José La Arada',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '18',
            'nombre_ciudad' => 'San Juan Ermita',
        ]);

        // EL PROGRESO

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'El Jicaro',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'Guastatoya',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'Morazán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'San Agustín Acasaguastlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'San Antonio La Paz',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'San Cristobal Acasguastlan',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'Sanarate',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '19',
            'nombre_ciudad' => 'Sansare',
        ]);

        // ESCUINTLA

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Escuintla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Guanagazapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Iztapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'La Democracia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'La Gomera',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Masagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Nueva Concepción',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Palín',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'San José',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'San Vicente Pacaya',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Santa Lucía Cotzumalguapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Siquinalá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '20',
            'nombre_ciudad' => 'Tiquisate',
        ]);

        // GUATEMALA

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'Amatitlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'Chinautla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'Chuarrancho',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'Fraijanes',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'Palencia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'San José del Golfo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'San José Pinula',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'San Juan Sacatepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'San Miguel Petapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'San Pedro Ayampuc',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'San Pedro Sacatepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'San Raymundo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '21',
            'nombre_ciudad' => 'Santa Catarina Pinula',
        ]);

        //HUEHUETENANGO

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Aguacatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Chiantla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Colotenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Concepción Huista',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Cuilco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Huehuetenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Jacaltenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'La Democracia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'La Libertad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Malacatancito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Nentón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Antonio Huista',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Gaspar Ixchil',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Ildefonso Ixtahuacan',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Juan Atitán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Juan Ixcoy',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Mateo Ixtatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Miguel Acatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Pedro Necta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Rafaél La Independencia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Rafaél Petzal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Sebastián Coatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Sebastián Huehuetenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Santa Ana Huista',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Santa Bárbara',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Santa Cruz Barillas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Santa Eulalia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Santiago Chimaltenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'San Pedro Soloma',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Tectitán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Todos Santos Cuchumatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '22',
            'nombre_ciudad' => 'Unión Cantinil',
        ]);

        // IZABAL

        DB::table('ciudades')->insert([
            'departamento_id' => '23',
            'nombre_ciudad' => 'Puerto Barrios',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '23',
            'nombre_ciudad' => 'Los Amates',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '23',
            'nombre_ciudad' => 'Livingston',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '23',
            'nombre_ciudad' => 'Morales',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '23',
            'nombre_ciudad' => 'El Estor',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '23',
            'nombre_ciudad' => 'Historia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '23',
            'nombre_ciudad' => 'Turismo',
        ]);

        // JALAPA

        DB::table('ciudades')->insert([
            'departamento_id' => '24',
            'nombre_ciudad' => 'Jalapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '24',
            'nombre_ciudad' => 'Mataquescuintla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '24',
            'nombre_ciudad' => 'Monjas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '24',
            'nombre_ciudad' => 'San Carlos Alzatate',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '24',
            'nombre_ciudad' => 'San Luis Jilotepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '24',
            'nombre_ciudad' => 'San Manuel Chaparrón',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '24',
            'nombre_ciudad' => 'San Pedro Pinula',
        ]);

        // JUTIAPA

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Agua Blanca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Asunción Mita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Atescatempa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Comapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Conguaco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'El Adelanto',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'El Progreso',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Jalpatagua',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Jutiapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Moyuta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Pasaco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Quezada',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'San José Acatempa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Santa Catarina Mita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Yupiltepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '25',
            'nombre_ciudad' => 'Zapotitlán',
        ]);

        // PETÉN

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'Dolores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'Flores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'La Libertad',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'Melchor de Mencos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'Poptún',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'San Andrés',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'San Benito',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'San Francisco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'San José',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'San Luis',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'Santa Ana',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '26',
            'nombre_ciudad' => 'Sayaxché',
        ]);

        // Quetzaltenango

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Almolonga',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Cabricán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Cajolá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Cantel',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Coatepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Colomba',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Concepción Chiquirichapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'El Palmar',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Flores Costa Cuca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Génova',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Huitán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'La Esperanza',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Olintepeque',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Palestina de Los Altos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Quetzaltenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Salcajá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'San Carlos Sija',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'San Francisco La Unión',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'San Juan Ostuncalco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'San Martín Sacatepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'San Mateo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'San Miguel Siguilá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Sibilia',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '27',
            'nombre_ciudad' => 'Zunil',
        ]);

        // QUICHÉ

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Canillá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Chajúl',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Chicamán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Chiché',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Chinique',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Cunen',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Joyabaj',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Pachalum',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Patzité',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Playa Grande Ixcán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Sacapulas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'San Andrés Sajcabajá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'San Antonio Ilotenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'San Bartolomé Jocotenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'San Juan Cotzal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Uspatan',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'San Pedro Jocopilas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Santa Cruz del Quiché',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Chichicastenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '28',
            'nombre_ciudad' => 'Zacualpa',
        ]);

        // RETALHULEU

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'Champerico',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'El Asintal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'Nuevo San Carlos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'Retalhuleu',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'San Andrés Villa Seca',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'San Felipe',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'San Martín Zapotitlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'San Sebastián',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '29',
            'nombre_ciudad' => 'Santa Cruz Muluá',
        ]);

        // ZACATEPEQUEZ

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Alotenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Antigua Guatemala',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Ciudad Vieja',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Jocotenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Magdalena Milpas Altas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Pastores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'San Antonio Aguas Calientes',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'San Bartolomé Milpas Altas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'San Lucas Sacatepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'San Miguel Dueñas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Santa Catarina Barahona',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Santa Lucía Milpas Altas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Santa María de Jesús',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Santiago Sacatepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Santo Domingo Xenacoj',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '30',
            'nombre_ciudad' => 'Sumpango',
        ]);

        //SAN MARCOS

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Ayutla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Catarina',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Comitancillo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Concepción Tutuapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'El Quetzal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'El Rodeo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'El Tumbador',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Esquipulas Palo Gordo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Ixchiguán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'La Reforma',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Malacatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Nuevo Progreso',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Ocós',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Pajapita',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Río Blanco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Cristobal Cucho',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Antonio Sacatepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San José Ojetenam',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Lorenzo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Marcos',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Miguel Ixtahuacán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Pablo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Pedro Sacatepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'San Rafael Pie de la Cuesta',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Sibinal',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Sipacapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Tacana',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Tajumulco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '31',
            'nombre_ciudad' => 'Tejutla',
        ]);

        // SANTA ROSA

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Barberena',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Casillas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Chiquimulilla',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Cuilapa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Guazacapán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Nueva Santa Rosa',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Oratorio',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Pueblo Nuevo Viñas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'San Juan Tecuaco',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'San Rafael Las Flores',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Santa Cruz Naranjo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Santa María Ixhuatán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Santa Rosa de Lima',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '32',
            'nombre_ciudad' => 'Taxisco',
        ]);

        // SOLOLÁ

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Concepción',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Nahualá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Panajachel',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'San Andrés Semetabaj',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'San José Chacayá',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'San Juan La Laguna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'San Lucas Tolimán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'San Marcos La Laguna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'San Pablo La Laguna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'San Pedro La Laguna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Santa Catarina Ixtahuacán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Santa Clara La Laguna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Santa Cruz La Laguna',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Santa Lucia Utatlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Santa María Visitación',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Santiago Atitlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '33',
            'nombre_ciudad' => 'Sololá',
        ]);

        // SUCHITEPÉQUEZ
        
        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Chicacao',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Cuyotenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Mazatenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Patulul',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Pueblo Nuevo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Rio Bravo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Samayac',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Antonio Suchitepéquez',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Bernardino',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Francisco Zapotitlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Gabriel',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San José El Idolo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Juan Bautista',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Lorenzo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Miguel',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'San Pablo Jocopilas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Santa Barbara',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Santo Tomas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '34',
            'nombre_ciudad' => 'Zunilito',
        ]);

        // TETONICAPÁN

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'Momostenango',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'San Andrés Xecul',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'San Bartolo Aguas Calientes',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'San Francisco El Alto',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'Santa Lucia La Reforma',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'Santa Maria Chiquimula',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'Totonicapán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '35',
            'nombre_ciudad' => 'San Cristóbal Totonicapán',
        ]);

        // ZACAPA

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Cabañas',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Estanzuela',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Gualán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Huité',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'La Unión',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Río Hondo',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'San Diego',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Teculután',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Usumatlán',
        ]);

        DB::table('ciudades')->insert([
            'departamento_id' => '36',
            'nombre_ciudad' => 'Zacapa',
        ]);

    }
}
