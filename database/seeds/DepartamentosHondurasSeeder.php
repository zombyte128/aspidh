<?php

use Illuminate\Database\Seeder;

class DepartamentosHondurasSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Atlántida',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Colón',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Comayagua',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Copán',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Cortés',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Choluteca',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'El Paraíso',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Francisco Morazán',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Gracias a Dios',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Intibucá',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Islas de la Bahía',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'La Paz',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Lempira',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Ocotepeque',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Olancho',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Santa Bárbara',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Valle',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '6',
            'nombre_departamento' => 'Yoro',
        ]);
    }
}
//