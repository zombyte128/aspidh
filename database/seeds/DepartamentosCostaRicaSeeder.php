<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentosCostaRicaSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('departamentos')->insert([
            'pais_id' => '4',
            'nombre_departamento' => 'Alajuela',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '4',
            'nombre_departamento' => 'Cartago',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '4',
            'nombre_departamento' => 'Guanacaste',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '4',
            'nombre_departamento' => 'Heredia',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '4',
            'nombre_departamento' => 'Limón',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '4',
            'nombre_departamento' => 'Puntarenas',
        ]);

        DB::table('departamentos')->insert([
            'pais_id' => '4',
            'nombre_departamento' => 'San José',
        ]);
    }
}
