/* ----------------------------------------------------------- */
/* Pasos denuncia
/* ----------------------------------------------------------- */
$("#vistadenuncias").on("click", function () {
    //Mostrando formulario denuncias y ocultando pasos
    $("#denuncia").css("display", "block");
    $("#paso").css("display", "none");
    window.scrollTo(0, 860);
});

/* ----------------------------------------------------------- */
/* Formulario denuncias
/* ----------------------------------------------------------- */
$("#atras").on("click", function () {
    //Mostrando pasos y ocultando formulario denuncias
    $("#paso").css("display", "block");
    $("#denuncia").css("display", "none");
    window.scrollTo(0, 860);
});

/* ----------------------------------------------------------- */
/* Sector realizo una denuncia formal
/* ----------------------------------------------------------- */

$("#denuniciasi").click(function () {
    if (document.getElementById("denuniciasi").checked) {
        $("#dondelahizo").css("display", "block");
        $("#fechadenuncia").prop("disabled", false);
    }
});

$("#denuniciano").click(function () {
    if (document.getElementById("denuniciano").checked) {
        $("#dondelahizo").css("display", "none");
        $("#fechadenuncia").prop("disabled", true);
        $("#fechadenuncia").val("");
        $("#dondelahizo").val("");
    }
});

/* ----------------------------------------------------------- */
/* dropify para mostrar imagenes
/* ----------------------------------------------------------- */
$('.dropify14').dropify({
    messages: {
        'default': 'Subir Documento',
        'replace': '',
        'remove':  'Remover',
        'error':   'Ooops, Error al cargar.'
    },
    tpl: {
        wrap:            '<div class="dropify-wrapper largo"></div>',
        clearButton:     '<button type="button" class="dropify-clear">{{ remove }}</button>',
        message:         '<div class="dropify-message"><span class="icon-cloud-upload" /> <p class="sinborder">Imagen</p></div>',
    }
});
$('.dropify15').dropify({
    messages: {
        'default': 'Subir Documento',
        'replace': '',
        'remove':  'Remover',
        'error':   'Ooops, Error al cargar.'
    },
    tpl: {
        wrap:            '<div class="dropify-wrapper largo"></div>',
        clearButton:     '<button type="button" class="dropify-clear">{{ remove }}</button>',
        message:         '<div class="dropify-message"><span class="icon-cloud-upload" /> <p class="sinborder">Pdf</p></div>',
    }
});