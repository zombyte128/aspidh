Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Denuncia Realizada',
    toast: true,
    footer:'Nos comunicaremos contigo',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  })