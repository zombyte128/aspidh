/*==========================================================
  Script para ver contrasena
==========================================================*/

function mostrarPassword() {
    var cambio = document.getElementById("txtPassword");
    if (cambio.type == "password") {
        cambio.type = "text";
    } else {
        cambio.type = "password";
    }
}

/*==========================================================
  Dui, Nit, Passaporte, Ninguno
==========================================================*/
$(document).ready(function () {
    $("#dui").on("click", function () {
        if (this.checked) {
            $("#1").css("display", "block");
            $("#2").css("display", "none");
            $("#3").css("display", "none");
            $("#4").css("display", "none");
            $("#limpiar2").val("");
            $("#limpiar3").val("");
            $("#limpiar4").val("");
            document.getElementById("limpiar1").name = "documento_identificacion";
            document.getElementById("limpiar2").name = "";
            document.getElementById("limpiar3").name = "";
            document.getElementById("limpiar4").name = "";
        }
    });
});
$(document).ready(function () {
    $("#nit").on("click", function () {
        if (this.checked) {
            $("#2").css("display", "block");
            $("#1").css("display", "none");
            $("#3").css("display", "none");
            $("#4").css("display", "none");
            $("#limpiar1").val("");
            $("#limpiar3").val("");
            $("#limpiar4").val("");
            document.getElementById("limpiar2").name = "documento_identificacion";
            document.getElementById("limpiar1").name = "";
            document.getElementById("limpiar3").name = "";
            document.getElementById("limpiar4").name = "";
       }
    });
});
$(document).ready(function () {
    $("#pasaporte").on("click", function () {
        if (this.checked) {
            $("#3").css("display", "block");
            $("#2").css("display", "none");
            $("#1").css("display", "none");
            $("#4").css("display", "none");
            $("#limpiar2").val("");
            $("#limpiar1").val("");
            $("#limpiar4").val("");
            document.getElementById("limpiar3").name = "documento_identificacion";
            document.getElementById("limpiar1").name = "";
            document.getElementById("limpiar2").name = "";
            document.getElementById("limpiar4").name = "";
        }
    });
});
$(document).ready(function () {
    $("#ninguno").on("click", function () {
        if (this.checked) {
            $("#4").css("display", "block");
            $("#1").css("display", "none");
            $("#2").css("display", "none");
            $("#3").css("display", "none");
            $("#limpiar1").val("");
            $("#limpiar2").val("");
            $("#limpiar3").val("");
            document.getElementById("limpiar4").name = "documento_identificacion";
            document.getElementById("limpiar1").name = "";
            document.getElementById("limpiar3").name = "";
            document.getElementById("limpiar2").name = "";
        }
    });
});

/*==========================================================
  Educacion mostrar textarea
==========================================================*/
$(document).ready(function () {
    $("#estudiasi").on("click", function () {
        if (this.checked) {
            $("#razonestudios").css("display", "none");
            $("#limpieza5").val("");
        }
    });
});
$(document).ready(function () {
    $("#estudiano").on("click", function () {
        if (this.checked) {
            $("#razonestudios").css("display", "block");
        }
    });
});

/*==========================================================
  Salud mostrar textarea
==========================================================*/
$(document).ready(function () {
    $("#beneficiadosi").on("click", function () {
        if (this.checked) {
            $("#raczonbenedicio").css("display", "block");
        }
    });
});
$(document).ready(function () {
    $("#beneficiadono").on("click", function () {
        if (this.checked) {
            $("#raczonbenedicio").css("display", "none");
            $("#limpieza6").val("");
        }
    });
});

/*==========================================================
  Trabajo mostrar textarea
==========================================================*/
$(document).ready(function () {
    $("#beneficiadotsi").on("click", function () {
        if (this.checked) {
            $("#raczonbenediciot").css("display", "block");
        }
    });
});
$(document).ready(function () {
    $("#beneficiadotno").on("click", function () {
        if (this.checked) {
            $("#raczonbenediciot").css("display", "none");
            $("#limpieza7").val("");
        }
    });
});

/*==========================================================
  vivienda #1 mostrar textarea
==========================================================*/
$(document).ready(function () {
    $("#creditosi").on("click", function () {
        if (this.checked) {
            $("#creditotorgado").css("display", "none");
            $("#limpieza8").val("");
        }
    });
});
$(document).ready(function () {
    $("#creditono").on("click", function () {
        if (this.checked) {
            $("#creditotorgado").css("display", "block");
        }
    });
});

/*==========================================================
  vivienda #2 mostrar textarea
==========================================================*/
$(document).ready(function () {
    $("#programasi").on("click", function () {
        if (this.checked) {
            $("#programagubernamental").css("display", "block");
        }
    });
});
$(document).ready(function () {
    $("#programano").on("click", function () {
        if (this.checked) {
            $("#programagubernamental").css("display", "none");
            $("#limpieza9").val("");
        }
    });
});

/*==========================================================
  Politica y Religion mostrar textarea
==========================================================*/
$(document).ready(function () {
    $("#ejercervotosi").on("click", function () {
        if (this.checked) {
            $("#razonejercervoto").css("display", "block");
        }
    });
});
$(document).ready(function () {
    $("#ejercervotono").on("click", function () {
        if (this.checked) {
            $("#razonejercervoto").css("display", "none");
            $("#limpieza10").val("");
        }
    });
});

/*==========================================================
  Orentacion sexual mostrar modal
==========================================================*/
$(document).ready(function () {
    $("#orientacion").change(function () {
        var id = $(this).val();

        if (id == "1") {
            Swal.fire({
                title: "Heterosexual",
                timer: 8000,
                confirmButtonText: 'Elegir',
                text:
                    "Es la atracción romántica, atracción sexual o comportamiento sexual entre personas de distinto sexo.",
            });
        } else if (id == "2") {
            Swal.fire({
                title: "Mujer Lesbiana",
                confirmButtonText: 'Elegir',
                timer: 8000,
                text:
                    "Lesbianismo es el término para hacer referencia a la homosexualidad femenina, es decir, las mujeres que experimentan amor romántico o atracción sexual por otras mujeres.",
            });
        }else if (id == "3") {
            Swal.fire({
                title: "Hombre Gay",
                confirmButtonText: 'Elegir',
                timer: 8000,
                text:
                    "Es una manera de designar a las personas homosexuales​ masculinas, es decir, a aquellos hombres a los que les atraen sexual y emocionalmente otros hombres.",
            });
        }else if (id == "4") {
            Swal.fire({
                title: "Bisexual",
                confirmButtonText: 'Elegir',
                timer: 8000,
                text:
                    "Se define como la atracción romántica, la atracción sexual o la conducta sexual dirigida tanto hacia hombres como hacia mujeres.",
            });
        }
    });
});

/*==========================================================
  Identidad de género mostrar modal
==========================================================*/
$(document).ready(function () {
    $("#tipogenero").change(function () {
        var id = $(this).val();

        if (id == "1") {
            Swal.fire({
                title: "Cisgénero",
                timer: 8000,
                confirmButtonText: 'Elegir',
                text:
                    "Son a las personas cuya identidad de género concuerdan con el género asignado al nacer.",
            });
        } else if (id == "2") {
            Swal.fire({
                title: "Transgénero",
                timer: 8000,
                confirmButtonText: 'Elegir',
                text:
                    "Hace referencia a las personas cuyas identidades de género son diferentes del sexo​ que se les asignó al nacer.",
            });
        } else if (id == "3") {
            Swal.fire({
                title: "Transexual",
                confirmButtonText: 'Elegir',
                timer: 8000,
                text:
                    "Es un término que se refiere a una persona que considera someterse a tratamiento hormonal y quirúrgico como necesario para adquirir la apariencia física de las personas del sexo con el que se identifican.",
            });
        } else if (id == "4") {
            Swal.fire({
                title: "No Binario",
                confirmButtonText: 'Elegir',
                timer: 8000,
                text:
                    "La expresión género no binario se aplica a las personas que no se autoperciben varón ni mujer y que pueden identificarse con un tercer género o ninguno.",
            });
        }
    });
});

/*==========================================================
  Filtro para departamentos por ciudad
==========================================================*/
$(document).ready(function () {
    $("#departamento").change(function () {
        var id = $(this).val();
        $.get("departamentos/" + id, function (data) {
            var select = '<option value="">Seleccione Ciudad</option>';
            for (var i = 0; i < data.length; i++)
                select +=
                    '<option value="' +
                    data[i].id +
                    '">' +
                    data[i].nombre_ciudad +
                    "</option>";
            $("#ciudad_id").html(select);
            $("#ciudad_id").prop("disabled", false);
        });
    });
});

/*==========================================================
  ultimos estudios habilitar select
==========================================================*/
$("#ultimoestudio").change(function () {
    if ($("#ultimoestudio").val() == "Ninguno") {
        $("#estudioprimario").prop("disabled", true);
    } else {
        $("#estudioprimario").prop("disabled", false);
    }
});

$("#ultimoestudio").change(function () {
    if (
        $("#ultimoestudio").val() == "1" ||
        $("#ultimoestudio").val() == "4" ||
        $("#ultimoestudio").val() == "7"
    ) {
        $("#estudiouniversidad").prop("disabled", true);
    } else if ($("#ultimoestudio").val() == "Ninguno") {
        $("#estudiouniversidad").prop("disabled", true);
    } else if ($("#ultimoestudio").val() == "Bachillerato") {
        $("#estudiouniversidad").prop("disabled", true);
    } else {
        $("#estudiouniversidad").prop("disabled", false);
    }
});

/*==========================================================
  Sector Trabajo mostrar textarea
==========================================================*/
$("#formalsi").click(function () {
    if (document.getElementById("formalsi").checked) {
        $("#sectortrabajo").prop("disabled", false);
    }
});

$("#formalno").click(function () {
    if (document.getElementById("formalno").checked) {
        $("#sectortrabajo").prop("disabled", true);
    }
});

/*==========================================================
  El crédito le fue otorgado
==========================================================*/
$("#creditoviviendasi").click(function () {
    if (document.getElementById("creditoviviendasi").checked) {
        $("#creditootorgado").css("display", "block");
    }
});

$("#creditoviviendano").click(function () {
    if (document.getElementById("creditoviviendano").checked) {
        $("#creditootorgado").css("display", "none");
        $("#limpieza8").val("");
        document.getElementById("creditosi").checked = false;
        document.getElementById("creditono").checked = false;
    }
});

/*==========================================================
  select Por quien ha recibido discriminacion
==========================================================*/
$("#porquien").change(function () {
    if ($("#porquien").val() == "Otros") {
        $("#otroporquien2").css("display", "block");
    } else {
        $("#otroporquien2").css("display", "none");
        $("#otroporquien").val("");    
    }
});

/*==========================================================
  select agresion con mayor frecuencia
==========================================================*/
$("#tipoagresion").change(function () {
    if ($("#tipoagresion").val() == "Otros") {
        $("#otrotipoagresion2").css("display", "block");
    } else {
        $("#otrotipoagresion2").css("display", "none");
        $("#otrotipoagresion").val("");
    }
});