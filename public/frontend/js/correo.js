Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Enviado con exito',
    footer: 'Revise su correo electronico',
    toast: true,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  })