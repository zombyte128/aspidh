Swal.fire({
    position: 'top-end',
    icon: 'error',
    title: 'Verifique su email',
    footer:'No hay ningun email asociado',
    toast: true,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  })