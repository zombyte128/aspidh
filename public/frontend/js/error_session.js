
Swal.fire({
    position: 'top-end',
    icon: 'error',
    title: 'Credenciales Incorrectas',
    footer:'Verifique sus datos',
    toast: true,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  })