/* ----------------------------------------------------------- */
/* Progress bar inicio
/* ----------------------------------------------------------- */
$("#bar").animate({
    width: "14%"
}, 2500);

/* ----------------------------------------------------------- */
/* Educacion
/* ----------------------------------------------------------- */
$("#siguiente1").on("click", function () {
    //Validando si estan vacios los input
    if (($('#general1').val() != "") && ($('#general2').val() != "") && ($('#username').val() != "") && ($('#txtPassword').val() != "")){
    
        //Mostrando educacion y ocultando los demas
        $("#educacion").css("display", "block");
        $("#datosgenerales").css("display", "none");
        $("#vivienda").css("display", "none");
        $("#salud").css("display", "none");
        $("#trabajo").css("display", "none");
        $("#politica_religion").css("display", "none");
        $("#derechos").css("display", "none");
        window.scrollTo(0, 860);

        //progess bar
        $("#bar2").animate({
            width: "28%"
        }, 2500);

    //Alerta de error
    }else{
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Algunos campos son obligatorios',
            footer:'Usuario | Password | Nombre | Apellido',
            toast: true,
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true
          })
    }
});

/* ----------------------------------------------------------- */
/* Salud
/* ----------------------------------------------------------- */
$("#siguiente2").on("click", function () {
    //Mostrando educacion y ocultando los demas
    $("#salud").css("display", "block");
    $("#datosgenerales").css("display", "none");
    $("#educacion").css("display", "none");
    $("#vivienda").css("display", "none");
    $("#trabajo").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 861);

     //progess bar
     $("#bar3").animate({
        width: "42%"
    }, 2500);
});

/* ----------------------------------------------------------- */
/* Trabajo
/* ----------------------------------------------------------- */
$("#siguiente3").on("click", function () {
    //Mostrando educacion y ocultando los demas
    $("#trabajo").css("display", "block");
    $("#educacion").css("display", "none");
    $("#general").css("display", "none");
    $("#salud").css("display", "none");
    $("#vivienda").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 860);

     //progess bar
     $("#bar4").animate({
        width: "56%"
    }, 2500);
});

/* ----------------------------------------------------------- */
/* Vivienda
/* ----------------------------------------------------------- */
$("#siguiente4").on("click", function () {
    //Mostrando educacion y ocultando los demas
    $("#vivienda").css("display", "block");
    $("#trabajo").css("display", "none");
    $("#educacion").css("display", "none");
    $("#general").css("display", "none");
    $("#salud").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 861);

     //progess bar
     $("#bar5").animate({
        width: "70%"
    }, 2500);
});

/* ----------------------------------------------------------- */
/* Politica y religion
/* ----------------------------------------------------------- */
$("#siguiente5").on("click", function () {
    //Mostrando educacion y ocultando los demas
    $("#politica_religion").css("display", "block");
    $("#vivienda").css("display", "none");
    $("#trabajo").css("display", "none");
    $("#educacion").css("display", "none");
    $("#general").css("display", "none");
    $("#salud").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 860);

    //progess bar
    $("#bar6").animate({
        width: "84%"
    }, 2500);
});

/* ----------------------------------------------------------- */
/* Derechos
/* ----------------------------------------------------------- */
$("#siguiente6").on("click", function () {
    //Mostrando educacion y ocultando los demas
    $("#derechos").css("display", "block");
    $("#politica_religion").css("display", "none");
    $("#vivienda").css("display", "none");
    $("#trabajo").css("display", "none");
    $("#educacion").css("display", "none");
    $("#general").css("display", "none");
    $("#salud").css("display", "none");
    window.scrollTo(0, 861);

    //progess bar
    $("#bar7").animate({
        width: "100%"
    }, 2500);
});

/* ----------------------------------------------------------- */
/* Enviar
/* ----------------------------------------------------------- */
$("#siguiente7").on("click", function () {
     //Funcion para hacer submit    
    $("#formulario").submit();
});

/* ----------------------------------------------------------- */
/* Atras General
/* ----------------------------------------------------------- */
$("#atras1").on("click", function () {
    $("#datosgenerales").css("display", "block");
    $("#educacion").css("display", "none");
    $("#salud").css("display", "none");
    $("#trabajo").css("display", "none");
    $("#vivienda").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 860);
});

/* ----------------------------------------------------------- */
/* Atras Educacion
/* ----------------------------------------------------------- */
$("#atras2").on("click", function () {
    $("#educacion").css("display", "block");
    $("#salud").css("display", "none");
    $("#datosgenerales").css("display", "none");
    $("#trabajo").css("display", "none");
    $("#vivienda").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 861);
});

/* ----------------------------------------------------------- */
/* Atras Salud
/* ----------------------------------------------------------- */
$("#atras3").on("click", function () {
    $("#salud").css("display", "block");
    $("#trabajo").css("display", "none");
    $("#educacion").css("display", "none");
    $("#datosgenerales").css("display", "none");
    $("#vivienda").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 860);
});

/* ----------------------------------------------------------- */
/* Atras Trabajo
/* ----------------------------------------------------------- */
$("#atras4").on("click", function () {
    $("#trabajo").css("display", "block");
    $("#vivienda").css("display", "none");
    $("#salud").css("display", "none");
    $("#educacion").css("display", "none");
    $("#datosgenerales").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 861);
});

/* ----------------------------------------------------------- */
/* Atras Vivienda
/* ----------------------------------------------------------- */
$("#atras5").on("click", function () {
    $("#vivienda").css("display", "block");
    $("#trabajo").css("display", "none");
    $("#salud").css("display", "none");
    $("#educacion").css("display", "none");
    $("#datosgenerales").css("display", "none");
    $("#politica_religion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 860);
});

/* ----------------------------------------------------------- */
/* Atras Politica y religion
/* ----------------------------------------------------------- */
$("#atras6").on("click", function () {
    $("#politica_religion").css("display", "block");
    $("#vivienda").css("display", "none");
    $("#trabajo").css("display", "none");
    $("#salud").css("display", "none");
    $("#educacion").css("display", "none");
    $("#derechos").css("display", "none");
    window.scrollTo(0, 861);
});
