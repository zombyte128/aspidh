Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Bienvenido Usuario',
    footer:'Has iniciado sesión',
    toast: true,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  })