/* ----------------------------------------------------------- */
/* Todos Los Administradores Eliminar
/* ----------------------------------------------------------- */

function deleteadministrador(id) {
    Swal.fire({
        title: "¿Esta Seguro?",
        text: "!No Podras Revertir Esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("administrador-delete-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Todos Los Donantes Eliminar
/* ----------------------------------------------------------- */

function deletedonante(id) {
    Swal.fire({
        title: "¿Esta Seguro?",
        text: "!No Podras Revertir Esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("donante-delete-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Marcar denuncia como completada
/* ----------------------------------------------------------- */

function denunciaCompletada(id) {
    Swal.fire({
        title: "¿Esta seguro de marcar como completada esta denuncia?",
        text:
            "!Ten encuenta que al marcar esta opcion significa que la denuncia ya llego hasta las entidades que se encargaran de hacer justicia!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, marcar como completada!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("denuncia-completada-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Marcar denuncia como finalizada
/* ----------------------------------------------------------- */

function darlefinalizar(id) {
    Swal.fire({
        title: "¿Esta segura marcar esta denuncia como finalizada?",
        text:
            "!Ten encuenta que esta acción significa que la denuncia ya concluyo todo el proceso judicial y se dio un veredicto final departe de las autoridades competentes!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Finalizar!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("denuncia-finalizada-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Marcar denuncia como seguimiento
/* ----------------------------------------------------------- */

function darleSeguimiento(id) {
    Swal.fire({
        title: "¿Esta segura darle Seguimiento a esta denuncia?",
        text:
            "!Ten encuenta que esta acción pondra en movimiento a las personas encargadas en seguir todo el proceso de esa denuncia hasta completarla!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Darle Seguimiento!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("denuncia-seguimiento-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Banear denuncia
/* ----------------------------------------------------------- */

function banear(id) {
    Swal.fire({
        title: "¿Esta segura descartar esta denuncia?",
        text:
            "!Ten encuenta que esta acción descartara completamente el proceso de seguimiento para esta denuncia!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Banear",
    }).then((result) => {
        if (result.value) {
            document.getElementById("denuncia-baneada-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Eliminar denuncia en estado pendiente
/* ----------------------------------------------------------- */

function deletedenunciapendiente(id) {
    Swal.fire({
        title: "¿Esta Seguro?",
        text: "!No Podras Revertir Esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("denuncia-eliminada-pendiente-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Eliminar denuncia en estado finalizada
/* ----------------------------------------------------------- */

function finalizadadelete(id) {
    Swal.fire({
        title: "¿Esta Seguro?",
        text: "!No Podras Revertir Esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("denuncia-eliminada-finalizada-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Eliminar denuncia en estado baneada
/* ----------------------------------------------------------- */

function baneadadelete(id) {
    Swal.fire({
        title: "¿Esta Seguro?",
        text: "!No Podras Revertir Esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("denuncia-eliminada-baneada-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Eliminar persona
/* ----------------------------------------------------------- */

function deleteUser(id) {
    Swal.fire({
        title: "¿Esta Seguro?",
        text: "!No Podras Revertir Esto!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
        if (result.value) {
            document.getElementById("delete-user-" + id).submit();
        } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
        ) {
            swal.fire("Cancelado", "Los Datos Estan Seguros :)", "error");
        }
    });
}

/* ----------------------------------------------------------- */
/* Ejecutar Data Table
/* ----------------------------------------------------------- */

$(document).ready(function () {
    var table = $("#example").DataTable({
        order: [],
        lengthChange: false,
        buttons: ["excel"],
        searching: true,
        info: false,
        "iDisplayStart ": 3,
        iDisplayLength: 8,
        language: {
            searchPlaceholder: "Buscar",
            zeroRecords: "No se han encontrado coincidencias.",
            emptyTable: "No hay datos disponibles en la tabla.",
            search: " ",
            paginate: {
                first: "Primera",
                last: "Última",
                next: "Siguiente",
                previous: "Anterior",
            },
        },
    });
    table.buttons().container().appendTo("#example_wrapper .col-md-6:eq(0)");
});

