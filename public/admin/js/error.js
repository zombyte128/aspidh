Swal.fire({
    position: 'top-end',
    icon: 'error',
    title: 'Verifique sus datos',
    footer:'No enviado',
    toast: true,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
  })