@extends('admin.partials.app')
@section('title','ASPIDH - Trabajo u ocupación personas transexual')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Trabajo u ocupación personas transexuales</li>
        </ol>
    </nav>
    <div class="row">
        <!--======cuenta con empleo formal=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart15->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======sectores de trabajo=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart16->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======negado por su orientacion sexual=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart17->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======programa de acceso al trabajo gubernamental=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart18->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart15->script() !!}
{!! $chart16->script() !!}
{!! $chart17->script() !!}
{!! $chart18->script() !!}
@endsection