@extends('admin.partials.app')
@section('title','ASPIDH - Denuncias Baneadas')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Denuncias Baneadas</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========tabla denuncias baneadas=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th hidden>¿Quien la realiza?</th>
                                    <th>Nombre</th>
                                    <th>Documento Identificacion</th>
                                    <th>Teléfono</th>
                                    <th hidden>Donde Ocurrio</th>
                                    <th hidden>Breve Relato</th>
                                    <th hidden>Tipo De Violencia</th>
                                    <th hidden>Tipo Secundario Violencia</th>
                                    <th hidden>Cantidad De Agresores</th>
                                    <th hidden>Forma Agresion</th>
                                    <th hidden>Informacion Del Autor</th>
                                    <th hidden>Realizo Denuncia Formal</th>
                                    <th hidden>Lugar Donde Realizo La Denuncia</th>
                                    <th>Fecha Del Suceso</th>
                                    <th hidden>Fecha Que Realizo La Denuncia</th>
                                    <th>Departamento</th>
                                    <th>Ciudad</th>
                                    <th hidden>Direccion</th>
                                    <th>Estado</th>
                                    <th hidden>Documento Respaldatorios</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($baneadas as $key=>$baneada)
                                <tr>
                                    <td hidden>{{ $baneada->quien_registra_denuncia }}</td>
                                    <td>{{ $baneada->users->nombres_segun_dui }}
                                        {{ $baneada->users->apellidos_segun_dui }}</td>
                                    <td>{{ $baneada->users->documento_identificacion }}</td>
                                    <td>{{ $baneada->users->numero_telefono }}</td>
                                    <td hidden>{{ $baneada->donde_ocurrio }}</td>
                                    <td hidden>{{ $baneada->breve_relato_del_hecho_consecuencias }}</td>
                                    <td hidden>{{ $baneada->tipo_principal_violacion_abuso }}</td>
                                    <td hidden>{{ $baneada->tipo_secundario_violacion_abuso }}</td>
                                    <td hidden>{{ $baneada->indique_cantidad_agresores }}</td>
                                    <td hidden>{{ $baneada->forma_de_agrecion }}</td>
                                    <td hidden>{{ $baneada->informacion_del_autor_del_hecho }}</td>
                                    <td hidden>{{ $baneada->realizo_una_denuncia_formal }}</td>
                                    <td hidden>{{ $baneada->lugar_donde_realizo_la_denuncia_formal }}</td>
                                    <td>{{ date('d/m/Y',strtotime($baneada->fecha_del_suceso)) }}</td>
                                    <td hidden>
                                        {{  date('d/m/Y',strtotime($baneada->fecha_cuando_se_reaizo_la_denuncia_formal)) }}
                                    </td>
                                    <td>@if( isset($baneada->tipogenero->nombre_tipo_genero) )
                                        {{ $baneada->departamentos->nombre_departamento }} @endif</td>
                                    <td>@if( isset($baneada->tipogenero->nombre_tipo_genero) )
                                        {{ $baneada->ciudades->nombre_ciudad }} @endif</td>
                                    <td hidden>{{ $baneada->direccion_completa }}</td>
                                    <td>
                                        <div class="badge badge-dark badge-fw">{{ $baneada->estado_de_la_denuncia }}
                                        </div>
                                    </td>
                                    <td hidden>{{ $baneada->documentacion_respaldatoria }}</td>
                                    <td>{{ date('d/m/Y',strtotime($baneada->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($baneada->updated_at)) }}</td>
                                    <td>
                                        <!--========Ver=========-->
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Finalizar" title="Ver Denuncia"
                                            href="{{ URL::to('admin/pdf-denuncia/'.$baneada->id) }}"><i
                                                class="icon-eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection