<!--========modal=========-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--========formulario=========-->
        <form class="forms-sample" action="{{ route('admin.denuncias.create') }}" method="GET">
            <div class="modal-content">
                <div class="modal-body">
                    <!--========email=========-->
                    <input type="text" class="form-control" placeholder="Buscar Usuario (ID || Email)" name="email"
                        autocomplete="off" required>
                </div>
                <!--========botones=========-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Buscar</button>
                </div>
            </div>
        </form>
    </div>
</div>