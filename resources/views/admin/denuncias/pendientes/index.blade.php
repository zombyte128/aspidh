@extends('admin.partials.app')
@section('title','ASPIDH - Denuncias Pendientes')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Denuncias Pendientes</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========tabla denuncias pendientes=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th hidden>¿Quien la realiza?</th>
                                    <th>Nombre</th>
                                    <th>Documento Identificacion</th>
                                    <th>Teléfono</th>
                                    <th hidden>Donde Ocurrio</th>
                                    <th hidden>Breve Relato</th>
                                    <th hidden>Tipo De Violencia</th>
                                    <th hidden>Tipo Secundario Violencia</th>
                                    <th hidden>Cantidad De Agresores</th>
                                    <th hidden>Forma Agresion</th>
                                    <th hidden>Informacion Del Autor</th>
                                    <th hidden>Realizo Denuncia Formal</th>
                                    <th hidden>Lugar Donde Realizo La Denuncia</th>
                                    <th>Fecha Del Suceso</th>
                                    <th hidden>Fecha Que Realizo La Denuncia</th>
                                    <th>Departamento</th>
                                    <th>Ciudad</th>
                                    <th hidden>Direccion</th>
                                    <th>Estado</th>
                                    <th hidden>Documento Respaldatorios</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pendientes as $key=>$pendiente)
                                <tr>
                                <td hidden>{{ $pendiente->quien_registra_denuncia }}</td>
                                    <td>{{ $pendiente->users->nombres_segun_dui }}
                                        {{ $pendiente->users->apellidos_segun_dui }}</td>
                                    <td>{{ $pendiente->users->documento_identificacion }}</td>
                                    <td>{{ $pendiente->users->numero_telefono }}</td>
                                    <td hidden>{{ $pendiente->donde_ocurrio }}</td>
                                    <td hidden>{{ $pendiente->breve_relato_del_hecho_consecuencias }}</td>
                                    <td hidden>{{ $pendiente->tipo_principal_violacion_abuso }}</td>
                                    <td hidden>{{ $pendiente->tipo_secundario_violacion_abuso }}</td>
                                    <td hidden>{{ $pendiente->indique_cantidad_agresores }}</td>
                                    <td hidden>{{ $pendiente->forma_de_agrecion }}</td>
                                    <td hidden>{{ $pendiente->informacion_del_autor_del_hecho }}</td>
                                    <td hidden>{{ $pendiente->realizo_una_denuncia_formal }}</td>
                                    <td hidden>{{ $pendiente->lugar_donde_realizo_la_denuncia_formal }}</td>
                                    <td>{{ date('d/m/Y',strtotime($pendiente->fecha_del_suceso)) }}</td>
                                    <td hidden>
                                        {{ date('d/m/Y',strtotime($pendiente->fecha_cuando_se_reaizo_la_denuncia_formal)) }}
                                    </td>
                                    <td>@if( isset($pendiente->tipogenero->nombre_tipo_genero) )
                                        {{ $pendiente->departamentos->nombre_departamento }} @endif</td>
                                    <td>@if( isset($persona->tipogenero->nombre_tipo_genero) )
                                        {{ $pendiente->ciudades->nombre_ciudad }} @endif</td>
                                    <td hidden>{{ $pendiente->direccion_completa }}</td>
                                    <td>
                                        <div class="badge badge-danger badge-fw">{{ $pendiente->estado_de_la_denuncia }}
                                        </div>
                                    </td>
                                    <td hidden>{{ $pendiente->documentacion_respaldatoria }}</td>
                                    <td>{{ date('d/m/Y',strtotime($pendiente->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($pendiente->updated_at)) }}</td>
                                    <td>
                                        <!--========dropdown=========-->
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                                id="dropdownMenuButton4" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <i class="icon-wrench"></i>
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton4"
                                                x-placement="bottom-start"
                                                style="position: absolute; transform: translate3d(0px, 33px, 0px); top: -20px; left: 0px; will-change: transform;">
                                                <!--========Ver=========-->
                                                <a class="dropdown-item"
                                                    href="{{ URL::to('admin/pdf-denuncia/'.$pendiente->id) }}">Ver</a>
                                                <!--========editar=========-->
                                                <a class="dropdown-item"
                                                    href="{{route('admin.denuncias-pendientes.edit', $pendiente->id)}}">Editar</a>
                                                <!--========cambiar estado=========-->
                                                <a class="dropdown-item" href="javascript:void(0)"
                                                    onclick="darleSeguimiento({{ $pendiente->id }})">Darle
                                                    Seguimiento</a>
                                                <!--========cambiar estado=========-->
                                                <a class="dropdown-item" onclick="banear({{ $pendiente->id }})"
                                                    href="javascript:void(0)">Banear
                                                    Denuncia</a>
                                            </div>
                                        </div>
                                        <!--========form cambiar estado=========-->
                                        <form id="denuncia-seguimiento-{{ $pendiente->id }}"
                                            action="{{ route('admin.denuncias-pendientes.update',$pendiente->id) }}"
                                            method="POST" style="display: none;">
                                            @csrf
                                            @method('PUT')
                                        </form>
                                        <!--========form cambiar estado=========-->
                                        <form id="denuncia-baneada-{{ $pendiente->id }}"
                                            action="{{ route('admin.denuncias-baneadas.update',$pendiente->id) }}"
                                            method="POST" style="display: none;">
                                            @csrf
                                            @method('PUT')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection