@extends('admin.partials.app')
@section('title','ASPIDH - Denuncias En Seguimiento')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Denuncias En Seguimiento</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========table=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th hidden>¿Quien la realiza?</th>
                                    <th>Nombre</th>
                                    <th>Documento Identificacion</th>
                                    <th>Teléfono</th>
                                    <th hidden>Donde Ocurrio</th>
                                    <th hidden>Breve Relato</th>
                                    <th hidden>Tipo De Violencia</th>
                                    <th hidden>Tipo Secundario Violencia</th>
                                    <th hidden>Cantidad De Agresores</th>
                                    <th hidden>Forma Agresion</th>
                                    <th hidden>Informacion Del Autor</th>
                                    <th hidden>Realizo Denuncia Formal</th>
                                    <th hidden>Lugar Donde Realizo La Denuncia</th>
                                    <th>Fecha Del Suceso</th>
                                    <th hidden>Fecha Que Realizo La Denuncia</th>
                                    <th>Departamento</th>
                                    <th>Ciudad</th>
                                    <th hidden>Direccion</th>
                                    <th>Estado</th>
                                    <th hidden>Documento Respaldatorios</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($seguimientos as $key=>$seguimiento)
                                <tr>
                                    <td hidden>{{ $seguimiento->quien_registra_denuncia }}</td>
                                    <td>{{ $seguimiento->users->nombres_segun_dui }}
                                        {{ $seguimiento->users->apellidos_segun_dui }}</td>
                                    <td>{{ $seguimiento->users->documento_identificacion }}</td>
                                    <td>{{ $seguimiento->users->numero_telefono }}</td>
                                    <td hidden>{{ $seguimiento->donde_ocurrio }}</td>
                                    <td hidden>{{ $seguimiento->breve_relato_del_hecho_consecuencias }}</td>
                                    <td hidden>{{ $seguimiento->tipo_principal_violacion_abuso }}</td>
                                    <td hidden>{{ $seguimiento->tipo_secundario_violacion_abuso }}</td>
                                    <td hidden>{{ $seguimiento->indique_cantidad_agresores }}</td>
                                    <td hidden>{{ $seguimiento->forma_de_agrecion }}</td>
                                    <td hidden>{{ $seguimiento->informacion_del_autor_del_hecho }}</td>
                                    <td hidden>{{ $seguimiento->realizo_una_denuncia_formal }}</td>
                                    <td hidden>{{ $seguimiento->lugar_donde_realizo_la_denuncia_formal }}</td>
                                    <td>{{ date('d/m/Y',strtotime($seguimiento->fecha_del_suceso)) }}</td>
                                    <td hidden>
                                        {{ date('d/m/Y',strtotime($seguimiento->fecha_cuando_se_reaizo_la_denuncia_formal)) }}
                                    </td>
                                    <td>@if( isset($seguimiento->departamentos->nombre_departamento) )
                                        {{ $seguimiento->departamentos->nombre_departamento }} @endif</td>
                                    <td>@if( isset($seguimiento->ciudades->nombre_ciudad) )
                                        {{ $seguimiento->ciudades->nombre_ciudad }} @endif</td>
                                    <td hidden>{{ $seguimiento->direccion_completa }}</td>
                                    <td>
                                        <div class="badge badge-primary badge-fw">
                                            {{ $seguimiento->estado_de_la_denuncia }}</div>
                                    </td>
                                    <td hidden>{{ $seguimiento->documentacion_respaldatoria }}</td>
                                    <td>{{ date('d/m/Y',strtotime($seguimiento->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($seguimiento->updated_at)) }}</td>
                                    <td>
                                        <!--========dropdown=========-->
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Finalizar" title="Denuncia Completada"
                                            href="javascript:void(0)"
                                            onclick="denunciaCompletada({{ $seguimiento->id }})"><i
                                                class="icon-plus"></i></a>
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Finalizar" title="Ver Denuncia"
                                            href="{{ URL::to('admin/pdf-denuncia/'.$seguimiento->id) }}"><i
                                                class="icon-eye"></i></a>
                                        <!--========form denuncia completada=========-->
                                        <form id="denuncia-completada-{{ $seguimiento->id }}"
                                            action="{{ route('admin.denuncias-seguimiento.update',$seguimiento->id) }}"
                                            method="POST" style="display: none;">
                                            @csrf
                                            @method('PUT')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection