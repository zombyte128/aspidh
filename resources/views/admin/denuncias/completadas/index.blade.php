@extends('admin.partials.app')
@section('title','ASPIDH - Denuncias Completadas')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Denuncias Completadas</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========tabla denuncias completadas=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th hidden>¿Quien la realiza?</th>
                                    <th>Nombre</th>
                                    <th>Documento Identificacion</th>
                                    <th>Teléfono</th>
                                    <th hidden>Donde Ocurrio</th>
                                    <th hidden>Breve Relato</th>
                                    <th hidden>Tipo De Violencia</th>
                                    <th hidden>Tipo Secundario Violencia</th>
                                    <th hidden>Cantidad De Agresores</th>
                                    <th hidden>Forma Agresion</th>
                                    <th hidden>Informacion Del Autor</th>
                                    <th hidden>Realizo Denuncia Formal</th>
                                    <th hidden>Lugar Donde Realizo La Denuncia</th>
                                    <th>Fecha Del Suceso</th>
                                    <th hidden>Fecha Que Realizo La Denuncia</th>
                                    <th>Departamento</th>
                                    <th>Ciudad</th>
                                    <th hidden>Direccion</th>
                                    <th>Estado</th>
                                    <th hidden>Documento Respaldatorios</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($completadas as $key=>$completa)
                                <tr>
                                    <td hidden>{{ $completa->quien_registra_denuncia }}</td>
                                    <td>{{ $completa->users->nombres_segun_dui }}
                                        {{ $completa->users->apellidos_segun_dui }}</td>
                                    <td>{{ $completa->users->documento_identificacion }}</td>
                                    <td>{{ $completa->users->numero_telefono }}</td>
                                    <td hidden>{{ $completa->donde_ocurrio }}</td>
                                    <td hidden>{{ $completa->breve_relato_del_hecho_consecuencias }}</td>
                                    <td hidden>{{ $completa->tipo_principal_violacion_abuso }}</td>
                                    <td hidden>{{ $completa->tipo_secundario_violacion_abuso }}</td>
                                    <td hidden>{{ $completa->indique_cantidad_agresores }}</td>
                                    <td hidden>{{ $completa->forma_de_agrecion }}</td>
                                    <td hidden>{{ $completa->informacion_del_autor_del_hecho }}</td>
                                    <td hidden>{{ $completa->realizo_una_denuncia_formal }}</td>
                                    <td hidden>{{ $completa->lugar_donde_realizo_la_denuncia_formal }}</td>
                                    <td>{{ date('d/m/Y',strtotime($completa->fecha_del_suceso)) }}</td>
                                    <td hidden>
                                        {{ date('d/m/Y',strtotime($completa->fecha_cuando_se_reaizo_la_denuncia_formal)) }}
                                    </td>
                                    <td>@if( isset($completa->tipogenero->nombre_tipo_genero) )
                                        {{ $completa->departamentos->nombre_departamento }} @endif</td>
                                    <td>@if( isset($completa->tipogenero->nombre_tipo_genero) )
                                        {{ $completa->ciudades->nombre_ciudad }} @endif</td>
                                    <td hidden>{{ $completa->direccion_completa }}</td>
                                    <td>
                                        <div class="badge badge-success badge-fw">
                                            {{ $completa->estado_de_la_denuncia }}</div>
                                    </td>
                                    <td hidden>{{ $completa->documentacion_respaldatoria }}</td>
                                    <td>{{ date('d/m/Y',strtotime($completa->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($completa->updated_at)) }}</td>
                                    <td>
                                        <!--========cambiar estado=========-->
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Finalizar" title="Finalizar" href="javascript:void(0)"
                                            onclick="darlefinalizar({{ $completa->id }})"><i class="icon-plus"></i></a>
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Finalizar" title="Ver Denuncia"
                                            href="{{ URL::to('admin/pdf-denuncia/'.$completa->id) }}"><i
                                                class="icon-eye"></i></a>
                                        <!--=====form finalizar=======-->
                                        <form id="denuncia-finalizada-{{ $completa->id }}"
                                            action="{{ route('admin.denuncias-completadas.update',$completa->id) }}"
                                            method="POST" style="display: none;">
                                            @csrf
                                            @method('PUT')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection