@extends('admin.partials.app')
@section('title','ASPIDH - Datos Generales')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Datos generales</li>
        </ol>
    </nav>
    <div class="row">
        <!--======Genero=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart3->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======orientacion=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart4->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======total de personas departamento=========-->
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart5->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======total personas registradas por mes=========-->
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="switch-field responsive_radio">
                        <form action="{{route('admin.general')}}" method="POST">
                            @csrf
                            <input type="submit" id="20201" name="anios" value="2020">
                            <label for="20201">2020</label>
                            <input type="submit" id="20211" name="anios" value="2021">
                            <label for="20211">2021</label>
                            <input type="submit" id="20221" name="anios" value="2022">
                            <label for="20221">2022</label>
                            <input type="submit" id="20231" name="anios" value="2023">
                            <label for="20231">2023</label>
                            <input type="submit" id="20241" name="anios" value="2024">
                            <label for="20241">2024</label>
                        </form>
                    </div>
                    <div class="card-body graficas">
                        {!! $chart6->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart3->script() !!}
{!! $chart4->script() !!}
{!! $chart5->script() !!}
{!! $chart6->script() !!}
@endsection