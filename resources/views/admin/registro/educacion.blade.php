@extends('admin.partials.app')
@section('title','ASPIDH - Educación')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Educación</li>
        </ol>
    </nav>
    <div class="row">
        <!--======estudian actualmente=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart7->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======Aprobado grato de estudio=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart8->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======centro de estudio primario=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart9->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======centro de estudio universitario=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart10->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart7->script() !!}
{!! $chart8->script() !!}
{!! $chart9->script() !!}
{!! $chart10->script() !!}
@endsection