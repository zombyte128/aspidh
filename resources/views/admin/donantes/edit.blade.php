@extends('admin.partials.app')
@section('title','ASPIDH - Editar Donante')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.donantes.index')}}">Donantes</a></li>
        </ol>
    </nav>
    <div class="col-md-6 stretch-card mx-auto" style="margin: 100px;">
        <div class="card">
            <div class="card-body cards">
                <!--=========formulario===========-->
                <form class="forms-sample" action="{{ route('admin.donantes.update',$donantes->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <!--=========nombre===========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text"
                                    class="form-control{{ $errors->has('nombres_segun_dui') ? ' is-invalid' : '' }}"
                                    placeholder="Nombres" name="nombres_segun_dui" autocomplete="off"
                                    value="{{ $donantes->nombres_segun_dui }}" required minlength="2" maxlength="30">
                                @if ($errors->has('nombres_segun_dui'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombres_segun_dui') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!--=========apellido===========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text"
                                    class="form-control{{ $errors->has('apellidos_segun_dui') ? ' is-invalid' : '' }}"
                                    placeholder="Apellidos" name="apellidos_segun_dui" autocomplete="off"
                                    value="{{ $donantes->apellidos_segun_dui }}" required minlength="2" maxlength="30">
                                @if ($errors->has('apellidos_segun_dui'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidos_segun_dui') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--=========email===========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    placeholder="Email" name="email" value="{{ $donantes->email }}" required
                                    autocomplete="off">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!--=========password===========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    placeholder="Password" name="password" value="{{ $donantes->contrasena }}" required
                                    minlength="6" autocomplete="off">
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <br>
                    <!--=========submit===========-->
                    <center class="paddingbutton">
                        <button type="submit" class="btn btn-danger mr-2 paddingeventos">Añadir</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection