@extends('admin.partials.app')
@section('title','ASPIDH - Personas Registradas')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========Tabla personas=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th hidden>nombre_social</th>
                                    <th hidden>Fecha nacimiento</th>
                                    <th hidden>Edad</th>
                                    <th>Documento De Identificación</th>
                                    <th>Teléfono</th>
                                    <th>Email</th>
                                    <th>Denuncias</th>
                                    <th hidden>Tipo de genero</th>
                                    <th hidden>Orientacion sexual</th>
                                    <th hidden>Facebook</th>
                                    <th hidden>Twitter</th>
                                    <th hidden>Instagram</th>
                                    <th hidden>Nombre emergencia</th>
                                    <th hidden>Numero emergencia</th>
                                    <th hidden>Direccion emergencia</th>
                                    <th hidden>Parentesco o cercanía</th>
                                    <th>Departamento</th>
                                    <th>Ciudad</th>
                                    <th hidden>Direccion completa</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th hidden>Estudia actualmente</th>
                                    <th hidden>Razon de no estudiar</th>
                                    <th hidden>Ultimo grado aprobado</th>
                                    <th hidden>Lugar de estudio primario</th>
                                    <th hidden>Lugar de estudio universitario</th>
                                    <th hidden>Afiliado al seguro social</th>
                                    <th hidden>Utiliza servicios de salud</th>
                                    <th hidden>Centro medico que asiste</th>
                                    <th hidden>Beneficiario de programa de salud gubernamental</th>
                                    <th hidden>Cual programa de salud</th>
                                    <th hidden>Cuenta con empleo formal</th>
                                    <th hidden>Sector de trabajo</th>
                                    <th hidden>Negacion por identidad de genero</th>
                                    <th hidden>Programa de acceso al trabajo gubernamental</th>
                                    <th hidden>Cual programa de trabajo</th>
                                    <th hidden>vive actualmente</th>
                                    <th hidden>Con quien vive</th>
                                    <th hidden>Ingreso financiero mensual</th>
                                    <th hidden>Solicitud credito para vivienda</th>
                                    <th hidden>Le fue otorgado el credito</th>
                                    <th hidden>Justificacion del por que no</th>
                                    <th hidden>Beneficiario de programa de acceso a la vivienda gubernamental</th>
                                    <th hidden>Cual programa de vivienda</th>
                                    <th hidden>Ejercio su derecho al voto</th>
                                    <th hidden>Problemas al derecho al voto</th>
                                    <th hidden>Cual fue el problema</th>
                                    <th hidden>Participa en la politica</th>
                                    <th hidden>Practica alguna religion</th>
                                    <th hidden>tipo de religion</th>
                                    <th hidden>Discriminacion centro de estudio</th>
                                    <th hidden>Discriminacion trabajo</th>
                                    <th hidden>Discriminacion centro de salud</th>
                                    <th hidden>Por quien ha recibido agresion</th>
                                    <th hidden>Otro sujeto de agresion</th>
                                    <th hidden>Tipo de agresion frecuente</th>
                                    <th hidden>Otro tipo de agresion</th>
                                    <th hidden>Conoce mecanismos de defensa de sus derechos</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $personas as $key => $persona)
                                <tr>
                                    <td>{{ $persona->id }}</td>
                                    <td>{{ $persona->nombres_segun_dui }}</td>
                                    <td>{{ $persona->apellidos_segun_dui }}</td>
                                    <td hidden>{{ $persona->nombre_social }}</td>
                                    <td hidden>{{ $persona->fecha_nacimiento }}</td>
                                    <td hidden>{{ $persona->edad }}</td>
                                    <td>{{ $persona->documento_identificacion }}</td>
                                    <td>{{ $persona->numero_telefono }}</td>
                                    <td>{{ $persona->email }}</td>
                                    <td>{{ $persona->denuncias->count() }}</td>
                                    <td hidden>@if( isset($persona->tipogenero->nombre_tipo_genero) )
                                        {{ $persona->tipogenero->nombre_tipo_genero }}@endif</td>
                                    <td hidden>@if( isset($persona->orientacionsexual->nombre_orientacion_sexual) )
                                        {{ $persona->orientacionsexual->nombre_orientacion_sexual }}@endif</td>
                                    <td hidden>{{ $persona->redes_social_facebook }}</td>
                                    <td hidden>{{ $persona->redes_social_twitter }}</td>
                                    <td hidden>{{ $persona->redes_social_instagram }}</td>
                                    <td hidden>{{ $persona->nombres_emergencia }}</td>
                                    <td hidden>{{ $persona->telefono_emergencia }}</td>
                                    <td hidden>{{ $persona->direccion_emergencia }}</td>
                                    <td hidden>{{ $persona->redes_sociales_emergencia }}</td>
                                    <td>@if( isset($persona->departamentos->nombre_departamento) )
                                        {{ $persona->departamentos->nombre_departamento }} @endif</td>
                                    <td>@if( isset($persona->ciudades->nombre_ciudad) )
                                        {{ $persona->ciudades->nombre_ciudad }} @endif</td>
                                    <td hidden>{{ $persona->direccion_completa }}</td>
                                    <td>{{ date('d/m/Y',strtotime($persona->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($persona->updated_at)) }}</td>
                                    <td hidden>{{ $persona->estudia_actualmente }}</td>
                                    <td hidden>{{ $persona->razon_estudio }}</td>
                                    <td hidden>{{ $persona->ultimo_grado_estudio_aprobado }}</td>
                                    <td hidden>{{ $persona->lugar_estudio_primario }}</td>
                                    <td hidden>{{ $persona->lugar_estudio_universitario }}</td>
                                    <td hidden>{{ $persona->afiliado_seguro_social }}</td>
                                    <td hidden>{{ $persona->utiliza_servcios_de_salud }}</td>
                                    <td hidden>{{ $persona->centro_medico_asiste }}</td>
                                    <td hidden>{{ $persona->beneficiario_programa_salud_gubernamental }}</td>
                                    <td hidden>{{ $persona->cual_programa_salud }}</td>
                                    <td hidden>{{ $persona->cuenta_con_empleo_formal }}</td>
                                    <td hidden>{{ $persona->sector_trabajo }}</td>
                                    <td hidden>{{ $persona->negacion_trabajo_por_identidad_genero_orientacion_sexual }}
                                    </td>
                                    <td hidden>{{ $persona->beneficiario_programa_acceso_trabajo_gubernamental }}</td>
                                    <td hidden>{{ $persona->cual_programa_trabajo }}</td>
                                    <td hidden>{{ $persona->vive_actualmente }}</td>
                                    <td hidden>{{ $persona->con_quien_vive }}</td>
                                    <td hidden>{{ $persona->ingresos_financieros_mensual_aproximado }}</td>
                                    <td hidden>{{ $persona->solicitud_credito_para_vivienda }}</td>
                                    <td hidden>{{ $persona->credito_otorgado }}</td>
                                    <td hidden>{{ $persona->justificacion_credito }}</td>
                                    <td hidden>{{ $persona->beneficiario_programa_acceso_vivienda_gubernamental }}</td>
                                    <td hidden>{{ $persona->cual_programa_vivienda }}</td>
                                    <td hidden>{{ $persona->ejecicio_derecho_voto }}</td>
                                    <td hidden>{{ $persona->problemas_al_derecho_voto }}</td>
                                    <td hidden>{{ $persona->cual_problema_voto }}</td>
                                    <td hidden>{{ $persona->participa_en_politica }}</td>
                                    <td hidden>{{ $persona->practica_alguna_religion }}</td>
                                    <td hidden>{{ $persona->tipo_religion }}</td>
                                    <td hidden>{{ $persona->discriminacion_agrecion_centro_estudio }}</td>
                                    <td hidden>{{ $persona->discriminacion_agrecion_trabajo }}</td>
                                    <td hidden>{{ $persona->discriminacion_agrecion_centro_salud }}</td>
                                    <td hidden>{{ $persona->por_quien_hecho_de_agresion_discriminacion }}</td>
                                    <td hidden>{{ $persona->otro_por_quien_discrimanacion }}</td>
                                    <td hidden>{{ $persona->tipo_agresion_frecuente }}</td>
                                    <td hidden>{{ $persona->otro_tipo_agresion }}</td>
                                    <td hidden>{{ $persona->conocimientos_mecanimos_defensa_de_derechos }}</td>
                                    <td>
                                        <!--========Editar=========-->
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Editar" title="Editar" role="link"
                                            href="{{route('admin.personas.edit',$persona->id)}}"><i
                                                class="icon-pencil"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--========Boton nuevo usuario=========-->
    <a role="link" href="{{route('admin.personas.create')}}" class="btn btn-sm btn-primary">Nuevo Usuario</a>
</div>
@endsection