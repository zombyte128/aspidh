@extends('admin.partials.app')
@section('title','ASPIDH - Crear Usuario')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.personas.index')}}">Usuarios</a></li>
        </ol> 
    </nav>
    <!--========Formulario=========-->
    <form class="form-horizontal" action="{{ route('admin.personas.store') }}" method="POST"
        enctype="multipart/form-data" id="formulario">
        @csrf
        <!--=======DATOS GENERALES=============-->
        @include('admin.personas.formulario.generales')
        <!--=======EDUCACION=============-->
        @include('admin.personas.formulario.educacion')
        <!--=======ACCESO A LA SALUD=============-->
        @include('admin.personas.formulario.salud')
        <!--=======TRABAJO Y EMPLEO=============-->
        @include('admin.personas.formulario.trabajo')
        <!--=======ACCESO A LA VIVIENDA=============-->
        @include('admin.personas.formulario.vivienda')
        <!--=======POLITICA Y RELIGION=============-->
        @include('admin.personas.formulario.politica_religion')
        <!--=======VULNERACIÓN DE DERECHOS=============-->
        @include('admin.personas.formulario.derechos')
    </form>
</div>
@endsection