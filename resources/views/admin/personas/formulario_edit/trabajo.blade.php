<!--=======Trabajo=============-->
<div id="trabajo" style="display:none" class="wow fadeInUp" data-wow-duration="1s">
    <div class="row">
        <div class="col-md-4 mx-auto stretch-card">
            <div class="card">
                <div class="card-body">
                    <h2 class="texto-form">TRABAJO U OCUPACIÓN</h2>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======radio Cuenta con empleo formal==========-->
                        <div class="col-md-4">
                            <center>
                                <p>¿Actualmente cuenta con empleo formal?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="cuenta_con_empleo_formal" value="Si" id="formalsi"
                                    autocomplete="off"
                                    {{ $personas->cuenta_con_empleo_formal == 'Si'  ? 'checked' : '' }}>
                                <label for="formalsi" class="tamano">Si</label>
                                <input type="radio" name="cuenta_con_empleo_formal" value="No" id="formalno"
                                    autocomplete="off"
                                    {{ $personas->cuenta_con_empleo_formal == 'No'  ? 'checked' : '' }}>
                                <label for="formalno" class="tamano">No</label>
                            </div>
                        </div>
                        <!--=======radio Le han negado trabajo==========-->
                        <div class="col-md-5">
                            <center>
                                <p>¿Alguna vez le han negado un trabajo debido a su orientación sexual y/o
                                    identidad de género?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="negacion_trabajo_por_identidad_genero_orientacion_sexual"
                                    value="Si" id="negadosi" autocomplete="off"
                                    {{ $personas->negacion_trabajo_por_identidad_genero_orientacion_sexual == 'Si' ? 'checked' : '' }}>
                                <label for="negadosi" class="tamano">Si</label>
                                <input type="radio" name="negacion_trabajo_por_identidad_genero_orientacion_sexual"
                                    value="No" id="negadono" autocomplete="off"
                                    {{ $personas->negacion_trabajo_por_identidad_genero_orientacion_sexual == 'No' ? 'checked' : '' }}>
                                <label for="negadono" class="tamano">No</label>
                            </div>
                        </div>
                        <!--=======select su trabajo pertenece al sector ==========-->
                        <div class="col-md-3">
                            <center>
                                <p>¿Su trabajo pertenece al sector?</p>
                            </center>
                            <div class="form-group magenes">
                                <select class="form-control select2" name="sector_trabajo" id="sectortrabajo">
                                    <option value="">Seleccione Sector</option>
                                    <option value="Privado"
                                        {{ $personas->sector_trabajo == 'Privado' ? 'selected' : '' }}>
                                        Privado</option>
                                    <option value="Gobierno"
                                        {{ $personas->sector_trabajo == 'Gobierno' ? 'selected' : '' }}>
                                        Gobierno</option>
                                    <option value="ONG" {{ $personas->sector_trabajo == 'ONG' ? 'selected' : '' }}>ONG
                                    </option>
                                    <option value="Informal"
                                        {{ $personas->sector_trabajo == 'Informal' ? 'selected' : '' }}>
                                        Informal</option>
                                    <option value="Propio"
                                        {{ $personas->sector_trabajo == 'Propio' ? 'selected' : '' }}>
                                        Propio
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 mx-auto paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======radio beneficiado programa gubernamental acceso al trabajo==========-->
                        <div class="col-md-12 mx-auto">
                            <center>
                                <p>¿Ha sido beneficiario/a de algún programa gubernamental de acceso al
                                    trabajo?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="beneficiario_programa_acceso_trabajo_gubernamental"
                                    id="beneficiadotsi" value="Si" autocomplete="off"
                                    {{ $personas->beneficiario_programa_acceso_trabajo_gubernamental == 'Si'  ? 'checked' : '' }}>
                                <label for="beneficiadotsi" class="tamano2">Si</label>
                                <input type="radio" name="beneficiario_programa_acceso_trabajo_gubernamental"
                                    id="beneficiadotno" value="No" autocomplete="off"
                                    {{ $personas->beneficiario_programa_acceso_trabajo_gubernamental == 'No'  ? 'checked' : '' }}>
                                <label for="beneficiadotno" class="tamano2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--=======textarea razon del beneficio==========-->
                        <div class="col-md-10 mx-auto" id="raczonbenediciot" style="display: none;">
                            <div class="form-group magenes">
                                <textarea id="limpieza7" placeholder="¿Ingrese el nombre del programa?"
                                    name="cual_programa_trabajo" class="form-control"
                                    style="text-align:center;padding-top: 10px;">{{$personas->cual_programa_trabajo}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras3" type="button" class="btn btn-primary btn-lg float-left botones">Atras</button>
            <button id="siguiente4" type="button" class="btn btn-primary btn-lg float-right">Siguiente</button>
        </div>
    </div>
</div>