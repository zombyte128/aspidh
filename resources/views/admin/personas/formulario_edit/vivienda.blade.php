<!--=======Vivienda=============-->
<div id="vivienda" style="display:none" class="wow fadeInUp" data-wow-duration="1.5s">
    <div class="row">
        <div class="col-md-4 mx-auto stretch-card">
            <div class="card">
                <div class="card-body">
                    <h2 class="texto-form">ACCESO A LA VIVIENDA</h2>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======select estado vivienda actual==========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Estado Vivienda Actual
                                    </div>
                                </center>
                                <select class="form-control select2" name="vive_actualmente" id="viviendaactual">
                                    <option value="">Estado Vivienda Actual</option>
                                    <option value="Propio"
                                        {{ $personas->vive_actualmente == 'Propio' ? 'selected' : '' }}>
                                        Propio</option>
                                    <option value="Alquilado"
                                        {{ $personas->vive_actualmente == 'Alquilado' ? 'selected' : '' }}>Alquilado
                                    </option>
                                    <option value="Vivienda familiar"
                                        {{ $personas->vive_actualmente == 'Vivienda familiar' ? 'selected' : '' }}>
                                        Vivienda
                                        familiar</option>
                                    <option value="Otro" {{ $personas->vive_actualmente == 'Otro' ? 'selected' : '' }}>
                                        Otro
                                    </option>
                                </select>
                            </div>
                        </div>
                        <!--=======select con quien vive==========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">¿Con quién vive?
                                    </div>
                                </center>
                                <select class="form-control select2" name="con_quien_vive" id="conquienvive">
                                    <option value="">¿Con quién vive?</option>
                                    <option value="Solo/a"
                                        {{ $personas->con_quien_vive == 'Solo/a' ? 'selected' : '' }}>
                                        Solo/a
                                    </option>
                                    <option value="Familia nuclear"
                                        {{$personas->con_quien_vive == 'Familia nuclear' ? 'selected' : '' }}>Familia
                                        nuclear
                                    </option>
                                    <option value="Amigos/as"
                                        {{ $personas->con_quien_vive == 'Amigos/as' ? 'selected' : '' }}>
                                        Amigos/as</option>
                                    <option value="Pareja"
                                        {{ $personas->con_quien_vive == 'Pareja' ? 'selected' : '' }}>
                                        Pareja
                                    </option>
                                    <option value="Otros" {{ $personas->con_quien_vive == 'Otros' ? 'selected' : '' }}>
                                        Otros
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--=======select ingreso mensual==========-->
                        <div class="col-md-7 mx-auto">
                            <div class="form-group magenes">
                                <center>
                                    <div class="badge badge-outline-primary menos">¿cuánto es su ingreso mensual?
                                    </div>
                                </center>
                                <select class="form-control select2" name="ingresos_financieros_mensual_aproximado"
                                    id="ingresosmensuales">
                                    <option value="">¿cuánto es su ingreso mensual?</option>
                                    <option value="Menos de $250"
                                        {{ $personas->ingresos_financieros_mensual_aproximado == 'Menos de $250' ? 'selected' : '' }}>
                                        Menos de $250</option>
                                    <option value="$251 a $500"
                                        {{ $personas->ingresos_financieros_mensual_aproximado == '$251 a $500' ? 'selected' : '' }}>
                                        $251 a $500</option>
                                    <option value="$501 a $1,000"
                                        {{ $personas->ingresos_financieros_mensual_aproximado == '$501 a $1,000' ? 'selected' : '' }}>
                                        $501 a $1,000</option>
                                    <option value="$1,000 a $1,500"
                                        {{ $personas->ingresos_financieros_mensual_aproximado == '$1,000 a $1,500' ? 'selected' : '' }}>
                                        $1,000 a $1,500</option>
                                    <option value="Mas de $1,500"
                                        {{ $personas->ingresos_financieros_mensual_aproximado == 'Mas de $1,500' ? 'selected' : '' }}>
                                        Más de $1,500</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card padding5">
                <div class="card-body">
                    <div class="row">
                        <!--=======radio programa gubernamental acceso a la vivienda==========-->
                        <div class="col-md-7 mx-auto">
                            <center>
                                <p>¿Ha sido beneficiario/a de un programa gubernamental de acceso a la vivienda?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="beneficiario_programa_acceso_vivienda_gubernamental"
                                    id="programasi" value="Si" autocomplete="off"
                                    {{ $personas->beneficiario_programa_acceso_vivienda_gubernamental == 'Si'  ? 'checked' : '' }}>
                                <label for="programasi" class="tamano2">Si</label>
                                <input type="radio" name="beneficiario_programa_acceso_vivienda_gubernamental"
                                    id="programano" value="No" autocomplete="off"
                                    {{ $personas->beneficiario_programa_acceso_vivienda_gubernamental == 'No'  ? 'checked' : '' }}>
                                <label for="programano" class="tamano2">No</label>
                            </div>
                        </div>
                        <!--=======textarea credito otorgado razon==========-->
                        <div class="col-md-5" id="programagubernamental" style="display: none;">
                            <div class="form-group magenes">
                                <textarea id="limpieza9" placeholder="¿Ingrese el nombre del programa?"
                                    name="cual_programa_vivienda" class="form-control" rows="4"
                                    style="text-align:center;">{{$personas->beneficiario_programa_acceso_vivienda_gubernamental}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 mx-auto paddingregistro">
            <div class="card">
                <div class="card-body">
                    <!--=======radio Solicito Credito vivienda==========-->
                    <div class="col-md-7 mx-auto">
                        <center>
                            <p>¿Alguna vez ha solicitado un crédito para compra de vivienda?
                            </p>
                        </center>
                        <div class="form-group centrado magenes">
                            <input type="radio" name="solicitud_credito_para_vivienda" id="creditoviviendasi" value="Si"
                                autocomplete="off"
                                {{ $personas->solicitud_credito_para_vivienda == 'Si'  ? 'checked' : '' }}>
                            <label for="creditoviviendasi" class="tamano2">Si</label>
                            <input type="radio" name="solicitud_credito_para_vivienda" value="No" id="creditoviviendano"
                                autocomplete="off"
                                {{ $personas->solicitud_credito_para_vivienda == 'No'  ? 'checked' : '' }}>
                            <label for="creditoviviendano" class="tamano2">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro" style="display:none" id="creditootorgado">
            <div class="card padding3">
                <div class="card-body">
                    <div class="row">
                        <!--=======radio Credito le fue otrogado==========-->
                        <div class="col-md-7 mx-auto">
                            <center>
                                <p>¿El crédito le fue otorgado?
                                </p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="credito_otorgado" id="creditosi" value="Si" autocomplete="off"
                                    {{ $personas->credito_otorgado == 'Si'  ? 'checked' : '' }}>
                                <label for="creditosi" class="tamano2">Si</label>
                                <input type="radio" name="credito_otorgado" value="No" id="creditono" autocomplete="off"
                                    {{ $personas->credito_otorgado == 'No'  ? 'checked' : '' }}>
                                <label for="creditono" class="tamano2">No</label>
                            </div>
                        </div>
                        <!--=======textarea credito otorgado razon==========-->
                        <div class="col-md-5 mx-auto" id="creditotorgado" style="display: none;">
                            <div class="form-group magenes">
                                <textarea id="limpieza8" placeholder="¿Cual es la razon?" name="justificacion_credito"
                                    class="form-control" rows="4"
                                    style="text-align:center;">{{$personas->justificacion_credito}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras4" type="button" class="btn btn-primary btn-lg float-left botones">Atras</button>
            <button id="siguiente5" type="button" class="btn btn-primary btn-lg float-right">Siguiente</button>
        </div>
    </div>
</div>