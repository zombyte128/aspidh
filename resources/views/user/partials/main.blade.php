<!--===========Formulario de registro==========-->
<section id="registro" data-aos="fade-up">
    <div class="container container2">
        <div class="row">
            <div class="col-lg-8 mx-auto formulario">
                <h1>DENUNCIAS ASPIDH</h1>
                <center><img src="{{asset('frontend/img/title.png')}}"></center>
            </div>
        </div>
        <div class="row">
            <div class="container-fluid">
                <!--pasos realizar denuncia-->
                @include('user.formulario.pasos')
                <!--======Formulario denuncias=======-->
                <form method="POST" class="container" action="{{ route('user.denuncias')}}"
                    enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <!--Denuncia formulario-->
                    @include('user.formulario.denuncias')
                </form>
            </div>
        </div>
    </div>
    <!--======Imagenes de presentacion=======-->
    <div id="shap">
        <img class="shap1" src="{{asset('frontend/img/widget.png')}}">
        <img class="shap2" src="{{asset('frontend/img/widget2.png')}}">
        <img class="shap3" src="{{asset('frontend/img/widget3.png')}}">
    </div>
</section>