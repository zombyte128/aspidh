<!--===========Header==========-->
<section id="header" data-aos="zoom-in">
    <h3 class="nombre">
        {{Auth::user()->nombres_segun_dui.' '.Auth::user()->apellidos_segun_dui}}</h3>
    <img src="{{asset('frontend/img/logo.png')}}" class="logo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--======Titulo=======-->
                <h2 data-aos="fade-left" data-aos-delay="600">ASOCIACION ASPIDH</h2>
                <hr data-aos="zoom-in" data-aos-delay="600">
                <!--===========Botones denuncia==========-->
                <center><a href="{{ route('logout')  }}" data-aos="zoom-in" data-aos-delay="600"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                        class="btn registrar">Cerrar sesión</a></center>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</section>