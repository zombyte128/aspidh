<!--=====Formulario denuncias======-->
<div id="denuncia" style="display:none">
    <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-8 mx-auto">
            <div class="box_grafica">
                <h2 class="texto-form">INFORMACIÓN DEL HECHO</h2>
                <br>
                <!--====radius quien realizo la denuncia======-->
                <input type="radio" name="quien_registra_denuncia" value="Personal" id="quienregistroper"
                    autocomplete="off" checked>
                <label for="quienregistroper" class="tamano">Personal</label>
                <input type="radio" value="Tercero" name="quien_registra_denuncia" id="quienregistroter"
                    autocomplete="off">
                <label for="quienregistroter" class="tamano">Tercero</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--====input fecha======-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="tel" class="form-control @error('fecha_del_suceso') is-invalid @enderror"
                                placeholder="Fecha suceso | 00/00/0000" name="fecha_del_suceso" id="fechasuceso"
                                value="{{ old('fecha_del_suceso') }}" style="text-align:center;" autocomplete="off"
                                data-mask="00/00/0000" required>
                            @if ($errors->has('fecha_del_suceso'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('fecha_del_suceso')}}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <!--====select Donde Ocurrio======-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control select2" name="donde_ocurrio" id="dondeocurrio">
                                <option value="">¿Dónde ocurrió?</option>
                                <option value="Zona de trabajo sexual"
                                    {{ old('donde_ocurrio') == 'Zona de trabajo sexual' ? 'selected' : '' }}>Zona de
                                    trabajo sexual</option>
                                <option value="Calle villa publica"
                                    {{ old('donde_ocurrio') == 'Calle villa publica' ? 'selected' : '' }}>Calle villa
                                    publica</option>
                                <option value="Servicio de salud"
                                    {{ old('donde_ocurrio') == 'Servicio de salud' ? 'selected' : '' }}>Servicio de
                                    salud</option>
                                <option value="Sistema educativo"
                                    {{ old('donde_ocurrio') == 'Sistema educativo' ? 'selected' : '' }}>Sistema
                                    educativo</option>
                                <option value="Lugar de trabajo"
                                    {{ old('donde_ocurrio') == 'Lugar de trabajo' ? 'selected' : '' }}>Lugar de trabajo
                                </option>
                                <option value="Domicilio de la persona afectada"
                                    {{ old('donde_ocurrio') == 'Domicilio de la persona afectada' ? 'selected' : '' }}>
                                    Domicilio de la persona afectada
                                </option>
                                <option value="Otro domicilio particular"
                                    {{ old('donde_ocurrio') == 'Otro domicilio particular' ? 'selected' : '' }}>Otro
                                    domicilio particular</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======textarea Breve relato de los hechos==========-->
                    <div class="col-md-12 mx-auto">
                        <div class="form-group">
                            <textarea placeholder="Breve relato de los hechos y consecuencias"
                                name="breve_relato_del_hecho_consecuencias" class="form-control" id="breverelato"
                                style="text-align:center"
                                required>{{ old('breve_relato_del_hecho_consecuencias') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--====select Tipo de violacion/Abuso======-->
                    <div class="col-lg-8 col-md-12 mx-auto">
                        <div class="form-group">
                            <select class="form-control select2" name="tipo_principal_violacion_abuso"
                                id="tipodeviolacion">
                                <option value="">Tipo de violencia recibida</option>
                                <option value="Economica"
                                    {{ old('tipo_principal_violacion_abuso') == 'Economica' ? 'selected' : '' }}>
                                    Económica</option>
                                <option value="Feminicida/Homicida"
                                    {{ old('tipo_principal_violacion_abuso') == 'Feminicida/Homicida' ? 'selected' : '' }}>
                                    Feminicida/Homicida</option>
                                <option value="Fisica"
                                    {{ old('tipo_principal_violacion_abuso') == 'Fisica' ? 'selected' : '' }}>Física
                                </option>
                                <option value="psicologica/Emocional"
                                    {{ old('tipo_principal_violacion_abuso') == 'psicologica/Emocional' ? 'selected' : '' }}>
                                    psicólogica/Emocional
                                </option>
                                <option value="Patrimonial"
                                    {{ old('tipo_principal_violacion_abuso') == 'Patrimonial' ? 'selected' : '' }}>
                                    Patrimonial</option>
                                <option value="Sexual"
                                    {{ old('tipo_principal_violacion_abuso') == 'Sexual' ? 'selected' : '' }}>
                                    Sexual</option>
                                <option value="Simbolica"
                                    {{ old('tipo_principal_violacion_abuso') == 'Simbolica' ? 'selected' : '' }}>
                                    Simbólica</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======input tipo de violacion secundario==========-->
                    <div class="col-md-12 mx-auto">
                        <div class="form-group">
                            <textarea placeholder="Si has sufrido otros tipos de violencia ingresalos aqui"
                                name="tipo_secundario_violacion_abuso" class="form-control"
                                style="text-align:center;">{{ old('tipo_secundario_violacion_abuso') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12 mx-auto">
            <div class="box_general">
                <center>
                    <p>Lugar de los hechos</p>
                </center>
                <div class="row">
                    <!--=======select departamento==========-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control select2" id="departamento" name="departamento_id">
                                <option value="">Seleccione Departamento</option>
                                @foreach( $departamentos as $d)
                                <option value="{{ $d->id }}">{{ $d->nombre_departamento }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!--=======select ciudad==========-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control select2" name="ciudad_id" id="ciudad_id" disabled>
                                <option value="">Seleccione Ciudad</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======textarea direccion completa==========-->
                    <div class="col-md-12 mx-auto">
                        <div class="form-group">
                            <textarea placeholder="Colonia, pasaje, # de casa, edificio, apartamento"
                                name="direccion_completa" id="direccioncompleta" class="form-control"
                                style=" text-align:center;">{{ old('direccion_completa') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 mx-auto">
            <div class="box_general">
                <center>
                    <p class="documento">Informacion del autor del hecho</p>
                </center>
                <div class="row">
                    <!--=======select cantidad de agresores==========-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="number" name="indique_cantidad_agresores" class="form-control"
                                id=" cantidadagresor" placeholder="Cantidad de agresores" style="text-align:center"
                                autocomplete="off" value="{{ old('indique_cantidad_agresores') }}"></input>
                        </div>
                    </div>
                    <!--=======select que ocupo para agredir==========-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="forma_de_agrecion" class="form-control" id="formaagresion"
                                placeholder="Con qué objeto fue violentado" style="text-align:center" autocomplete="off"
                                value="{{ old('forma_de_agrecion') }}"></input>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======select Informacion del autor==========-->
                    <div class="col-md-12 mx-auto">
                        <div class="form-group">
                            <textarea name="informacion_del_autor_del_hecho" class="form-control" id="informacionautor"
                                placeholder="Breve descripción de los autores" cols="30"
                                style="text-align:center;">{{ old('informacion_del_autor_del_hecho') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12 mx-auto">
            <div class="box_general">
                <center>
                    <p>Acciones realizadas</p>
                </center>
                <div class="row">
                    <!--=======Radio de realizo denuncia formal==========-->
                    <div class="col-md-6 mx-auto">
                        <center>
                            <p class="sinborder">¿Realizó una denuncia formal?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="realizo_una_denuncia_formal" value="Si" id="denuniciasi"
                                autocomplete="off">
                            <label for="denuniciasi" class="tamano">Si</label>
                            <input type="radio" value="No" name="realizo_una_denuncia_formal" id="denuniciano"
                                autocomplete="off">
                            <label for="denuniciano" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======select Donde la hizo==========-->
                    <div class="col-md-6 mx-auto" style="display:none" id="dondelahizo">
                        <center>
                            <p class="sinborder">¿dónde la hizo?</p>
                        </center>
                        <div class="form-group">
                            <select class="form-control select2" name="lugar_donde_realizo_la_denuncia_formal"
                                id="lugardonderealizo">
                                <option value="">Seleccione Lugar</option>
                                <option value="Delegacion de la policía PNC">Delegación de la policía PNC</option>
                                <option value="Autoridad local">Autoridad local</option>
                                <option value="Fiscalia General de la republica FGR">Fiscalía General de la república
                                    FGR</option>
                                <option value="Ministerio de salud">Ministerio de salud</option>
                                <option value="Ministerio de trabajo">Ministerio de trabajo</option>
                                <option value="Procuraduría para la defensa de los DDHH de el salvador PDDH">
                                    Procuraduría para la defensa de los DDHH de el salvador PDDH</option>
                                <option value="Instituto salvadoreño del seguro social ISSS">Instituto salvadoreño del
                                    seguro social ISSS</option>
                                <option value="Otro">Otro</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 mx-auto">
            <div class="box_general">
                <center>
                    <p class="documento">Acciones realizadas</p>
                </center>
                <div class="row">
                    <!--====input fecha que realizo la denuncia formal======-->
                    <div class="col-md-6 mx-auto">
                        <div class="form-group">
                            <input type="tel" id="fechadenuncia" disabled
                                class="form-control @error('fecha_cuando_se_reaizo_la_denuncia_formal') is-invalid @enderror"
                                placeholder="Fecha denuncia | 00/00/0000"
                                name="fecha_cuando_se_reaizo_la_denuncia_formal" style="text-align:center;"
                                data-mask="00/00/0000">
                            @if ($errors->has('fecha_cuando_se_reaizo_la_denuncia_formal'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('fecha_cuando_se_reaizo_la_denuncia_formal')}}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======select Documentacion respaldatoria==========-->
                    <div class="col-md-10 mx-auto">
                        <div class="form-group">
                            <select class="form-control select2" name="documentacion_respaldatoria"
                                id="documentacionrespaldatoria">
                                <option value="">Documentación respaldatoria</option>
                                <option value="No cuenta con la documentacion"
                                    {{ old('documentacion_respaldatoria') == 'No cuenta con la documentacion' ? 'selected' : '' }}>
                                    No cuenta con la documentación</option>
                                <option value="Documento de actas oficiales"
                                    {{ old('documentacion_respaldatoria') == 'Documento de actas oficiales' ? 'selected' : '' }}>
                                    Documento de actas oficiales</option>
                                <option value="Documentacion medica"
                                    {{ old('documentacion_respaldatoria') == 'Documentacion medica' ? 'selected' : '' }}>
                                    Documentación medica</option>
                                <option value="Documentacion forense"
                                    {{ old('documentacion_respaldatoria') == 'Documentacion forense' ? 'selected' : '' }}>
                                    Documentación forense</option>
                                <option value="Material audiovisual"
                                    {{ old('documentacion_respaldatoria') == 'Material audiovisual' ? 'selected' : '' }}>
                                    Material audiovisual</option>
                                <option value="Articulos periodicos"
                                    {{ old('documentacion_respaldatoria') == 'Articulos periodicos' ? 'selected' : '' }}>
                                    Artículos periódicos</option>
                                <option value="Cuenta con otra documentacion"
                                    {{ old('documentacion_respaldatoria') == 'Cuenta con otra documentacion' ? 'selected' : '' }}>
                                    Cuenta con otra documentación</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-8 mx-auto">
            <div class="box_general min">
                <h2 class="texto-form">Adjuntar documentos</h2>
                <center><span class="subtitulo">(Requerimos Imagenes de al menos un documento o en formato pdf)</span>
                </center>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3 col-lg-6 col-md-6 mx-auto">
            <div class="box_general">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <p>Dui</p>
                        </center>
                    </div>
                    <!--=======File de Dui==========-->
                    <div class="col-md-6 bottoms">
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_dui_frente">
                    </div>
                    <div class="col-md-6 bottoms">
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_dui_atras">
                    </div>
                </div>
                <!--=======Documento Dui==========-->
                <div class="row">
                    <div class="col-md-12 bottoms">
                        <input type="file" class="dropify15" accept="application/pdf" name="documento_dui">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 mx-auto">
            <div class="box_general">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <p>Nit</p>
                        </center>
                    </div>
                    <!--=======File de Nit==========-->
                    <div class="col-md-6 bottoms">
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_nit_frente">
                    </div>
                    <div class="col-md-6 bottoms">
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_nit_atras">
                    </div>
                </div>
                <!--=======Documento Nit==========-->
                <div class="row">
                    <div class="col-md-12 bottoms">
                        <input type="file" class="dropify15" accept="application/pdf" name="documento_nit">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 mx-auto">
            <div class="box_general">
                <div class="row">
                    <!--=======File de acta==========-->
                    <div class="col-md-6 bottoms">
                        <center>
                            <p class="acta">Acta</p>
                        </center>
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_acta">
                    </div>
                    <!--=======Documento acta mobil==========-->
                    <div class="col-md-6 bottoms d-block d-sm-block d-md-none">
                        <input type="file" class="dropify15" accept="application/pdf" name="documento_acta">
                    </div>
                    <!--=======File de Denuncia==========-->
                    <div class="col-md-6 bottoms">
                        <center>
                            <p class="denuncia">Denuncia</p>
                        </center>
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_denuncia">
                    </div>
                    <!--=======Documento acta pc==========-->
                    <div class="col-md-6 bottoms d-none d-sm-none d-md-block">
                        <input type="file" class="dropify15" accept="application/pdf" name="documento_acta">
                    </div>
                    <!--=======Documento denuncia==========-->
                    <div class="col-md-6 bottoms">
                        <input type="file" class="dropify15" accept="application/pdf" name="documento_denuncia">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 mx-auto">
            <div class="box_general">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <p class="documento4">Otra Documentacion</p>
                        </center>
                    </div>
                    <!--=======File de otra documentacion==========-->
                    <div class="col-md-6 bottoms">
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_otro">
                    </div>
                    <div class="col-md-6 bottoms">
                        <input type="file" class="dropify14" accept="image/x-png,image/jpeg" name="foto_otro2">
                    </div>
                </div>
                <!--=======Documento otro==========-->
                <div class="row">
                    <div class="col-md-12 mx-auto bottoms">
                        <input type="file" class="dropify15" accept="application/pdf" name="documento_otro">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras" type="button" class="float-left atras">Atras</button>
            <button type="submit" id="enviar" class="float-right siguientes">Enviar</button>
        </div>
    </div>
</div>