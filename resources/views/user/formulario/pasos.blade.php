<div id="paso">
    <!--=====Pasos Denuncias======-->
    <div class="container">
        <div class="row">
            <div class="form_title">
                <h3><strong>1</strong> Opciones de denuncias</h3>
            </div>
        </div>
    </div>
    <!--=========Seccion para ver paso 1===========-->
    <section id="pasos">
        <div class="row">
            <!--=====primer cuadro======-->
            <div class="col-xs-12 col-md-6 col-lg-4">
                <center>
                    <img src="{{asset('frontend/img/dot.png')}}">
                </center>
                <center>
                    <p>Personalmente Usted puede dirigirse a la ODAC/PDDH más cercana al lugar donde le ocurrió el
                        suceso.
                    </p>
                </center>
            </div>
            <!--=====segundo cuadro======-->
            <div class="col-xs-12 col-md-6 col-lg-4">
                <center>
                    <img src="{{asset('frontend/img/dot.png')}}">
                </center>
                <center>
                    <p>Telefónicamente Puede llamar al teléfono <a href="tel://911">911</a>, donde le comunicaran
                        con los profesionales que
                        le atenderán.
                    </p>
                </center>
            </div>
            <!--=====tercer cuadro======-->
            <div class="col-xs-12 col-md-6 col-lg-4 mx-auto">
                <center>
                    <img src="{{asset('frontend/img/dot.png')}}">
                </center>
                <center>
                    <p>Internet Ingresando a la página: <a href="https://www.fiscalia.gob.sv/"
                            target="_blank">www.fiscalia.gob.sv</a> para
                        llenar el formulario para hacer la denuncia.</p>
                </center>
            </div>
        </div>
    </section>
    <hr>
    <!--=====paso 2 una denuncia======-->
    <div class="container">
        <div class="row">
            <div class="form_title">
                <h3><strong>2</strong> Datos para la denuncia</h3>
            </div>
        </div>
    </div>
    <!--=========Seccion para ver paso 2===========-->
    <section id="pasos">
        <div class="row">
            <!--=====primer cuadro======-->
            <div class="col-xs-12 col-md-6 col-lg-4">
                <center>
                    <img src="{{asset('frontend/img/dot.png')}}">
                </center>
                <center>
                    <p>Que solicita el denunciante.
                    </p>
                </center>
            </div>
            <!--=====segundo cuadro======-->
            <div class="col-xs-12 col-md-6 col-lg-4">
                <center>
                    <img src="{{asset('frontend/img/dot.png')}}">
                </center>
                <center>
                    <p>Identificación y datos generales del agresor como: (aspecto físico).
                    </p>
                </center>
            </div>
            <!--=====tercer cuadro======-->
            <div class="col-xs-12 col-md-6 col-lg-4">
                <center>
                    <img src="{{asset('frontend/img/dot.png')}}">
                </center>
                <center>
                    <p>Descripción de los hechos que originaron la denuncia.</p>
                </center>
            </div>
            <!--=====cuarto cuadro======-->
            <div class="col-xs-12 col-md-6 col-lg-8 mx-auto">
                <center>
                    <img src="{{asset('frontend/img/dot2.png')}}" class="separador">
                </center>
                <center>
                    <p>Identificación y datos generales del denunciante; Nombre, Dirección, Identificación personal,
                        ocupación, profesión, edad y N° de teléfono. </p>
                </center>
            </div>
        </div>
    </section>
    <hr>
    <!--=====paso 3 una denuncia======-->
    <div class="container">
        <div class="row">
            <div class="form_title">
                <h3><strong>3</strong> Denuncia en nuestra plataforma</h3>
            </div>
        </div>
    </div>
    <section id="pasos">
        <div class="row">
            <div class="col-md-12 contenido">
                <center>
                    <p>Si Necesitas ayuda para realizar una denuncia en nuestra plataforma puedes comunicarte con
                        nosotros
                        al email <a href="mailto:aspidh.arcoiris.trans@gmail.com">aspidh.arcoiris.trans@gmail.com</a>
                        o puedes llamarnos al siguiente número de teléfono <a href="tel://22005469">2200-5469</a>.</p>
                </center>
            </div>
        </div>
    </section>
    <!--=====Button siguiente======-->
    <section id="pasos">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-5 col-xl-4 mx-auto">
                <!--======Button denuncia en aspidh=========-->
                <a class="denuncias" id="vistadenuncias">REGISTRAR DENUNCIA EN
                    ASPIDH</a>
            </div>
        </div>
    </section>
</div>