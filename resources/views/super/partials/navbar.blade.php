<!--============Logo o Nombre==============-->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="{{route('super.dashboard')}}">ASPIDH</a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">
        <!--============Boton de nueva denuncia==============-->
        <ul class="navbar-nav">
            <li class="nav-item dropdown d-lg-flex">
                <a class="nav-link dropdown-toggle nav-btn" href="#" data-toggle="modal" data-target="#exampleModal">
                    <span class="btn">+ Nueva Denuncia</span>
                </a>
            </li>
        </ul>
        <!--=====boton menu mobile=======-->
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="icon-menu"></span>
        </button>
    </div>
</nav>