<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--Informacion encabezado-->
    <link rel="icon" type="image/ico" href="{{asset('admin/img/icono.ico')}}">
    <title>@yield('title')</title>
    <!--icons-->
    <link href="{{asset('admin/css/simple-line-icons/css/simple-line-icons.css')}}" type="text/css" rel="stylesheet">
    <!--toastr-->
    <link href="{{asset('admin/css/toastr.min.css')}}" type="text/css" rel="stylesheet">
    <!--datatable-->
    <link href="{{asset('admin/css/datatable/dataTables.bootstrap4.css')}}" type="text/css" rel="stylesheet">
    <!--Select2-->
    <link href="{{asset('admin/css/select2.min.css')}}" type="text/css" rel="stylesheet">
    <!--dropify-->
    <link href="{{asset('admin/css/dropify.min.css')}}" type="text/css" rel="stylesheet">
    <!--diseno-->
    <link href="{{asset('admin/css/style.css')}}" type="text/css" rel="stylesheet">

</head>

<body>

    <!--Carga de pagina-->
    <div id="preloader">
        <div class="loader" id="loader-1"></div>
    </div>

    <!--Plantilla-->
    <div class="container-scroller">
        @include('super.partials.navbar')
        <div class="container-fluid page-body-wrapper">
            <div class="row row-offcanvas row-offcanvas-right">
                @include('super.partials.sidebar')
                @yield('content')
                @include('super.partials.footer')
            </div>
        </div>
    </div>

    <!--jquery -->
    <script src="{{asset('admin/js/jquery.min.js')}}"></script>
    <!--toastr-->
    <script src="{{asset('admin/js/toastr.min.js')}}"></script>
    {!! Toastr::message() !!}
    <!--bundle-->
    <script src="{{asset('admin/js/vendor.bundle.base.js')}}"></script>
    <!--navbar mobile-->
    <script src="{{asset('admin/js/off-canvas.js')}}"></script>
    <!--menu collapse-->
    <script src="{{asset('admin/js/misc.js')}}"></script>
    <!--Script para select2-->
    <script src="{{asset('admin/js/select2.full.min.js')}}"></script>
    <!--Graficas-->
    <script src="{{asset('admin/js/highcharts.js')}}"></script>
    <script src="{{asset('admin/js/exporting.js')}}"></script>
    <script src="{{asset('admin/js/export-data.js')}}"></script>
    <script src="{{asset('admin/js/offline-exporting.js')}}"></script>
    <!--Script sweetalert-->
    <script src="{{asset('admin/js/sweetalert2@9.js')}}"></script>
    <!--Scrit para datatable-->
    <script src="{{asset('admin/js/datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/js/datatable/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('admin/js/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('admin/js/datatable/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/js/datatable/jszip.min.js')}}"></script>
    <script src="{{asset('admin/js/datatable/buttons.html5.min.js')}}"></script>
    <!--dropify-->
    <script src="{{asset('admin/js/dropify.min.js')}}"></script>
    <!--Scrit para mantenimientos-->
    <script src="{{asset('admin/js/mantenimientos.js')}}"></script>
    <!--Script Funcionalidades-->
    <script src="{{asset('admin/js/script.js')}}"></script>
    <!--Script Formulario Denuncias-->
    <script src="{{asset('admin/js/formulario_denuncia.js')}}"></script>
    <!--Script Formulario registro-->
    <script src="{{asset('admin/js/formulario_registro.js')}}"></script>
    <!--Script para validar input-->
    <script src="{{asset('admin/js/jquery.mask.js')}}"></script>
    <!--Modal de Denuncias-->
    @include('super.denuncias.denuncia.modal')
    <!--=======Errores de formulario==========-->
    @if ($errors->all())
    <script src="{{asset('admin/js/error.js')}}"></script>
    @endif

</body>

</html>