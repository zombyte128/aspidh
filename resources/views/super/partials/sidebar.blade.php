<!---=============Opciones de menu lateral==============-->
<div id="right-sidebar" class="settings-panel">
    <div class="tab-content" id="setting-content">
        <div class="tab-pane fade show active scroll-wrapper" role="tabpanel" aria-labelledby="todo-section">
        </div>
    </div>
</div>
<!---=============Menu==============-->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <!--========img=========-->
                <div class="profile-image">
                    <img src="{{asset('admin/img/logo.png')}}">
                    <span class="online-status online"></span>
                </div>
            </div>
            <hr>
        </li>
        <!---===dashboard======-->
        <li class="nav-item">
            <a class="nav-link" href="{{route('super.dashboard')}}">
                <i class="icon-rocket menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <!---===Administradores======-->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('super.administradores.index') }}">
                <i class="icon-people menu-icon"></i>
                <span class="menu-title">Administradores</span>
            </a>
        </li>
        <!---===Donantes======-->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('super.donantes.index') }}">
                <i class="icon-heart menu-icon"></i>
                <span class="menu-title">Donantes</span>
            </a>
        </li>
        <!---===Personas registradas======-->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('super.personas.index') }}">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">Personas Registradas</span>
            </a>
        </li>
        <!---===Denuncias======-->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false"
                aria-controls="page-layouts">
                <i class="icon-docs menu-icon"></i>
                <span class="menu-title">Denuncias</span>
                <span class="badge badge-danger" title="Denuncias Pendientes">{{ $total_denuncias_pendientes }}</span>
            </a>
            <div class="collapse" id="page-layouts">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link"
                            href="{{ route('super.denuncias-pendientes.index') }}">Denuncias Pendientes</a></li>
                    <li class="nav-item"><a class="nav-link"
                            href="{{ route('super.denuncias-seguimiento.index') }}">Denuncias Seguimiento</a></li>
                    <li class="nav-item"><a class="nav-link"
                            href="{{ route('super.denuncias-completadas.index') }}">Denuncias Completadas</a></li>
                    <li class="nav-item"><a class="nav-link"
                            href="{{ route('super.denuncias-finalizadas.index') }}">Denuncias Finalizadas</a></li>
                    <li class="nav-item"> <a class="nav-link"
                            href="{{ route('super.denuncias-baneadas.index') }}">Denuncias Baneadas</a></li>
                </ul>
            </div>
        </li>
        <!---===Graficas registro======-->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#sidebar-layouts" aria-expanded="false"
                aria-controls="sidebar-layouts">
                <i class="icon-chart menu-icon"></i>
                <span class="menu-title">Graficas Registro</span>
            </a>
            <div class="collapse" id="sidebar-layouts">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{route('super.datos_generales')}}">Datos
                            generales</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('super.educacion')}}">Educación</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('super.acesso_salud')}}">Acesso a la
                            salud</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('super.trabajo_ocupacion')}}">Trabajo u
                            ocupación</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('super.acceso_vivienda')}}">Acceso a la
                            vivienda</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('super.politica_religion')}}">política y
                            religión</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('super.derechos')}}">Vulneración de
                            derechos</a></li>
                </ul>
            </div>
        </li>
        <!---===Graficas personas transgenero======-->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#sidebar-layoutss" aria-expanded="false"
                aria-controls="sidebar-layoutss">
                <i class="icon-chart menu-icon"></i>
                <span class="menu-title">Graficas mujer transexual</span>
            </a>
            <div class="collapse" id="sidebar-layoutss">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{route('super.datos_generales_trans')}}">Datos
                            generales</a></li>
                    <li class="nav-item"><a class="nav-link"
                            href="{{ route('super.educacion_personas_trans') }}">Educación</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('super.acceso_salud_trans') }}">Acesso a la
                            salud</a></li>
                    <li class="nav-item"> <a class="nav-link"
                            href="{{ route('super.trabajo_ocupacion_trans') }}">Trabajo u
                            ocupación</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('super.acceso_vivienda_trans') }}">Acceso a
                            la
                            vivienda</a></li>
                    <li class="nav-item"> <a class="nav-link"
                            href="{{ route('super.politica_religion_trans') }}">política y
                            religión</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('super.derechos_trans') }}">Vulneración de
                            derechos</a></li>
                </ul>
            </div>
        </li>
        <hr>
        <!---===Cerrar Sesion======-->
        <li class="nav-item nav-doc">
            <a class="nav-link bg-primary" href="{{ route('logout')  }}"
                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="icon-power menu-icon"></i>
                <span class="menu-title">Cerrar sesión</span>
            </a>
            <!--=====form sesion=======-->
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>