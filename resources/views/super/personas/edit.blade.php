@extends('super.partials.app')
@section('title','ASPIDH - Editar Usuario')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('super.personas.index')}}">Usuarios</a></li>
        </ol>
    </nav>
    <!--========Formulario=========-->
    <form class="form-horizontal" action="{{ route('super.personas.update', $personas->id) }}" method="POST"
        enctype="multipart/form-data" id="formulario">
        @csrf
        @method('PUT')
        <!--=======DATOS GENERALES=============-->
        @include('super.personas.formulario_edit.generales')
        <!--=======EDUCACION=============-->
        @include('super.personas.formulario_edit.educacion')
        <!--=======ACCESO A LA SALUD=============-->
        @include('super.personas.formulario_edit.salud')
        <!--=======TRABAJO Y EMPLEO=============-->
        @include('super.personas.formulario_edit.trabajo')
        <!--=======ACCESO A LA VIVIENDA=============-->
        @include('super.personas.formulario_edit.vivienda')
        <!--=======POLITICA Y RELIGION=============-->
        @include('super.personas.formulario_edit.politica_religion')
        <!--=======VULNERACIÓN DE DERECHOS=============-->
        @include('super.personas.formulario_edit.derechos')
    </form>
</div>
@endsection