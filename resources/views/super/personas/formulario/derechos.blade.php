<!--=======Derechos=============-->
<div id="derechos" style="display:none" class="wow fadeInUp" data-wow-duration="1.5s">
    <div class="row">
        <div class="col-md-6 mx-auto stretch-card">
            <div class=" card">
                <div class="card-body">
                    <h2 class="texto-form">VULNERACIÓN DE DERECHOS</h2>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar7"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======Radio de centro de estudios==========-->
                        <div class="col-md-12">
                            <center>
                                <p>¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en su Centro de
                                    Estudio?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="discriminacion_agrecion_centro_estudio" value="Si"
                                    id="sufridosi" autocomplete="off"
                                    {{ old('discriminacion_agrecion_centro_estudio') == 'Si' ? 'checked' : '' }}>
                                <label for="sufridosi" class="tamano">Si</label>
                                <input type="radio" name="discriminacion_agrecion_centro_estudio" value="No"
                                    id="sufridono" autocomplete="off"
                                    {{ old('discriminacion_agrecion_centro_estudio') == 'No' ? 'checked' : '' }}>
                                <label for="sufridono" class="tamano">No</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======Radio de descriminacion trabajo==========-->
                        <div class="col-md-12">
                            <center>
                                <p>¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en su
                                    Trabajo?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="discriminacion_agrecion_trabajo" value="Si"
                                    id="descriminacionsi" autocomplete="off"
                                    {{ old('discriminacion_agrecion_trabajo') == 'Si' ? 'checked' : '' }}>
                                <label for="descriminacionsi" class="tamano">Si</label>
                                <input type="radio" name="discriminacion_agrecion_trabajo" value="No"
                                    id="descriminacionno" autocomplete="off"
                                    {{ old('discriminacion_agrecion_trabajo') == 'No' ? 'checked' : '' }}>
                                <label for="descriminacionno" class="tamano">No</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row derechoss">
                        <!--=======Radio de descriminacion defensa de sus derechos==========-->
                        <div class="col-md-12">
                            <center>
                                <p>¿Conoce los mecanismos de defensa de sus derechos?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="conocimientos_mecanimos_defensa_de_derechos" value="Si"
                                    id="mecanismossi" autocomplete="off"
                                    {{ old('conocimientos_mecanimos_defensa_de_derechos') == 'Si' ? 'checked' : '' }}>
                                <label for="mecanismossi" class="tamano">Si</label>
                                <input type="radio" name="conocimientos_mecanimos_defensa_de_derechos" value="No"
                                    id="mecanismosno" autocomplete="off"
                                    {{ old('conocimientos_mecanimos_defensa_de_derechos') == 'No' ? 'checked' : '' }}>
                                <label for="mecanismosno" class="tamano">No</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======Radio de descriminacion salud==========-->
                        <div class="col-md-12 mx-auto">
                            <center>
                                <p>¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en los centros
                                    de salud?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="discriminacion_agrecion_centro_salud" value="Si"
                                    id="descriminacionsaludsi" autocomplete="off"
                                    {{ old('discriminacion_agrecion_centro_salud') == 'Si' ? 'checked' : '' }}>
                                <label for="descriminacionsaludsi" class="tamano">Si</label>
                                <input type="radio" name="discriminacion_agrecion_centro_salud" value="No"
                                    id="descriminacionsaludno" autocomplete="off"
                                    {{ old('discriminacion_agrecion_centro_salud') == 'No' ? 'checked' : '' }}>
                                <label for="descriminacionsaludno" class="tamano">No</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======select Por quien ha recibido discriminacion ==========-->
                        <div class="col-md-7 mx-auto">
                            <center>
                                <p>¿Por quién ha recibido algún hecho de agresión y/o discriminación?</p>
                            </center>
                            <div class="form-group">
                                <select class="form-control select2" name="por_quien_hecho_de_agresion_discriminacion"
                                    id="porquien" required>
                                    <option value="">Seleccione Opcion</option>
                                    <option value="Familiares"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Familiares' ? 'selected' : '' }}>
                                        Familiares</option>
                                    <option value="Amigos"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Amigos' ? 'selected' : '' }}>
                                        Amigos</option>
                                    <option value="Pareja"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Pareja' ? 'selected' : '' }}>
                                        Pareja</option>
                                    <option value="Iglesia"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Iglesia' ? 'selected' : '' }}>
                                        Iglesia</option>
                                    <option value="Laboral"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Laboral' ? 'selected' : '' }}>
                                        Laboral</option>
                                    <option value="PNC"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'PNC' ? 'selected' : '' }}>
                                        PNC</option>
                                    <option value="Militares"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Militares' ? 'selected' : '' }}>
                                        Militares</option>
                                    <option value="Medicos"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Medicos' ? 'selected' : '' }}>
                                        Médicos</option>
                                    <option value="Otros"
                                        {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Otros' ? 'selected' : '' }}>
                                        Otros</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5" style="display:none" id="otroporquien2">
                            <textarea name="otro_por_quien_discrimanacion" id="otroporquien" placeholder="¿Quienes?"
                                rows="2" style="text-align:center;padding-top: 30px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row derechos2">
                        <!--=======select agresion con mayor frecuencia ==========-->
                        <div class="col-md-7 mx-auto">
                            <center>
                                <p>¿Qué tipo de agresión ha recibido con mayor frecuencia?
                                </p>
                            </center>
                            <div class="form-group magenes">
                                <select class="form-control select2" name="tipo_agresion_frecuente" id="tipoagresion">
                                    <option value="">Seleccione Opcion</option>
                                    <option value="Fisica"
                                        {{ old('tipo_agresion_frecuente') == 'Fisica' ? 'selected' : '' }}>
                                        Física</option>
                                    <option value="Psicologica"
                                        {{ old('tipo_agresion_frecuente') == 'Psicologica' ? 'selected' : '' }}>
                                        Psicológica
                                    </option>
                                    <option value="Economica"
                                        {{ old('tipo_agresion_frecuente') == 'Economica' ? 'selected' : '' }}>Económica
                                    </option>
                                    <option value="Otros"
                                        {{ old('tipo_agresion_frecuente') == 'Otros' ? 'selected' : '' }}>Otros
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5" style="display:none" id="otrotipoagresion2">
                            <textarea name="otro_tipo_agresion" id="otrotipoagresion" placeholder="¿Quienes?" rows="2"
                                style="text-align:center;padding-top: 30px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras6" type="button" class="btn btn-primary btn-lg float-left botones">Atras</button>
            <button type="button" id="siguiente7"
                class="btn btn-primary btn-lg float-right btnsubmit botonsubmitt">Enviar</button>
        </div>
    </div>
</div>