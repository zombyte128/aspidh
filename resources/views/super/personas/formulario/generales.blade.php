<!--=======DATOS GENERALES=============-->
<div id="datosgenerales" class="wow fadeInUp" data-wow-duration="1.5s">
    <div class="row">
        <div class="col-md-4 mx-auto stretch-card">
            <div class="card">
                <div class="card-body">
                    <h2 class="texto-form">DATOS GENERALES</h2>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--====input nombre======-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Nombres Documento Unico
                                    </div>
                                </center>
                                <input type="text" class="form-control" id="general1"
                                    style="text-transform: capitalize; text-align:center"
                                    placeholder="Nombres Documento Unico" name="nombres_segun_dui" autocomplete="off"
                                    value="{{ old('nombres_segun_dui') }}">
                            </div>
                        </div>
                        <!--====input apellido======-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Apellidos Documento Unico
                                    </div>
                                </center>
                                <input type="text" style="text-transform:capitalize;text-align:center"
                                    class="form-control" id="general2" placeholder="Apellidos Documento Unico"
                                    name="apellidos_segun_dui" autocomplete="off"
                                    value="{{ old('apellidos_segun_dui') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--====input edad======-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Edad
                                    </div>
                                </center>
                                <input type="number" class="form-control" placeholder="Edad" name="edad"
                                    autocomplete="off" value="{{ old('edad') }}" id="general3"
                                    style="text-align:center;text-transform: capitalize;">
                            </div>
                        </div>
                        <!--=======Input Fecha nacimiento==========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Fecha Nacimiento
                                    </div>
                                </center>
                                <input type="tel" class="form-control @error('fecha_nacimiento') is-invalid @enderror"
                                    placeholder="Fecha Nacimiento | 00-00-0000" id="general4" name="fecha_nacimiento"
                                    autocomplete="off" value="{{ old('fecha_nacimiento') }}" style="text-align:center;"
                                    data-mask="00-00-0000">
                                @if ($errors->has('fecha_nacimiento'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$errors->first('fecha_nacimiento')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--=======select identidad genero==========-->
                        <div class="col-md-12">
                            <div class="form-group magenes">
                                <center>
                                    <div class="badge badge-outline-primary menos">Identidad De Género
                                    </div>
                                </center>
                                <select class="form-control select2" name="tipo_genero_id" id="tipogenero">
                                    <option value="">Identidad De Género</option>
                                    @foreach( $genero as $g)
                                    <option value="{{ $g->id }}"
                                        {{ old('tipo_genero_id') == $g->id ? "selected" : "" }}>
                                        {{ $g->nombre_tipo_genero }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--====input nombre social======-->
                        <div class="col-md-12">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Nombre por cual te identificas
                                    </div>
                                </center>
                                <input type="text" class="form-control"
                                    style="text-transform: capitalize; text-align:center"
                                    placeholder="Nombre por cual te identificas" name="nombre_social" autocomplete="off"
                                    value="{{ old('nombre_social') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--====input telefono======-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Telefono
                                    </div>
                                </center>
                                <input type="tel" class="form-control" placeholder="Telefono" name="numero_telefono"
                                    autocomplete="off" value="{{ old('numero_telefono') }}" style="text-align:center;"
                                    data-mask="0000-0000" id="general5">
                            </div>
                        </div>
                        <!--=======Input email==========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Correo Electronico
                                    </div>
                                </center>
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    placeholder="Correo Electronico" name="email" autocomplete="off"
                                    value="{{ old('email') }}" style="text-align:center;" id="emails">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$errors->first('email')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--=======select orientacion sexual==========-->
                        <div class="col-md-12">
                            <div class="form-group magenes">
                                <center>
                                    <div class="badge badge-outline-primary menos">Orientación Sexual
                                    </div>
                                </center>
                                <select class="form-control select2" id="orientacion" name="orientacion_sexual_id">
                                    <option value="">Orientación Sexual</option>
                                    @foreach( $orientacion as $o)
                                    <option value="{{ $o->id }}"
                                        {{ old('orientacion_sexual_id') == $o->id ? "selected" : "" }}>
                                        {{ $o->nombre_orientacion_sexual }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <center>
                        <p class="documento">Residencia</p>
                    </center>
                    <!--=======select departamento==========-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Departamento
                                    </div>
                                </center>
                                <select class="form-control select2 departamento_registro" id="departamento"
                                    name="departamento_id">
                                    <option value="">Seleccione Departamento</option>
                                    @foreach( $departamentos as $d)
                                    <option value="{{ $d->id }}">{{ $d->nombre_departamento }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!--=======select ciudad==========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Ciudad
                                    </div>
                                </center>
                                <select class="form-control select2 ciudad_registro" name="ciudad_id" id="ciudad_id"
                                    disabled>
                                    <option value="">Seleccione Ciudad</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mx-auto">
                            <div class="form-group magenes">
                                <center>
                                    <div class="badge badge-outline-primary menos">Ingrese su direccion actual
                                    </div>
                                </center>
                                <textarea placeholder="Ingrese su direccion actual" name="direccion_completa"
                                    class="form-control" style="text-align:center;"
                                    id="general6">{{ old('direccion_completa') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <center>
                        <p class="documento">Contacto de emergencia</p>
                    </center>
                    <!--=======input nombre completo emergencia==========-->
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Nombre Completo
                                    </div>
                                </center>
                                <input type="text" class="form-control"
                                    style="text-transform: capitalize; text-align:center" placeholder="Nombre Completo"
                                    name="nombres_emergencia" autocomplete="off" value="{{ old('nombres_emergencia') }}"
                                    id="general7">
                            </div>
                        </div>
                        <!--=======input telefono emeregencia==========-->
                        <div class="col-md-5">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Telefono
                                    </div>
                                </center>
                                <input type="tel" class="form-control" placeholder="Telefono" name="telefono_emergencia"
                                    autocomplete="off" value="{{ old('telefono_emergencia') }}"
                                    style="text-align:center;" data-mask="0000-0000" id="general8">
                            </div>
                        </div>
                    </div>
                    <!--=======input direccion emergencia==========-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group magenes2">
                                <center>
                                    <div class="badge badge-outline-primary menos">Direccion Completa
                                    </div>
                                </center>
                                <textarea placeholder="Direccion Completa" name="direccion_emergencia"
                                    class="form-control" style="text-align:center;"
                                    id="direccionemergencia">{{ old('direccion_emergencia') }}</textarea>
                            </div>
                        </div>
                        <!--=======input redes sociales emeregencia==========-->
                        <div class="col-md-6">
                            <div class="form-group magenes">
                                <center>
                                    <div class="badge badge-outline-primary menos">Indicar el parentesco o cercanía
                                    </div>
                                </center>
                                <textarea placeholder="Indicar el parentesco o cercanía"
                                    name="redes_sociales_emergencia" class="form-control" style="text-align:center;"
                                    value="{{ old('redes_sociales_emergencia') }}"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <center>
                        <p class="documento margenes">Documento identificacion</p>
                        <span class="subtitulo">(Seleccione una opcion)</span>
                    </center>
                    <div class="row">
                        <!--=======Radio de documento de identificacion==========-->
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="radio" name="documento" id="dui" autocomplete="off">
                                <label for="dui">DUI</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="radio" name="documento" id="nit" autocomplete="off">
                                <label for="nit">NIT</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="radio" name="documento" id="pasaporte" autocomplete="off">
                                <label for="pasaporte">Pasaporte</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="radio" name="documento" value="ninguno" id="ninguno" autocomplete="off">
                                <label for="ninguno">Ninguno</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--====input dui======-->
                        <div class="col-md-6 mx-auto" id="1" style="display: none;">
                            <div class="form-group magenes">
                                <input type="tel" style="text-align:center;text-transform: capitalize;"
                                    class="form-control" placeholder="00000000-0" autocomplete="off"
                                    value="{{ old('documento_identificacion') }}" data-mask="00000000-0" id="limpiar1">
                            </div>
                        </div>
                        <!--====input Nit======-->
                        <div class="col-md-6 mx-auto" id="2" style="display: none;">
                            <div class="form-group magenes">
                                <input type="tel" style="text-align:center;text-transform: capitalize;"
                                    class="form-control" placeholder="0000-000000-000-0" autocomplete="off"
                                    value="{{ old('documento_identificacion') }}" data-mask="0000-000000-000-0"
                                    id="limpiar2">
                            </div>
                        </div>
                        <!--====input Pasaporte======-->
                        <div class="col-md-6 mx-auto" id="3" style="display: none;">
                            <div class="form-group magenes">
                                <input type="text" style="text-align:center;text-transform: capitalize;"
                                    class="form-control" placeholder="000000000" autocomplete="off"
                                    value="{{ old('documento_identificacion') }}" id="limpiar3" maxlength="9">
                            </div>
                        </div>
                        <!--====input Ninguno======-->
                        <div class="col-md-6 mx-auto" id="4" style="display: none;">
                            <div class="form-group magenes">
                                <textarea placeholder="¿Cual es la razon?" class="form-control"
                                    style="text-align:center;" id="limpiar4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6 paddingregistro">
            <div class="card padding2">
                <div class="card-body">
                    <center>
                        <p class="documento">Redes sociales</p>
                    </center>
                    <!--====input Link facebook======-->
                    <div class="row general">
                        <div class="col-md-6 mx-auto">
                            <center>
                                <div class="badge badge-outline-primary menos">Usuario de facebook
                                </div>
                            </center>
                            <div class="form-group">
                                <input type="text" style="text-align:center;text-transform: capitalize;"
                                    class="form-control" placeholder="Usuario de facebook" name="redes_social_facebook"
                                    autocomplete="off" value="{{ old('redes_social_facebook') }}">
                            </div>
                        </div>
                        <!--====input Link de twitter======-->
                        <div class="col-md-6 mx-auto">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Usuario de twitter
                                    </div>
                                </center>
                                <input type="text" style="text-align:center;text-transform: capitalize;"
                                    class="form-control" placeholder="Usuario de twitter" name="redes_social_twitter"
                                    autocomplete="off" value="{{ old('redes_social_twitter') }}">
                            </div>
                        </div>
                        <!--====input Link de Instagram======-->
                        <div class="col-md-6 mx-auto">
                            <div class="form-group magenes">
                                <center>
                                    <div class="badge badge-outline-primary menos">Usuario de Instagram
                                    </div>
                                </center>
                                <input type="text" style="text-align:center;text-transform: capitalize;"
                                    class="form-control" placeholder="Usuario de Instagram"
                                    name="redes_social_instagram" autocomplete="off"
                                    value="{{ old('redes_social_instagram') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <center>
                        <p class="documento margenes">Registra tu usuario aspidh</p>
                        <span class="subtitulo">(Para realizar una denuncia debe acordarse de los siguientes
                            datos)</span>
                    </center>
                    <!--====input Username======-->
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Usuario
                                    </div>
                                </center>
                                <input type="text" style="text-align:center;"
                                    class="form-control @error('username') is-invalid @enderror" placeholder="Usuario"
                                    name="username" autocomplete="off" id="username">
                                @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$errors->first('username')}}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!--=======Input contrasena==========-->
                        <div class="col-lg-10 mx-auto">
                            <div class="form-group magenes">
                                <div class="form-group">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Password
                                        </div>
                                    </center>
                                    <input id="txtPassword" type="Password" name="password"
                                        Class="form-control @error('password') is-invalid @enderror"
                                        placeholder="Password" style="text-align:center;">
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$errors->first('password')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <button type="button" id="siguiente1" class="btn btn-primary btn-lg btn-block">Siguiente</button>
</div>