<!--=======Salud=============-->
<div id="salud" style="display:none" class="wow fadeInUp" data-wow-duration="1.5s">
    <div class="row">
        <div class="col-md-4 mx-auto stretch-card">
            <div class="card">
                <div class="card-body">
                    <h2 class="texto-form">ACCESO A LA SALUD</h2>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======radio Afiliado==========-->
                        <div class="col-md-3">
                            <center>
                                <p>¿Es afiliado del Seguro Social?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="afiliado_seguro_social" value="Si" id="segurosi"
                                    autocomplete="off" {{ $personas->afiliado_seguro_social == 'Si' ? 'checked' : '' }}>
                                <label for="segurosi" class="tamano">Si</label>
                                <input type="radio" name="afiliado_seguro_social" value="No" id="segurono"
                                    autocomplete="off" {{ $personas->afiliado_seguro_social == 'No' ? 'checked' : '' }}>
                                <label for="segurono" class="tamano">No</label>
                            </div>
                        </div>
                        <!--=======radio Servicio de salud==========-->
                        <div class="col-md-5">
                            <center>
                                <p>¿Utiliza o ha utilizado los servicios de los centros de salud?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="utiliza_servcios_de_salud" value="Si" id="segurosi2"
                                    autocomplete="off"
                                    {{ $personas->utiliza_servcios_de_salud == 'Si' ? 'checked' : '' }}>
                                <label for="segurosi2" class="tamano">Si</label>
                                <input type="radio" name="utiliza_servcios_de_salud" value="No" id="segurono2"
                                    autocomplete="off"
                                    {{ $personas->utiliza_servcios_de_salud == 'No' ? 'checked' : '' }}>
                                <label for="segurono2" class="tamano">No</label>
                            </div>
                        </div>
                        <!--=======select Centro medico ==========-->
                        <div class="col-md-4">
                            <center>
                                <p>¿A qué centro médico ha asistido o asiste?</p>
                            </center>
                            <div class="form-group magenes">
                                <select class="form-control select2" name="centro_medico_asiste" id="centromedico">
                                    <option value="">Centro Medico</option>
                                    <option value="Hospital Publico"
                                        {{ $personas->centro_medico_asiste == 'Hospital Publico' ? 'selected' : '' }}>
                                        Hospital
                                        Público</option>
                                    <option value="Hospital Privado"
                                        {{ $personas->centro_medico_asiste == 'Hospital Privado' ? 'selected' : '' }}>
                                        Hospital
                                        Privado</option>
                                    <option value="Clinica comunal"
                                        {{ $personas->centro_medico_asiste == 'Clinica comunal' ? 'selected' : '' }}>
                                        Clínica
                                        comunal</option>
                                    <option value="Unidad de Salud"
                                        {{ $personas->centro_medico_asiste == 'Unidad de Salud' ? 'selected' : '' }}>
                                        Unidad
                                        de
                                        Salud</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 mx-auto paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======radio beneficiado programa gubernamental acceos a la salud==========-->
                        <div class="col-md-12 mx-auto">
                            <center>
                                <p>¿Ha sido beneficiario/a de algún programa gubernamental de acceso a la
                                    salud?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="beneficiario_programa_salud_gubernamental" id="beneficiadosi"
                                    Value="Si" autocomplete="off"
                                    {{ $personas->beneficiario_programa_salud_gubernamental == 'Si'  ? 'checked' : '' }}>
                                <label for="beneficiadosi" class="tamano2">Si</label>
                                <input type="radio" name="beneficiario_programa_salud_gubernamental" Value="No"
                                    id="beneficiadono" autocomplete="off"
                                    {{ $personas->beneficiario_programa_salud_gubernamental == 'No'  ? 'checked' : '' }}>
                                <label for="beneficiadono" class="tamano2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--=======textarea razon del beneficio==========-->
                        <div class="col-md-10 mx-auto" id="raczonbenedicio" style="display: none;">
                            <div class="form-group">
                                <textarea id="limpieza6" placeholder="¿Ingrese nombre del programa?"
                                    name="cual_programa_salud" class="form-control"
                                    style="text-align:center;padding-top: 10px;">{{$personas->cual_programa_salud}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras2" type="button" class="btn btn-primary btn-lg float-left botones">Atras</button>
            <button id="siguiente3" type="button" class="btn btn-primary btn-lg float-right">Siguiente</button>
        </div>
    </div>
</div>