<!--=======Educacion=============-->
<div id="educacion" style="display:none" class="wow fadeInUp" data-wow-duration="1.5s">
    <div class="row">
        <div class="col-md-4 mx-auto stretch-card">
            <div class="card">
                <div class="card-body">
                    <h2 class="texto-form">EDUCACIÓN</h2>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 paddingregistro">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--=======select Grado de estudios aprobado==========-->
                        <div class="col-md-6">
                            <div class="form-group icono">
                                <center>
                                    <div class="badge badge-outline-primary menos">Estudios aprobado
                                    </div>
                                </center>
                                <select class="form-control select2 prueba" name="ultimo_grado_estudio_aprobado"
                                    id="ultimoestudio">
                                    <option value="">Estudios aprobado</option>
                                    <option value="1"
                                        {{ $personas->ultimo_grado_estudio_aprobado == '1' ? 'selected' : '' }}>1° a 3°
                                    </option>
                                    <option value="4"
                                        {{ $personas->ultimo_grado_estudio_aprobado == '4' ? 'selected' : '' }}>4° a 6°
                                    </option>
                                    <option value="7"
                                        {{ $personas->ultimo_grado_estudio_aprobado == '7' ? 'selected' : '' }}>7° a 9°
                                    </option>
                                    <option value="Bachillerato"
                                        {{ $personas->ultimo_grado_estudio_aprobado == 'Bachillerato' ? 'selected' : '' }}>
                                        Bachillerato</option>
                                    <option value="Universitario"
                                        {{ $personas->ultimo_grado_estudio_aprobado == 'Universitario' ? 'selected' : '' }}>
                                        Universitario</option>
                                    <option value="Maestria"
                                        {{ $personas->ultimo_grado_estudio_aprobado == 'Maestria' ? 'selected' : '' }}>
                                        Maestría</option>
                                    <option value="Doctorado"
                                        {{ $personas->ultimo_grado_estudio_aprobado == 'Doctorado' ? 'selected' : '' }}>
                                        Doctorado</option>
                                    <option value="Ninguno"
                                        {{ $personas->ultimo_grado_estudio_aprobado == 'Ninguno' ? 'selected' : '' }}>
                                        Ninguno</option>
                                </select>
                            </div>
                        </div>
                        <!--=======select Tipo de estudios primarios==========-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <center>
                                    <div class="badge badge-outline-primary menos">Tipo de estudios primarios
                                    </div>
                                </center>
                                <select class="form-control select2 prueba" name="lugar_estudio_primario"
                                    id="estudioprimario">
                                    <option value="">Tipo de estudios primarios</option>
                                    <option value="Publico"
                                        {{ $personas->lugar_estudio_primario == 'Publico' ? 'selected' : '' }}>Público
                                    </option>
                                    <option value="Privado"
                                        {{ $personas->lugar_estudio_primario == 'Privado' ? 'selected' : '' }}>Privado
                                    </option>
                                    <option value="distancia"
                                        {{ $personas->lugar_estudio_primario == 'distancia' ? 'selected' : '' }}>A
                                        distancia</option>
                                    <option value="Ninguno"
                                        {{ $personas->lugar_estudio_primario == 'Ninguno' ? 'selected' : '' }}>Ninguno
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--=======select Tipo de estudios universitarios ==========-->
                        <div class="col-md-7 mx-auto">
                            <div class="form-group magenes">
                                <center>
                                    <div class="badge badge-outline-primary menos">Tipo de estudios universitarios
                                    </div>
                                </center>
                                <select class="form-control select2" name="lugar_estudio_universitario"
                                    id="estudiouniversidad">
                                    <option value="">Tipo de estudios universitarios</option>
                                    <option value="Publico"
                                        {{ $personas->lugar_estudio_universitario == 'Publico' ? 'selected' : '' }}>
                                        Público</option>
                                    <option value="Privado"
                                        {{ $personas->lugar_estudio_universitario == 'Privado' ? 'selected' : '' }}>
                                        Privado</option>
                                    <option value="distancia"
                                        {{ $personas->lugar_estudio_universitario == 'distancia' ? 'selected' : '' }}>A
                                        distancia</option>
                                    <option value="Ninguno"
                                        {{ $personas->lugar_estudio_universitario == 'Ninguno' ? 'selected' : '' }}>
                                        Ninguno</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paddingregistro">
            <div class="card padding4">
                <div class="card-body">
                    <div class="row education">
                        <!--=======Radio de estudia actualmente==========-->
                        <div class="col-md-6 mx-auto icono">
                            <center>
                                <p>¿Estudia actualmente?</p>
                            </center>
                            <div class="form-group centrado magenes">
                                <input type="radio" name="estudia_actualmente" value="Si" id="estudiasi"
                                    autocomplete="off" {{ $personas->estudia_actualmente == 'Si'  ? 'checked' : '' }}>
                                <label for="estudiasi" class="tamano">Si</label>
                                <input type="radio" value="No" name="estudia_actualmente" id="estudiano"
                                    autocomplete="off" {{ $personas->estudia_actualmente == 'No'  ? 'checked' : '' }}>
                                <label for="estudiano" class="tamano">No</label>
                            </div>
                        </div>
                        <!--=======textarea razonestudios==========-->
                        <div class="col-md-6" id="razonestudios" style="display: none;">
                            <div class="form-group magenes">
                                <textarea id="limpieza5" placeholder="¿Cuál fue la razón?" name="razon_estudio"
                                    class="form-control" rows="4"
                                    style="text-align:center;">{{$personas->razon_estudio}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras1" type="button" class="btn btn-primary btn-lg float-left botones">Atras</button>
            <button id="siguiente2" type="button" class="btn btn-primary btn-lg float-right">Siguiente</button>
        </div>
    </div>
</div>