@extends('super.partials.app')
@section('title','ASPIDH - Crear Usuario')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('super.personas.index')}}">Usuarios</a></li>
        </ol>
    </nav>
    <!--========Formulario=========-->
    <form class="form-horizontal" action="{{ route('super.personas.store') }}" method="POST"
        enctype="multipart/form-data" id="formulario">
        @csrf
        <!--=======DATOS GENERALES=============-->
        @include('super.personas.formulario.generales')
        <!--=======EDUCACION=============-->
        @include('super.personas.formulario.educacion')
        <!--=======ACCESO A LA SALUD=============-->
        @include('super.personas.formulario.salud')
        <!--=======TRABAJO Y EMPLEO=============-->
        @include('super.personas.formulario.trabajo')
        <!--=======ACCESO A LA VIVIENDA=============-->
        @include('super.personas.formulario.vivienda')
        <!--=======POLITICA Y RELIGION=============-->
        @include('super.personas.formulario.politica_religion')
        <!--=======VULNERACIÓN DE DERECHOS=============-->
        @include('super.personas.formulario.derechos')
    </form>
</div>
@endsection