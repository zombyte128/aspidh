@extends('super.partials.app')
@section('title','ASPIDH - Vulneración de derechos')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Vulneración de derechos</li>
        </ol>
    </nav>
    <div class="row">
        <!--======discriminacion centro de estudio=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart30->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======discriminacion en el trabajo=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart31->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======discriminacion centro de salud=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart32->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======discriminacion de entidades=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart33->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======sufrido algun tipo de agresion=========-->
        <div class="col-md-6 mx-auto">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart34->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======mecanismos de defensa de sus derechos=========-->
        <div class="col-md-6 mx-auto">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart35->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart30->script() !!}
{!! $chart31->script() !!}
{!! $chart32->script() !!}
{!! $chart33->script() !!}
{!! $chart34->script() !!}
{!! $chart35->script() !!}
@endsection