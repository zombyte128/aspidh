@extends('super.partials.app')
@section('title','ASPIDH - Administradores')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Administradores</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========Tabla Administradores=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th>Rol</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Email</th>
                                    <th>contraseña</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $administradores as $key => $administrador)
                                <tr>
                                    <td>{{ $administrador->role->nombre_rol }}</td>
                                    <td>{{ $administrador->nombres_segun_dui }}</td>
                                    <td>{{ $administrador->apellidos_segun_dui }}</td>
                                    <td>{{ $administrador->email }}</td>
                                    <td>{{ $administrador->contrasena }}</td>
                                    <td>{{ date('d/m/Y',strtotime($administrador->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($administrador->updated_at)) }}</td>
                                    <td>
                                        <!--========Editar=========-->
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Editar" title="Editar" role="link"
                                            href="{{route('super.administradores.edit',$administrador->id)}}"><i
                                                class="icon-pencil"></i></a>
                                        <!--========Eliminar=========-->
                                        <a class="btn btn-danger" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Eliminar" title="Eliminar" href="javascript:void(0)"
                                            onclick="deleteadministrador({{$administrador->id}})"><i
                                                class="icon-trash"></i></a>
                                        <!--=====form delete=======-->
                                        <form id="administrador-delete-{{$administrador->id}}"
                                            action="{{route('super.administradores.destroy',$administrador->id)}}"
                                            method="POST" style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--========Boton nuevo administrador=========-->
        <a role="link" href="{{route('super.administradores.create')}}" class="btn btn-sm btn-primary">
            Nuevo Administrador</a>
    </div>
</div>
@endsection