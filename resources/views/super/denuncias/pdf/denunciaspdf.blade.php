<!--=======PDF========-->
<table style="width:100%">
    <tr>
        <th style="padding-right:100px"><img src="{{asset('admin/img/logo.png')}}" width="100px">
        </th>
        <th style="width:800px">
            <h3 class="text-right">Caso N°:{{$seguimiento->id}}</h3>
        </th>
    </tr>
</table>
<hr>
<br>
<table class="table" style="width:100%">
    <thead>
        <tr>
            <th>Nombre De La Persona Denunciante:</th>
            <th style="width:50px"></th>
            <th>Documento de identificación:</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{ $seguimiento->users->nombres_segun_dui.' '.$seguimiento->users->apellidos_segun_dui }}
            </td>
            <td style="width:50px"></td>
            <td>{{ $seguimiento->users->documento_identificacion }}</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table" style="width:100%">
    <thead>
        <tr>
            <th>Numero De Contacto</th>
            <th style="width:50px"></th>
            <th>Fecha Cuando Ocurrio Suceso:</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{ $seguimiento->users->numero_telefono }}</td>
            <td style="width:50px"></td>
            <td>{{ date('d/m/Y',strtotime($seguimiento->fecha_del_suceso)) }}</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table" style="width:100%">
    <thead>
        <tr>
            <th>Donde Ocurrio EL Hecho:</th>
            <th style="width:50px"></th>
            <th>Dirección Donde Ocurrieron Los Hechos</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{ $seguimiento->donde_ocurrio }}</td>
            <td style="width:50px"></td>
            <td>{{$seguimiento->direccion_completa }}</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table" style="width:100%">
    <thead>
        <tr>
            <th>Departamento Donde Ocurrio El Hecho</th>
            <th style="width:50px"></th>
            <th>Ciudad Donde Ocurrio EL Hecho</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>@if( isset($seguimiento->departamentos->nombre_departamento) )
                {{ $seguimiento->departamentos->nombre_departamento }} @endif</td>
            <td style="width:50px"></td>
            <td>@if( isset($seguimiento->ciudades->nombre_ciudad) )
                {{ $seguimiento->ciudades->nombre_ciudad }} @endif</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table" style="width:100%">
    <thead>
        <tr>
            <th>Breve Relato Del Hecho</th>
            <th style="width:50px"></th>
            <th>Tipo De Violencia</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{ $seguimiento->breve_relato_del_hecho_consecuencias }}</td>
            <td style="width:50px"></td>
            <td>{{ $seguimiento->tipo_principal_violacion_abuso }}</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table" style="width:100%">
    <thead>
        <tr>
            <th>Cantidad De Agresores</th>
            <th style="width:50px"></th>
            <th>Forma Agresión</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{  $seguimiento->indique_cantidad_agresores }}</td>
            <td style="width:50px"></td>
            <td>{{ $seguimiento->forma_de_agrecion }}</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table" style="width:100%">
    <thead>
        <tr>
            <th>Información Del Autor Del Hech</th>
            <th style="width:50px"></th>
            <th>Realizo Denuncia Formal</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{  $seguimiento->informacion_del_autor_del_hecho }}</td>
            <td style="width:50px"></td>
            <td>{{ $seguimiento->realizo_una_denuncia_formal }}</td>
        </tr>
    </tbody>
</table>
<br>
<table class="table" style="width:100%">
    @if($seguimiento->tipo_secundario_violacion_abuso == NULL)
    <thead>
        <tr>
            <th>Tipo Secundario Violencia</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>Denunciante No Especifico</td>
        </tr>
    </tbody>
    @else
    <thead>
        <tr>
            <th>Tipo Secundario Violencia</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{ $seguimiento->tipo_secundario_violacion_abuso }}</td>
        </tr>
    </tbody>
    @endif
</table>
<table class="table" style="width:100%">
    @if($seguimiento->realizo_una_denuncia_formal == "No")
    <thead>
        <tr>
            <th>Lugar Donde Realizo La Denuncia</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>Aun No Ha Realizado Denuncia Formal</td>
        </tr>
    </tbody>
    @else
    <thead>
        <tr>
            <th>Lugar Donde Realizo La Denuncia</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{ $seguimiento->lugar_donde_realizo_la_denuncia_formals }}</td>
        </tr>
    </tbody>
    @endif
</table>
<br>
<table class="table" style="width:100%">
    @if($seguimiento->fecha_cuando_se_reaizo_la_denuncia_formal == NULL)
    <thead>
        <tr>
            <th>Fecha Que Realizo La Denuncia Formal</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>Aun No Ha Realizado Denuncia Formal</td>
        </tr>
    </tbody>
    @else
    <thead>
        <tr>
            <th>Fecha Que Realizo La Denuncia Formal</th>
        </tr>
    </thead>
    <tbody>
        <tr style="text-align:center">
            <td>{{  date('d/m/Y',strtotime($seguimiento->fecha_cuando_se_reaizo_la_denuncia_formal)) }}
            </td>
        </tr>
    </tbody>
    @endif
</table>
<br>
<br>
<br>
<hr>
<br>
<br>
<br>
<!--Anexos-->
<center>
    <h2 class="m-5">Anexos</h2>
</center>
<!--Foto dui frente-->
@if($seguimiento->foto_dui_frente == NULL)
<p><b>Foto Del DUI Frente</b></p>
<p>Usuario No Registro Foto Del Dui</p>
@else
<p><b>Foto Del DUI Frente</b></p>
<center><img style="width: 50%;height: auto;" src="{{ asset('documentos/dui/'.$seguimiento->foto_dui_frente) }}">
</center>
@endif
<!--foto dui atras-->
@if($seguimiento->foto_dui_frente == NULL)
<p><b>Foto Del DUI Atras</b></p>
<p>Usuario No Registro Foto Del Dui</p>
@else
<p><b>Foto Del DUI Atras</b></p>
<center><img style="width: 50%;height: auto;" src="{{ asset('documentos/dui/'.$seguimiento->foto_dui_atras) }}">
</center>
@endif
<!--documento dui-->
@if($seguimiento->documento_dui == NULL)
<p><b>PDF Del DUI</b></p>
<p>Usuario No Registro PDF Del Dui</p>
@else
<p>PDF Del DUI</p>
<a href="{{ asset('documentos/dui/'.$seguimiento->documento_dui) }}" download>Descargar</a>
@endif
<br>
<br>
<br>
<!--foto nit frente-->
@if($seguimiento->foto_nit_frente == NULL)
<p><b>Foto Del NIT Frente</b></p>
<p>Usuario No Registro Foto Del NIT</p>
@else
<p><b>Foto Del NIT Frente</b></p>
<center><img style="width: 50%;height: auto;" src="{{ asset('documentos/nit/'.$seguimiento->foto_nit_frente) }}">
</center>
@endif
<!--foto nit atras-->
@if($seguimiento->foto_nit_atras == NULL)
<p><b>Foto Del NIT Atras</b></p>
<p>Usuario No Registro Foto Del NIT</p>
@else
<p><b>Foto Del NIT Atras</b></p>
<center><img style="width: 50%;height: auto;" src="{{ asset('documentos/nit/'.$seguimiento->foto_nit_atras) }}">
</center>
@endif
<!--documento nit-->
@if($seguimiento->documento_nit == NULL)
<p><b>PDF Del NIT</b></p>
<p>Usuario No Registro PDF Del Nit</p>
@else
<p><b>PDF Del NIT</b></p>
<a href="{{ asset('documentos/nit/'.$seguimiento->documento_nit) }}" download>Descargar</a>
@endif
<br>
<br>
<br>
<!--foto acta-->
@if($seguimiento->foto_acta == NULL)
<p><b>Foto Del Acta</b></p>
<p>Usuario No Registro Foto Del Acta</p>
@else
<p><b>Foto Del Acta</b></p>
<center><img style="width: 50%;height: auto;" src="{{ asset('documentos/acta_denuncia/'.$seguimiento->foto_acta) }}">
</center>
@endif
<!--documento acta-->
@if($seguimiento->documento_acta == NULL)
<p class="mb-0 mt-5 text-center"><b>Documento Del Acta</b></p>
<p>Usuario No Registro PDF Del Acta</p>
@else
<p><b>Documento Del Acta</b></p>
<a href="{{ asset('documentos/acta_denuncia/'.$seguimiento->documento_acta) }}" download>Descargar</a>
@endif
<br>
<br>
<br>
<!--foto denuncia-->
@if($seguimiento->foto_denuncia == NULL)
<p><b>Foto De La Denuncia</b></p>
<p>Usuario No Registro Foto De La Denuncia</p>
@else
<p><b>Foto De La Denuncia</b></p>
<center><img style="width: 50%;height: auto;"
        src="{{ asset('documentos/acta_denuncia/'.$seguimiento->foto_denuncia) }}"></center>
@endif
<!--documento denuncia-->
@if($seguimiento->documento_denuncia == NULL)
<p><b>PDF De La Denuncia</b></p>
<p>Usuario No Registro PDF De La Denuncia</p>
@else
<p><b>Foto De La Denuncia</b></p>
<a href="{{ asset('documentos/acta_denuncia/'.$seguimiento->documento_denuncia) }}" download>Descargar</a>
@endif
<br>
<br>
<br>
<!--foto otro documentos-->
@if($seguimiento->foto_otro == NULL)
<p><b>Foto De Otro Documento</b></p>
<p>Usuario No Registro Foto De Otro Documento</p>
@else
<p><b>Foto De Otros documentos</b></p>
<center><img style="width: 50%;height: auto;" src="{{ asset('documentos/otros/'.$seguimiento->foto_otro) }}"></center>
@endif
<!--foto otro documentos2-->
@if($seguimiento->foto_otro2 == NULL)
<p><b>Foto De Otro Documento</b></p>
<p>Usuario No Registro Foto De Otro Documento</p>
@else
<p><b>Foto De Otros documentos</b></p>
<center><img style="width: 50%;height: auto;" src="{{ asset('documentos/otros/'.$seguimiento->foto_otro2) }}"></center>
@endif
<!--documentos otro-->
@if($seguimiento->documento_otro == NULL)
<div class="col-lg-3 pl-0">
    <p><b>PDF De Otro Documento</b></p>
    <p>Usuario No Registro PDF De Otro Documento</p>
</div>
@else
<p><b>PDF De Otro Documento</b></p>
<a href="{{ asset('documentos/otros/'.$seguimiento->documento_otro) }}" download>Descargar</a>
@endif