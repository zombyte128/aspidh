@extends('super.partials.app')
@section('title','ASPIDH - Crear Denuncias')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Crear Denuncia</li>
        </ol>
    </nav>
    <!--========tabs=========-->
    <div class="col-md-3 mx-auto">
        <ul class="nav nav-tabs tab-solid tab-solid-primary" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="tab-2-1" data-toggle="tab" href="#home-2-1" role="tab"
                    aria-controls="home-2-1" aria-selected="true"><i class="icon-home"></i>Formulario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-2-2" data-toggle="tab" href="#profile-2-2" role="tab"
                    aria-controls="profile-2-2" aria-selected="false"><i class="icon-user"></i>Perfil</a>
            </li>
        </ul>
    </div>
    <div class="tab-pane fade show active" id="home-2-1" role="tabpanel" aria-labelledby="tab-2-1">
        <!--========Formulario=========-->
        <form class="form-horizontal" action="{{ route('super.denuncias.store') }}" method="POST"
            enctype="multipart/form-data" id="formulariodenuncia2">
            @csrf
            <!--========usuario=========-->
            <input type="hidden" name="user_id" value="{{$idusuario->id}}">
            <div class="row">
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <!--====input fecha del suceso======-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Fecha del suceso
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="tel"
                                            class="form-control @error('fecha_del_suceso') is-invalid @enderror"
                                            placeholder="Fecha suceso | 00/00/0000" name="fecha_del_suceso"
                                            id="fechasuceso" value="{{ old('fecha_del_suceso') }}"
                                            style="text-align:center;" autocomplete="off" data-mask="00/00/0000"
                                            required>
                                        @if ($errors->has('fecha_del_suceso'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$errors->first('fecha_del_suceso')}}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <!--====select Donde Ocurrio======-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">¿Dónde ocurrió?
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2" name="donde_ocurrio" id="dondeocurrio">
                                            <option value="">¿Dónde ocurrió?</option>
                                            <option value="Zona de trabajo sexual"
                                                {{ old('donde_ocurrio') == 'Zona de trabajo sexual' ? 'selected' : '' }}>
                                                Zona de
                                                trabajo sexual</option>
                                            <option value="Calle villa publica"
                                                {{ old('donde_ocurrio') == 'Calle villa publica' ? 'selected' : '' }}>
                                                Calle
                                                villa
                                                publica</option>
                                            <option value="Servicio de salud"
                                                {{ old('donde_ocurrio') == 'Servicio de salud' ? 'selected' : '' }}>
                                                Servicio
                                                de
                                                salud</option>
                                            <option value="Sistema educativo"
                                                {{ old('donde_ocurrio') == 'Sistema educativo' ? 'selected' : '' }}>
                                                Sistema
                                                educativo</option>
                                            <option value="Lugar de trabajo"
                                                {{ old('donde_ocurrio') == 'Lugar de trabajo' ? 'selected' : '' }}>Lugar
                                                de
                                                trabajo
                                            </option>
                                            <option value="Domicilio de la persona afectada"
                                                {{ old('donde_ocurrio') == 'Domicilio de la persona afectada' ? 'selected' : '' }}>
                                                Domicilio de la persona afectada
                                            </option>
                                            <option value="Otro domicilio particular"
                                                {{ old('donde_ocurrio') == 'Otro domicilio particular' ? 'selected' : '' }}>
                                                Otro domicilio particular</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--=======textarea breve relato de los hechos==========-->
                                <div class="col-md-12 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Breve relato de los hechos
                                    </center>
                                    <div class="form-group magenes">
                                        <textarea placeholder="Breve relato de los hechos y consecuencias"
                                            name="breve_relato_del_hecho_consecuencias" class="form-control"
                                            id="breverelato" required
                                            style="text-align:center">{{ old('breve_relato_del_hecho_consecuencias') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <!--====select Tipo de violacion/Abuso======-->
                                <div class="col-md-5 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Tipo de violencia recibida
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2" name="tipo_principal_violacion_abuso"
                                            id="tipodeviolacion">
                                            <option value="">Tipo de violencia recibida</option>
                                            <option value="Economica"
                                                {{ old('tipo_principal_violacion_abuso') == 'Economica' ? 'selected' : '' }}>
                                                Económica</option>
                                            <option value="Feminicida/Homicida"
                                                {{ old('tipo_principal_violacion_abuso') == 'Feminicida/Homicida' ? 'selected' : '' }}>
                                                Feminicida/Homicida</option>
                                            <option value="Fisica"
                                                {{ old('tipo_principal_violacion_abuso') == 'Fisica' ? 'selected' : '' }}>
                                                Física
                                            </option>
                                            <option value="psicologica/Emocional"
                                                {{ old('tipo_principal_violacion_abuso') == 'psicologica/Emocional' ? 'selected' : '' }}>
                                                psicólogica/Emocional
                                            </option>
                                            <option value="Patrimonial"
                                                {{ old('tipo_principal_violacion_abuso') == 'Patrimonial' ? 'selected' : '' }}>
                                                Patrimonial</option>
                                            <option value="Sexual"
                                                {{ old('tipo_principal_violacion_abuso') == 'Sexual' ? 'selected' : '' }}>
                                                Sexual</option>
                                            <option value="Simbolica"
                                                {{ old('tipo_principal_violacion_abuso') == 'Simbolica' ? 'selected' : '' }}>
                                                Simbólica</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <!--====radius quien realizo la denuncia======-->
                                    <center>
                                        <div class="badge badge-outline-primary menos">¿Quien la realiza?
                                        </div>
                                    </center>
                                    <div class="form-group" style="text-align:center">
                                        <input type="radio" name="quien_registra_denuncia" value="Personal"
                                            id="quienregistroper" autocomplete="off" checked>
                                        <label for="quienregistroper" class="tamano">Personal</label>
                                        <input type="radio" value="Terceros" name="quien_registra_denuncia"
                                            id="quienregistroter" autocomplete="off">
                                        <label for="quienregistroter" class="tamano">Terceros</label>
                                    </div>
                                </div>
                                <!--=======input tipo de violacion secundario==========-->
                                <div class="col-md-12 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Descripcion de violencia
                                        </div>
                                    </center>
                                    <div class="form-group magenes">
                                        <textarea placeholder="Descripcion de violencia"
                                            name="tipo_secundario_violacion_abuso" class="form-control pruebassd"
                                            style="text-align:center;">{{ old('tipo_secundario_violacion_abuso') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento2">Lugar donde ocurrieron los hechos</p>
                            </center>
                            <!--=======select departamento==========-->
                            <div class="row">
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Seleccione Departamento
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2 departamento_denuncia" id="departamento"
                                            name="departamento_id">
                                            <option value="">Seleccione Departamento</option>
                                            @foreach( $departamentos as $d)
                                            <option value="{{ $d->id }}">{{ $d->nombre_departamento }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!--=======select ciudad==========-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Seleccione Ciudad
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2 ciudad_denuncia" name="ciudad_id"
                                            id="ciudad_id" disabled>
                                            <option value="">Seleccione Ciudad</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--========textarea colonia, pasaje, # de casa, edificio, apartamento=========-->
                                <div class="col-md-12 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Colonia, pasaje, # de casa,
                                            edificio, apartamento
                                        </div>
                                    </center>
                                    <div class="form-group magenes">
                                        <textarea placeholder="Colonia, pasaje, # de casa, edificio, apartamento"
                                            name="direccion_completa" id="direccioncompleta" class="form-control"
                                            style=" text-align:center;">{{ old('direccion_completa') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento">Informacion del autor del hecho</p>
                            </center>
                            <div class="row">
                                <!--=======select cantidad de agresores==========-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Cantidad de agresores
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="number" name="indique_cantidad_agresores" class="form-control"
                                            id=" cantidadagresor" placeholder="Cantidad de agresores"
                                            style="text-align:center" autocomplete="off"
                                            value="{{ old('indique_cantidad_agresores') }}"></input>
                                    </div>
                                </div>
                                <!--=======select que ocupo para agredir==========-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Con qué objeto fue violentado
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="text" name="forma_de_agrecion" class="form-control"
                                            id="formaagresion" placeholder="Con qué objeto fue violentado"
                                            style="text-align:center" autocomplete="off"
                                            value="{{ old('forma_de_agrecion') }}"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--========select informacion del autor=========-->
                                <div class="col-md-9 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Breve descripción de los
                                            autores
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <textarea name="informacion_del_autor_del_hecho" class="form-control"
                                            id="informacionautor" placeholder="Breve descripción de los autores"
                                            cols="30" rows="1"
                                            style="text-align:center">{{ old('informacion_del_autor_del_hecho') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento2">Acciones realizadas</p>
                            </center>
                            <div class="row">
                                <!--=======Radio realizo una denuncia formal==========-->
                                <div class="col-md-6 mx-auto">
                                    <center>
                                        <p>¿Realizó una denuncia formal?</p>
                                    </center>
                                    <div class="form-group centrado magenes">
                                        <input type="radio" name="realizo_una_denuncia_formal" value="Si"
                                            id="denuniciasi" autocomplete="off">
                                        <label for="denuniciasi" class="tamano">Si</label>
                                        <input type="radio" value="No" name="realizo_una_denuncia_formal"
                                            id="denuniciano" autocomplete="off">
                                        <label for="denuniciano" class="tamano">No</label>
                                    </div>
                                </div>
                                <!--========select donde la hizo=========-->
                                <div class="col-md-6 mx-auto" id="dondelahizo" style="display:none">
                                    <center>
                                        <p>¿dónde la hizo?</p>
                                    </center>
                                    <div class="form-group magenes">
                                        <select class="form-control select2"
                                            name="lugar_donde_realizo_la_denuncia_formal" id="lugardonderealizo">
                                            <option value="">Seleccione Lugar</option>
                                            <option value="Delegacion de la policía PNC">Delegación de la policía PNC
                                            </option>
                                            <option value="Autoridad local">Autoridad local</option>
                                            <option value="Fiscalia General de la republica FGR">Fiscalía General de la
                                                república
                                                FGR</option>
                                            <option value="Ministerio de salud">Ministerio de salud</option>
                                            <option value="Ministerio de trabajo">Ministerio de trabajo</option>
                                            <option
                                                value="Procuraduría para la defensa de los DDHH de el salvador PDDH">
                                                Procuraduría para la defensa de los DDHH de el salvador PDDH</option>
                                            <option value="Instituto salvadoreño del seguro social ISSS">Instituto
                                                salvadoreño del
                                                seguro social ISSS</option>
                                            <option value="Otro">Otro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento">Acciones realizadas</p>
                            </center>
                            <div class="row">
                                <!--====input fecha de la denuncia======-->
                                <div class="col-md-6 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Fecha de la denuncia
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="tel" id="fechadenuncia" disabled
                                            class="form-control @error('fecha_cuando_se_reaizo_la_denuncia_formal') is-invalid @enderror"
                                            placeholder="Fecha denuncia | 00/00/0000"
                                            name="fecha_cuando_se_reaizo_la_denuncia_formal" style="text-align:center;"
                                            data-mask="00/00/0000">
                                        @if ($errors->has('fecha_cuando_se_reaizo_la_denuncia_formal'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$errors->first('fecha_cuando_se_reaizo_la_denuncia_formal')}}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <!--========select documentacion respaldatoria=========-->
                                <div class="col-md-6 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Documentación respaldatoria</div>
                                    </center>
                                    <div class="form-group magenes">
                                        <select class="form-control select2" name="documentacion_respaldatoria"
                                            id="documentacionrespaldatoria">
                                            <option value="">Documentación respaldatoria</option>
                                            <option value="No cuenta con la documentacion"
                                                {{ old('documentacion_respaldatoria') == 'No cuenta con la documentacion' ? 'selected' : '' }}>
                                                No cuenta con la documentación</option>
                                            <option value="Documento de actas oficiales"
                                                {{ old('documentacion_respaldatoria') == 'Documento de actas oficiales' ? 'selected' : '' }}>
                                                Documento de actas oficiales</option>
                                            <option value="Documentacion medica"
                                                {{ old('documentacion_respaldatoria') == 'Documentacion medica' ? 'selected' : '' }}>
                                                Documentación medica</option>
                                            <option value="Documentacion forense"
                                                {{ old('documentacion_respaldatoria') == 'Documentacion forense' ? 'selected' : '' }}>
                                                Documentación forense</option>
                                            <option value="Material audiovisual"
                                                {{ old('documentacion_respaldatoria') == 'Material audiovisual' ? 'selected' : '' }}>
                                                Material audiovisual</option>
                                            <option value="Articulos periodicos"
                                                {{ old('documentacion_respaldatoria') == 'Articulos periodicos' ? 'selected' : '' }}>
                                                Artículos periódicos</option>
                                            <option value="Cuenta con otra documentacion"
                                                {{ old('documentacion_respaldatoria') == 'Cuenta con otra documentacion' ? 'selected' : '' }}>
                                                Cuenta con otra documentación</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mx-auto titulos">
                    <div class="card tituloradius">
                        <div class="card-body">
                            <center class="paddingformunalrio">
                                <h4 class="texto-form">Adjuntar documentos</h4>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <p class="documento">Dui</p>
                                    </center>
                                </div>
                                <!--=======File de Dui==========-->
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_dui_frente" id="image">
                                </div>
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_dui_atras" id="image1">
                                </div>
                            </div>
                            <!--=======Documento Dui==========-->
                            <div class="row ppadingimagenes">
                                <div class="col-md-12">
                                    <input type="file" class="dropify15" accept="application/pdf" name="documento_dui"
                                        id="choose-file">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <p class="documento">Nit</p>
                                    </center>
                                </div>
                                <!--=======File de Nit==========-->
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_nit_frente" id="image3">
                                </div>
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_nit_atras" id="image4">
                                </div>
                            </div>
                            <!--=======Documento Nit==========-->
                            <div class="row ppadingimagenes">
                                <div class="col-md-12">
                                    <input type="file" class="dropify15" accept="application/pdf" name="documento_nit"
                                        id="choose-file2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <!--=======File de acta==========-->
                                <div class="col-md-6 ppadingimagenes">
                                    <center>
                                        <p class="documento3">Acta</p>
                                    </center>
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_acta" id="image5">
                                </div>
                                <div class="col-md-6 ppadingimagenes">
                                    <center>
                                        <p class="documento3">Denuncia</p>
                                    </center>
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_denuncia" id="image6">
                                </div>
                                <!--=======Documento acta==========-->
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify15" accept="application/pdf" name="documento_acta"
                                        id="choose-file3">
                                </div>
                                <!--=======Documento denuncia==========-->
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify15" accept="application/pdf"
                                        name="documento_denuncia" id="choose-file4">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <p class="documento">Otra Documentacion</p>
                                    </center>
                                </div>
                                <!--=======File de otra documentacion==========-->
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_otro" id="image7">
                                </div>
                                <div class="col-md-6 ppadingimagenes">
                                    <input type="file" class="dropify14" accept="image/x-png,image/jpeg"
                                        name="foto_otro2" id="image8">
                                </div>
                            </div>
                            <!--=======Documento otro==========-->
                            <div class="row ppadingimagenes">
                                <div class="col-md-12 mx-auto">
                                    <input type="file" class="dropify15" accept="application/pdf" name="documento_otro"
                                        id="choose-file5">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--=========submit===========-->
            <center class="paddingbutton">
                <button type="submit" id="enviar2" class="btn btn-danger mr-2 paddingeventos">Enviar</button>
            </center>
        </form>
    </div>
    <!--=======Perfil del usuario=======-->
    <div class="tab-pane fade" id="profile-2-2" role="tabpanel" aria-labelledby="tab-2-2">
        <div class="card">
            <div class="card-body">
                <div class="row grid-margin">
                    <div class="col-12">
                        <div class="table-responsive">
                            <!--========tabla=========-->
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Documento De Identificación</th>
                                        <th>Teléfono</th>
                                        <th>Email</th>
                                        <th>Departamento</th>
                                        <th>Ciudad</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>{{ $idusuario->nombres_segun_dui }}</td>
                                        <td>{{ $idusuario->apellidos_segun_dui }}</td>
                                        <td>{{ $idusuario->documento_identificacion }}</td>
                                        <td>{{ $idusuario->numero_telefono }}</td>
                                        <td>{{ $idusuario->email }}</td>
                                        <td>@if( isset($idusuario->departamentos->nombre_departamento) )
                                            {{ $idusuario->departamentos->nombre_departamento }} @endif</td>
                                        <td>@if( isset($idusuario->departamentos->nombre_departamento) )
                                            {{ $idusuario->ciudades->nombre_ciudad }} @endif</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection