@extends('super.partials.app')
@section('title','ASPIDH - Denuncias Finalizadas')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Denuncias Finalizadas</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========tabla denuncias finalizadas=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th hidden>¿Quien la realiza?</th>
                                    <th>Nombre</th>
                                    <th>Documento Identificacion</th>
                                    <th>Teléfono</th>
                                    <th hidden>Donde Ocurrio</th>
                                    <th hidden>Breve Relato</th>
                                    <th hidden>Tipo De Violencia</th>
                                    <th hidden>Tipo Secundario Violencia</th>
                                    <th hidden>Cantidad De Agresores</th>
                                    <th hidden>Forma Agresion</th>
                                    <th hidden>Informacion Del Autor</th>
                                    <th hidden>Realizo Denuncia Formal</th>
                                    <th hidden>Lugar Donde Realizo La Denuncia</th>
                                    <th>Fecha Del Suceso</th>
                                    <th hidden>Fecha Que Realizo La Denuncia</th>
                                    <th>Departamento</th>
                                    <th>Ciudad</th>
                                    <th hidden>Direccion</th>
                                    <th>Estado</th>
                                    <th hidden>Documento Respaldatorios</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($finalizadas as $key=>$finalizada)
                                <tr>
                                    <td hidden>{{ $finalizada->quien_registra_denuncia }}</td>
                                    <td>{{ $finalizada->users->nombres_segun_dui }}
                                        {{ $finalizada->users->apellidos_segun_dui }}</td>
                                    <td>{{ $finalizada->users->documento_identificacion }}</td>
                                    <td>{{ $finalizada->users->numero_telefono }}</td>
                                    <td hidden>{{ $finalizada->donde_ocurrio }}</td>
                                    <td hidden>{{ $finalizada->breve_relato_del_hecho_consecuencias }}</td>
                                    <td hidden>{{ $finalizada->tipo_principal_violacion_abuso }}</td>
                                    <td hidden>{{ $finalizada->tipo_secundario_violacion_abuso }}</td>
                                    <td hidden>{{ $finalizada->indique_cantidad_agresores }}</td>
                                    <td hidden>{{ $finalizada->forma_de_agrecion }}</td>
                                    <td hidden>{{ $finalizada->informacion_del_autor_del_hecho }}</td>
                                    <td hidden>{{ $finalizada->realizo_una_denuncia_formal }}</td>
                                    <td hidden>{{ $finalizada->lugar_donde_realizo_la_denuncia_formal }}</td>
                                    <td>{{ date('d/m/Y',strtotime($finalizada->fecha_del_suceso)) }}</td>
                                    <td hidden>
                                        {{ date('d/m/Y',strtotime($finalizada->fecha_cuando_se_reaizo_la_denuncia_formal)) }}
                                    </td>
                                    <td>@if( isset($finalizada->tipogenero->nombre_tipo_genero) )
                                        {{ $finalizada->departamentos->nombre_departamento }} @endif</td>
                                    <td>@if( isset($finalizada->tipogenero->nombre_tipo_genero) )
                                        {{ $finalizada->ciudades->nombre_ciudad }} @endif</td>
                                    <td hidden>{{ $finalizada->direccion_completa }}</td>
                                    <td>
                                        <div class="badge badge-info badge-fw">{{ $finalizada->estado_de_la_denuncia }}
                                        </div>
                                    </td>
                                    <td hidden>{{ $finalizada->documentacion_respaldatoria }}</td>
                                    <td>{{ date('d/m/Y',strtotime($finalizada->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($finalizada->updated_at)) }}</td>
                                    <td>
                                        <!--========Ver=========-->
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Finalizar" title="Ver Denuncia"
                                            href="{{ URL::to('super/pdf-denuncia/'.$finalizada->id) }}"><i
                                                class="icon-eye"></i></a>
                                        <!--========eliminar=========-->
                                        <a class="btn btn-danger" data-toggle="tooltip" data-placement="bottom"
                                            href="javascript:void(0)" data-original-title="Eliminar" title="Eliminar"
                                            onclick="finalizadadelete({{$finalizada->id}})"><i
                                                class="icon-trash"></i></a>
                                        <!--=====form delete=======-->
                                        <form id="denuncia-eliminada-finalizada-{{$finalizada->id}}"
                                            action="{{route('super.denuncias-finalizadas.destroy',$finalizada->id)}}"
                                            method="POST" style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection