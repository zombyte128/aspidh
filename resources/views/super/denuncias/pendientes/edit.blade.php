@extends('super.partials.app')
@section('title','ASPIDH - Editar Denuncias')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('super.denuncias-pendientes.index')}}">Denuncia
                    Pendientes</a></li>
        </ol>
    </nav>
    <!--========Tabs=========-->
    <div class="col-md-5 mx-auto">
        <ul class="nav nav-tabs tab-solid tab-solid-primary" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="tab-2-1" data-toggle="tab" href="#home-2-1" role="tab"
                    aria-controls="home-2-1" aria-selected="true"><i class="icon-home"></i>Formulario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-2-3" data-toggle="tab" href="#documentos-2-3" role="tab"
                    aria-controls="documentos-2-3" aria-selected="false"><i class="icon-docs"></i>Documentos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-2-2" data-toggle="tab" href="#profile-2-2" role="tab"
                    aria-controls="profile-2-2" aria-selected="false"><i class="icon-user"></i>Perfil</a>
            </li>
        </ul>
    </div>
    <div class="tab-pane fade show active" id="home-2-1" role="tabpanel" aria-labelledby="tab-2-1">
        <!--========formulario=========-->
        <form class="form-horizontal" action="{{route('super.editar-pendientes', $pendientes->id)}}" method="POST"
            enctype="multipart/form-data" id="formulariodenuncia">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <!--====input fecha del suceso======-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Fecha del suceso
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="tel"
                                            class="form-control @error('fecha_del_suceso') is-invalid @enderror"
                                            placeholder="Fecha del suceso" placeholder="Fecha Suceso | 00-00-0000"
                                            data-mask="00-00-0000" id="fechasuceso" name="fecha_del_suceso"
                                            style="text-align:center;" required autocomplete="off"
                                            @if($pendientes->fecha_del_suceso ==
                                        NULL)
                                        value=""
                                        @else
                                        value="{{\Carbon\Carbon::parse($pendientes->fecha_del_suceso)->format('d-m-Y')}}"
                                        @endif >
                                        @if ($errors->has('fecha_del_suceso'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$errors->first('fecha_del_suceso')}}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <!--====select Donde Ocurrio======-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">¿Dónde ocurrió?
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2" name="donde_ocurrio" id="dondeocurrio">
                                            <option value="">¿Dónde ocurrió?</option>
                                            <option value="Zona de trabajo sexual"
                                                {{  $pendientes->donde_ocurrio == 'Zona de trabajo sexual' ? 'selected' : '' }}>
                                                Zona de
                                                trabajo sexual</option>
                                            <option value="Calle villa publica"
                                                {{ $pendientes->donde_ocurrio == 'Calle villa publica' ? 'selected' : '' }}>
                                                Calle
                                                villa
                                                publica</option>
                                            <option value="Servicio de salud"
                                                {{ $pendientes->donde_ocurrio == 'Servicio de salud' ? 'selected' : '' }}>
                                                Servicio de
                                                salud</option>
                                            <option value="Sistema educativo"
                                                {{ $pendientes->donde_ocurrio == 'Sistema educativo' ? 'selected' : '' }}>
                                                Sistema
                                                educativo</option>
                                            <option value="Lugar de trabajo"
                                                {{ $pendientes->donde_ocurrio == 'Lugar de trabajo' ? 'selected' : '' }}>
                                                Lugar
                                                de
                                                trabajo
                                            </option>
                                            <option value="Domicilio de la persona afectada"
                                                {{ $pendientes->donde_ocurrio == 'Domicilio de la persona afectada' ? 'selected' : '' }}>
                                                Domicilio de la persona afectada
                                            </option>
                                            <option value="Otro domicilio particular"
                                                {{ $pendientes->donde_ocurrio == 'Otro domicilio particular' ? 'selected' : '' }}>
                                                Otro domicilio particular</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--=======textarea breve relato de hechos==========-->
                                <div class="col-md-12 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Breve relato de los hechos
                                    </center>
                                    <div class="form-group magenes">
                                        <textarea placeholder="Breve relato de los hechos y consecuencias"
                                            name="breve_relato_del_hecho_consecuencias" id="breverelato"
                                            class="form-control" required
                                            style="text-align:center">{{$pendientes->breve_relato_del_hecho_consecuencias}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <!--====select Tipo de violacion/Abuso======-->
                                <div class="col-md-5 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Tipo de violencia recibida
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2" name="tipo_principal_violacion_abuso"
                                            id="tipodeviolacion">
                                            <option value="">Tipo de violencia recibida</option>
                                            <option value="Economica"
                                                {{ $pendientes->tipo_principal_violacion_abuso == 'Economica' ? 'selected' : '' }}>
                                                Económica</option>
                                            <option value="Feminicida/Homicida"
                                                {{ $pendientes->tipo_principal_violacion_abuso == 'Feminicida/Homicida' ? 'selected' : '' }}>
                                                Feminicida/Homicida</option>
                                            <option value="Fisica"
                                                {{ $pendientes->tipo_principal_violacion_abuso == 'Fisica' ? 'selected' : '' }}>
                                                Física
                                            </option>
                                            <option value="psicologica/Emocional"
                                                {{ $pendientes->tipo_principal_violacion_abuso == 'psicologica/Emocional' ? 'selected' : '' }}>
                                                psicólogica/Emocional
                                            </option>
                                            <option value="Patrimonial"
                                                {{ $pendientes->tipo_principal_violacion_abuso == 'Patrimonial' ? 'selected' : '' }}>
                                                Patrimonial</option>
                                            <option value="Sexual"
                                                {{ $pendientes->tipo_principal_violacion_abuso == 'Sexual' ? 'selected' : '' }}>
                                                Sexual</option>
                                            <option value="Simbolica"
                                                {{ $pendientes->tipo_principal_violacion_abuso == 'Simbolica' ? 'selected' : '' }}>
                                                Simbólica</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <!--====radius quien realizo la denuncia======-->
                                    <center>
                                        <div class="badge badge-outline-primary menos">¿Quien la realiza?
                                        </div>
                                    </center>
                                    <div class="form-group" style="text-align:center">
                                        <input type="radio" name="quien_registra_denuncia" value="Personal"
                                            id="quienregistroper" autocomplete="off"
                                            {{ $pendientes->quien_registra_denuncia == 'Personal' ? 'checked' : '' }}>
                                        <label for="quienregistroper" class="tamano">Personal</label>
                                        <input type="radio" value="Terceros" name="quien_registra_denuncia"
                                            id="quienregistroter" autocomplete="off"
                                            {{ $pendientes->quien_registra_denuncia == 'Terceros' ? 'checked' : '' }}>
                                        <label for="quienregistroter" class="tamano">Terceros</label>
                                    </div>
                                </div>
                                <!--=======input tipo de violacion secundario==========-->
                                <div class="col-md-12 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Descripcion de violencia
                                        </div>
                                    </center>
                                    <div class="form-group magenes">
                                        <textarea placeholder="Descripcion de violencia"
                                            name="tipo_secundario_violacion_abuso" class="form-control pruebassd"
                                            style="text-align:center;">{{$pendientes->tipo_secundario_violacion_abuso}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento2">Lugar donde ocurrieron los hechos</p>
                            </center>
                            <!--=======select departamento==========-->
                            <div class="row">
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Seleccione Departamento
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2 departamento_edit" id="departamento"
                                            name="departamento_id">
                                            <option value="">Seleccione Departamento</option>
                                            @foreach( $departamentos as $d)
                                            <option value="{{ $d->id }}"
                                                {{  $d->id == $pendientes->departamento_id ? 'selected' : '' }}>
                                                {{ $d->nombre_departamento }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!--=======select ciudad==========-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Seleccione Ciudad
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <select class="form-control select2 ciudad_edit" name="ciudad_id"
                                            id="ciudad_id">
                                            <option value="{{ $pendientes->ciudad_id }}">
                                                @if( isset($pendientes->ciudades->nombre_ciudad) )
                                                {{ $pendientes->ciudades->nombre_ciudad }} @endif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--========Colonia, Pasaje, # de casa, edificio, apartamento=========-->
                                <div class="col-md-12 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Colonia, pasaje, # de casa,
                                            edificio,
                                            apartamento
                                        </div>
                                    </center>
                                    <div class="form-group magenes">
                                        <textarea placeholder="Colonia, pasaje, # de casa, edificio, apartamento"
                                            name="direccion_completa" id="direccioncompleta" class="form-control"
                                            style=" text-align:center;">{{$pendientes->direccion_completa}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento">Informacion del autor del hecho</p>
                            </center>
                            <div class="row">
                                <!--=======select cantidad de agresores==========-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Cantidad de agresores
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="number" name="indique_cantidad_agresores" class="form-control"
                                            id="cantidadagresor" placeholder="Cantidad de agresores"
                                            value="{{$pendientes->indique_cantidad_agresores}}"
                                            style="text-align:center" autocomplete="off"></input>
                                    </div>
                                </div>
                                <!--=======select que ocupo para agredir==========-->
                                <div class="col-md-6">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Con qué objeto fue violentado
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="text" name="forma_de_agrecion" class="form-control"
                                            id="formaagresion" placeholder="Con qué objeto fue violentado"
                                            style="text-align:center" autocomplete="off"
                                            value="{{$pendientes->forma_de_agrecion}}"></input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--========select informacion del autor=========-->
                                <div class="col-md-9 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Breve descripción de los
                                            autores
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <textarea name="informacion_del_autor_del_hecho" class="form-control"
                                            id="informacionautor" placeholder="Breve descripción de los autores"
                                            cols="30" rows="2"
                                            style="text-align:center">{{$pendientes->informacion_del_autor_del_hecho}}</textarea>
                                        @if ($errors->has('informacion_del_autor_del_hecho'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$errors->first('informacion_del_autor_del_hecho')}}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento2">Acciones realizadas</p>
                            </center>
                            <div class="row">
                                <!--=======Radio realizo denuncia formal==========-->
                                <div class="col-md-6 mx-auto">
                                    <center>
                                        <p>¿Realizó una denuncia formal?</p>
                                    </center>
                                    <div class="form-group centrado magenes">
                                        <input type="radio" name="realizo_una_denuncia_formal" value="Si"
                                            autocomplete="off" id="denuniciasi"
                                            {{ $pendientes->realizo_una_denuncia_formal == 'Si' ? 'checked' : '' }}>
                                        <label for="denuniciasi" class="tamano">Si</label>
                                        <input type="radio" value="No" name="realizo_una_denuncia_formal"
                                            autocomplete="off" id="denuniciano"
                                            {{ $pendientes->realizo_una_denuncia_formal == 'No' ? 'checked' : '' }}>
                                        <label for="denuniciano" class="tamano">No</label>
                                    </div>
                                </div>
                                <!--========select donde la realizo=========-->
                                <div class="col-md-6 mx-auto" id="dondelahizo" style="display:none">
                                    <center>
                                        <p>¿dónde la hizo?</p>
                                    </center>
                                    <div class="form-group magenes">
                                        <select class="form-control select2"
                                            name="lugar_donde_realizo_la_denuncia_formal" id="lugardonderealizo">
                                            <option value="">Seleccione Lugar</option>
                                            <option value="Delegacion de la policía PNC"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Delegacion de la policía PNC' ? 'selected' : '' }}>
                                                Delegación de la policía PNC</option>
                                            <option value="Autoridad local"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Autoridad local' ? 'selected' : '' }}>
                                                Autoridad local</option>
                                            <option value="Fiscalia General de la republica FGR"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Fiscalia General de la republica FGR' ? 'selected' : '' }}>
                                                Fiscalía General de la república FGR</option>
                                            <option value="Ministerio de salud"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Ministerio de salud' ? 'selected' : '' }}>
                                                Ministerio de salud</option>
                                            <option value="Ministerio de trabajo"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Ministerio de trabajo' ? 'selected' : '' }}>
                                                Ministerio de trabajo</option>
                                            <option value="Procuraduría para la defensa de los DDHH de el salvador PDDH"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Procuraduría para la defensa de los DDHH de el salvador PDDH' ? 'selected' : '' }}>
                                                Procuraduría para la defensa de los DDHH de el salvador PDDH</option>
                                            <option value="Instituto salvadoreño del seguro social ISSS"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Instituto salvadoreño del seguro social ISSS' ? 'selected' : '' }}>
                                                Instituto salvadoreño del seguro social ISSS</option>
                                            <option value="Otro"
                                                {{ $pendientes->lugar_donde_realizo_la_denuncia_formal == 'Otro' ? 'selected' : '' }}>
                                                Otro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <p class="documento">Acciones realizadas</p>
                            </center>
                            <div class="row">
                                <!--====input fecha de la denuncia======-->
                                <div class="col-md-6 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Fecha de la denuncia
                                        </div>
                                    </center>
                                    <div class="form-group">
                                        <input type="tel"
                                            class="form-control @error('fecha_cuando_se_reaizo_la_denuncia_formal') is-invalid @enderror"
                                            placeholder="Fecha de la denuncia" id="fechadenuncia" autocomplete="off"
                                            name="fecha_cuando_se_reaizo_la_denuncia_formal" style="text-align:center;"
                                            disabled @if($pendientes->fecha_cuando_se_reaizo_la_denuncia_formal == NULL)
                                        value=""
                                        @else
                                        value="{{\Carbon\Carbon::parse($pendientes->fecha_cuando_se_reaizo_la_denuncia_formal)->format('d/m/Y')}}"
                                        @endif>
                                        @if ($errors->has('fecha_cuando_se_reaizo_la_denuncia_formal'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$errors->first('fecha_cuando_se_reaizo_la_denuncia_formal')}}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <!--========Select documentacion respaldatoria=========-->
                                <div class="col-md-6 mx-auto">
                                    <center>
                                        <div class="badge badge-outline-primary menos">Documentación respaldatoria</div>
                                    </center>
                                    <div class="form-group magenes">
                                        <select class="form-control select2" name="documentacion_respaldatoria"
                                            id="documentacionrespaldatoria">
                                            <option value="">Documentación respaldatoria</option>
                                            <option value="No cuenta con la documentacion"
                                                {{ $pendientes->documentacion_respaldatoria == 'No cuenta con la documentacion' ? 'selected' : '' }}>
                                                No cuenta con la documentación</option>
                                            <option value="Documento de actas oficiales"
                                                {{ $pendientes->documentacion_respaldatoria == 'Documento de actas oficiales' ? 'selected' : '' }}>
                                                Documento de actas oficiales</option>
                                            <option value="Documentacion medica"
                                                {{ $pendientes->documentacion_respaldatoria == 'Documentacion medica' ? 'selected' : '' }}>
                                                Documentación medica</option>
                                            <option value="Documentacion forense"
                                                {{ $pendientes->documentacion_respaldatoria == 'Documentacion forense' ? 'selected' : '' }}>
                                                Documentación forense</option>
                                            <option value="Material audiovisual"
                                                {{ $pendientes->documentacion_respaldatoria == 'Material audiovisual' ? 'selected' : '' }}>
                                                Material audiovisual</option>
                                            <option value="Articulos periodicos"
                                                {{ $pendientes->documentacion_respaldatoria == 'Articulos periodicos' ? 'selected' : '' }}>
                                                Artículos periódicos</option>
                                            <option value="Cuenta con otra documentacion"
                                                {{ $pendientes->documentacion_respaldatoria == 'Cuenta con otra documentacion' ? 'selected' : '' }}>
                                                Cuenta con otra documentación</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--=========submit===========-->
            <center class="paddingbutton">
                <button type="submit" id="enviar" class="btn btn-danger mr-2 paddingeventos">Editar</button>
            </center>
        </form>
    </div>
    <!--=======Modificar Documentos=======-->
    <div class="tab-pane fade" id="documentos-2-3" role="tabpanel" aria-labelledby="tab-2-3">
        <!--========formulario=========-->
        <form class="form-horizontal" action="{{route('super.editar-pendientes-documentos', $pendientes->id)}}"
            method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <!--===========Dui===========-->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Dui frente</p>
                            </center>
                            <input type="file" name="foto_dui_frente" class="dropify" accept="image/x-png,image/jpeg"
                                onclick="hiddens();" @if($pendientes->foto_dui_frente != "")
                            data-default-file="{{ asset('documentos/dui/'.$pendientes->foto_dui_frente) }}"
                            @endif>
                            <input type="hidden" id="boton1" value="prueba">
                        </div>
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Dui atras</p>
                            </center>
                            <input type="file" class="dropify2" accept="image/x-png,image/jpeg" name="foto_dui_atras"
                                onclick="hiddens2();" @if($pendientes->foto_dui_atras != "")
                            data-default-file="{{ asset('documentos/dui/'.$pendientes->foto_dui_atras) }}"
                            @endif>
                            <input type="hidden" id="boton2" value="prueba">
                        </div>
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Dui Pdf</p>
                            </center>
                            <input type="file" class="dropify3" accept="application/pdf" name="documento_dui"
                                onclick="hiddens3();" @if($pendientes->documento_dui != "")
                            data-default-file="{{ asset('documentos/dui/'.$pendientes->documento_dui) }}"
                            @endif>
                            <input type="hidden" id="boton3" value="prueba">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--===========Nit===========-->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Nit frente</p>
                            </center>
                            <input type="file" name="foto_nit_frente" class="dropify4" accept="image/x-png,image/jpeg"
                                onclick="hiddens4();" @if($pendientes->foto_nit_frente != "")
                            data-default-file="{{ asset('documentos/nit/'.$pendientes->foto_nit_frente) }}"
                            @endif>
                            <input type="hidden" id="boton4" value="prueba">
                        </div>
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Nit atras</p>
                            </center>
                            <input type="file" class="dropify5" accept="image/x-png,image/jpeg" name="foto_nit_atras"
                                onclick="hiddens5();" @if($pendientes->foto_nit_atras != "")
                            data-default-file="{{ asset('documentos/nit/'.$pendientes->foto_nit_atras) }}"
                            @endif>
                            <input type="hidden" id="boton5" value="prueba">
                        </div>
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Nit Pdf</p>
                            </center>
                            <input type="file" class="dropify6" accept="application/pdf" name="documento_nit"
                                onclick="hiddens6();" @if($pendientes->documento_nit != "")
                            data-default-file="{{ asset('documentos/dui/'.$pendientes->documento_nit) }}"
                            @endif>
                            <input type="hidden" id="boton6" value="prueba">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--===========Acta-Denuncia===========-->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 ppadingimagenes">
                            <center>
                                <p class="documento">Acta</p>
                            </center>
                            <input type="file" name="foto_acta" class="dropify7" accept="image/x-png,image/jpeg"
                                onclick="hiddens7();" @if($pendientes->foto_acta != "")
                            data-default-file="{{ asset('documentos/acta_denuncia/'.$pendientes->foto_acta) }}"
                            @endif>
                            <input type="hidden" id="boton7" value="prueba">
                        </div>
                        <div class="col-md-3 ppadingimagenes">
                            <center>
                                <p class="documento">Acta pdf</p>
                            </center>
                            <input type="file" class="dropify8" accept="application/pdf" name="documento_acta"
                                onclick="hiddens8();" @if($pendientes->documento_acta != "")
                            data-default-file="{{ asset('documentos/acta_denuncia/'.$pendientes->documento_acta) }}"
                            @endif>
                            <input type="hidden" id="boton8" value="prueba">
                        </div>
                        <div class="col-md-3 ppadingimagenes">
                            <center>
                                <p class="documento">Denuncia</p>
                            </center>
                            <input type="file" class="dropify9" accept="image/x-png,image/jpeg" name="foto_denuncia"
                                onclick="hiddens9();" @if($pendientes->foto_denuncia != "")
                            data-default-file="{{ asset('documentos/acta_denuncia/'.$pendientes->foto_denuncia) }}"
                            @endif>
                            <input type="hidden" id="boton9" value="prueba">
                        </div>
                        <div class="col-md-3 ppadingimagenes">
                            <center>
                                <p class="documento">Denuncia pdf</p>
                            </center>
                            <input type="file" class="dropify10" accept="application/pdf" name="documento_denuncia"
                                onclick="hiddens10();" @if($pendientes->documento_denuncia != "")
                            data-default-file="{{ asset('documentos/acta_denuncia/'.$pendientes->documento_denuncia) }}"
                            @endif>
                            <input type="hidden" id="boton10" value="prueba">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--===========Otros Documentos===========-->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Otra Documentacion</p>
                            </center>
                            <input type="file" name="foto_otro" class="dropify11" accept="image/x-png,image/jpeg"
                                onclick="hiddens11();" @if($pendientes->foto_otro != "")
                            data-default-file="{{ asset('documentos/otros/'.$pendientes->foto_otro) }}"
                            @endif>
                            <input type="hidden" id="boton11" value="prueba">
                        </div>
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Otra Documentacion</p>
                            </center>
                            <input type="file" class="dropify12" accept="image/x-png,image/jpeg" name="foto_otro2"
                                onclick="hiddens12();" @if($pendientes->foto_otro2 != "")
                            data-default-file="{{ asset('documentos/otros/'.$pendientes->foto_otro2) }}"
                            @endif>
                            <input type="hidden" id="boton12" value="prueba">
                        </div>
                        <div class="col-md-4 ppadingimagenes">
                            <center>
                                <p class="documento">Otra Documentacion pdf</p>
                            </center>
                            <input type="file" class="dropify13" accept="application/pdf" name="documento_otro"
                                onclick="hiddens13();" @if($pendientes->documento_otro != "")
                            data-default-file="{{ asset('documentos/otros/'.$pendientes->documento_otro) }}"
                            @endif>
                            <input type="hidden" id="boton13" value="prueba">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--=========submit===========-->
            <center class="paddingbutton">
                <button type="submit" class="btn btn-danger mr-2 paddingeventos">Editar Documentos</button>
            </center>
        </form>
    </div>
    <!--=======Perfil del usuario=======-->
    <div class="tab-pane fade" id="profile-2-2" role="tabpanel" aria-labelledby="tab-2-2">
        <div class="card">
            <div class="card-body">
                <div class="row grid-margin">
                    <div class="col-12">
                        <div class="table-responsive">
                            <!--========tabla=========-->
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Documento De Identificación</th>
                                        <th>Teléfono</th>
                                        <th>Email</th>
                                        <th>Departamento</th>
                                        <th>Ciudad</th>
                                        <th>Fecha del suceso</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $pendientes->users->nombres_segun_dui }}</td>
                                        <td>{{ $pendientes->users->apellidos_segun_dui }}</td>
                                        <td>{{ $pendientes->users->documento_identificacion }}</td>
                                        <td>{{ $pendientes->users->numero_telefono }}</td>
                                        <td>{{ $pendientes->users->email }}</td>
                                        <td>@if( isset($pendientes->departamentos->nombre_departamento) )
                                            {{ $pendientes->departamentos->nombre_departamento }} @endif</td>
                                        <td>@if( isset($pendientes->ciudades->nombre_ciudad) )
                                            {{ $pendientes->ciudades->nombre_ciudad }} @endif</td>
                                        <td>{{ date('d/m/Y',strtotime($pendientes->fecha_del_suceso)) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection