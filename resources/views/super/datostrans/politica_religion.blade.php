@extends('super.partials.app')
@section('title','ASPIDH - Participación política y religión personas transexuales')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Participación política y religión personas transexuales</li>
        </ol>
    </nav>
    <div class="row">
        <!--======ha ejercido derecho al voto=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart25->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======problemas al ejercer derecho al voto=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart26->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======participacion politica=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart27->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======practica una religion=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart28->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======tipo de religion=========-->
        <div class="col-md-12 mx-auto">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart29->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart25->script() !!}
{!! $chart26->script() !!}
{!! $chart27->script() !!}
{!! $chart28->script() !!}
{!! $chart29->script() !!}
@endsection