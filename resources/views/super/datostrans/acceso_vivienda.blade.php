@extends('super.partials.app')
@section('title','ASPIDH - Acceso a la vivienda personas transexuales')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Acceso a la vivienda personas transexuales</li>
        </ol>
    </nav>
    <div class="row">
        <!--======tipo de vivienda=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart19->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======Con quien vive=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart20->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======porcentaje financiero viviendal=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart21->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======credito para una vivienda=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart22->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======credito para vivienda y recibieron respuesta=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart23->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======ayuda gubernamental de acceso a la vivienda=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart24->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart19->script() !!}
{!! $chart20->script() !!}
{!! $chart21->script() !!}
{!! $chart22->script() !!}
{!! $chart23->script() !!}
{!! $chart24->script() !!}
@endsection