@extends('super.partials.app')
@section('title','ASPIDH - Donantes')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('super.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Donantes</li>
        </ol>
    </nav>
    <div class="card">
        <div class="card-body cards">
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="table-responsive">
                        <!--========Tabla donantes=========-->
                        <table id="example" class="table">
                            <thead>
                                <tr>
                                    <th>Rol</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Email</th>
                                    <th>contraseña</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $donantes as $key => $donante)
                                <tr>
                                    <td>{{ $donante->role->nombre_rol }}</td>
                                    <td>{{ $donante->nombres_segun_dui }}</td>
                                    <td>{{ $donante->apellidos_segun_dui }}</td>
                                    <td>{{ $donante->email }}</td>
                                    <td>{{ $donante->contrasena }}</td>
                                    <td>{{ date('d/m/Y',strtotime($donante->created_at)) }}</td>
                                    <td>{{ date('d/m/Y',strtotime($donante->updated_at)) }}</td>
                                    <td>
                                        <!--========Editar=========-->
                                        <a class="btn btn-info" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Editar" title="Editar" role="link"
                                            href="{{route('super.donantes.edit',$donante->id)}}"><i
                                                class="icon-pencil"></i></a>
                                        <!--========Eliminar=========-->
                                        <a class="btn btn-danger" data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="Eliminar" title="Eliminar" href="javascript:void(0)"
                                            onclick="deletedonante({{$donante->id}})"><i class="icon-trash"></i></a>
                                        <!--=====form delete=======-->
                                        <form id="donante-delete-{{$donante->id}}"
                                            action="{{route('super.donantes.destroy',$donante->id)}}" method="POST"
                                            style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--========Boton nuevo donante=========-->
        <a role="link" href="{{route('super.donantes.create')}}" class="btn btn-sm btn-primary">Nuevo Donantes</a>
    </div>
</div>
@endsection