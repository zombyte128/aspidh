<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--Informacion encabezado-->
    <link rel="icon" type="image/png" href="{{asset('admin/img/icono.ico')}}">
    <title>@yield('title')</title>
    <!--icons-->
    <link href="{{asset('admin/css/simple-line-icons/css/simple-line-icons.css')}}" type="text/css" rel="stylesheet">
    <!--diseno-->
    <link href="{{asset('admin/css/style.css')}}" type="text/css" rel="stylesheet">

</head>

<body>

    <!--Carga de pagina-->
    <div id="preloader">
        <div class="loader" id="loader-1"></div>
    </div>

    <!--Plantilla-->
    <div class="container-scroller">
        @include('donantes.partials.navbar')
        <div class="container-fluid page-body-wrapper">
            <div class="row row-offcanvas row-offcanvas-right">
                @include('donantes.partials.sidebar')
                @yield('content')
                @include('donantes.partials.footer')
            </div>
        </div>
    </div>

    <!--jquery -->
    <script src="{{asset('admin/js/jquery.min.js')}}"></script>
    <!--bundle-->
    <script src="{{asset('admin/js/vendor.bundle.base.js')}}"></script>
    <!--navbar mobile-->
    <script src="{{asset('admin/js/off-canvas.js')}}"></script>
    <!--menu collapse-->
    <script src="{{asset('admin/js/misc.js')}}"></script>
    <!--Graficas-->
    <script src="{{asset('admin/js/highcharts.js')}}"></script>
    <script src="{{asset('admin/js/exporting.js')}}"></script>
    <script src="{{asset('admin/js/export-data.js')}}"></script>
    <script src="{{asset('admin/js/offline-exporting.js')}}"></script>
    <!--Script preloader-->
    <script src="{{asset('admin/js/preloader.js')}}"></script>
    <!--Modal de Denuncias-->
    @include('donantes.modal.modal')

</body>

</html>