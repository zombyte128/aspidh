<!---=============Opciones de menu lateral==============-->
<div id="right-sidebar" class="settings-panel">
    <div class="tab-content" id="setting-content">
        <div class="tab-pane fade show active scroll-wrapper" role="tabpanel" aria-labelledby="todo-section">
        </div>
    </div>
</div>
<!---=============Menu==============-->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <!--========img=========-->
                <div class="profile-image">
                    <img src="{{asset('admin/img/logo.png')}}">
                    <span class="online-status online"></span>
                </div>
            </div>
            <hr>
        </li>
        <!---===dashboard======-->
        <li class="nav-item">
            <a class="nav-link" href="{{route('donante.dashboard')}}">
                <i class="icon-rocket menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <!---===Graficas registro======-->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false"
                aria-controls="page-layouts">
                <i class="icon-chart menu-icon"></i>
                <span class="menu-title">Graficas Registro</span>
            </a>
            <div class="collapse" id="page-layouts">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{route('donante.datos_generales')}}">Datos
                            generales</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('donante.educacion')}}">Educación</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('donante.acesso_salud')}}">Acesso a la
                            salud</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('donante.trabajo_ocupacion')}}">Trabajo u
                            ocupación</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('donante.acceso_vivienda')}}">Acceso a la
                            vivienda</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('donante.politica_religion')}}">política y
                            religión</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('donante.derechos')}}">Vulneración de
                            derechos</a></li>

                </ul>
            </div>
        </li>
        <hr>
        <!---===Cerrar Sesion======-->
        <li class="nav-item nav-doc">
            <a class="nav-link bg-primary" href="{{ route('logout')  }}"
                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="icon-power menu-icon"></i>
                <span class="menu-title">Cerrar sesión</span>
            </a>
            <!--=====form sesion=======-->
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>