@extends('donantes.partials.app')
@section('title','ASPIDH - Donantes')
@section('content')
<div class="content-wrapper">
    <!--========Detalle de totales card=========-->
    <div class="row">
        <div class="col-md-6 col-lg-3 grid-margin stretch-card mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-md-center">
                        <i class="icon-user icon-lg text-success"></i>
                        <div class="ml-3">
                            <p class="mb-0">Personas Registradas</p>
                            <h6 class="total">Total: {{ $total_personas }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 grid-margin stretch-card mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-md-center">
                        <i class=" icon-folder icon-lg text-danger"></i>
                        <div class="ml-3">
                            <p class="mb-0">Denuncias</p>
                            <h6 class="total">Total: {{ $total_denuncias_general }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======Top Departamento grafica=========-->
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart1->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======Top Ciudad grafica=========-->
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart2->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======principal de abuso en las denuncias grafica=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart36->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======autores de denuncias grafica=========-->
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart1->script() !!}
{!! $chart2->script() !!}
{!! $chart36->script() !!}
@endsection