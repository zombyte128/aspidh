@extends('donantes.partials.app')
@section('title','ASPIDH - Datos Generales')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('donante.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Datos generales</li>
        </ol>
    </nav>
    <div class="row">
        <!--======Genero=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart3->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======orientacion=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart4->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======total de personas departamento=========-->
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart5->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======total personas registradas por mes=========-->
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <form action="{{route('donante.general')}}" method="POST">
                        @csrf
                        <select name="anios" id="anos">
                            <option value="">Seleccione Año</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                        </select>
                        <button>generar grafica</button>
                    </form>
                    <div class="card-body graficas">
                        {!! $chart6->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========script de graficas=========-->
{!! $chart3->script() !!}
{!! $chart4->script() !!}
{!! $chart5->script() !!}
{!! $chart6->script() !!}
@endsection