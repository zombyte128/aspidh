@extends('donantes.partials.app')
@section('title','ASPIDH - Acesso a la salud')
@section('content')
<div class="content-wrapper">
    <!--========Navbar=========-->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb bg-dark">
            <li class="breadcrumb-item"><a href="{{route('donante.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Acceso a la salud</li>
        </ol>
    </nav>
    <div class="row">
        <!--======Afiliada al seguro social=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart11->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======servicios del seguro social=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart12->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!--======tipos de servicios de salud=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart13->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--======programa de salud gubernamenta=========-->
        <div class="col-md-6">
            <div class="table-responsive paddinggraficas">
                <div class="card">
                    <div class="card-body graficas">
                        {!! $chart14->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--========Script de graficas=========-->
{!! $chart11->script() !!}
{!! $chart12->script() !!}
{!! $chart13->script() !!}
{!! $chart14->script() !!}
@endsection