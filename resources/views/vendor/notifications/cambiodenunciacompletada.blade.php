<!--==============Denuncias Aspidh================-->
<table
    style="background: linear-gradient(88deg, #13b4ca, #E7015E);border-radius:20px;border-collapse:collapse;font-family:helvetica,arial,sans-serif;font-size:16px;font-weight:400;line-height:1.5;margin:0;min-width:375px!important;padding:0;text-align:left;width:100%!important">
    <tbody>
        <tr>
            <td style="border-collapse:collapse;padding:40px 20px">
                <table style="border-collapse:collapse;margin:0 auto;max-width:600px;padding:0;width:100%">
                    <tbody>
                        <tr>
                            <td bgcolor="#e9e9e9"
                                style="border-collapse:collapse;color:#e9e9e9;font-size:0;height:0;padding:0;width:100%">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#f7f7f7" style="border-collapse:collapse;padding:0;width:100%">
                                <table style="border-collapse:collapse;padding:0;width:100%">
                                    <tbody>
                                        <tr>
                                            <td style="border-collapse:collapse;padding:0;width:60%"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <!--==============Mostrar el primer parrafo================-->
                            <td bgcolor="#fff"
                                style="border-bottom:1px solid #e9e9e9;border-collapse:collapse;border-top:1px solid #e9e9e9;padding:8%;width:100%">
                                <table style="border-collapse:collapse;padding:0;text-align:center;width:100%">
                                    <tbody>
                                        <tr>
                                            <td style="border-collapse:collapse;padding:0">
                                                <p style="color:#000;font-size:20px;margin:0;font-weight: bold;">
                                                    Tu denuncia acaba de cambiar de estado
                                                </p>
                                                <p style="color:#666666;font-size:18px;margin:0">
                                                    <em style="font-weight:bold;">¡Hola!
                                                        {{$seguimientos->users->nombres_segun_dui.' '.$seguimientos->users->apellidos_segun_dui}}</em>
                                                </p>
                                                <p>
                                                    Te informamos que la denuncia que realizaste el dia <em
                                                        style="font-weight:bold;">{{ date('d/m/Y',strtotime($seguimientos->created_at)) }}</em>
                                                    ya esta Completada
                                                    eso significa que tu denuncia ya se encuentra en manos de las
                                                    autoridades competentes.
                                                </p>
                                                <p style="color:#666666;font-size:14px;margin:0">
                                                    para mayor información sobre su denuncia puede comunicarse con
                                                    nosotros con los siguientes medios electronicos y comunicación
                                                </p>
                                            </td>
                                        </tr>
                                        <br>
                                        <table style="width:100%;text-align:center">
                                            <tbody>
                                                <tr>
                                                    <td><strong style="font-size:11px">Télefono</strong></td>
                                                    <td><strong style="font-size:11px">Email</strong></td>
                                                </tr>
                                                <tr>
                                                    <td><span
                                                            style="font-size:11px;background-color:#f0f0f0;color:#C82333;border-radius:6px;padding:5px;font-weight:bold;min-width:70%;width:70%;display:block;margin:0 auto">2200-5469</span>
                                                    </td>
                                                    <td><span
                                                            style="font-size:11px;background-color:#f0f0f0;color:#C82333;border-radius:6px;padding:5px;font-weight:bold;min-width:70%;width:70%;display:block;margin:0 auto">aspidh.arcoiris.trans@gmail.com</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br>
                                        <!--==============Mostrar el boton de link================-->
                                        <tr>
                                            <td style="border-collapse:collapse;padding:10px 0 10px">
                                                <table
                                                    style="border-collapse:collapse;display:inline-block;padding:0;vertical-align:bottom">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="border-collapse:collapse;color:#000000;font-size:20px;font-weight:bold;letter-spacing:2px;min-width:160px;padding:0 16px;text-align:center">
                                                                <a class="boton_personalizado"
                                                                    style="text-decoration: none;padding: 10px;font-weight: 600;font-size:18px;color: #ffffff;background-color: #249938;border-radius: 6px;">
                                                                    Completada</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <br>
                        </tr>
                        <!--==============Mostrar el segundo parrafo================-->
                        <tr>
                            <td bgcolor="#fff"
                                style="border-collapse:collapse;color:#666666;font-size:12px;padding:10px;text-align:center;width:100%">
                                <center>
                                    <p>© Copyright ASPIDH. All Rights Reserved</p>
                                </center>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>