<!--==============Denuncias Aspidh================-->
<table
    style="background: linear-gradient(88deg, #13b4ca, #E7015E);border-radius:20px;border-collapse:collapse;font-family:helvetica,arial,sans-serif;font-size:16px;font-weight:400;line-height:1.5;margin:0;min-width:375px!important;padding:0;text-align:left;width:100%!important">
    <tbody>
        <tr>
            <td style="border-collapse:collapse;padding:40px 20px">
                <table style="border-collapse:collapse;margin:0 auto;max-width:600px;padding:0;width:100%">
                    <tbody>
                        <tr>
                            <td bgcolor="#e9e9e9"
                                style="border-collapse:collapse;color:#e9e9e9;font-size:0;height:0;padding:0;width:100%">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#f7f7f7" style="border-collapse:collapse;padding:0;width:100%">
                                <table style="border-collapse:collapse;padding:0;width:100%">
                                    <tbody>
                                        <tr>
                                            <td style="border-collapse:collapse;padding:0;width:60%"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <!--==============Mostrar el primer parrafo================-->
                            <td bgcolor="#fff"
                                style="border-bottom:1px solid #e9e9e9;border-collapse:collapse;border-top:1px solid #e9e9e9;padding:8%;width:100%">
                                <table style="border-collapse:collapse;padding:0;text-align:center;width:100%">
                                    <tbody>
                                        <tr>
                                            <td style="border-collapse:collapse;padding:0">
                                                <p style="color:#000;font-size:20px;margin:0;font-weight: bold;">
                                                    Estas denuncias requieren atención ya que tienen 15 dias que no
                                                    cambian de estado
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <thead style="font-size: 14px">
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>Documento De Identificación</th>
                                                            <th>Teléfono</th>
                                                            <th>Fecha Del Suceso</th>
                                                            <th>Departamento</th>
                                                            <th>Ciudad</th>
                                                            <th>Estado</th>
                                                            <th>Creado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach( $denuncias_pendientes as $key => $denuncias)
                                                        <tr>
                                                            <td>{{ $denuncias->users->nombres_segun_dui }}
                                                                {{ $denuncias->users->apellidos_segun_dui }}</td>
                                                            <td>{{ $denuncias->users->documento_identificacion }}</td>
                                                            <td>{{ $denuncias->users->numero_telefono }}</td>
                                                            <td>{{ date('d/m/Y',strtotime($denuncias->fecha_del_suceso)) }}
                                                            </td>
                                                            <td>{{ $denuncias->departamentos->nombre_departamento }}
                                                            </td>
                                                            <td>{{ $denuncias->ciudades->nombre_ciudad }}</td>
                                                            <td>{{ $denuncias->estado_de_la_denuncia }}</td>
                                                            <td>{{ date('d/m/Y',strtotime($denuncias->created_at)) }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                            </td>
                                        </tr>
                                        <br>
                                        <br>
                                        <!--==============Mostrar el boton de link================-->
                                        <tr>
                                            <td style="border-collapse:collapse;padding:10px 0 10px">
                                                <table
                                                    style="border-collapse:collapse;display:inline-block;padding:0;vertical-align:bottom">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="border-collapse:collapse;color:#000000;font-size:20px;font-weight:bold;letter-spacing:2px;min-width:160px;padding:0 16px;text-align:center">
                                                                <a class="boton_personalizado"
                                                                    style="text-decoration: none;padding: 10px;font-weight: 600;font-size:18px;color: #ffffff;background-color: #C82333;border-radius: 6px;">
                                                                    Atención</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <br>
                        </tr>
                        <!--==============Mostrar el segundo parrafo================-->
                        <tr>
                            <td bgcolor="#fff"
                                style="border-collapse:collapse;color:#666666;font-size:12px;padding:10px;text-align:center;width:100%">
                                <center>
                                    <p>© Copyright ASPIDH. All Rights Reserved</p>
                                </center>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</table>