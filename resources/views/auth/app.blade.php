<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Informacion encabezado-->
    <link rel="icon" type="image/png" href="{{asset('frontend/img/icono.ico')}}">
    <title>@yield('title')</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Animation -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
    <!-- Template styles-->
    <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
    <!-- Responsive styles-->
    <link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">

</head>

<body>
    <!--Partials-->
    @yield('content')
    @include('frontend.partials.footer')

    <!-- initialize jQuery Library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>
    <!-- animation -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <!--Script sweetalert-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.14.0/dist/sweetalert2.all.min.js"></script>
    <!-- Template custom -->
    <script src="{{asset('frontend/js/main.js')}}"></script>
    <!--==============Estatus de enviar password================-->
    @if (session('status'))
    <script src="{{asset('frontend/js/correo.js')}}"></script>
    @endif
    <!--=======Estatus Error de email==========-->
    @error('email')
    <script src="{{asset('frontend/js/error_correo.js')}}"></script>
    @endif

</body>

</html>