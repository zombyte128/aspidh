@extends('auth.app')
@section('title','Aspidh - Restablecer Contraseña')
@section('content')
<section id="reset" data-aos="zoom-in">
    <div class="container">
        <div class="row">
            <div class="container-fluid">
                <div id="passw">
                    <!--======Formulario de Reinicio=======-->
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="box_form">
                            <div class="box_login">
                                <center>
                                    <img src="{{asset('frontend/img/logo.png')}}" width="150px">
                                </center>
                            </div>
                            <div class="box_login last">
                                <br>
                                <!--=======email==========-->
                                <div class="form-group">
                                    <input type="email" name="email"
                                        class="form-control  @error('email') is-invalid @enderror" placeholder="Email"
                                        value="{{ old('email') }}" required autocomplete="off" autofocus>
                                </div>
                                <!--=======submit==========-->
                                <div class="form-group">
                                    <center><button class="btn_1" type="submit">Enviar</button></center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--======Imagenes de presentacion=======-->
    <div id="shap">
        <img class="shap1" src="{{asset('frontend/img/widget.png')}}">
        <img class="shap2" src="{{asset('frontend/img/widget2.png')}}">
        <img class="shap3" src="{{asset('frontend/img/widget3.png')}}">
    </div>
</section>
@endsection