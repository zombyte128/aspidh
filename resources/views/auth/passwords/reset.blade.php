@extends('auth.app')
@section('title','Aspidh - Restablecer Contraseña')
@section('content')
<section id="password" data-aos="zoom-in">
    <div class="container">
        <div class="row">
            <div class="container-fluid">
                <div id="passw">
                    <!--===========Formulario para reiniciar contrasena==============-->
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <!--=======token==========-->
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="box_form">
                            <div class="box_login">
                                <center>
                                    <img src="{{asset('frontend/img/logo.png')}}" width="150px">
                                </center>
                            </div>
                            <div class="box_login last">
                                <br>
                                <!--=======email==========-->
                                <div class="form-group">
                                    <input type="email" name="email"
                                        class="form-control @error('email') is-invalid @enderror" placeholder="Email"
                                        required autocomplete="off" autofocus>
                                </div>
                                <!--=======password==========-->
                                <div class="form-group">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        name="password" required placeholder="Nueva Contraseña">
                                </div>
                                <!--==============password repli===============-->
                                <div class="form-group">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        name="password_confirmation" required placeholder="Confirme Contraseña">
                                </div>
                                <!--=======submit==========-->
                                <div class="form-group">
                                    <center><button class="btn_1" type="submit">Cambiar contraseña</button></center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--======Imagenes de presentacion=======-->
    <div id="shap">
        <img class="shap1" src="{{asset('frontend/img/widget.png')}}">
        <img class="shap2" src="{{asset('frontend/img/widget2.png')}}">
        <img class="shap3" src="{{asset('frontend/img/widget3.png')}}">
    </div>
</section>
@endsection