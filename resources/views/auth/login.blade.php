 <!--=======modal login======-->
 <div class="modal fade" id="login" tabindex="-1" role="dialog">
     <div class="modal-dialog modal-dialog-centered modal-notify" role="document">
         <div class="modal-content">
             <div class="modal-body">
                 <!--===========Formulario para login==============-->
                 <form method="POST" action="{{ route('login') }}">
                     @csrf
                     <!--======Imagen login=======-->
                     <div class="box_login">
                         <center>
                             <img src="{{asset('frontend/img/logo.png')}}" width="150px">
                         </center>
                     </div>
                     <div class="box_login last">
                         <!--=======usuario==========-->
                         <div class="form-group">
                             <input type="text" name="username" class="form-control" placeholder="Usuario"
                                 value="{{ old('username') }}" required autocomplete="off">
                         </div>
                         <!--=======password==========-->
                         <div class="form-group">
                             <input type="password" class="form-control" placeholder="Password" name="password" required
                                 autocomplete="current-password">
                             <!--=======link para restaurar contrasena==========-->
                             @if (Route::has('password.request'))
                             <a href="{{ route('password.request') }}">¿olvidaste tu
                                 contraseña?</a>
                             @endif
                         </div>
                         <!--=======submit==========-->
                         <div class="form-group">
                             <center><button type="submit">Iniciar sesión</button></center>
                         </div>
                     </div>
                 </form>
             </div>
         </div>
     </div>
 </div>