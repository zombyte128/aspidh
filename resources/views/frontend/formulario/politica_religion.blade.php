<!--=======Politica y religion=============-->
<div id="politica_religion" style="display:none">
    <div class="row">
        <div class="col-lg-5 col-md-6 mx-auto">
            <div class="box_grafica">
                <h2>PARTICIPACIÓN POLÍTICA Y RELIGIÓN</h2>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                        aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar6"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12 mx-auto">
            <div class="box_general">
                <div class="row">
                    <!--=======radio ejercido su voto==========-->
                    <div class="col-md-6">
                        <center>
                            <p class="sinborder">¿Ha ejercido su derecho al voto?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="ejecicio_derecho_voto" value="Si" id="ejercidosi"
                                autocomplete="off" {{ old('ejecicio_derecho_voto') == 'Si' ? 'checked' : '' }}>
                            <label for="ejercidosi" class="tamano">Si</label>
                            <input type="radio" name="ejecicio_derecho_voto" value="No" id="ejercidono"
                                autocomplete="off" {{ old('ejecicio_derecho_voto') == 'No' ? 'checked' : '' }}>
                            <label for="ejercidono" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======radio practica alguna religion==========-->
                    <div class="col-md-6">
                        <center>
                            <p class="sinborder">¿Practica alguna religión?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="practica_alguna_religion" value="Si" id="practicasi"
                                autocomplete="off" {{ old('practica_alguna_religion') == 'Si' ? 'checked' : '' }}>
                            <label for="practicasi" class="tamano">Si</label>
                            <input type="radio" name="practica_alguna_religion" value="No" id="practicano"
                                autocomplete="off" {{ old('practica_alguna_religion') == 'No' ? 'checked' : '' }}>
                            <label for="practicano" class="tamano">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 mx-auto">
            <div class="box_general">
                <div class="row">
                    <!--=======radio ejercer su voto==========-->
                    <div class="col-md-9 mx-auto">
                        <center>
                            <p class="sinborder">¿Ha tenido dificultades para ejercer su derecho al voto?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="problemas_al_derecho_voto" id="ejercervotosi" value="Si"
                                autocomplete="off">
                            <label for="ejercervotosi" class="tamano">Si</label>
                            <input type="radio" name="problemas_al_derecho_voto" id="ejercervotono" value="No"
                                autocomplete="off">
                            <label for="ejercervotono" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======textarea razondel ejercer su voto==========-->
                    <div class="col-md-10 mx-auto" id="razonejercervoto" style="display: none;">
                        <div class="form-group">
                            <textarea id="limpieza10" placeholder="¿Cuál fue la razón?" name="cual_problema_voto"
                                class="form-control" style="text-align:center;padding-top: 30px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--=======select religion que profesa==========-->
                    <div class="col-md-4 mx-auto">
                        <center>
                            <p class="sinborder">¿Qué religión profesa?</p>
                        </center>
                        <div class="form-group">
                            <select class="form-control select2" name="tipo_religion" id="religion">
                                <option value="">Seleccione religion</option>
                                <option value="Catolica" {{ old('tipo_religion') == 'Catolica' ? 'selected' : '' }}>
                                    Católica</option>
                                <option value="Evangelica" {{ old('tipo_religion') == 'Evangelica' ? 'selected' : '' }}>
                                    Evangélica</option>
                                <option value="Ninguna" {{ old('tipo_religion') == 'Ninguna' ? 'selected' : '' }}>
                                    Ninguna</option>
                                <option value="Otra" {{ old('tipo_religion') == 'Otra' ? 'selected' : '' }}>Otra
                                </option>
                            </select>
                        </div>
                    </div>
                    <!--=======radio participa activamente en politica==========-->
                    <div class="col-md-7">
                        <center>
                            <p class="sinborder">¿Está usted actualmente organizado en alguna asociación u organización “política no
                                partidaria” que participe
                                activamente en política?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="participa_en_politica" value="Si" id="politicasi"
                                autocomplete="off" {{ old('participa_en_politica') == 'Si' ? 'checked' : '' }}>
                            <label for="politicasi" class="tamano">Si</label>
                            <input type="radio" name="participa_en_politica" value="No" id="politicano"
                                autocomplete="off" {{ old('participa_en_politica') == 'No' ? 'checked' : '' }}>
                            <label for="politicano" class="tamano">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras5" type="button" class="float-left atras">Atras</button>
            <button id="siguiente6" type="button" class="float-right siguientes">Siguiente</button>
        </div>
    </div>
</div>