<!--=======Vivienda=============-->
<div id="vivienda" style="display:none">
    <div class="row">
        <div class="col-lg-4 col-md-6 mx-auto">
            <div class="box_grafica">
                <h2>ACCESO A LA VIVIENDA</h2>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                        aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar5"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--=======select estado vivienda actual==========-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control select2" name="vive_actualmente" id="viviendaactual">
                                <option value="">Estado Vivienda Actual</option>
                                <option value="Propio" {{ old('vive_actualmente') == 'Propio' ? 'selected' : '' }}>
                                    Propio</option>
                                <option value="Alquilado"
                                    {{ old('vive_actualmente') == 'Alquilado' ? 'selected' : '' }}>Alquilado</option>
                                <option value="Vivienda familiar"
                                    {{ old('vive_actualmente') == 'Vivienda familiar' ? 'selected' : '' }}>Vivienda
                                    familiar</option>
                                <option value="Otro" {{ old('vive_actualmente') == 'Otro' ? 'selected' : '' }}>Otro
                                </option>
                            </select>
                        </div>
                    </div>
                    <!--=======select con quien vive==========-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control select2" name="con_quien_vive" id="conquienvive">
                                <option value="">¿Con quién vive?</option>
                                <option value="Solo/a" {{ old('con_quien_vive') == 'Solo/a' ? 'selected' : '' }}>Solo/a
                                </option>
                                <option value="Familia nuclear"
                                    {{ old('con_quien_vive') == 'Familia nuclear' ? 'selected' : '' }}>Familia nuclear
                                </option>
                                <option value="Amigos/as" {{ old('con_quien_vive') == 'Amigos/as' ? 'selected' : '' }}>
                                    Amigos/as</option>
                                <option value="Pareja" {{ old('con_quien_vive') == 'Pareja' ? 'selected' : '' }}>Pareja
                                </option>
                                <option value="Otros" {{ old('con_quien_vive') == 'Otros' ? 'selected' : '' }}>Otros
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======select ingreso mensual==========-->
                    <div class="col-md-7 mx-auto">
                        <div class="form-group">
                            <select class="form-control select2" name="ingresos_financieros_mensual_aproximado"
                                id="ingresosmensuales">
                                <option value="">¿cuánto es su ingreso mensual?</option>
                                <option value="Menos de $250"
                                    {{ old('ingresos_financieros_mensual_aproximado') == 'Menos de $250' ? 'selected' : '' }}>
                                    Menos de $250</option>
                                <option value="$251 a $500"
                                    {{ old('ingresos_financieros_mensual_aproximado') == '$251 a $500' ? 'selected' : '' }}>
                                    $251 a $500</option>
                                <option value="$501 a $1,000"
                                    {{ old('ingresos_financieros_mensual_aproximado') == '$501 a $1,000' ? 'selected' : '' }}>
                                    $501 a $1,000</option>
                                <option value="$1,000 a $1,500"
                                    {{ old('ingresos_financieros_mensual_aproximado') == '$1,000 a $1,500' ? 'selected' : '' }}>
                                    $1,000 a $1,500</option>
                                <option value="Mas de $1,500"
                                    {{ old('ingresos_financieros_mensual_aproximado') == 'Mas de $1,500' ? 'selected' : '' }}>
                                    Más de $1,500</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--=======radio programa gubernamental acceso a la vivienda==========-->
                    <div class="col-md-7 mx-auto">
                        <center>
                            <p class="sinborder">¿Ha sido beneficiario/a de un programa gubernamental de acceso a la
                                vivienda?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="beneficiario_programa_acceso_vivienda_gubernamental"
                                id="programasi" value="Si" autocomplete="off">
                            <label for="programasi" class="tamano">Si</label>
                            <input type="radio" name="beneficiario_programa_acceso_vivienda_gubernamental"
                                id="programano" value="No" autocomplete="off">
                            <label for="programano" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======textarea credito otorgado razon==========-->
                    <div class="col-md-5" id="programagubernamental" style="display: none;">
                        <div class="form-group">
                            <textarea id="limpieza9" placeholder="¿Ingrese el nombre del programa?"
                                name="cual_programa_vivienda" class="form-control" rows="3"
                                style="text-align:center;padding-top: 20px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12 mx-auto">
            <div class="box_general">
                <!--=======radio Solicito Credito vivienda==========-->
                <div class="col-md-7 mx-auto">
                    <center>
                        <p class="sinborder">¿Alguna vez ha solicitado un crédito para compra de vivienda?</p>
                    </center>
                    <div class="form-group">
                        <input type="radio" name="solicitud_credito_para_vivienda" id="creditoviviendasi" value="Si"
                            autocomplete="off">
                        <label for="creditoviviendasi" class="tamano">Si</label>
                        <input type="radio" name="solicitud_credito_para_vivienda" value="No" id="creditoviviendano"
                            autocomplete="off">
                        <label for="creditoviviendano" class="tamano">No</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12" style="display:none" id="creditootorgado">
            <div class="box_general">
                <div class="row">
                    <!--=======radio Credito le fue otrogado==========-->
                    <div class="col-md-7 mx-auto">
                        <center>
                            <p class="sinborder">¿El crédito para comparar la vivienda le fue otorgado?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="credito_otorgado" id="creditosi" value="Si" autocomplete="off">
                            <label for="creditosi" class="tamano">Si</label>
                            <input type="radio" name="credito_otorgado" value="No" id="creditono" autocomplete="off">
                            <label for="creditono" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======textarea credito otorgado razon==========-->
                    <div class="col-md-5 mx-auto" id="creditotorgado" style="display: none;">
                        <div class="form-group">
                            <textarea id="limpieza8" placeholder="¿Cual es la razon?" name="justificacion_credito"
                                class="form-control" rows="2" style="text-align:center;padding-top: 30px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras4" type="button" class="float-left atras">Atras</button>
            <button id="siguiente5" type="button"
                class="float-right siguientes">Siguiente</button>
        </div>
    </div>
</div>