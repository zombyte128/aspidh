<!--=======Derechos=============-->
<div id="derechos" style="display:none" class="wow fadeInUp" data-wow-duration="1.5s">
    <div class="row">
        <div class="col-lg-4 col-md-6 mx-auto">
            <div class="box_grafica">
                <h2>VULNERACIÓN DE DERECHOS</h2>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                        aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar7"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box_general">
                <div class="row">
                    <!--=======Radio de centro de estudios==========-->
                    <div class="col-md-12">
                        <center>
                            <p class="sinborder">¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en
                                su Centro de
                                Estudio?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="discriminacion_agrecion_centro_estudio" value="Si" id="sufridosi"
                                autocomplete="off"
                                {{ old('discriminacion_agrecion_centro_estudio') == 'Si' ? 'checked' : '' }}>
                            <label for="sufridosi" class="tamano">Si</label>
                            <input type="radio" name="discriminacion_agrecion_centro_estudio" value="No" id="sufridono"
                                autocomplete="off"
                                {{ old('discriminacion_agrecion_centro_estudio') == 'No' ? 'checked' : '' }}>
                            <label for="sufridono" class="tamano">No</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_general">
                <div class="row">
                    <!--=======Radio de descriminacion trabajo==========-->
                    <div class="col-md-12">
                        <center>
                            <p class="sinborder">¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en
                                su Trabajo?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="discriminacion_agrecion_trabajo" value="Si" id="descriminacionsi"
                                autocomplete="off"
                                {{ old('discriminacion_agrecion_trabajo') == 'Si' ? 'checked' : '' }}>
                            <label for="descriminacionsi" class="tamano">Si</label>
                            <input type="radio" name="discriminacion_agrecion_trabajo" value="No" id="descriminacionno"
                                autocomplete="off"
                                {{ old('discriminacion_agrecion_trabajo') == 'No' ? 'checked' : '' }}>
                            <label for="descriminacionno" class="tamano">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box_general">
                <div class="row">
                    <!--=======Radio de descriminacion defensa de sus derechos==========-->
                    <div class="col-md-12">
                        <center>
                            <p class="sinborder">¿Conoce alguno o varios mecanismos de defensa de sus derechos humanos?
                            </p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="conocimientos_mecanimos_defensa_de_derechos" value="Si"
                                id="mecanismossi" autocomplete="off"
                                {{ old('conocimientos_mecanimos_defensa_de_derechos') == 'Si' ? 'checked' : '' }}>
                            <label for="mecanismossi" class="tamano">Si</label>
                            <input type="radio" name="conocimientos_mecanimos_defensa_de_derechos" value="No"
                                id="mecanismosno" autocomplete="off"
                                {{ old('conocimientos_mecanimos_defensa_de_derechos') == 'No' ? 'checked' : '' }}>
                            <label for="mecanismosno" class="tamano">No</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_general">
                <div class="row">
                    <!--=======Radio de descriminacion salud==========-->
                    <div class="col-md-12 mx-auto">
                        <center>
                            <p class="sinborder">¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en
                                los centros de
                                salud?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="discriminacion_agrecion_centro_salud" value="Si"
                                id="descriminacionsaludsi" autocomplete="off"
                                {{ old('discriminacion_agrecion_centro_salud') == 'Si' ? 'checked' : '' }}>
                            <label for="descriminacionsaludsi" class="tamano">Si</label>
                            <input type="radio" name="discriminacion_agrecion_centro_salud" value="No"
                                id="descriminacionsaludno" autocomplete="off"
                                {{ old('discriminacion_agrecion_centro_salud') == 'No' ? 'checked' : '' }}>
                            <label for="descriminacionsaludno" class="tamano">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box_general">
                <div class="row">
                    <!--=======select Por quien ha recibido discriminacion ==========-->
                    <div class="col-lg-7 col-md-12 mx-auto">
                        <center>
                            <p class="sinborder">¿Por quién ha recibido algún hecho de agresión y/o discriminación?</p>
                        </center>
                        <div class="form-group">
                            <select class="form-control select2" name="por_quien_hecho_de_agresion_discriminacion"
                                id="porquien">
                                <option value="">Seleccione Opcion</option>
                                <option value="Familiar"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Familiar' ? 'selected' : '' }}>
                                    Familiar</option>
                                <option value="Comunidad"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Comunidad' ? 'selected' : '' }}>
                                    Comunidad</option>
                                <option value="Gobierno"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Gobierno' ? 'selected' : '' }}>
                                    Gobierno</option>
                                <option value="Seguridad"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Seguridad' ? 'selected' : '' }}>
                                    Seguridad</option>
                                <option value="Educacion"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Educacion' ? 'selected' : '' }}>
                                    Educación</option>
                                <option value="Salud"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Salud' ? 'selected' : '' }}>
                                    Salud</option>
                                <option value="Religioso"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Religioso' ? 'selected' : '' }}>
                                    Religioso</option>
                                <option value="Laboral"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Laboral' ? 'selected' : '' }}>
                                    Laboral</option>
                                <option value="Otros"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Otros' ? 'selected' : '' }}>
                                    Otros</option>
                                <option value="Ninguno"
                                    {{ old('por_quien_hecho_de_agresion_discriminacion') == 'Ninguno' ? 'selected' : '' }}>
                                    Ninguno</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-12" style="display:none" id="otroporquien2">
                        <textarea name="otro_por_quien_discrimanacion" id="otroporquien" placeholder="¿Quienes?"
                            rows="2" style="text-align:center;padding-top: 30px;"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box_general">
                <div class="row">
                    <!--=======select agresion con mayor frecuencia ==========-->
                    <div class="col-lg-7 col-md-12 mx-auto">
                        <center>
                            <p class="sinborder">¿Qué tipo de agresión ha recibido con mayor frecuencia?</p>
                        </center>
                        <div class="form-group">
                            <select class="form-control select2" name="tipo_agresion_frecuente" id="tipoagresion">
                                <option value="">Seleccione Opcion</option>
                                <option value="Fisica"
                                    {{ old('tipo_agresion_frecuente') == 'Fisica' ? 'selected' : '' }}>Física</option>
                                <option value="Psicologica"
                                    {{ old('tipo_agresion_frecuente') == 'Psicologica' ? 'selected' : '' }}>Psicológica
                                </option>
                                <option value="Economica"
                                    {{ old('tipo_agresion_frecuente') == 'Economica' ? 'selected' : '' }}>Económica
                                </option>
                                <option value="Otros" {{ old('tipo_agresion_frecuente') == 'Otros' ? 'selected' : '' }}>
                                    Otros</option>
                                <option value="Ninguno"
                                    {{ old('tipo_agresion_frecuente') == 'Ninguno' ? 'selected' : '' }}>
                                    Ninguno</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-12" style="display:none" id="otrotipoagresion2">
                        <textarea name="otro_tipo_agresion" id="otrotipoagresion" placeholder="¿Quienes?" rows="2"
                            style="text-align:center;padding-top: 30px;"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras6" type="button" class="float-left atras">Atras</button>
            <button type="button" id="siguiente7" class="float-right siguientes">Enviar</button>
        </div>
    </div>
</div>