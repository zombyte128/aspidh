<!--=======Educacion=============-->
<div id="educacion" style="display:none">
    <div class="row">
        <div class="col-lg-4 col-md-6 mx-auto">
            <div class="box_grafica">
                <h2>EDUCACIÓN</h2>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                        aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar2"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--=======select Grado de estudios aprobado==========-->
                    <div class="col-md-6">
                        <div class="form-group icono">
                            <select class="form-control select2" name="ultimo_grado_estudio_aprobado"
                                id="ultimoestudio">
                                <option value="">Estudios aprobado</option>
                                <option value="1">1° a 3°</option>
                                <option value="4">4° a 6°</option>
                                <option value="7">7° a 9°</option>
                                <option value="Bachillerato">Bachillerato</option>
                                <option value="Universitario">Universitario</option>
                                <option value="Maestria">Maestría</option>
                                <option value="Doctorado">Doctorado</option>
                                <option value="Ninguno">Ninguno</option>
                            </select>
                        </div>
                    </div>
                    <!--=======select Tipo de estudios primarios==========-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control select2" name="lugar_estudio_primario" id="estudioprimario"
                                disabled>
                                <option value="">Tipo de estudios primarios</option>
                                <option value="Publico">Público</option>
                                <option value="Privado">Privado</option>
                                <option value="distancia">A distancia</option>
                                <option value="Ninguno">Ninguno</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======select Tipo de estudios universitarios ==========-->
                    <div class="col-md-7 mx-auto">
                        <div class="form-group">
                            <select class="form-control select2" name="lugar_estudio_universitario"
                                id="estudiouniversidad" disabled>
                                <option value="">Tipo de estudios universitarios</option>
                                <option value="Publico">Público</option>
                                <option value="Privado">Privado</option>
                                <option value="distancia">A distancia</option>
                                <option value="Ninguno">Ninguno</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--=======Radio de estudia actualmente==========-->
                    <div class="col-md-6 mx-auto">
                        <center>
                            <p class="sinborder">¿Estudia actualmente?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="estudia_actualmente" value="Si" id="estudiasi" autocomplete="off">
                            <label for="estudiasi" class="tamano">Si</label>
                            <input type="radio" value="No" name="estudia_actualmente" id="estudiano" autocomplete="off">
                            <label for="estudiano" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======textarea razonestudios==========-->
                    <div class="col-md-6" id="razonestudios" style="display: none;">
                        <div class="form-group magenes">
                            <textarea id="limpieza5" placeholder="¿Cuál fue la razón?" name="razon_estudio"
                                class="form-control" rows="2" style="text-align:center;padding-top: 30px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras1" type="button" class="float-left atras">Atras</button>
            <button id="siguiente2" type="button" class="float-right siguientes">Siguiente</button>
        </div>
    </div>
</div>