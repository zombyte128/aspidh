<!--=======Trabajo=============-->
<div id="trabajo" style="display:none">
    <div class="row">
        <div class="col-lg-4 col-md-6 mx-auto">
            <div class="box_grafica">
                <h2>TRABAJO U OCUPACIÓN</h2>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                        aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="bar4"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box_general">
                <div class="row">
                    <!--=======radio Cuenta con empleo formal==========-->
                    <div class="col-lg-4 col-md-5">
                        <center>
                            <p class="sinborder">¿Actualmente cuenta con empleo formal?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="cuenta_con_empleo_formal" value="Si" id="formalsi"
                                autocomplete="off">
                            <label for="formalsi" class="tamano">Si</label>
                            <input type="radio" name="cuenta_con_empleo_formal" value="No" id="formalno"
                                autocomplete="off">
                            <label for="formalno" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======radio Le han negado trabajo==========-->
                    <div class="col-lg-5 col-md-7">
                        <center>
                            <p class="sinborder">¿Alguna vez le han negado un trabajo debido a su orientación sexual y/o
                                identidad de género?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="negacion_trabajo_por_identidad_genero_orientacion_sexual"
                                value="Si" id="negadosi" autocomplete="off"
                                {{ old('negacion_trabajo_por_identidad_genero_orientacion_sexual') == 'Si' ? 'checked' : '' }}>
                            <label for="negadosi" class="tamano">Si</label>
                            <input type="radio" name="negacion_trabajo_por_identidad_genero_orientacion_sexual"
                                value="No" id="negadono" autocomplete="off"
                                {{ old('negacion_trabajo_por_identidad_genero_orientacion_sexual') == 'No' ? 'checked' : '' }}>
                            <label for="negadono" class="tamano">No</label>
                        </div>
                    </div>
                    <!--=======select su trabajo pertenece al sector ==========-->
                    <div class="col-lg-3 col-md-6 mx-auto">
                        <center>
                            <p class="sinborder">¿Su trabajo pertenece al sector?</p>
                        </center>
                        <div class="form-group">
                            <select class="form-control select2" name="sector_trabajo" id="sectortrabajo" disabled>
                                <option value="">Seleccione Sector</option>
                                <option value="Privado" {{ old('sector_trabajo') == 'Privado' ? 'selected' : '' }}>
                                    Privado</option>
                                <option value="Gobierno" {{ old('sector_trabajo') == 'Gobierno' ? 'selected' : '' }}>
                                    Gobierno</option>
                                <option value="ONG" {{ old('sector_trabajo') == 'ONG' ? 'selected' : '' }}>ONG</option>
                                <option value="Informal" {{ old('sector_trabajo') == 'Informal' ? 'selected' : '' }}>
                                    Informal</option>
                                <option value="Propio" {{ old('sector_trabajo') == 'Propio' ? 'selected' : '' }}>Propio
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 mx-auto">
            <div class="box_general">
                <div class="row">
                    <!--=======radio beneficiado programa gubernamental acceso al trabajo==========-->
                    <div class="col-md-11 mx-auto">
                        <center>
                            <p class="sinborder">¿Ha sido beneficiario/a de algún programa gubernamental de acceso al
                                trabajo?</p>
                        </center>
                        <div class="form-group">
                            <input type="radio" name="beneficiario_programa_acceso_trabajo_gubernamental"
                                id="beneficiadotsi" value="Si" autocomplete="off">
                            <label for="beneficiadotsi" class="tamano">Si</label>
                            <input type="radio" name="beneficiario_programa_acceso_trabajo_gubernamental"
                                id="beneficiadotno" value="No" autocomplete="off">
                            <label for="beneficiadotno" class="tamano">No</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--=======textarea razon del beneficio==========-->
                    <div class="col-md-10 mx-auto" id="raczonbenediciot" style="display: none;">
                        <div class="form-group">
                            <textarea id="limpieza7" placeholder="¿Ingrese el nombre del programa?"
                                name="cual_programa_trabajo" class="form-control"
                                style="text-align:center;padding-top: 10px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!--======Button=========-->
    <div class="row">
        <div class="col-md-12">
            <button id="atras3" type="button" class="float-left atras">Atras</button>
            <button id="siguiente4" type="button"
                class="float-right siguientes">Siguiente</button>
        </div>
    </div>
</div>