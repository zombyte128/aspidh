<!--===========Formulario de registro==========-->
<section id="registro" data-aos="fade-up">
    <div class="container container2">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h1>FORMULARIO ASPIDH</h1>
                <center><img src="{{asset('frontend/img/title.png')}}"></center>
            </div>
        </div>
        <div class="row">
            <div class="container-fluid">
                <!--======Formulario de registro=======-->
                <form class="form-horizontal" method="POST" class="container" id="formulario"
                    action="{{ route('registro')}}" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <!--=======DATOS GENERALES=============-->
                    @include('frontend.formulario.generales')
                    <!--=======EDUCACION=============-->
                    @include('frontend.formulario.educacion')
                    <!--=======ACCESO A LA SALUD=============-->
                    @include('frontend.formulario.salud')
                    <!--=======TRABAJO Y EMPLEO=============-->
                    @include('frontend.formulario.trabajo')
                    <!--=======ACCESO A LA VIVIENDA=============-->
                    @include('frontend.formulario.vivienda')
                    <!--=======POLITICA Y RELIGION=============-->
                    @include('frontend.formulario.politica_religion')
                    <!--=======VULNERACIÓN DE DERECHOS=============-->
                    @include('frontend.formulario.derechos')
                </form>
            </div>
        </div>
    </div>
    <!--======Imagenes de presentacion=======-->
    <div id="shap">
        <img class="shap1" src="{{asset('frontend/img/widget.png')}}">
        <img class="shap2" src="{{asset('frontend/img/widget2.png')}}">
        <img class="shap3" src="{{asset('frontend/img/widget3.png')}}">
    </div>
</section>