<!--===========Header==========-->
<section id="header" data-aos="zoom-in">
    <h3 class="nombre">
        Sistema de denuncias Aspidh</h3>
    <img src="{{asset('frontend/img/logo.png')}}" class="logo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--======Titulo=======-->
                <!--<h1 data-aos="fade-right" data-aos-delay="600">¡BIENVENIDO!</h1>-->
                <h2 data-aos="fade-left" data-aos-delay="600">ASOCIACIÓN ASPIDH</h2>
                <hr data-aos="zoom-in" data-aos-delay="600">
                <!--===========Botones denuncia==========-->
                <center><a data-aos="zoom-in" data-aos-delay="600" href="#" data-toggle="modal" data-target="#login"
                        class="btn registrar">Registrar
                        denuncia</a></center>
            </div>
        </div>
    </div>
</section>