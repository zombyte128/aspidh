<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Debe ser aceptado.',
    'active_url'           => 'No es una URL válida.',
    'after'                => 'Debe ser una fecha posterior a :date.',
    'after_or_equal'       => 'Debe ser una fecha posterior o igual a :date.',
    'alpha'                => 'Solo puede contener letras.',
    'alpha_dash'           => 'Solo puede contener letras, números, guiones y guiones bajos.',
    'alpha_num'            => 'Solo puede contener letras y números.',
    'array'                => 'Debe ser un array.',
    'before'               => 'Debe ser una fecha anterior a :date.',
    'before_or_equal'      => 'Debe ser una fecha anterior o igual a :date.',
    'between'              => [
        'numeric' => 'Debe ser un valor entre :min y :max.',
        'file'    => 'Debe pesar entre :min y :max kilobytes.',
        'string'  => 'Debe contener entre :min y :max caracteres.',
        'array'   => 'Debe contener entre :min y :max elementos.',
    ],
    'boolean'              => 'Debe ser verdadero o falso.',
    'confirmed'            => 'Confirmación de :attribute no coincide.',
    'date'                 => 'No corresponde con una fecha válida.',
    'date_equals'          => 'Debe ser una fecha igual a :date.',
    'date_format'          => 'No corresponde con el formato de fecha :format.',
    'different'            => ':attribute y :other deben ser diferentes.',
    'digits'               => 'Debe ser un número de :digits dígitos.',
    'digits_between'       => 'Debe contener entre :min y :max dígitos.',
    'dimensions'           => 'Tiene dimensiones de imagen inválidas.',
    'distinct'             => 'Tiene un valor duplicado.',
    'email'                => 'Debe ser una dirección de correo válida.',
    'ends_with'            => 'Debe finalizar con alguno de los siguientes valores: :values',
    'exists'               => 'Seleccionado no existe.',
    'file'                 => 'Debe ser un archivo.',
    'filled'               => 'Debe tener un valor.',
    'gt'                   => [
        'numeric' => ':attribute debe ser mayor a :value.',
        'file'    => ':attribute debe pesar más de :value kilobytes.',
        'string'  => ':attribute debe contener más de :value caracteres.',
        'array'   => ':attribute debe contener más de :value elementos.',
    ],
    'gte'                  => [
        'numeric' => ':attribute debe ser mayor o igual a :value.',
        'file'    => ':attribute debe pesar :value o más kilobytes.',
        'string'  => ':attribute debe contener :value o más caracteres.',
        'array'   => ':attribute debe contener :value o más elementos.',
    ],
    'image'                => 'El campo :attribute debe ser una imagen.',
    'in'                   => 'El campo :attribute es inválido.',
    'in_array'             => 'El campo :attribute no existe en :other.',
    'integer'              => 'El campo :attribute debe ser un número entero.',
    'ip'                   => 'El campo :attribute debe ser una dirección IP válida.',
    'ipv4'                 => 'El campo :attribute debe ser una dirección IPv4 válida.',
    'ipv6'                 => 'El campo :attribute debe ser una dirección IPv6 válida.',
    'json'                 => 'El campo :attribute debe ser una cadena de texto JSON válida.',
    'lt'                   => [
        'numeric' => 'El campo :attribute debe ser menor a :value.',
        'file'    => 'El archivo :attribute debe pesar menos de :value kilobytes.',
        'string'  => 'El campo :attribute debe contener menos de :value caracteres.',
        'array'   => 'El campo :attribute debe contener menos de :value elementos.',
    ],
    'lte'                  => [
        'numeric' => 'Debe ser menor o igual a :value.',
        'file'    => 'Debe pesar :value o menos kilobytes.',
        'string'  => 'Debe contener :value o menos caracteres.',
        'array'   => 'Debe contener :value o menos elementos.',
    ],
    'max'                  => [
        'numeric' => 'No debe ser mayor a :max.',
        'file'    => 'No debe pesar más de :max kilobytes.',
        'string'  => 'No debe contener más de :max caracteres.',
        'array'   => 'No debe contener más de :max elementos.',
    ],
    'mimes'                => 'El campo :attribute debe ser un archivo de tipo: :values.',
    'mimetypes'            => 'El campo :attribute debe ser un archivo de tipo: :values.',
    'min'                  => [
        'numeric' => 'Debe ser al menos :min.',
        'file'    => 'Debe pesar al menos :min kilobytes.',
        'string'  => 'Debe contener al menos :min caracteres.',
        'array'   => 'Debe contener al menos :min elementos.',
    ],
    'not_in'               => 'El campo :attribute seleccionado es inválido.',
    'not_regex'            => 'El formato del campo :attribute es inválido.',
    'numeric'              => 'Debe ser un número.',
    'password'             => 'La contraseña es incorrecta.',
    'present'              => 'El campo :attribute debe estar presente.',
    'regex'                => 'El formato del campo :attribute es inválido.',
    'required'             => 'Es obligatorio.',
    'required_if'          => 'El campo :attribute es obligatorio cuando el campo :other es :value.',
    'required_unless'      => 'El campo :attribute es requerido a menos que :other se encuentre en :values.',
    'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es obligatorio cuando :values están presentes.',
    'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de los campos :values están presentes.',
    'same'                 => 'Los campos :attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'Debe ser :size.',
        'file'    => 'Debe pesar :size kilobytes.',
        'string'  => 'Debe contener :size caracteres.',
        'array'   => 'Ddebe contener :size elementos.',
    ],
    'starts_with'          => 'Debe comenzar con uno de los siguientes valores: :values',
    'string'               => 'Debe ser una cadena de caracteres.',
    'timezone'             => 'Debe ser una zona horaria válida.',
    'unique'               => 'Ya está en uso.',
    'uploaded'             => 'No se pudo subir.',
    'url'                  => 'El formato del campo es inválido.',
    'uuid'                 => 'Debe ser un UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
