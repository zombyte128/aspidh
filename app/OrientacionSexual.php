<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrientacionSexual extends Model
{
    protected $table = "orientacion_sexual";
    
    public function users()
    {
        return $this->hasMany('App\User');
    }
}