<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    //Redireccionar si esta logeado
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()&& Auth::user()->role->id==1)
         {
            return redirect()->route('super.dashboard');

        }elseif (Auth::guard($guard)->check()&& Auth::user()->role->id==2) 
        {
            return redirect()->route('admin.dashboard');

        }elseif (Auth::guard($guard)->check()&& Auth::user()->role->id==3)
        {
            return redirect()->route('user.home')->with('iniciada', 'Sesion iniciada');

        }elseif (Auth::guard($guard)->check()&& Auth::user()->role->id==4)
        {
            return redirect()->route('donante.dashboard');

        }else{
            return $next($request);
        }

    }
}
