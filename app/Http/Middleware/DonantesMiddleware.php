<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class DonantesMiddleware
{
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->role->id==4)
        {
        return $next($request);
        }else{
         return redirect()->route('login');
        }
    }
}
