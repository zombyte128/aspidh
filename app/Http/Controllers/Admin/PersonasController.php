<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\OrientacionSexual;
use App\Departamentos;
use App\TipoGenero;
use Carbon\Carbon;
use App\User;
use App\Denuncias;

class PersonasController extends Controller
{
    //Vista personas
    public function index()
    {
        $personas = User::where('role_id','3')->orderBy('created_at','desc')->get();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('admin.personas.index',compact('personas','total_denuncias_pendientes'));
    }

    //Vista crear nueva persona
    public function create()
    {
        $departamentos = Departamentos::all();
        $genero = TipoGenero::all();
        $orientacion = OrientacionSexual::all();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('admin.personas.create', compact('departamentos','genero','orientacion','total_denuncias_pendientes'));
    }

    //Funcion para Agregar un nuevo dato
    public function store(Request $request)
    {
        $this->validate($request,[
            //Datos Generales//
            'nombres_segun_dui' => 'required',
            'apellidos_segun_dui' => 'required',
            //'email' => 'unique:users',
            'fecha_nacimiento' => 'date',
            //Seguridad//
            'password' => 'required',
            'username' => 'required|unique:users',
        ]);

        $personas = new User();
        //Datos Generales//
        $personas->role_id = 3;
        $personas->pais_id = 1;
        $personas->nombres_segun_dui = $request->nombres_segun_dui;
        $personas->apellidos_segun_dui = $request->apellidos_segun_dui;
        $personas->edad = $request->edad;
        $personas->fecha_nacimiento = Carbon::parse($request->fecha_nacimiento)->format('y-m-d');
        $personas->tipo_genero_id = $request->tipo_genero_id;
        $personas->numero_telefono = $request->numero_telefono;
        $personas->email = $request->email;
        $personas->orientacion_sexual_id = $request->orientacion_sexual_id;
        $personas->departamento_id = $request->departamento_id;
        $personas->ciudad_id = $request->ciudad_id;
        $personas->direccion_completa = $request->direccion_completa;
        $personas->nombres_emergencia = $request->nombres_emergencia;
        $personas->telefono_emergencia = $request->telefono_emergencia;
        $personas->direccion_emergencia = $request->direccion_emergencia;
        $personas->documento_identificacion = $request->documento_identificacion;
        $personas->nombre_social = $request->nombre_social;
        $personas->redes_sociales_emergencia = $request->redes_sociales_emergencia;
        $personas->redes_social_facebook = $request->redes_social_facebook;
        $personas->redes_social_twitter = $request->redes_social_twitter;
        $personas->redes_social_instagram = $request->redes_social_instagram;
        //Seguridad//
        $personas->password=bcrypt($request->password);
        $personas->username=$request->username;
        //Educacion//
        $personas->ultimo_grado_estudio_aprobado = $request->ultimo_grado_estudio_aprobado;
        $personas->estudia_actualmente = $request->estudia_actualmente;
        $personas->razon_estudio = $request->razon_estudio;
        $personas->lugar_estudio_primario = $request->lugar_estudio_primario;
        $personas->lugar_estudio_universitario = $request->lugar_estudio_universitario;
        //Acceso a la salud// 
        $personas->afiliado_seguro_social = $request->afiliado_seguro_social;
        $personas->utiliza_servcios_de_salud = $request->utiliza_servcios_de_salud;
        $personas->centro_medico_asiste = $request->centro_medico_asiste;
        $personas->beneficiario_programa_salud_gubernamental = $request->beneficiario_programa_salud_gubernamental;
        $personas->cual_programa_salud = $request->cual_programa_salud;
        //Trabajo empleo//
        $personas->cuenta_con_empleo_formal = $request->cuenta_con_empleo_formal;
        $personas->negacion_trabajo_por_identidad_genero_orientacion_sexual = $request->negacion_trabajo_por_identidad_genero_orientacion_sexual;
        $personas->beneficiario_programa_acceso_trabajo_gubernamental = $request->beneficiario_programa_acceso_trabajo_gubernamental;
        $personas->cual_programa_trabajo = $request->cual_programa_trabajo;
        $personas->sector_trabajo = $request->sector_trabajo;
        //Acceso a la vivienda// 
        $personas->vive_actualmente = $request->vive_actualmente;
        $personas->con_quien_vive = $request->con_quien_vive;
        $personas->ingresos_financieros_mensual_aproximado = $request->ingresos_financieros_mensual_aproximado;
        $personas->solicitud_credito_para_vivienda = $request->solicitud_credito_para_vivienda;
        $personas->credito_otorgado = $request->credito_otorgado;
        $personas->justificacion_credito = $request->justificacion_credito;
        $personas->beneficiario_programa_acceso_vivienda_gubernamental = $request->beneficiario_programa_acceso_vivienda_gubernamental;
        $personas->cual_programa_vivienda = $request->cual_programa_vivienda;
        //Politica y religion// 
        $personas->ejecicio_derecho_voto = $request->ejecicio_derecho_voto;
        $personas->problemas_al_derecho_voto = $request->problemas_al_derecho_voto;
        $personas->cual_problema_voto = $request->cual_problema_voto;
        $personas->participa_en_politica = $request->participa_en_politica;
        $personas->practica_alguna_religion = $request->practica_alguna_religion;
        $personas->tipo_religion = $request->tipo_religion;
        //Vulneración de derechos//
        $personas->discriminacion_agrecion_centro_estudio = $request->discriminacion_agrecion_centro_estudio;
        $personas->discriminacion_agrecion_trabajo = $request->discriminacion_agrecion_trabajo;
        $personas->discriminacion_agrecion_centro_salud = $request->discriminacion_agrecion_centro_salud;
        $personas->por_quien_hecho_de_agresion_discriminacion = $request->por_quien_hecho_de_agresion_discriminacion;
        $personas->otro_por_quien_discrimanacion = $request->otro_por_quien_discrimanacion;
        $personas->tipo_agresion_frecuente = $request->tipo_agresion_frecuente;
        $personas->otro_tipo_agresion = $request->otro_tipo_agresion;
        $personas->conocimientos_mecanimos_defensa_de_derechos = $request->conocimientos_mecanimos_defensa_de_derechos;
        $personas->save();
        Toastr::success('Usuario Creado Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('admin.personas.index');
    }

    public function edit($id)
    {
        $departamentos = Departamentos::all();
        $genero = TipoGenero::all();
        $orientacion = OrientacionSexual::all();
        $personas = User::find($id);
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('admin.personas.edit',compact('personas','departamentos','genero','orientacion','total_denuncias_pendientes'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            //Datos Generales//
            'nombres_segun_dui' => 'required',
            'apellidos_segun_dui' => 'required',
            //'email' => 'unique:users,email,'.$id,
            'fecha_nacimiento' => 'date',
            //Seguridad//
            'username' => 'required|unique:users,username,'.$id,
        ]);
        $personas = User::find($id);
        //Datos Generales//
        $personas->role_id = 3;
        $personas->pais_id = 1;
        $personas->nombres_segun_dui = $request->nombres_segun_dui;
        $personas->apellidos_segun_dui = $request->apellidos_segun_dui;
        $personas->edad = $request->edad;
        $personas->fecha_nacimiento = Carbon::parse($request->fecha_nacimiento)->format('y-m-d');
        $personas->tipo_genero_id = $request->tipo_genero_id;
        $personas->numero_telefono = $request->numero_telefono;
        $personas->email = $request->email;
        $personas->orientacion_sexual_id = $request->orientacion_sexual_id;
        $personas->departamento_id = $request->departamento_id;
        $personas->ciudad_id = $request->ciudad_id;
        $personas->direccion_completa = $request->direccion_completa;
        $personas->nombres_emergencia = $request->nombres_emergencia;
        $personas->telefono_emergencia = $request->telefono_emergencia;
        $personas->direccion_emergencia = $request->direccion_emergencia;
        $personas->documento_identificacion = $request->documento_identificacion;
        $personas->nombre_social = $request->nombre_social;
        $personas->redes_sociales_emergencia = $request->redes_sociales_emergencia;
        $personas->redes_social_facebook = $request->redes_social_facebook;
        $personas->redes_social_twitter = $request->redes_social_twitter;
        $personas->redes_social_instagram = $request->redes_social_instagram;
        //Seguridad//
        if($request->password != ""):
        $personas->password=bcrypt($request->password);
        endif;        
        $personas->username=$request->username;
        //Educacion//
        $personas->ultimo_grado_estudio_aprobado = $request->ultimo_grado_estudio_aprobado;
        $personas->estudia_actualmente = $request->estudia_actualmente;
        $personas->razon_estudio = $request->razon_estudio;
        $personas->lugar_estudio_primario = $request->lugar_estudio_primario;
        $personas->lugar_estudio_universitario = $request->lugar_estudio_universitario;
        //Acceso a la salud// 
        $personas->afiliado_seguro_social = $request->afiliado_seguro_social;
        $personas->utiliza_servcios_de_salud = $request->utiliza_servcios_de_salud;
        $personas->centro_medico_asiste = $request->centro_medico_asiste;
        $personas->beneficiario_programa_salud_gubernamental = $request->beneficiario_programa_salud_gubernamental;
        $personas->cual_programa_salud = $request->cual_programa_salud;
        //Trabajo empleo//
        $personas->cuenta_con_empleo_formal = $request->cuenta_con_empleo_formal;
        $personas->negacion_trabajo_por_identidad_genero_orientacion_sexual = $request->negacion_trabajo_por_identidad_genero_orientacion_sexual;
        $personas->beneficiario_programa_acceso_trabajo_gubernamental = $request->beneficiario_programa_acceso_trabajo_gubernamental;
        $personas->cual_programa_trabajo = $request->cual_programa_trabajo;
        $personas->sector_trabajo = $request->sector_trabajo;
        //Acceso a la vivienda// 
        $personas->vive_actualmente = $request->vive_actualmente;
        $personas->con_quien_vive = $request->con_quien_vive;
        $personas->ingresos_financieros_mensual_aproximado = $request->ingresos_financieros_mensual_aproximado;
        $personas->solicitud_credito_para_vivienda = $request->solicitud_credito_para_vivienda;
        $personas->credito_otorgado = $request->credito_otorgado;
        $personas->justificacion_credito = $request->justificacion_credito;
        $personas->beneficiario_programa_acceso_vivienda_gubernamental = $request->beneficiario_programa_acceso_vivienda_gubernamental;
        $personas->cual_programa_vivienda = $request->cual_programa_vivienda;
        //Politica y religion// 
        $personas->ejecicio_derecho_voto = $request->ejecicio_derecho_voto;
        $personas->problemas_al_derecho_voto = $request->problemas_al_derecho_voto;
        $personas->cual_problema_voto = $request->cual_problema_voto;
        $personas->participa_en_politica = $request->participa_en_politica;
        $personas->practica_alguna_religion = $request->practica_alguna_religion;
        $personas->tipo_religion = $request->tipo_religion;
        //Vulneración de derechos//
        $personas->discriminacion_agrecion_centro_estudio = $request->discriminacion_agrecion_centro_estudio;
        $personas->discriminacion_agrecion_trabajo = $request->discriminacion_agrecion_trabajo;
        $personas->discriminacion_agrecion_centro_salud = $request->discriminacion_agrecion_centro_salud;
        $personas->por_quien_hecho_de_agresion_discriminacion = $request->por_quien_hecho_de_agresion_discriminacion;
        $personas->otro_por_quien_discrimanacion = $request->otro_por_quien_discrimanacion;
        $personas->tipo_agresion_frecuente = $request->tipo_agresion_frecuente;
        $personas->otro_tipo_agresion = $request->otro_tipo_agresion;
        $personas->conocimientos_mecanimos_defensa_de_derechos = $request->conocimientos_mecanimos_defensa_de_derechos;
        $personas->save();
        Toastr::warning('Usuario Editado Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();
    }

    //Mantenimiento eliminar Persona
    public function destroy($id)
    {
        User::find($id)->delete();
        Toastr::error('Usuario Eliminado Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();

    }
}