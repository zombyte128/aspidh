<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Denuncias;

class DenunciasFinalizadasController extends Controller
{
    //Vista para denuncias finalizadas
    public function index()
    {
        $finalizadas = Denuncias::where('estado_de_la_denuncia','Finalizada')->orderBy('created_at','desc')->get();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('admin.denuncias.finalizadas.index',compact('finalizadas','total_denuncias_pendientes'));
    }
    
    //Mantenimiento eliminar denuncia finalizada
    public function destroy($id)
    {
        Denuncias::find($id)->delete();
        Toastr::error('Denuncia Eliminada Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();
    }
}