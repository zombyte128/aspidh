<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Denuncias;

class DenunciasCompletadasController extends Controller
{
    //vista denuncia completada
    public function index()
    {
        $completadas = Denuncias::where('estado_de_la_denuncia','Completada')->orderBy('created_at','desc')->get();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('admin.denuncias.completadas.index',compact('completadas','total_denuncias_pendientes'));
    }

    //mantenimiento cambiar estado finalizada
    public function update(Request $request, $id)
    {
        $completadas = Denuncias::find($id);
        $completadas->estado_de_la_denuncia  = "Finalizada";
        $completadas->save();
        Toastr::info('Denuncia Finalizada Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('admin.denuncias-finalizadas.index');
    }
}