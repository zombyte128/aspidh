<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ciudades;

class FiltroDepartamentoController extends Controller
{
    //Funcion para filtrar ciudades por departamentos
    public function Departament($id){
        return Ciudades::where('departamento_id',$id)->get();
    }
}
 