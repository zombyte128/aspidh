<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Departamentos;
use Carbon\Carbon;
use App\Denuncias;
use App\User; 
use PDF;

class DenunciasController extends Controller
{
    //Vista para crear nueva denuncia
    public function create(Request $request)
    {
        //Buscando el usuario
        $validation = User::where('email',$request->email)->orwhere('id',$request->email)->where('role_id', 3)->count();
        if($validation == 0){
            Toastr::error('No Coincide con ningun usuario' ,'¡Error!');
            return redirect()->back();
        }else{
        //Mostrando vista si existe el usuario
        $idusuario = User::where('email',$request->email)->orwhere('id',$request->email)->where('role_id', 3)->first();
        $departamentos = Departamentos::all();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('admin.denuncias.denuncia.create',compact('idusuario','departamentos','total_denuncias_pendientes'));
        }
    }

    //Mantenimiento para crear denuncia
    public function store(Request $request)
    {
        $this->validate($request,[
            //Datos Generales//
            'fecha_del_suceso' => 'required|date',
            'fecha_cuando_se_reaizo_la_denuncia_formal' => 'date',
            'quien_registra_denuncia' => 'required',
            'breve_relato_del_hecho_consecuencias' => 'required',
            'foto_dui_frente' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_dui_atras' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_nit_frente' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_nit_atras' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_acta' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_denuncia' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_otro' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_otro2' => 'max:5000|mimes:jpeg,jpg,png',
            'documento_dui' => 'mimes:pdf',
            'documento_nit' => 'mimes:pdf',
            'documento_acta' => 'mimes:pdf',
            'documento_denuncia' => 'mimes:pdf',
            'documento_otro' => 'mimes:pdf',
        ]);

        $denuncia = new Denuncias();
        //Datos Generales//
        $denuncia->user_id = $request->user_id;
        $denuncia->estado_de_la_denuncia = "Pendiente";
        $denuncia->fecha_del_suceso = Carbon::parse($request->fecha_del_suceso)->format('y-m-d');
        $denuncia->donde_ocurrio = $request->donde_ocurrio;
        $denuncia->quien_registra_denuncia = $request->quien_registra_denuncia;
        $denuncia->breve_relato_del_hecho_consecuencias = $request->breve_relato_del_hecho_consecuencias;
        $denuncia->tipo_principal_violacion_abuso = $request->tipo_principal_violacion_abuso;
        $denuncia->pais_id = 1;
        $denuncia->departamento_id = $request->departamento_id;
        $denuncia->ciudad_id = $request->ciudad_id;
        $denuncia->direccion_completa = $request->direccion_completa;
        $denuncia->tipo_secundario_violacion_abuso = $request->tipo_secundario_violacion_abuso;
        $denuncia->indique_cantidad_agresores = $request->indique_cantidad_agresores;
        $denuncia->forma_de_agrecion = $request->forma_de_agrecion;
        $denuncia->informacion_del_autor_del_hecho = $request->informacion_del_autor_del_hecho;
        $denuncia->realizo_una_denuncia_formal = $request->realizo_una_denuncia_formal;
        //Si la fecha viene como null
        if( $request->fecha_cuando_se_reaizo_la_denuncia_formal == NULL)
        {
        $denuncia->fecha_cuando_se_reaizo_la_denuncia_formal = NULL;
        }else{
        $denuncia->fecha_cuando_se_reaizo_la_denuncia_formal = Carbon::parse($request->fecha_cuando_se_reaizo_la_denuncia_formal)->format('y-m-d');
        }
        $denuncia->realizo_una_denuncia_formal = $request->realizo_una_denuncia_formal;
        $denuncia->documentacion_respaldatoria = $request->documentacion_respaldatoria;
        //dui frente
        $dui1=$request->file('foto_dui_frente');
        if (isset($dui1))
        {
        $currentDate1 = Carbon::now()->toDateString();
        $imagename1 = $currentDate1.'-'. uniqid() .'.'. $dui1->getClientOriginalName();
        $dui1->move('documentos/dui/',$imagename1);
        $denuncia->foto_dui_frente = $imagename1;
        }
        //dui atras
        $dui2=$request->file('foto_dui_atras');
        if (isset($dui2))
        {
        $currentDate2 = Carbon::now()->toDateString();
        $imagename2 = $currentDate2.'-'. uniqid() .'.'. $dui2->getClientOriginalName();
        $dui2->move('documentos/dui/',$imagename2);
        $denuncia->foto_dui_atras = $imagename2;
        }
        //dui documento
        $documentodui=$request->file('documento_dui');
        if (isset($documentodui))
        {
        $currentDate11 = Carbon::now()->toDateString();
        $imagename11 = $currentDate11.'-'. uniqid() .'.'. $documentodui->getClientOriginalName();
        $documentodui->move('documentos/dui/',$imagename11);
        $denuncia->documento_dui = $imagename11;
        }
        //nit frente
        $nit1=$request->file('foto_nit_frente');
        if (isset($nit1))
        {
        $currentDate3 = Carbon::now()->toDateString();
        $imagename3 = $currentDate3.'-'. uniqid() .'.'. $nit1->getClientOriginalName();
        $nit1->move('documentos/nit/',$imagename3);
        $denuncia->foto_nit_frente = $imagename3;
        }
        //nit atras
        $nit2=$request->file('foto_nit_atras');
        if (isset($nit2))
        {
        $currentDate4 = Carbon::now()->toDateString();
        $imagename4 = $currentDate4.'-'. uniqid() .'.'. $nit2->getClientOriginalName();
        $nit2->move('documentos/nit/',$imagename4);
        $denuncia->foto_nit_atras = $imagename4;
        }
        //nit documento
        $documentonit=$request->file('documento_nit');
        if (isset($documentonit))
        {
        $currentDate22 = Carbon::now()->toDateString();
        $imagename22 = $currentDate22.'-'. uniqid() .'.'. $documentonit->getClientOriginalName();
        $documentonit->move('documentos/nit/',$imagename22);
        $denuncia->documento_nit = $imagename22;
        }
        //acta
        $acta=$request->file('foto_acta');
        if (isset($acta))
        {
        $currentDate5 = Carbon::now()->toDateString();
        $imagename5 = $currentDate5.'-'. uniqid() .'.'. $acta->getClientOriginalName();
        $acta->move('documentos/acta_denuncia/',$imagename5);
        $denuncia->foto_acta = $imagename5;
        }
        //acta documento
        $documentoacta=$request->file('documento_acta');
        if (isset($documentoacta))
        {
        $currentDate33 = Carbon::now()->toDateString();
        $imagename33 = $currentDate33.'-'. uniqid() .'.'. $documentoacta->getClientOriginalName();
        $documentoacta->move('documentos/acta_denuncia/',$imagename33);
        $denuncia->documento_acta = $imagename33;
        }
        //denuncia
        $denuncias=$request->file('foto_denuncia');
        if (isset($denuncias))
        {
        $currentDate6 = Carbon::now()->toDateString();
        $imagename6 = $currentDate6.'-'. uniqid() .'.'. $denuncias->getClientOriginalName();
        $denuncias->move('documentos/acta_denuncia/',$imagename6);
        $denuncia->foto_denuncia = $imagename6;
        }
        //denuncia documento
        $documentodenuncia=$request->file('documento_denuncia');
        if (isset($documentodenuncia))
        {
        $currentDate44 = Carbon::now()->toDateString();
        $imagename44 = $currentDate33.'-'. uniqid() .'.'. $documentodenuncia->getClientOriginalName();
        $documentodenuncia->move('documentos/acta_denuncia/',$imagename33);
        $denuncia->documento_denuncia = $imagename44;
        }
        //Otros
        $otros=$request->file('foto_otro');
        if (isset($otros))
        {
        $currentDate7 = Carbon::now()->toDateString();
        $imagename7 = $currentDate7.'-'. uniqid() .'.'. $otros->getClientOriginalName();
        $otros->move('documentos/otros/',$imagename7);
        $denuncia->foto_otro = $imagename7;
        }
        //Otros 2
        $otros2=$request->file('foto_otro2');
        if (isset($otros2))
        {
        $currentDate8 = Carbon::now()->toDateString();
        $imagename8 = $currentDate8.'-'. uniqid() .'.'. $otros2->getClientOriginalName();
        $otros2->move('documentos/otros/',$imagename8);
        $denuncia->foto_otro2 = $imagename8;
        }
        //otro documento
        $documentootro=$request->file('documento_otro');
        if (isset($documentootro))
        {
        $currentDate55 = Carbon::now()->toDateString();
        $imagename55 = $currentDate55.'-'. uniqid() .'.'. $documentootro->getClientOriginalName();
        $documentootro->move('documentos/otros/',$imagename55);
        $denuncia->documento_otro = $imagename55;
        }
        $denuncia->save();
        $subject = "Denuncia Aspidh";
        $for = $request->user_id;
        Mail::send('vendor.notifications.denuncias',compact('denuncia'),function($msj) use ($subject,$for)
            { 
                $msj->from("no-reply@aspidh.org","ASPIDH");
                $msj->subject($subject);
                //$msj->to("david.chavarria2@gmail.com");
                $msj->to("aspidh.arcoiris.trans@gmail.com","camila.aspidh@gmail.com");
            });
        Toastr::success('Denuncia Enviada Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('admin.denuncias-pendientes.index');
    }

        //funcion para exportar a pdf
        public function createPDF($id) 
        {
            // consulta que busca ek id de la denuncia que se exportara
            $seguimiento = Denuncias::find($id);
            // aqui se declara que usaremos la vista para generar el pdf
            $pdf = PDF::loadView('admin.denuncias.pdf.denunciaspdf', compact('seguimiento'));
      
            // metodo que sirve para descargar la exportacion
            return $pdf->download('denuncia-'.$seguimiento->users->documento_identificacion.'.pdf');
        }
}