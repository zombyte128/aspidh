<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
   
    use AuthenticatesUsers;

    protected $redirectTo;

    public function __construct(Request $request)
    {
        //hacemos un request de los parametros que se envian por el formulario del login
        $username = $request->username;
        $password = $request->password;

        //hacemos una condicion donde hacemos un filtro y validamos si viene un email en el mismo request
        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
        //esta consulta verifica que si los parametros que se envian en el request en el formulario es igual a lo que tiene el usuario en la base de datos
         Auth::attempt(['email' => $username, 'password' => $password]);
        }else{
        //esta consulta verifica que si los parametros que se envian en el request en el formulario es igual a lo que tiene el usuario en la base de datos
        Auth::attempt(['username' => $username, 'password' => $password]);
        }

        //nota: el helper Auth::check() se encarga de validar los datos que se comparan el if de arriba por ende no es necesario agregar mas condiciones en el if de las rutas 

        if (Auth::check() && Auth::user()->role->id==1)
        {
        $this->redirectTo=route('super.dashboard');
        }elseif(Auth::check() && Auth::user()->role->id==2)
        {
        $this->redirectTo=route('admin.dashboard');
        }elseif(Auth::check() && Auth::user()->role->id==3)
        {
        $this->redirectTo=route('user.home');
        }
        elseif(Auth::check() && Auth::user()->role->id==4)
        {
        $this->redirectTo=route('donante.dashboard');
        }else{
         $this->redirectTo=route('login');
        }
        $this->middleware('guest')->except('logout');
    }


}