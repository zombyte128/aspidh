<?php

namespace App\Http\Controllers\Donante;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//General
use App\User;
use App\TipoGenero;
use App\Charts\TotalPersonasGeneros;
use App\OrientacionSexual;
use App\Charts\TotalPersonasOrientacionSexual;
use App\Departamentos;
use App\Charts\TotalPersonasRegistradasDepartamento;
use App\Charts\RegistroPersonasMensual;
//educacion
use App\Charts\TotalPersonasEstudianActualmente;
use App\Charts\UltimoGradoEstudioAprovado;
use App\Charts\LugarEstudioPrimario;
use App\Charts\LugarEstudioUniversitario;
//salud
use App\Charts\AfiliacionSeguroSocialPersonas;
use App\Charts\UtilizacionServiciosSaludPersonas;
use App\Charts\BeneficiariosProgramaSaludGobierno;
//empleo
use App\Charts\CuentaEmpleoFormalPersonas;
use App\Charts\SectorTrabajoPersonas;
use App\Charts\NegacionTrabajoPersonas;
use App\Charts\BeneficiarioProgramaTrabajoGobierno;
//vivienda
use App\Charts\TipoVivienda;  
use App\Charts\ConQuienVive;
use App\Charts\IngresosFinancieronAproximados;
use App\Charts\CreditoVivienda;
use App\Charts\CreditoOtorgado;
use App\Charts\AyudaProgramaGobiernoVivienda;
//politica y religion
use App\Charts\EjercioDerechoVoto;
use App\Charts\ProblemasDerechoVoto;
use App\Charts\ParticipaPolitica;
use App\Charts\PracticaReligion;
use App\Charts\TipoReligion;
//derecho
use App\Charts\DiscriminacionAbusoCentroEstudio;
use App\Charts\DiscriminacionAbusoTrabajo;
use App\Charts\DiscriminacionCentroSalud;
use App\Charts\EntidadesDiscriminacion;
use App\Charts\TipoAgresionFrecuente;
use App\Charts\ConocimientoMecanismosDefensaDerechos;


class RegistroController extends Controller
{
    //Vista graficas general
    public function general(Request $request)
    {
        //Algoritmo para obtener el id de los departamentos de el salvador
        $dp = Departamentos::where('pais_id','1')->get();
        $p=[];
        foreach($dp as $k => $v){
            $p[] = $v->id;
        }
        //total de personas
        $total_personas = User::where('role_id','3')->count();
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"
        ];

        //grafica para mostrar la cantidad de personas en cada genero 
        $tipo_genero = TipoGenero::all();
        $nombres_generos = [];
        $total_personas_generos = [];
        foreach($tipo_genero as $llave => $valor)
        {
            $nombres_generos[] = $valor->nombre_tipo_genero;
            $total_personas_generos[] = User::where('tipo_genero_id',$valor->id)->count();
        }
        $count_cisgenero = User::where('tipo_genero_id',1)->count();
        $count_transgenero = User::where('tipo_genero_id',2)->count();
        $count_transexual= User::where('tipo_genero_id',3)->count();
        $count_binario=User::where('tipo_genero_id',4)->count();
        $suma_total_generos_registrados = $count_cisgenero+$count_transgenero+$count_transexual+$count_binario;
        if($suma_total_generos_registrados == 0)
        {
            $re_cisgenero = 0;
            $re_transgenero =0;
            $re_transexual =0;
            $re_binario =0;

        }else{
            $re_cisgenero = $count_cisgenero/$suma_total_generos_registrados*100;
        $re_transgenero = $count_transgenero/$suma_total_generos_registrados*100;
        $re_transexual = $count_transexual/$suma_total_generos_registrados*100;
        $re_binario = $count_binario/$suma_total_generos_registrados*100;
        }
        $chart3 = new TotalPersonasGeneros;
        $chart3->title('Identidad De Género');
        $chart3->labels(['Cisgénero ->'.round($re_cisgenero).'%','Transgénero ->'.round($re_transgenero).'%','Transexual ->'.round($re_transexual).'%','No Binario ->'.round($re_binario).'%']);
        $chart3->dataset('Total de personas = '.$total_personas,'pie',($total_personas_generos))->color($borderColors);
        ////////////////////////////////////////////////////////
        
        //grafica para mostrar la cantidad de personas en cada orientacion 
        $orientacion = OrientacionSexual::all();
        $nombre_orientacion = [];
        $total_personas_orientaion = [];
        foreach( $orientacion as $or => $fill)
        {
            $nombre_orientacion[] = $fill->nombre_orientacion_sexual;
            $total_personas_orientacion[] = User::where('orientacion_Sexual_id',$fill->id)->count();
        }
        $count_hetero = User::where('orientacion_sexual_id',1)->count();
        $count_lesbiana = User::where('orientacion_sexual_id',2)->count();
        $count_gay = User::where('orientacion_sexual_id',3)->count();
        $count_bisexual = User::where('orientacion_sexual_id',4)->count();
        $suma_total_orientacion_sexual = $count_hetero+$count_lesbiana+$count_gay+$count_bisexual;
        if($suma_total_orientacion_sexual == 0)
        {
            $re_hetero =0;
            $re_lesbiana=0;
            $re_gay=0;
            $re_bisexual=0;
            
        }else{
            $re_hetero = $count_hetero/$suma_total_orientacion_sexual*100;
            $re_lesbiana = $count_lesbiana/$suma_total_orientacion_sexual*100;
            $re_gay = $count_gay/$suma_total_orientacion_sexual*100;
            $re_bisexual =  $count_bisexual/$suma_total_orientacion_sexual*100;
        }
        $chart4 = new TotalPersonasOrientacionSexual;
        $chart4->title('Orientación Sexual');
        $chart4->labels(['Heterosexual ->'.round($re_hetero).'%','Lesbiana ->'.round($re_lesbiana).'%','Gay ->'.round($re_gay).'%','Bisexual ->'.round($re_bisexual).'%']);
        $chart4->dataset('Total de personas = '.$total_personas,'pie',($total_personas_orientacion))->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar la cantidad de personas en cada departamento 
        $total_personas_departamento = Departamentos::whereIn('id',$p)->get();
        $name_departamento = [];
        $total_personas_d = [];
        foreach($total_personas_departamento as $tdp)
        {
            $name_departamento[] = $tdp->nombre_departamento;
            $total_personas_d[] = User::where('departamento_id',$tdp->id)->count();
        }
        $chart5 = new TotalPersonasRegistradasDepartamento;
        $chart5->title('Total De Personas Registradas Por Departamentos');
        $chart5->labels($name_departamento);
        $chart5->dataset('Total de personas = '.$total_personas,'column',($total_personas_d));
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas registadas por mes
        $enero = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','01')->count();
        $febrero = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','02')->count();
        $marzo = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','03')->count();
        $abril = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','04')->count();
        $mayo = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','05')->count();
        $junio = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','06')->count();
        $julio = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','07')->count();
        $agosto = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','08')->count();
        $septiembre = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','09')->count();
        $octubre = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','10')->count();
        $noviembre = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','11')->count();
        $diciembre = User::where('role_id','3')->whereYear('created_at','=',$request->anios)->whereMonth('created_at','=','12')->count();
        $chart6 = new RegistroPersonasMensual;
        $chart6->title('Total de Personas registradas por mes');
        $chart6->labels(['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']);
        $chart6->dataset('meses','line',[$enero,$febrero,$marzo,$abril,$mayo,$junio,$julio,$agosto,$septiembre,$octubre,$noviembre,$diciembre])->color('rgba(255, 99, 132, 1.0)');
        ////////////////////////////////////////////////////////

        return view('donantes.registro.datos_generales',compact('chart3','chart4','chart5','chart6'));
    }

    //Vista graficas educacion
    public function educacion(Request $request)
    {
        //total de personas
        $total_personas = User::where('role_id','3')->count();
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];

        //grafica para mostrar total de personas que estudian actualmente
        $estudian_actualmente_si = User::where('estudia_actualmente','Si')->count();
        $estudian_actualmente_no = User::where('estudia_actualmente','No')->count();
        $otro_motivo =User::where('razon_estudio','!=','NULL')->count();
        $porcent = $estudian_actualmente_si+$estudian_actualmente_no+$otro_motivo;
        if( $porcent == 0)
        {
            $re_no = 0;
            $re_si =0;
            $re_otro =0;
        }else{
            $re_no = $estudian_actualmente_no / $porcent *100;
            $re_si = $estudian_actualmente_si/$porcent*100;
            $re_otro = $otro_motivo/$porcent*100;
        }
        $chart7 = new TotalPersonasEstudianActualmente;
        $chart7->title('¿Estudia actualmente?');
        $chart7->labels(['Si Estudia -> '.round($re_si).'%','No Estudia -> '.round($re_no).'%',round($re_otro).'%'.' <- otro motivo']);
        $chart7->dataset('datos','pie',[$estudian_actualmente_si,$estudian_actualmente_no,$otro_motivo])->color(['rgba(255, 99, 132, 1.0)','purple']);
        ////////////////////////////////////////////////////////
        
        //grafica para mostrar total de personas que han aprobado estudios
        $primero_tercero = User::where('ultimo_grado_estudio_aprobado','1° a 3°')->count();
        $cuarto_sexto = User::where('ultimo_grado_estudio_aprobado','4° a 6°')->count();
        $septimo_noveno = User::where('ultimo_grado_estudio_aprobado','7° a 9')->count();
        $bachillerato = User::where('ultimo_grado_estudio_aprobado','Bachillerato')->count();
        $universitario =  User::where('ultimo_grado_estudio_aprobado','Universitario')->count();
        $maestria = User::where('ultimo_grado_estudio_aprobado','Maestria')->count();
        $doctorado = User::where('ultimo_grado_estudio_aprobado','Doctorado')->count();
        $ninguno = User::where('ultimo_grado_estudio_aprobado','Ninguno')->count();
        $total_suma_estudio_aprobado = $primero_tercero+$cuarto_sexto+$septimo_noveno+$bachillerato+$universitario+$maestria+$doctorado+$ninguno;
        if( $total_suma_estudio_aprobado == 0)
        {
            $re_primero_tercero = 0;
            $re_cuarto_sexto = 0;
            $re_septimo_noveno = 0;
            $re_bachillerato = 0;
            $re_universitario = 0;
            $re_maestria = 0;
            $re_doctorado = 0;
            $re_ninguno = 0;
        }else{
            $re_primero_tercero = $primero_tercero/$total_suma_estudio_aprobado*100;
        $re_cuarto_sexto = $cuarto_sexto/$total_suma_estudio_aprobado*100;
        $re_septimo_noveno = $septimo_noveno/$total_suma_estudio_aprobado*100;
        $re_bachillerato = $bachillerato/$total_suma_estudio_aprobado*100;
        $re_universitario = $universitario/$total_suma_estudio_aprobado*100;
        $re_maestria = $maestria/$total_suma_estudio_aprobado*100;
        $re_doctorado = $doctorado/$total_suma_estudio_aprobado*100;
        $re_ninguno = $ninguno/$total_suma_estudio_aprobado*100;
        } 
        $chart8 = new UltimoGradoEstudioAprovado;
        $chart8->title('Estudios aprobados');
        $chart8->labels(['1° - 3° ->'.round($re_primero_tercero).'%','4° - 6° ->'.round($re_cuarto_sexto).'%','7° - 9° ->'.round($re_septimo_noveno).'%','Bachillerato ->'.round($re_bachillerato).'%','Universitario ->'.round($re_universitario).'%','Maestria ->'.round($re_maestria).'%','Doctorado ->'.round($re_doctorado).'%','Ningun Estudio ->'.round($re_ninguno).'%']);
        $chart8->dataset('datos','pie',[$primero_tercero,$cuarto_sexto,$septimo_noveno,$bachillerato,$universitario,$maestria,$doctorado,$ninguno])->color($borderColors);
        ////////////////////////////////////////////////////////
        
        //grafica para mostrar lugar de estudio primario
        $lugar_estudio_primario_publico = User::where('lugar_estudio_primario','Publico')->count();
        $lugar_estudio_primario_privado = User::where('lugar_estudio_primario','Privado')->count();
        $lugar_estudio_primario_distancia = User::where('lugar_estudio_primario','A distancia')->count();
        $lugar_estudio_primario_ninguno = User::where('lugar_estudio_primario','Ninguno')->count();
        $total_suma_estudio_primario = $lugar_estudio_primario_publico+$lugar_estudio_primario_privado+$lugar_estudio_primario_distancia+$lugar_estudio_primario_ninguno;
        if( $total_suma_estudio_primario == 0)
        {
            $re_primario_publico =0;
            $re_primario_privado =0;
            $re_primario_distancia =0;
            $re_primario_ninguno =0;

        }else{
            $re_primario_publico =  $lugar_estudio_primario_publico/$total_suma_estudio_primario*100;
            $re_primario_privado = $lugar_estudio_primario_privado/$total_suma_estudio_primario*100;
            $re_primario_distancia =  $lugar_estudio_primario_distancia/$total_suma_estudio_primario*100;
            $re_primario_ninguno = $lugar_estudio_primario_ninguno/$total_suma_estudio_primario*100;
        }
        $chart9 = new LugarEstudioPrimario;
        $chart9->title('Tipo de estudios primarios');
        $chart9->labels(['Publico ->'.round($re_primario_publico).'%','Privado ->'.round($re_primario_privado).'%','A distancia ->'.round($re_primario_distancia).'%','Ninguno ->'.round($re_primario_ninguno).'%']);
        $chart9->dataset('datos','pie',[$lugar_estudio_primario_publico,$lugar_estudio_primario_privado,$lugar_estudio_primario_distancia,$lugar_estudio_primario_ninguno])->color($borderColors);
        ////////////////////////////////////////////////////////
        
        //grafica para mostrar lugar de estudio universitario
        $lugar_estudio_universitario_publico = User::where('lugar_estudio_universitario','Publico')->count();
        $lugar_estudio_universitario_privado = User::where('lugar_estudio_universitario','Privado')->count();
        $lugar_estudio_universitario_distancia =  User::where('lugar_estudio_universitario','distancia')->count();
        $lugar_estudio_universitario_ninguno = User::where('lugar_estudio_universitario','Ninguno')->count();
        $total_suma_estudio_universitario = $lugar_estudio_universitario_publico+$lugar_estudio_universitario_privado+$lugar_estudio_universitario_distancia+$lugar_estudio_universitario_ninguno;
        if($total_suma_estudio_universitario == 0)
        {
            $re_universitario_publico =0;
            $re_universitario_privado =0;
            $re_universitario_distancia =0;
            $re_universitario_ninguno =0;
        }else{
            $re_universitario_publico = $lugar_estudio_universitario_publico/$total_suma_estudio_universitario*100; 
            $re_universitario_privado = $lugar_estudio_universitario_privado/$total_suma_estudio_universitario*100;
            $re_universitario_distancia = $lugar_estudio_universitario_distancia/$total_suma_estudio_universitario*100;
            $re_universitario_ninguno =  $lugar_estudio_universitario_ninguno/$total_suma_estudio_universitario*100;
        }
        $chart10 = new LugarEstudioUniversitario;
        $chart10->title('Tipo de estudios universitarios');
        $chart10->labels(['Publico ->'.round($re_universitario_publico).'%','Privado ->'.round($re_universitario_privado).'%','distancia ->'.round($re_universitario_distancia).'%','Ninguno ->'.round($re_universitario_ninguno).'%']);
        $chart10->dataset('datos','pie',[$lugar_estudio_universitario_publico,$lugar_estudio_universitario_privado,$lugar_estudio_universitario_distancia,$lugar_estudio_universitario_ninguno])->color($borderColors);
        ////////////////////////////////////////////////////////

        return view('donantes.registro.educacion',compact('chart7','chart8','chart9','chart10'));
    }

    //Vista graficas salud
    public function salud(Request $request)
    {
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"
        ];

        //consulta para mostras total de personas afiliadas seguro social
        $afiliado_seguro_social_si = User::where('afiliado_seguro_social','Si')->count();
        $afiliado_seguro_social_no = User::where('afiliado_seguro_social','No')->count();
        $suma_afiliacion = $afiliado_seguro_social_si+$afiliado_seguro_social_no;
        if(  $suma_afiliacion == 0)
        {
            $re_seguro_social_si = 0;
            $re_seguro_social_no = 0;
        }else{
            $re_seguro_social_si = $afiliado_seguro_social_si/$suma_afiliacion*100;
            $re_seguro_social_no =  $afiliado_seguro_social_no/$suma_afiliacion*100;
        }
        //grafica para mostrar total de personas afiliadas seguro social
        $chart11 = new AfiliacionSeguroSocialPersonas;
        $chart11->title('¿Es afiliado del Seguro Social?');
        $chart11->labels(['Si ->'.round($re_seguro_social_si).'%','No ->'.round($re_seguro_social_no).'%']);
        $chart11->dataset('datos','pie',[$afiliado_seguro_social_si,$afiliado_seguro_social_no])->color($borderColors);

        $utiliza_sevicios_salud_si = User::where('utiliza_servcios_de_salud','Si')->count();
        $utiliza_sevicios_salud_no = User::where('utiliza_servcios_de_salud','No')->count();
        $suma_respuesta_servicios_salud = $utiliza_sevicios_salud_si+$utiliza_sevicios_salud_no;
        if( $suma_respuesta_servicios_salud == 0)
        {
            $re_servicios_salud_si =0;
            $re_seguro_social_no = 0;
        }else{
            $re_servicios_salud_si = $utiliza_sevicios_salud_si/$suma_respuesta_servicios_salud*100;
        $re_seguro_social_no = $utiliza_sevicios_salud_no/$suma_respuesta_servicios_salud*100;
        }
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas que utilizan los servicios de salud
        $chart12 = new UtilizacionServiciosSaludPersonas;
        $chart12->title('¿Utiliza o ha utilizado los servicios de los centros de salud?');
        $chart12->labels(['Si ->'.round($re_servicios_salud_si).'%','No ->'.round($re_seguro_social_no).'%']);
        $chart12->dataset('datos','pie',[$utiliza_sevicios_salud_si,$utiliza_sevicios_salud_no])->color($borderColors);
        $tipo_centro_medico_hospital_publico = User::where('centro_medico_asiste','Hospital Publico')->count();
        $tipo_centro_medico_hospital_Privado = User::where('centro_medico_asiste','Hospital Privado')->count();
        $tipo_centro_medico_clinica_comunal = User::where('centro_medico_asiste','Clinica comunal')->count();
        $tipo_centro_medico_unidad_salud = User::where('centro_medico_asiste','Unidad de Salud')->count();
        $suma_centros_salud = $tipo_centro_medico_hospital_publico+$tipo_centro_medico_hospital_Privado+$tipo_centro_medico_clinica_comunal+$tipo_centro_medico_unidad_salud;
        if($suma_centros_salud==0)
        {
            $re_hospital_publico =0;
            $re_hospital_privado =0;
            $re_clinica_comunal =0;
            $re_unidad_salud =0;
        }else{
        $re_hospital_publico = $tipo_centro_medico_hospital_publico/$suma_centros_salud*100;
        $re_hospital_privado = $tipo_centro_medico_hospital_Privado/$suma_centros_salud*100;
        $re_clinica_comunal = $tipo_centro_medico_clinica_comunal/$suma_centros_salud*100;
        $re_unidad_salud = $tipo_centro_medico_unidad_salud/$suma_centros_salud*100;
        }
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas los diferentes tipos de servicios de salud
        $chart13 = new UtilizacionServiciosSaludPersonas;
        $chart13->title('¿A qué centro médico ha asistido o asiste?');
        $chart13->labels(['Hospital Publico ->'.round($re_hospital_publico).'%','Hospital Privado ->'.round($re_hospital_privado).'%','Clinica Comunal ->'.round($re_clinica_comunal).'%','Unidad De Salud ->'.round($re_unidad_salud).'%']);
        $chart13->dataset('datos','pie',[$tipo_centro_medico_hospital_publico,$tipo_centro_medico_hospital_Privado,$tipo_centro_medico_clinica_comunal,$tipo_centro_medico_unidad_salud])->color($borderColors);
        $benefiiario_programa_salud_gobierno_si = User::where('beneficiario_programa_salud_gubernamental','Si')->count();
        $benefiiario_programa_salud_gobierno_no = User::where('beneficiario_programa_salud_gubernamental','No')->count();
        $suma_beneficiario = $benefiiario_programa_salud_gobierno_si+$benefiiario_programa_salud_gobierno_no;
        if($suma_beneficiario==0)
        {
            $re_salud_gobierno_si =0;
            $re_salud_gobierno_no =0;
        }else{
            $re_salud_gobierno_si =  $benefiiario_programa_salud_gobierno_si/$suma_beneficiario*100;
        $re_salud_gobierno_no = $benefiiario_programa_salud_gobierno_no/$suma_beneficiario*100;
        }
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas Beneficiadas Por Algun Programa De Salud Gubernamental
        $chart14 = new BeneficiariosProgramaSaludGobierno;
        $chart14->title('¿Ha sido beneficiario/a de algún programa gubernamental de acceso a la salud?');
        $chart14->labels(['Si ->'.round($re_salud_gobierno_si).'%','No ->'.round($re_salud_gobierno_no).'%']);
        $chart14->dataset('datos','pie',[$benefiiario_programa_salud_gobierno_si,$benefiiario_programa_salud_gobierno_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        return view('donantes.registro.acceso_salud',compact('chart11','chart12','chart13','chart14'));
    }

    //vista graficas trabajo
    public function trabajo(Request $request)
    {
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"
        ];

        //grafica para mostrar total de personas Que Cuentan Con Empleo Formal
        $cuenta_empleo_formal_si = User::where('cuenta_con_empleo_formal','Si')->count();
        $cuenta_empleo_formal_no = User::where('cuenta_con_empleo_formal','No')->count();
        $suma_empleo = $cuenta_empleo_formal_si+$cuenta_empleo_formal_no;
       if($suma_empleo ==0)
        {
            $re_empleo_si =0;
            $re_empleo_no =0;
        }else{
            $re_empleo_si = $cuenta_empleo_formal_si/$suma_empleo*100;
            $re_empleo_no = $cuenta_empleo_formal_no/$suma_empleo*100;
        }
        $chart15 = new CuentaEmpleoFormalPersonas;
        $chart15->title('¿Actualmente cuenta con empleo formal?');
        $chart15->labels(['Si ->'.round($re_empleo_si).'%','No ->'.round($re_empleo_no).'%']);
        $chart15->dataset('datos','pie',[ $cuenta_empleo_formal_si,$cuenta_empleo_formal_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas Que Trabajan En Diferentes Sectores De Trabajo
        $tipo_empleo_privado = User::where('sector_trabajo','Privado')->count();
        $tipo_empleo_gobierno = User::where('sector_trabajo','Gobierno')->count();
        $tipo_empleo_ong = User::where('sector_trabajo','ONG')->count();
        $tipo_empleo_informal = User::where('sector_trabajo','Informal')->count();
        $tipo_empleo_propio = User::where('sector_trabajo','Propio')->count();
        $suma_empleo = $tipo_empleo_privado+$tipo_empleo_gobierno+$tipo_empleo_ong+$tipo_empleo_informal+$tipo_empleo_propio;
        if($suma_empleo==0)
        {
            $re_empleo_privado =0;
            $re_empleo_gobierno =0;
            $re_empleo_ong =0;
            $re_empleo_informal =0;
            $re_empleo_propio =0;
        }else{
            $re_empleo_privado = $tipo_empleo_privado/$suma_empleo*100;
            $re_empleo_gobierno = $tipo_empleo_gobierno/$suma_empleo*100;
            $re_empleo_ong =  $tipo_empleo_ong/$suma_empleo*100;
            $re_empleo_informal = $tipo_empleo_informal/$suma_empleo*100;
            $re_empleo_propio =  $tipo_empleo_propio/$suma_empleo*100;
        }
        $chart16 = new SectorTrabajoPersonas;
        $chart16->title('¿Su trabajo pertenece al sector?');
        $chart16->labels(['Privado ->'.round($re_empleo_privado).'%','Gobierno ->'.round($re_empleo_gobierno).'%','ONG ->'.round($re_empleo_ong).'%','Informal ->'.round($re_empleo_informal).'%','Propio ->'.round($re_empleo_propio).'%']);
        $chart16->dataset('datos','pie',[$tipo_empleo_privado,$tipo_empleo_gobierno,$tipo_empleo_ong, $tipo_empleo_informal,$tipo_empleo_propio])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas Que Le Han Negado Empleo Por Su Orientación Sexual
        $negacion_trabajo_orientacion_si = User::where('negacion_trabajo_por_identidad_genero_orientacion_sexual','Si')->count();
        $negacion_trabajo_orientacion_no = User::where('negacion_trabajo_por_identidad_genero_orientacion_sexual','No')->count();
        $suma_negacion_trabajo = $negacion_trabajo_orientacion_si+$negacion_trabajo_orientacion_no;
        if($suma_negacion_trabajo==0)
        {
            $re_orientacion_si =0;
            $re_orientacion_no =0;

        }else{
            $re_orientacion_si = $negacion_trabajo_orientacion_si/$suma_negacion_trabajo*100;
            $re_orientacion_no = $negacion_trabajo_orientacion_no/$suma_negacion_trabajo*100;
        }
        $chart17 = new NegacionTrabajoPersonas;
        $chart17->title('¿Alguna vez le han negado un trabajo debido a su orientación sexual y/o identidad de género?');
        $chart17->labels(['Si ->'.round($re_orientacion_si).'%','No ->'.round($re_orientacion_no).'%']);
        $chart17->dataset('datos','pie',[$negacion_trabajo_orientacion_si,$negacion_trabajo_orientacion_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas Que Que Han Tenido Ayuda De Algun Programa De Acceso Al Trabajo Gubernamental
        $ayuda_gobierno_empleo_si = User::where('beneficiario_programa_acceso_trabajo_gubernamental','Si')->count();
        $ayuda_gobierno_empleo_no = User::where('beneficiario_programa_acceso_trabajo_gubernamental','No')->count();
        $suma_ayuda = $ayuda_gobierno_empleo_si+$ayuda_gobierno_empleo_no;
        if($suma_ayuda == 0)
        {
            $re_ayuda_gobierno_si =0;
            $re_ayuda_gobierno_no =0;
        }else{
            $re_ayuda_gobierno_si = $ayuda_gobierno_empleo_si/$suma_ayuda*100;
            $re_ayuda_gobierno_no = $ayuda_gobierno_empleo_no/$suma_ayuda*100;
        }
        $chart18 = new BeneficiarioProgramaTrabajoGobierno;
        $chart18->title('¿Ha sido beneficiario/a de algún programa gubernamental de acceso al trabajo?');
        $chart18->labels(['Si ->'.round($re_ayuda_gobierno_si).'%','No ->'.round($re_ayuda_gobierno_no).'%']);
        $chart18->dataset('datos','pie',[$ayuda_gobierno_empleo_si,$ayuda_gobierno_empleo_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        return view('donantes.registro.trabajo_ocupacion',compact('chart15','chart16','chart17','chart18'));
    }

    //vistas graficas vivienda
    public function vivienda(Request $request)
    {
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"
        ];

        //grafica para mostrar total de personas que Tipo De Vivienda  Donde Viven Las Personas Registradas
        $tipo_vivienda_propio = User::where('vive_actualmente','Propio')->count();
        $tipo_vivienda_alquilado = User::where('vive_actualmente','Alquilado')->count();
        $tipo_vivienda_vivienda_familiar = User::where('vive_actualmente','Vivienda familiar')->count();
        $tipo_vivienda_otro = User::where('vive_actualmente','Otro')->count();
        $total_vivienda = $tipo_vivienda_propio+$tipo_vivienda_alquilado+$tipo_vivienda_vivienda_familiar+$tipo_vivienda_otro;
        if($total_vivienda ==0)
        {
            $re_vivienda_propio =0;
            $re_vivienda_alquilado =0;
            $re_vivienda_familiar =0;
            $re_vivienda_otro =0;

        }else{
            $re_vivienda_propio = $tipo_vivienda_propio/$total_vivienda*100;
        $re_vivienda_alquilado = $tipo_vivienda_alquilado/$total_vivienda*100;
        $re_vivienda_familiar = $tipo_vivienda_vivienda_familiar/$total_vivienda*100;
        $re_vivienda_otro = $tipo_vivienda_otro/$total_vivienda*100;
        }
        $chart19 = new TipoVivienda;
        $chart19->title('Estado Vivienda Actual');
        $chart19->labels(['Propio ->'.round($re_vivienda_propio).'%','Alquilado ->'.round($re_vivienda_alquilado).'%','Vivienda familiar ->'.round($re_vivienda_familiar).'%','Otro ->'.round($re_vivienda_otro).'%']);
        $chart19->dataset('datos','pie',[$tipo_vivienda_propio,$tipo_vivienda_alquilado,$tipo_vivienda_vivienda_familiar,$tipo_vivienda_otro])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas Que Muestra Con Quienes Viven En Su Hogar
        $vive_solo = User::where('con_quien_vive','Solo/a')->count();
        $vive_familia_nuclear = User::where('con_quien_vive','Familia nuclear')->count();
        $vive_amigos = User::where('con_quien_vive','Amigos/as')->count();
        $vive_pareja = User::where('con_quien_vive','Pareja')->count();
        $vive_otro = User::where('con_quien_vive','Otros')->count();
        $suma_vive = $vive_solo+$vive_familia_nuclear+$vive_amigos+$vive_pareja+$vive_otro;
        if($suma_vive == 0)
        {
            $re_vive_solo =0;
            $re_familia_nuclear =0;
            $re_amigos =0;
            $re_pareja =0;
            $re_otro = 0;
        }else{
        $re_vive_solo = $vive_solo/$suma_vive*100;
        $re_familia_nuclear = $vive_familia_nuclear/$suma_vive*100;
        $re_amigos = $vive_amigos/$suma_vive*100;
        $re_pareja = $vive_pareja/$suma_vive*100;
        $re_otro = $vive_otro/$suma_vive*100;
        }
        $chart20 = new ConQuienVive;
        $chart20->title('¿Con quién vive?');
        $chart20->labels(['Solo/a ->'.round($re_vive_solo).'%','Familia nuclear ->'.round($re_familia_nuclear).'%','Amigos/as ->'.round($re_amigos).'%','Pareja ->'.round($re_pareja).'%','Otro ->'.round($re_otro).'%']);
        $chart20->dataset('datos','pie',[$vive_solo,$vive_familia_nuclear,$vive_amigos,$vive_pareja,$vive_otro])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de personas Que de porcentaje financiero aproximado de las personas registradas
        $financiero_250 = User::where('ingresos_financieros_mensual_aproximado','Menos de $250')->count();
        $financiero_500 = User::where('ingresos_financieros_mensual_aproximado','$251 a $500')->count();
        $financiero_1000 = User::where('ingresos_financieros_mensual_aproximado','$501 a $1,000')->count();
        $financiero_1500 = User::where('ingresos_financieros_mensual_aproximado','$1,000 a $1,500')->count();
        $financiero_mas_1500 = User::where('ingresos_financieros_mensual_aproximado','Más de $1,500')->count();
        $suma_financiera = $financiero_250 + $financiero_500 + $financiero_1000 + $financiero_1500 + $financiero_mas_1500;
        if($suma_financiera==0)
        {
            $re_250 =0;
            $re_500 =0;
            $re_1000 =0;
            $re_1500 =0;
            $re_mas_1500 =0;
        }else{
            $re_250 = $financiero_250/$suma_financiera*100;
        $re_500 = $financiero_500/$suma_financiera*100;
        $re_1000 = $financiero_1000/$suma_financiera*100;
        $re_1500 = $financiero_1500/$suma_financiera*100;
        $re_mas_1500 = $financiero_mas_1500/$suma_financiera*100;
        }
        $chart21 = new IngresosFinancieronAproximados;
        $chart21->title('¿cuánto es su ingreso mensual?*');
        $chart21->labels(['Menos de $250 ->'.round($re_250).'%','$251 a $500 ->'.round($re_500).'%','$501 a $1,000 ->'.round($re_1000).'%','$1,000 a $1,500 ->'.round($re_1500).'%','Más de $1,500 ->'.round($re_mas_1500).'%']);
        $chart21->dataset('datos','pie', [$financiero_250,$financiero_500,$financiero_1000,$financiero_1500,$financiero_mas_1500])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de porcentaje de personas que han solicitado credito para vivienda
        $credito_vivienda_si = User::where('solicitud_credito_para_vivienda', 'Si')->count();
        $credito_vivienda_no = User::where('solicitud_credito_para_vivienda', 'No')->count();
        $credito_total= $credito_vivienda_si+ $credito_vivienda_no;
        if($credito_total==0)
        {
            $re_vivienda_si=0;
            $re_vivienda_no=0;
        }else{
            $re_vivienda_si= $credito_vivienda_si/$credito_total*100;
            $re_vivienda_no= $credito_vivienda_no/$credito_total*100;
        }
        $chart22 = new CreditoVivienda;
        $chart22->title('¿Alguna vez ha solicitado un crédito para compra de vivienda?');
        $chart22->labels(['Si ->'.round($re_vivienda_si).'%', 'No ->'.round($re_vivienda_no).'%']);
        $chart22->dataset('datos', 'pie', [$credito_vivienda_si, $credito_vivienda_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Solicitaron Un Credito Para Vivienda y Resivieron Una Respuesta
        $credito_otorgado_si = User::where('credito_otorgado','Si')->count();
        $credito_otorgado_no = User::where('credito_otorgado','No')->count();
        $suma_otorgado = $credito_otorgado_no+$credito_otorgado_si;
        if( $suma_otorgado == 0)
        {
            $re_credito_si = 0;
            $re_credito_no = 0;
        }else{
            $re_credito_si = $credito_otorgado_si/$suma_otorgado*100;
            $re_credito_no = $credito_otorgado_no/$suma_otorgado*100;
        }
        $chart23 = new  CreditoOtorgado;
        $chart23->title('Personas Que Solicitaron Un Credito Para Vivienda y Recibieron Una Respuesta');
        $chart23->labels(['Si ->'.round($re_credito_si).'%','No ->'.round($re_credito_no).'%']);
        $chart23->dataset('datos','pie',[$credito_otorgado_si,$credito_otorgado_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Han Resivido Ayuda Gubernamental De Acceso A La Vivienda
        $ayuda_programa_vivienda_gobierno_si = User::where('beneficiario_programa_acceso_vivienda_gubernamental','Si')->count();
        $ayuda_programa_vivienda_gobierno_no = User::where('beneficiario_programa_acceso_vivienda_gubernamental','No')->count();
        $suma_ayuda_vivianda = $ayuda_programa_vivienda_gobierno_si+$ayuda_programa_vivienda_gobierno_no;
        if($suma_ayuda_vivianda==0)
        {
            $re_suma_vivienda_si =0;
            $re_suma_vivienda_no =0;
        }else{
            $re_suma_vivienda_si = $ayuda_programa_vivienda_gobierno_si/$suma_ayuda_vivianda*100;
        $re_suma_vivienda_no = $ayuda_programa_vivienda_gobierno_no/$suma_ayuda_vivianda*100;

        }
        $chart24 = new AyudaProgramaGobiernoVivienda;
        $chart24->title('¿Ha sido beneficiario/a de un programa gubernamental de acceso a la vivienda?');
        $chart24->labels(['Si ->'.round($re_suma_vivienda_si).'%','No ->'.round($re_suma_vivienda_no).'%']);
        $chart24->dataset('datos','pie',[$ayuda_programa_vivienda_gobierno_si,$ayuda_programa_vivienda_gobierno_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        return view('donantes.registro.acceso_vivienda',compact('chart19','chart20','chart21','chart22','chart23','chart24'));
    }

    //Vista graficas politica y religion
    public function politica(Request $request)
    {
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"
        ];

        //grafica para mostrar total de Porcentaje De Personas Que Han Ejercido El Derecho Al Voto
        $ejercicio_derecho_voto_si = User::where('ejecicio_derecho_voto','Si')->count();
        $ejercicio_derecho_voto_no = User::where('ejecicio_derecho_voto','No')->count();
        $suma_derecho_voto = $ejercicio_derecho_voto_si+$ejercicio_derecho_voto_no;
        if($suma_derecho_voto == 0)
        {
            $re_derecho_voto_si =0;
            $re_derecho_voto_no =0;

        }else{
            $re_derecho_voto_si = $ejercicio_derecho_voto_si/$suma_derecho_voto*100;
            $re_derecho_voto_no = $ejercicio_derecho_voto_no/$suma_derecho_voto*100;

        }
        $chart25 = new EjercioDerechoVoto;
        $chart25->title('¿Ha ejercido su derecho al voto?');
        $chart25->labels(['Si ->'.round($re_derecho_voto_si).'%','No ->'.round($re_derecho_voto_no).'%']);
        $chart25->dataset('datos','pie',[$ejercicio_derecho_voto_si,$ejercicio_derecho_voto_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Han Tenido Problemas Al Ejercer El Derecho Al Voto
        $problemas_derecho_voto_si = User::where('problemas_al_derecho_voto','Si')->count();
        $problemas_derecho_voto_no = User::where('problemas_al_derecho_voto','No')->count();
        $suma_problemas_derecho_voto = $problemas_derecho_voto_si+$problemas_derecho_voto_no;
        if($suma_problemas_derecho_voto ==0)
        {
            $re_problemas_voto_si =0;
            $re_problemas_voto_no =0;
        }else{
            $re_problemas_voto_si = $problemas_derecho_voto_si/$suma_problemas_derecho_voto*100;
        $re_problemas_voto_no = $problemas_derecho_voto_no/$suma_problemas_derecho_voto*100;

        }
        $chart26 = new ProblemasDerechoVoto;
        $chart26->title('¿Ha tenido dificultades para ejercer su derecho al voto?');
        $chart26->labels(['Si ->'.round($re_problemas_voto_si).'%','No ->'.round($re_problemas_voto_no).'%']);
        $chart26->dataset('datos','pie',[$problemas_derecho_voto_si,$problemas_derecho_voto_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Tienen Participacion En Politica
        $participa_politica_si = User::where('participa_en_politica','Si')->count();
        $participa_politica_no = User::where('participa_en_politica','No')->count();
        $suma_participacion_politica = $participa_politica_si+$participa_politica_no;
        if($suma_participacion_politica==0)
        {
            $re_participacion_si =0;
            $re_participacion_no =0;
        }else{
        $re_participacion_si =  $participa_politica_si/$suma_participacion_politica*100;
        $re_participacion_no = $participa_politica_no/$suma_participacion_politica*100;
        }
        $chart27 = new ParticipaPolitica;
        $chart27->title('¿Está usted actualmente organizado en alguna asociación u organización que participe activamente en política?');
        $chart27->labels(['Si ->'.round($re_participacion_si).'%','No ->'.round($re_participacion_no).'%']);
        $chart27->dataset('datos','pie',[$participa_politica_si,$participa_politica_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Practican Alguna Religion
        $practica_religion_si = User::where('practica_alguna_religion','Si')->count();
        $practica_religion_no = User::where('practica_alguna_religion','No')->count();
        $suma_religion = $practica_religion_si+$practica_religion_no;
        if($suma_religion ==0)
        {
            $re_religion_si =0;
            $re_religion_no =0;
        }else{
            $re_religion_si = $practica_religion_si/$suma_religion*100;
            $re_religion_no = $practica_religion_no/$suma_religion*100;
        }
        $chart28 = new PracticaReligion;
        $chart28->title('¿Practica alguna religión?');
        $chart28->labels(['Si ->'.round($re_religion_si).'%','No ->'.round($re_religion_no).'%']);
        $chart28->dataset('datos','pie',[$practica_religion_si,$practica_religion_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Del Tipo De Religion Que Practican
        $tipo_religion_catolica = User::where('tipo_religion','Catolica')->count();
        $tipo_religion_evangelica = User::where('tipo_religion','Evangelica')->count();
        $tipo_religion_ninguna = User::where('tipo_religion','Ninguna')->count();
        $tipo_religion_otra = User::where('tipo_religion','Otra')->count();
        $suma_tipo_religiones = $tipo_religion_catolica+$tipo_religion_evangelica+$tipo_religion_ninguna+$tipo_religion_otra;
        if($suma_tipo_religiones ==0)
        {
            $re_catolica =0;
            $re_evangelica =0;
            $re_ninguna_religion =0;
            $re_otra_religion =0;
        }else{
            $re_catolica = $tipo_religion_catolica/$suma_tipo_religiones*100;
        $re_evangelica = $tipo_religion_evangelica/$suma_tipo_religiones*100;
        $re_ninguna_religion = $tipo_religion_ninguna/$suma_tipo_religiones*100;
        $re_otra_religion = $tipo_religion_otra/$suma_tipo_religiones*100;
        }
        $chart29 = new TipoReligion;
        $chart29->title('¿Qué religión profesa?');
        $chart29->labels(['Católica ->'.round($re_catolica).'%','Evangelica ->'.round($re_evangelica).'%','Ninguna ->'.round($re_ninguna_religion).'%','Otra ->'.round($re_otra_religion).'%']);
        $chart29->dataset('datos','pie',[$tipo_religion_catolica,$tipo_religion_evangelica,$tipo_religion_ninguna,$tipo_religion_otra])->color($borderColors);
        ////////////////////////////////////////////////////////

        return view('donantes.registro.politica_religion',compact('chart25','chart26','chart27','chart28','chart29'));
    }

    //Vista derechos
    public function derechos(Request $request)
    {
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"
        ];
        //grafica para mostrar total de Porcentaje De Personas Que Han Sufrido Discriminacion o Abuso En Centros De Estudio
        $discriminacion_abuso_si = User::where('discriminacion_agrecion_centro_estudio','Si')->count();
        $discriminacion_abuso_no = User::where('discriminacion_agrecion_centro_estudio','No')->count();
        $suma_disciminacion_abuso = $discriminacion_abuso_si+$discriminacion_abuso_no;
        if($suma_disciminacion_abuso == 0)
        {
            $re_discriminacion_abuso_si =0;
            $re_discriminacion_abuso_no =0;
        }else{
        $re_discriminacion_abuso_si = $discriminacion_abuso_si/$suma_disciminacion_abuso*100;
        $re_discriminacion_abuso_no = $discriminacion_abuso_no/$suma_disciminacion_abuso*100;
        }
        $chart30 = new DiscriminacionAbusoCentroEstudio;
        $chart30->title('¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en su Centro de Estudio? ');
        $chart30->labels(['Si ->'.round($re_discriminacion_abuso_si).'%','No ->'.round($re_discriminacion_abuso_no).'%']);
        $chart30->dataset('datos','pie',[$discriminacion_abuso_si,$discriminacion_abuso_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Han Han Sufrido Discriminacion o Abuso En Sus Lugares De Trabajo
        $discriminacion_trabajo_si = User::where('discriminacion_agrecion_trabajo','Si')->count();
        $discriminacion_trabajo_no = User::where('discriminacion_agrecion_trabajo','No')->count();
        $suma_discriminacion_trabajo = $discriminacion_trabajo_si+$discriminacion_trabajo_no;
        if($suma_discriminacion_trabajo ==0)
        {
            $re_discriminacion_trabajo_si =0;
            $re_discriminacion_trabajo_no =0;
        }else{
            $re_discriminacion_trabajo_si = $discriminacion_trabajo_si/$suma_discriminacion_trabajo*100;
            $re_discriminacion_trabajo_no = $discriminacion_trabajo_no/$suma_discriminacion_trabajo*100;
        }
        $chart31 = new DiscriminacionAbusoTrabajo;
        $chart31->title('¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en su Trabajo? ');
        $chart31->labels(['Si ->'.round($re_discriminacion_trabajo_si).'%','No ->'.round($re_discriminacion_trabajo_no).'%']);
        $chart31->dataset('datos','pie',[$discriminacion_trabajo_si,$discriminacion_trabajo_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Han Sufrido Discriminacion o Abuso En Centros De Salu
        $discriminacion_centro_salud_si = User::where('discriminacion_agrecion_centro_salud','Si')->count();
        $discriminacion_centro_salud_no = User::where('discriminacion_agrecion_centro_salud','No')->count();
        $suma_discriminacion_centro_salud = $discriminacion_centro_salud_si+$discriminacion_centro_salud_no;
        if($suma_discriminacion_centro_salud == 0)
        {
            $re_centro_si =0;
            $re_centro_no =0;
        }else{
        $re_centro_si = $discriminacion_centro_salud_si/$suma_discriminacion_centro_salud*100;
        $re_centro_no = $discriminacion_centro_salud_no/$suma_discriminacion_centro_salud*100;
        }
        $chart32 = new DiscriminacionCentroSalud;
        $chart32->title('¿Sufre o ha sufrido usted algún hecho de agresión y/o discriminación en los centros de salud?');
        $chart32->labels(['Si ->'.round($re_centro_si).'%','No ->'.round($re_centro_no).'%']);
        $chart32->dataset('datos','pie',[$discriminacion_centro_salud_si,$discriminacion_centro_salud_no])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Han Sufrido Discriminacion o Abuso De Diferentes Entidades
        $entidad_agresion_familiar = User::where('por_quien_hecho_de_agresion_discriminacion','Familiar')->count();
        $entidad_agresion_comunidad = User::where('por_quien_hecho_de_agresion_discriminacion','Comunidad')->count();
        $entidad_agresion_gobierno = User::where('por_quien_hecho_de_agresion_discriminacion','Gobierno')->count();
        $entidad_agresion_seguridad = User::where('por_quien_hecho_de_agresion_discriminacion','Seguridad')->count();
        $entidad_agresion_educacion = User::where('por_quien_hecho_de_agresion_discriminacion','Educacion')->count();
        $entidad_agresion_salud = User::where('por_quien_hecho_de_agresion_discriminacion','Salud')->count();
        $entidad_agresion_religioso = User::where('por_quien_hecho_de_agresion_discriminacion','Religioso')->count();
        $entidad_agresion_laboral = User::where('por_quien_hecho_de_agresion_discriminacion','Laboral')->count();
        $entidad_agresion_otros = User::where('por_quien_hecho_de_agresion_discriminacion','Otros')->count();
        $entidad_agresion_ninguno = User::where('por_quien_hecho_de_agresion_discriminacion','Ninguno')->count();
        $suma_entidad_agrecion =$entidad_agresion_familiar+$entidad_agresion_comunidad+$entidad_agresion_gobierno+$entidad_agresion_seguridad+$entidad_agresion_educacion+$entidad_agresion_salud+$entidad_agresion_religioso+$entidad_agresion_laboral+$entidad_agresion_otros+$entidad_agresion_ninguno;
        if( $suma_entidad_agrecion==0)
        {
           $re_agresion_fami =0;
           $re_agresion_comu = 0;
           $re_agresion_gobi =0;
           $re_agresion_segu =0;
           $re_agresion_edu =0;
           $re_agresion_salu =0;
           $re_agresion_reli =0;
           $re_agresion_labo =0;
           $re_agresion_otros = 0;
           $re_agresion_ningu =0;
        }else{
            $re_agresion_fami = $entidad_agresion_familiar/$suma_entidad_agrecion*100;
            $re_agresion_comu = $entidad_agresion_comunidad/$suma_entidad_agrecion*100;
            $re_agresion_gobi = $entidad_agresion_gobierno/$suma_entidad_agrecion*100;
            $re_agresion_segu = $entidad_agresion_seguridad/$suma_entidad_agrecion*100;
            $re_agresion_edu = $entidad_agresion_educacion/$suma_entidad_agrecion*100;
            $re_agresion_salu = $entidad_agresion_salud/$suma_entidad_agrecion*100;
            $re_agresion_reli = $entidad_agresion_religioso/$suma_entidad_agrecion*100;
            $re_agresion_labo = $entidad_agresion_laboral/$suma_entidad_agrecion*100;
            $re_agresion_otros = $entidad_agresion_otros/$suma_entidad_agrecion*100;
            $re_agresion_ningu = $entidad_agresion_ninguno/$suma_entidad_agrecion*100;
        }
        $chart33 = new EntidadesDiscriminacion;
        $chart33->title('¿Por quién ha recibido algún hecho de agresión y/o discriminación?');
        $chart33->labels(['Familiar ->'.round($re_agresion_fami).'%','Comunidad ->'.round($re_agresion_comu).'%','Gobierno ->'.round($re_agresion_gobi).'%','Seguridad ->'.round($re_agresion_segu).'%',
        'Educacion ->'.round($re_agresion_edu).'%','Salud ->'.round($re_agresion_salu).'%','Religioso ->'.round($re_agresion_reli).'%','Laboral ->'.round($re_agresion_labo).'%','Otros ->'.round($re_agresion_otros).'%','Ninguno ->'.round($re_agresion_ningu).'%']);
        $chart33->dataset('datos','pie',[$entidad_agresion_familiar,$entidad_agresion_comunidad,$entidad_agresion_gobierno,$entidad_agresion_seguridad,$entidad_agresion_educacion,
        $entidad_agresion_salud,$entidad_agresion_religioso,$entidad_agresion_laboral,$entidad_agresion_otros,$entidad_agresion_ninguno])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Han Sufrido Frecuentemente Algun Tipo De Agresión
        $tipo_agresion_frecuente_fisica = User::where('tipo_agresion_frecuente','Fisica')->count();
        $tipo_agresion_frecuente_psicologica = User::where('tipo_agresion_frecuente','Psicologica')->count();
        $tipo_agresion_frecuente_economica = User::where('tipo_agresion_frecuente','Economica')->count();
        $tipo_agresion_frecuente_ninguno = User::where('tipo_agresion_frecuente','Ninguno')->count();
        $tipo_agresion_frecuente_otros = User::where('tipo_agresion_frecuente','Otros')->count();
        $suma_tipo_agresion_frecuente = $tipo_agresion_frecuente_fisica+$tipo_agresion_frecuente_psicologica+$tipo_agresion_frecuente_economica+$tipo_agresion_frecuente_ninguno+$tipo_agresion_frecuente_otros;
        if($suma_tipo_agresion_frecuente ==0)
        {
            $re_agresion_fisica = 0;
            $re_agresion_psicologica = 0;
            $re_agresion_economica =0;
            $re_agresion_ninguna = 0;
            $re_agresion_otros = 0;
        }else{
            $re_agresion_fisica = $tipo_agresion_frecuente_fisica/$suma_tipo_agresion_frecuente*100;
            $re_agresion_psicologica =  $tipo_agresion_frecuente_psicologica/$suma_tipo_agresion_frecuente*100;
            $re_agresion_economica = $tipo_agresion_frecuente_economica/$suma_tipo_agresion_frecuente*100;
            $re_agresion_ninguna =  $tipo_agresion_frecuente_ninguno/$suma_tipo_agresion_frecuente*100;
            $re_agresion_otros = $tipo_agresion_frecuente_otros/$suma_tipo_agresion_frecuente*100; 
        }
        $chart34 = new TipoAgresionFrecuente;
        $chart34->title('¿Qué tipo de agresión ha recibido con mayor frecuencia?');
        $chart34->labels(['Física ->'.round($re_agresion_fisica).'%','Psicológica ->'.round($re_agresion_psicologica).'%','Económica ->'.round($re_agresion_economica).'%','Otros ->'.round($re_agresion_otros).'%','Ninguno ->'.round($re_agresion_ninguna).'%']);
        $chart34->dataset('datos','pie',[$tipo_agresion_frecuente_fisica,$tipo_agresion_frecuente_psicologica,$tipo_agresion_frecuente_economica,$tipo_agresion_frecuente_ninguno,$tipo_agresion_frecuente_otros])->color($borderColors);
        ////////////////////////////////////////////////////////

        //grafica para mostrar total de Porcentaje De Personas Que Conocen Mecanimos De Defensa De Sus Derechos
        $conoce_mecanismos_si = User::where('conocimientos_mecanimos_defensa_de_derechos','Si')->count();
        $conoce_mecanismos_no = User::where('conocimientos_mecanimos_defensa_de_derechos','No')->count();
        $suma_conocimiento =  $conoce_mecanismos_si+$conoce_mecanismos_no;
        if( $suma_conocimiento==0)
        {
            $re_conocimiento_si =0;
            $re_conocimiento_no = 0;

        }else{
        $re_conocimiento_si = $conoce_mecanismos_si/$suma_conocimiento*100;
        $re_conocimiento_no = $conoce_mecanismos_no/$suma_conocimiento*100;
        } 
        $chart35 = new ConocimientoMecanismosDefensaDerechos;
        $chart35->title('¿Conoce los mecanismos de defensa de sus derechos?');
        $chart35->labels(['Si ->'.round($re_conocimiento_si).'%','No ->'.round($re_conocimiento_no).'%']);
        $chart35->dataset('datos','pie',[$conoce_mecanismos_si,$conoce_mecanismos_no])->color($borderColors);
        ////////////////////////////////////////////////////////
 
        return view('donantes.registro.derechos',compact('chart30','chart31','chart32','chart33','chart34','chart35'));
    }
}
 