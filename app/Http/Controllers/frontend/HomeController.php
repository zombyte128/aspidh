<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Departamentos;
use App\TipoGenero;
use App\OrientacionSexual;

class HomeController extends Controller
{
    public function index()
    { 
        $departamentos = Departamentos::all();
        $genero = TipoGenero::all();
        $orientacion = OrientacionSexual::all();
        return view('frontend.app', compact('departamentos','genero','orientacion'));
    }
}