<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class RegistroUsuariosController extends Controller
{
    //Funcion para Agregar un nuevo dato
    public function store(Request $request)
    {
        $this->validate($request,[
            //Datos Generales//
            'nombres_segun_dui' => 'required',
            'apellidos_segun_dui' => 'required',
            'email' => 'unique:users',
            'fecha_nacimiento' => 'date',
            //Seguridad//
            'password' => 'required',
            'username' => 'required|unique:users',
        ]);

        $registro = new User();
        //Datos Generales//
        $registro->role_id = 3;
        $registro->pais_id = 1;
        $registro->nombres_segun_dui = $request->nombres_segun_dui;
        $registro->apellidos_segun_dui = $request->apellidos_segun_dui;
        $registro->edad = $request->edad;
        if($registro->fecha_nacimiento != null):
        $registro->fecha_nacimiento = Carbon::parse($request->fecha_nacimiento)->format('y-m-d');
        endif;
        $registro->tipo_genero_id = $request->tipo_genero_id;
        $registro->numero_telefono = $request->numero_telefono;
        $registro->email = $request->email;
        $registro->orientacion_sexual_id = $request->orientacion_sexual_id;
        $registro->departamento_id = $request->departamento_id;
        $registro->ciudad_id = $request->ciudad_id;
        $registro->direccion_completa = $request->direccion_completa;
        $registro->nombres_emergencia = $request->nombres_emergencia;
        $registro->telefono_emergencia = $request->telefono_emergencia;
        $registro->direccion_emergencia = $request->direccion_emergencia;
        $registro->documento_identificacion = $request->documento_identificacion;
        $registro->nombre_social = $request->nombre_social;
        $registro->redes_sociales_emergencia = $request->redes_sociales_emergencia;
        $registro->redes_social_facebook = $request->redes_social_facebook;
        $registro->redes_social_twitter = $request->redes_social_twitter;
        $registro->redes_social_instagram = $request->redes_social_instagram;
        //Seguridad//
        $registro->password=bcrypt($request->password);
        $registro->username=$request->username;
        //Educacion//
        $registro->ultimo_grado_estudio_aprobado = $request->ultimo_grado_estudio_aprobado;
        $registro->estudia_actualmente = $request->estudia_actualmente;
        $registro->razon_estudio = $request->razon_estudio;
        $registro->lugar_estudio_primario = $request->lugar_estudio_primario;
        $registro->lugar_estudio_universitario = $request->lugar_estudio_universitario;
        //Acceso a la salud// 
        $registro->afiliado_seguro_social = $request->afiliado_seguro_social;
        $registro->utiliza_servcios_de_salud = $request->utiliza_servcios_de_salud;
        $registro->centro_medico_asiste = $request->centro_medico_asiste;
        $registro->beneficiario_programa_salud_gubernamental = $request->beneficiario_programa_salud_gubernamental;
        $registro->cual_programa_salud = $request->cual_programa_salud;
        //Trabajo empleo//
        $registro->cuenta_con_empleo_formal = $request->cuenta_con_empleo_formal;
        $registro->negacion_trabajo_por_identidad_genero_orientacion_sexual = $request->negacion_trabajo_por_identidad_genero_orientacion_sexual;
        $registro->beneficiario_programa_acceso_trabajo_gubernamental = $request->beneficiario_programa_acceso_trabajo_gubernamental;
        $registro->cual_programa_trabajo = $request->cual_programa_trabajo;
        $registro->sector_trabajo = $request->sector_trabajo;
        //Acceso a la vivienda// 
        $registro->vive_actualmente = $request->vive_actualmente;
        $registro->con_quien_vive = $request->con_quien_vive;
        $registro->ingresos_financieros_mensual_aproximado = $request->ingresos_financieros_mensual_aproximado;
        $registro->solicitud_credito_para_vivienda = $request->solicitud_credito_para_vivienda;
        $registro->credito_otorgado = $request->credito_otorgado;
        $registro->justificacion_credito = $request->justificacion_credito;
        $registro->beneficiario_programa_acceso_vivienda_gubernamental = $request->beneficiario_programa_acceso_vivienda_gubernamental;
        $registro->cual_programa_vivienda = $request->cual_programa_vivienda;
        //Politica y religion// 
        $registro->ejecicio_derecho_voto = $request->ejecicio_derecho_voto;
        $registro->problemas_al_derecho_voto = $request->problemas_al_derecho_voto;
        $registro->cual_problema_voto = $request->cual_problema_voto;
        $registro->participa_en_politica = $request->participa_en_politica;
        $registro->practica_alguna_religion = $request->practica_alguna_religion;
        $registro->tipo_religion = $request->tipo_religion;
        //Vulneración de derechos//
        $registro->discriminacion_agrecion_centro_estudio = $request->discriminacion_agrecion_centro_estudio;
        $registro->discriminacion_agrecion_trabajo = $request->discriminacion_agrecion_trabajo;
        $registro->discriminacion_agrecion_centro_salud = $request->discriminacion_agrecion_centro_salud;
        $registro->por_quien_hecho_de_agresion_discriminacion = $request->por_quien_hecho_de_agresion_discriminacion;
        $registro->otro_por_quien_discrimanacion = $request->otro_por_quien_discrimanacion;
        $registro->tipo_agresion_frecuente = $request->tipo_agresion_frecuente;
        $registro->otro_tipo_agresion = $request->otro_tipo_agresion;
        $registro->conocimientos_mecanimos_defensa_de_derechos = $request->conocimientos_mecanimos_defensa_de_derechos;
        $registro->save();
        Auth::login($registro);
        return redirect()->route('user.home')->with('iniciada', 'Sesion iniciada');
    }
}