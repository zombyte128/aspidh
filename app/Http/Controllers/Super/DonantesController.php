<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Denuncias;

class DonantesController extends Controller
{
    //Vista donantes
    public function index()
    {
        $donantes = User::where('role_id','4')->orderBy('created_at','desc')->get();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.donantes.index',compact('donantes','total_denuncias_pendientes'));
    }

    //Vista crear nuevo donante
    public function create()
    {
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.donantes.create',compact('total_denuncias_pendientes'));
    }

    //Mantenimiento agregar nuevo donante
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombres_segun_dui' => 'required|min:3|max:20',
            'apellidos_segun_dui' => 'required|min:3|max:20',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);
        $donantes = new User();
        $donantes->role_id = 4;
        $donantes->nombres_segun_dui = $request->nombres_segun_dui;
        $donantes->apellidos_segun_dui = $request->apellidos_segun_dui;
        $donantes->email = $request->email;
        $donantes->password = bcrypt($request->password);
        $donantes->contrasena = $request->password;
        $donantes->save();
        $subject = "Acceso a ASPIDH";
        Mail::send('vendor.notifications.emailnotificaciondonante',compact('donantes'),function($msj) use ($subject,$donantes)
            {
                $msj->from("aspidh.arcoiris.trans@gmail.com","ASPIDH");
                $msj->subject($subject);
                $msj->to($donantes->email);
                //$msj->to("aspidh.arcoiris.trans@gmail.com");
            });
        Toastr::success('Donante Creado Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('super.donantes.index');
    }

    //Vista editar donante
    public function edit($id)
    {
        $donantes = User::find($id);
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.donantes.edit',compact('donantes','total_denuncias_pendientes'));
    }

    //Mantenimiento editar donante
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nombres_segun_dui' => 'required|min:3|max:20',
            'apellidos_segun_dui' => 'required|min:3|max:20',
            'email' => 'required|unique:users,email,'.$id,
            'password' => 'required|min:6',
        ]);
        $donantes = User::find($id);
        $donantes->role_id = 4;
        $donantes->nombres_segun_dui = $request->nombres_segun_dui;
        $donantes->apellidos_segun_dui = $request->apellidos_segun_dui;
        $donantes->email = $request->email;
        $donantes->password = bcrypt($request->password);
        $donantes->contrasena = $request->password;
        $donantes->save();
        Toastr::warning('Donante Editado Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('super.donantes.index');
    }

    //Mantenimiento Eliminar donante
    public function destroy($id)
    {
        User::find($id)->delete();
        Toastr::error('Donante Eliminado Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();
    }
}