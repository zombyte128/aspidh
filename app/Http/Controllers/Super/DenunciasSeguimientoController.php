<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Departamentos;
use Carbon\Carbon;
use App\Denuncias;
use App\User;
use PDF;

class DenunciasSeguimientoController extends Controller
{
    //Vista denuncia seguimiento    
    public function index()
    {
        $seguimientos = Denuncias::where('estado_de_la_denuncia','Seguimiento')->orderBy('created_at','desc')->get();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.denuncias.seguimiento.index',compact('seguimientos','total_denuncias_pendientes'));
    }

    //mantenimiento cambiar estado denuncia seguimiento
    public function update(Request $request, $id)
    {
        $seguimientos = Denuncias::find($id);
        $seguimientos->estado_de_la_denuncia  = "Completada";
        $subject = "Su Denuncia Ya Esta Completada";
        Mail::send('vendor.notifications.cambiodenunciacompletada',compact('seguimientos'),function($msj) use ($subject,$seguimientos)
            {
                $msj->from("aspidh.arcoiris.trans@gmail.com","ASPIDH");
                $msj->subject($subject);
                $msj->to($seguimientos->users->email);
                //$msj->to("aspidh.arcoiris.trans@gmail.com");
            });
        $seguimientos->save();
        Toastr::info('Denuncia Marcada Como Completada Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('super.denuncias-completadas.index');
    }
}