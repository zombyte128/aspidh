<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Charts\TotalDenunciasDepartamento;
use App\Charts\TotalDenunciasCiudades;
use App\Charts\TipoPrincipalAbusoDenuncia;
use App\Charts\PrincipalesAutoresDenuncias; 
use App\User;
use App\TipoGenero;
use App\Denuncias;
use App\Departamentos;
use App\Ciudades;

class DashboardController extends Controller
{
    //Vista dashboard
    public function index(Request $request)
    {
        //datos
        $total_administradores = User::where('role_id','2')->count();
        $total_personas = User::where('role_id','3')->count();
        $total_donantes = User::where('role_id','4')->count();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        $total_denuncias_seguimientos = Denuncias::where('estado_de_la_denuncia','Seguimiento')->count();
        $total_denuncias_completadas = Denuncias::where('estado_de_la_denuncia','Completada')->count();
        $total_denuncias_baneadas = Denuncias::where('estado_de_la_denuncia','Baneada')->count();
        $total_denuncias_general = Denuncias::count();
        $total_denuncias_finalizadas=Denuncias::where('estado_de_la_denuncia','Finalizada')->count();
        //colores
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];

        //Algoritmo para obtener el id de los departamentos de el salvador
        $dp = Departamentos::where('pais_id','1')->get();
        $p=[];
        foreach($dp as $k => $v){
            $p[] = $v->id;
        }

        // Consulta y algoritmo para obtener el total de denuncias publicadas en cada departamento de el salvador
        $departs = Departamentos::whereIn('id',$p)->pluck('nombre_departamento','id');
        $departamentos = [];
        $denuncias = [];
        foreach($departs as $key => $val)
        {
            $departamentos[] = $val;
            $denuncias[] = Denuncias::where('departamento_id',$key)->count();
        }

        // grafica para mostrar el total de denuncias publicadas por cada departamento de el salvador
        $chart1 = new TotalDenunciasDepartamento;
        $chart1->title('Total de denuncias por departamento');
        $chart1->labels($departamentos);
        $chart1->dataset('Total de denuncias publicadas = '.$total_denuncias_general,'column', ($denuncias))->color('rgba(255, 99, 132, 1.0)');
        
        // algoritmo para traer id de las ciudades del el salvador
        $ci = Ciudades::whereIn('departamento_id',$p)->get();
        $c = [];
        foreach($ci as $ll => $valor)
        {
            $c[] = $valor->id;
        }

        //grafica para mostrar el total de denucias por cada municipio
        $citys = Ciudades::with('denuncias')->has('denuncias','>=','9')->limit(20)->whereIn('id',$c)->pluck('nombre_ciudad','id');
        //$citys = Ciudades::with('denuncias')->withCount('denuncias')->has('denuncias','>',10)->orderBy('denuncias_count','DESC')->get();
        $ciudades = [];
        $denuncias_ciudades = [];
        foreach($citys as $kk => $vv)
        {
            $ciudades[]= $vv;
            $denuncias_ciudades[] = Denuncias::where('ciudad_id',$kk)->count();
        }
        $chart2 = new TotalDenunciasCiudades;
        $chart2->title('Top 20 denuncias por ciudades');
        $chart2->labels($ciudades);
        $chart2->dataset('Total de denuncias publicadas = '.$total_denuncias_general,'column', ($denuncias_ciudades))->color('rgba(255, 99, 132, 1.0)');

        // aqui en adelante van las graficas para denuncias(las dos primeras estan al principio del controlador) que serian denuncias totales por departamento y municios
        $tipo_principal_abuso_economica = Denuncias::where('tipo_principal_violacion_abuso','Economica')->count();
        $tipo_principal_abuso_feminicida_homicida = Denuncias::where('tipo_principal_violacion_abuso','Feminicida/Homicida')->count();
        $tipo_principal_abuso_fisica = Denuncias::where('tipo_principal_violacion_abuso','Fisica')->count();
        $tipo_principal_abuso_psicologica_emocional = Denuncias::where('tipo_principal_violacion_abuso','psicologica/Emocional')->count();
        $tipo_principal_abuso_patrimonial = Denuncias::where('tipo_principal_violacion_abuso','Patrimonial')->count();
        $tipo_principal_abuso_sexual = Denuncias::where('tipo_principal_violacion_abuso','Sexual')->count();
        $tipo_principal_abuso_simbolica = Denuncias::where('tipo_principal_violacion_abuso','Simbolica')->count();
        $suma_abuso_principal = $tipo_principal_abuso_economica+$tipo_principal_abuso_feminicida_homicida+$tipo_principal_abuso_fisica+$tipo_principal_abuso_psicologica_emocional
        +$tipo_principal_abuso_patrimonial+$tipo_principal_abuso_sexual+$tipo_principal_abuso_simbolica;
        if($suma_abuso_principal==0)
        {
            $re_abuso_economica = 0;
            $re_abuso_feminicida_homicida = 0;
            $re_abuso_fisica = 0;
            $re_abuso_psicologica_emocional = 0;
            $re_abuso_patrimonial = 0;
            $re_abuso_sexual = 0;
            $re_abuso_simbolica = 0;

        }else{
            $re_abuso_economica = $tipo_principal_abuso_economica/$suma_abuso_principal*100;
            $re_abuso_feminicida_homicida = $tipo_principal_abuso_feminicida_homicida/$suma_abuso_principal*100;
            $re_abuso_fisica = $tipo_principal_abuso_fisica/$suma_abuso_principal*100;
            $re_abuso_psicologica_emocional = $tipo_principal_abuso_psicologica_emocional/$suma_abuso_principal*100;
            $re_abuso_patrimonial = $tipo_principal_abuso_patrimonial/$suma_abuso_principal*100;
            $re_abuso_sexual = $tipo_principal_abuso_sexual/$suma_abuso_principal*100;
            $re_abuso_simbolica = $tipo_principal_abuso_simbolica/$suma_abuso_principal*100;
        }
        ////////////////////////////////////////////////////////
        $chart36 = new TipoPrincipalAbusoDenuncia;
        $chart36->title('Tipo De Principal De Abuso En Las Denuncias');
        $chart36->labels(['Economica ->'.round($re_abuso_economica).'%','Feminicida/Homicida ->'.round($re_abuso_feminicida_homicida).'%','Fisica ->'.round($re_abuso_fisica).'%',
        'psicologica/Emocional ->'.round($re_abuso_psicologica_emocional).'%','Patrimonial ->'.round($re_abuso_patrimonial).'%','Sexual ->'.round($re_abuso_sexual).'%','Simbolica ->'.round($re_abuso_simbolica).'%']);
        $chart36->dataset('datos','pie',[$tipo_principal_abuso_economica,$tipo_principal_abuso_feminicida_homicida,$tipo_principal_abuso_fisica,
        $tipo_principal_abuso_psicologica_emocional,$tipo_principal_abuso_patrimonial,$tipo_principal_abuso_sexual,$tipo_principal_abuso_simbolica])->color($borderColors);


        /*$autor_fuerza_de_seguridad_uniformado = Denuncias::where('informacion_del_autor_del_hecho','Fuerza de seguridad/uniformado')->count();
        $autor_militares_seguridad_privada = Denuncias::where('informacion_del_autor_del_hecho','Militares, seguridad privada')->count();
        $autor_persona_de_servicio_salud_publico = Denuncias::where('informacion_del_autor_del_hecho','Persona de servicio salud publico')->count();
        $autor_persona_de_servicio_de_salud_privado = Denuncias::where('informacion_del_autor_del_hecho','Persona de servicio de salud privado')->count();
        $autor_persona_de_servicio_de_educacion_publico = Denuncias::where('informacion_del_autor_del_hecho','Persona de servicio de educacion publico')->count();
        $autor_persona_de_servicio_de_educacion_privado = Denuncias::where('informacion_del_autor_del_hecho','Persona de servicio de educacion privado')->count();
        $autor_funcionario_del_estado = Denuncias::where('informacion_del_autor_del_hecho','Funcionario del estado')->count();
        $autor_cliente_de_trabajo_sexual = Denuncias::where('informacion_del_autor_del_hecho','Cliente de trabajo sexual')->count();
        $autor_familiar = Denuncias::where('informacion_del_autor_del_hecho','Familiar')->count();
        $autor_individuo_civil = Denuncias::where('informacion_del_autor_del_hecho','Individuo civil')->count();
        $autor_compañero_a_de_trabajo = Denuncias::where('informacion_del_autor_del_hecho','Compañero/a de trabajo')->count();
        $autor_profesional_de_medio_de_comunicacion = Denuncias::where('informacion_del_autor_del_hecho','Profesional de medio de comunicacion')->count();
        $autor_religioso= Denuncias::where('informacion_del_autor_del_hecho','Religioso')->count();
        $autor_representante_de_la_sociedad_civil= Denuncias::where('informacion_del_autor_del_hecho','Representante de la sociedad civil')->count();
        $autor_grupo_delictivo= Denuncias::where('informacion_del_autor_del_hecho','Grupo delictivo')->count();
        $autor_no_sabe= Denuncias::where('informacion_del_autor_del_hecho','No sabe')->count();
        $autor_otro= Denuncias::where('informacion_del_autor_del_hecho','Otro')->count();
        $suma_total_autores = $autor_fuerza_de_seguridad_uniformado+$autor_militares_seguridad_privada+$autor_persona_de_servicio_salud_publico+
        $autor_persona_de_servicio_de_salud_privado+$autor_persona_de_servicio_de_educacion_publico+$autor_persona_de_servicio_de_educacion_privado+
        $autor_funcionario_del_estado+$autor_cliente_de_trabajo_sexual+$autor_familiar+$autor_individuo_civil+$autor_compañero_a_de_trabajo+
        $autor_profesional_de_medio_de_comunicacion+$autor_religioso+$autor_representante_de_la_sociedad_civil+$autor_grupo_delictivo+$autor_no_sabe+$autor_otro;
        if($suma_total_autores==0)
        {
            $re_autor_fuerza_de_seguridad_uniformado =0;
            $re_autor_militares_seguridad_privada =0;
            $re_autor_persona_de_servicio_salud_publico =0;
            $re_autor_persona_de_servicio_de_salud_privado =0;
            $re_autor_persona_de_servicio_de_educacion_publico =0;
            $re_autor_persona_de_servicio_de_educacion_privado =0;
            $re_autor_funcionario_del_estado  =0;
            $re_autor_cliente_de_trabajo_sexual =0;
            $re_autor_familiar  =0;
            $re_autor_individuo_civil =0;
            $re_autor_compañero_a_de_trabajo =0;
            $re_autor_profesional_de_medio_de_comunicacion =0;
            $re_autor_religioso =0;
            $re_autor_representante_de_la_sociedad_civil =0;
            $re_autor_grupo_delictivo =0;
            $re_autor_no_sabe =0;
            $re_autor_otro =0;
        }else{
        $re_autor_fuerza_de_seguridad_uniformado = $autor_fuerza_de_seguridad_uniformado/$suma_total_autores*100;
        $re_autor_militares_seguridad_privada =  $autor_militares_seguridad_privada/$suma_total_autores*100;
        $re_autor_persona_de_servicio_salud_publico = $autor_persona_de_servicio_salud_publico/$suma_total_autores*100;
        $re_autor_persona_de_servicio_de_salud_privado = $autor_persona_de_servicio_de_salud_privado/$suma_total_autores*100;
        $re_autor_persona_de_servicio_de_educacion_publico = $autor_persona_de_servicio_de_educacion_publico /$suma_total_autores*100;
        $re_autor_persona_de_servicio_de_educacion_privado = $autor_persona_de_servicio_de_educacion_privado/$suma_total_autores*100;
        $re_autor_funcionario_del_estado  = $autor_funcionario_del_estado/$suma_total_autores*100;
        $re_autor_cliente_de_trabajo_sexual = $autor_cliente_de_trabajo_sexual/$suma_total_autores*100;
        $re_autor_familiar  = $autor_familiar/$suma_total_autores*100;
        $re_autor_individuo_civil = $autor_individuo_civil/$suma_total_autores*100;
        $re_autor_compañero_a_de_trabajo = $autor_compañero_a_de_trabajo/$suma_total_autores*100;
        $re_autor_profesional_de_medio_de_comunicacion = $autor_profesional_de_medio_de_comunicacion/$suma_total_autores*100;
        $re_autor_religioso = $autor_religioso/$suma_total_autores*100;
        $re_autor_representante_de_la_sociedad_civil = $autor_representante_de_la_sociedad_civil/$suma_total_autores*100;
        $re_autor_grupo_delictivo = $autor_grupo_delictivo/$suma_total_autores*100;
        $re_autor_no_sabe = $autor_no_sabe/$suma_total_autores*100;
        $re_autor_otro = $autor_otro/$suma_total_autores*100;
        }
        ////////////////////////////////////////////////////////
        $chart37 = new PrincipalesAutoresDenuncias;
        $chart37->title('Principales Autores En Las Denuncian');
        $chart37->labels(['Fuerza de seguridad/uniformado ->'.round($re_autor_fuerza_de_seguridad_uniformado).'%','Militares, seguridad privada ->'.round($re_autor_militares_seguridad_privada).'%',
        'Persona de servicio salud publico ->'.round($re_autor_persona_de_servicio_salud_publico).'%','Persona de servicio de salud privado ->'.round($re_autor_persona_de_servicio_de_salud_privado).'%',
        'Persona de servicio de educacion publico ->'.round($re_autor_persona_de_servicio_de_educacion_publico).'%','Persona de servicio de educacion privado ->'.round($re_autor_persona_de_servicio_de_educacion_privado).'%',
        'Funcionario del estado ->'.round($re_autor_funcionario_del_estado).'%','Cliente de trabajo sexual ->'.round($re_autor_cliente_de_trabajo_sexual).'%','Familiar'.round($re_autor_familiar).'%',
        'Individuo civil ->'.round($re_autor_individuo_civil).'%','Compañero/a de trabajo ->'.round($re_autor_compañero_a_de_trabajo).'%','Profesional de medio de comunicacion ->'.round($re_autor_profesional_de_medio_de_comunicacion).'%',
        'Religioso ->'.round($re_autor_religioso).'%','Representante de la sociedad civil ->'.round($re_autor_representante_de_la_sociedad_civil).'%','Grupo delictivo ->'.round($re_autor_grupo_delictivo).'%',
        'No sabe ->'.round($re_autor_no_sabe).'%','Otro ->'.round($re_autor_otro).'%']);
        $chart37->dataset('datos','pie',[$autor_fuerza_de_seguridad_uniformado,$autor_militares_seguridad_privada,$autor_persona_de_servicio_salud_publico,
        $autor_persona_de_servicio_de_salud_privado,$autor_persona_de_servicio_de_educacion_publico,$autor_persona_de_servicio_de_educacion_privado,
        $autor_funcionario_del_estado,$autor_cliente_de_trabajo_sexual,$autor_familiar,$autor_individuo_civil,$autor_compañero_a_de_trabajo,
        $autor_profesional_de_medio_de_comunicacion,$autor_religioso,$autor_representante_de_la_sociedad_civil,$autor_grupo_delictivo,
        $autor_no_sabe,$autor_otro])->color($borderColors);
        ////////////////////////////////////////////////////////
        */
        
        return view('super.dashboard',compact('total_administradores','total_personas','total_donantes','total_denuncias_pendientes',
        'total_denuncias_seguimientos','total_denuncias_completadas','total_denuncias_baneadas','total_denuncias_general',
        'chart1','chart2','chart36','total_denuncias_finalizadas'));
    }

}