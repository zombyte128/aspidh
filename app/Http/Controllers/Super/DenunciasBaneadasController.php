<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Denuncias;

class DenunciasBaneadasController extends Controller
{
    //vista denuncias baneadas
    public function index()
    {
        $baneadas = Denuncias::where('estado_de_la_denuncia','Baneada')->orderBy('created_at','desc')->get();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.denuncias.Baneadas.index',compact('baneadas','total_denuncias_pendientes'));
    }

    //mantenimiento editar estado baneada
    public function update(Request $request, $id)
    {
        $baneadas = Denuncias::find($id);
        $baneadas->estado_de_la_denuncia  = "Baneada";
        $baneadas->save();
        Toastr::info('Denuncia Beneada Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('super.denuncias-baneadas.index');
    }

    //Mantenimiento eliminar denuncia
    public function destroy($id)
    {
        Denuncias::find($id)->delete();
        Toastr::error('Denuncia Eliminada Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();

    }
}