<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\User;
use App\Denuncias;

class AdministradoresController extends Controller
{
    //ver vista administrador
    public function index()
    {
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        $administradores = User::where('role_id','2')->orderBy('created_at','desc')->get();
        return view('super.administradores.index',compact('administradores','total_denuncias_pendientes'));
    }

    //Vista de agregar administrador
    public function create()
    {
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.administradores.create',compact('total_denuncias_pendientes'));
    }

    //Mantenimiento agregar administrador
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombres_segun_dui' => 'required|min:3|max:20',
            'apellidos_segun_dui' => 'required|min:3|max:20',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);
        $administradores = new User();
        $administradores->role_id = 2;
        $administradores->nombres_segun_dui = $request->nombres_segun_dui;
        $administradores->apellidos_segun_dui = $request->apellidos_segun_dui;
        $administradores->email = $request->email;
        $administradores->password = bcrypt($request->password);
        $administradores->contrasena = $request->password;
        $administradores->save();
        Toastr::success('Administrador Creado Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('super.administradores.index');
    }
    
    //vista editar administrador
    public function edit($id)
    {
        $administradores = User::find($id);
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.administradores.edit',compact('administradores','total_denuncias_pendientes'));
    }

    //mantenimiento editar administrador
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nombres_segun_dui' => 'required|min:3|max:20',
            'apellidos_segun_dui' => 'required|min:3|max:20',
            'email' => 'required|unique:users,email,'.$id,
            'password' => 'required|min:6',
        ]); 
        $administradores = User::find($id);
        $administradores->role_id = 2;
        $administradores->nombres_segun_dui = $request->nombres_segun_dui;
        $administradores->apellidos_segun_dui = $request->apellidos_segun_dui;
        $administradores->email = $request->email;
        $administradores->password = bcrypt($request->password);
        $administradores->contrasena = $request->password;
        $administradores->save();
        Toastr::warning('Administrador Editado Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('super.administradores.index');
    }

    //Mantenimiento eliminar administrador
    public function destroy($id)
    {
        User::find($id)->delete();
        Toastr::error('Administrador Eliminado Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();
    }
}