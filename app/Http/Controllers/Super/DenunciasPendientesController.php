<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Departamentos;
use Carbon\Carbon;
use App\Denuncias;
use App\User;


class DenunciasPendientesController extends Controller
{
    //vista denuncia pendiente
    public function index()
    {
        $pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->orderBy('created_at','desc')->get();
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.denuncias.pendientes.index',compact('pendientes','total_denuncias_pendientes'));
    }

    //vista editar denuncia pendiente
    public function edit($id)
    {
        $departamentos = Departamentos::all();
        $pendientes = Denuncias::find($id);
        $total_denuncias_pendientes = Denuncias::where('estado_de_la_denuncia','Pendiente')->count();
        return view('super.denuncias.pendientes.edit',compact('pendientes','departamentos','total_denuncias_pendientes'));
    }

    //Mantenimiento cambiar estado denuncia pendiente
    public function update(Request $request, $id)
    {
        $pendientes = Denuncias::find($id);
        $pendientes->estado_de_la_denuncia  = "Seguimiento";
        $subject = "Su Denuncia Ya Esta En Proceso De Seguimiento";
        Mail::send('vendor.notifications.cambioestadodenuncia',compact('pendientes'),function($msj) use ($subject,$pendientes)
            {
                $msj->from("aspidh.arcoiris.trans@gmail.com","ASPIDH");
                $msj->subject($subject);
                $msj->to($pendientes->users->email);
                //$msj->to("aspidh.arcoiris.trans@gmail.com");
            });
        $pendientes->save();
        Toastr::info('Denuncia Marcada En Seguimiento Con Exito :)' ,'¡Felicidades!');
        return redirect()->route('super.denuncias-seguimiento.index');
    }

    //Mantenimiento editar denuncia pendiente
    public function updatedenuncias(Request $request, $id)
    { 
        $this->validate($request,[
            //Datos Generales//
            'fecha_del_suceso' => 'required|date',
            'fecha_cuando_se_reaizo_la_denuncia_formal' => 'date',
            'quien_registra_denuncia' => 'required',
            'breve_relato_del_hecho_consecuencias' => 'required',
        ]);

        $pendientes = Denuncias::find($id);
        //Datos Generales//
        //Si la fecha viene como null
        if( $request->fecha_del_suceso == NULL)
        {
        $pendientes->fecha_del_suceso = NULL;
        }else{
        $pendientes->fecha_del_suceso = Carbon::parse($request->fecha_del_suceso)->format('y-m-d');
        }
        $pendientes->donde_ocurrio = $request->donde_ocurrio;
        $pendientes->breve_relato_del_hecho_consecuencias = $request->breve_relato_del_hecho_consecuencias;
        $pendientes->tipo_principal_violacion_abuso = $request->tipo_principal_violacion_abuso;
        $pendientes->departamento_id = $request->departamento_id;
        if($request->ciudad_id != ""){
        $pendientes->ciudad_id = $request->ciudad_id;
        }
        $pendientes->quien_registra_denuncia = $request->quien_registra_denuncia;
        $pendientes->direccion_completa = $request->direccion_completa;
        $pendientes->tipo_secundario_violacion_abuso = $request->tipo_secundario_violacion_abuso;
        $pendientes->indique_cantidad_agresores = $request->indique_cantidad_agresores;
        $pendientes->forma_de_agrecion = $request->forma_de_agrecion;
        $pendientes->informacion_del_autor_del_hecho = $request->informacion_del_autor_del_hecho;
        $pendientes->realizo_una_denuncia_formal = $request->realizo_una_denuncia_formal;
        $pendientes->lugar_donde_realizo_la_denuncia_formal = $request->lugar_donde_realizo_la_denuncia_formal;
        //Si la fecha viene como null
        if( $request->fecha_cuando_se_reaizo_la_denuncia_formal == NULL)
        {
        $pendientes->fecha_cuando_se_reaizo_la_denuncia_formal = NULL;
        }else{
        $pendientes->fecha_cuando_se_reaizo_la_denuncia_formal = Carbon::parse($request->fecha_cuando_se_reaizo_la_denuncia_formal)->format('y-m-d');
        }
        $pendientes->documentacion_respaldatoria = $request->documentacion_respaldatoria;
        $pendientes->save();
        Toastr::warning('Denuncia Modificada Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();
    }

    //Mantenimiento editar documento
    public function updatedocumentos(Request $request, $id)
    {
        $this->validate($request,[
            //Datos Generales//
            'foto_dui_frente' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_dui_atras' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_nit_frente' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_nit_atras' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_acta' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_denuncia' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_otro' => 'max:5000|mimes:jpeg,jpg,png',
            'foto_otro2' => 'max:5000|mimes:jpeg,jpg,png',
            'documento_dui' => 'mimes:pdf',
            'documento_nit' => 'mimes:pdf',
            'documento_acta' => 'mimes:pdf',
            'documento_denuncia' => 'mimes:pdf',
            'documento_otro' => 'mimes:pdf',
        ]);

        $pendientes = Denuncias::find($id);
        //dui frente
        $dui1=$request->file('foto_dui_frente');
        if (isset($dui1) && $request->foto_dui_frente != "prueba")
        {
        if($pendientes->foto_dui_frente != ""){
        unlink('documentos/dui/'.$pendientes->foto_dui_frente);
        }
        $currentDate1 = Carbon::now()->toDateString();
        $imagename1 = $currentDate1.'-'. uniqid() .'.'. $dui1->getClientOriginalName();
        $dui1->move('documentos/dui/',$imagename1);
        $pendientes->foto_dui_frente = $imagename1;
        }elseif($request->foto_dui_frente == "prueba"){
        unlink('documentos/dui/'.$pendientes->foto_dui_frente);
        $pendientes->foto_dui_frente = NULL;
        }
        //dui atras
        $dui2=$request->file('foto_dui_atras');
        if (isset($dui2) && $request->foto_dui_atras != "prueba")
        {
        if($pendientes->foto_dui_atras != ""){
        unlink('documentos/dui/'.$pendientes->foto_dui_atras);
        }
        $currentDate2 = Carbon::now()->toDateString();
        $imagename2 = $currentDate2.'-'. uniqid() .'.'. $dui2->getClientOriginalName();
        $dui2->move('documentos/dui/',$imagename2);
        $pendientes->foto_dui_atras = $imagename2;
        }elseif($request->foto_dui_atras == "prueba"){
        unlink('documentos/dui/'.$pendientes->foto_dui_atras);
        $pendientes->foto_dui_atras = NULL;
        }
        //dui documento
        $documentodui=$request->file('documento_dui');
        if (isset($documentodui) && $request->documento_dui != "prueba")
        {
        if($pendientes->documento_dui != ""){
        unlink('documentos/dui/'.$pendientes->documento_dui);
        }
        $currentDate11 = Carbon::now()->toDateString();
        $imagename11 = $currentDate11.'-'. uniqid() .'.'. $documentodui->getClientOriginalName();
        $documentodui->move('documentos/dui/',$imagename11);
        $pendientes->documento_dui = $imagename11;
        }elseif($request->documento_dui == "prueba"){
        unlink('documentos/dui/'.$pendientes->documento_dui);
        $pendientes->documento_dui = NULL;
        }
        //nit frente
        $nit1=$request->file('foto_nit_frente');
        if (isset($nit1) && $request->foto_nit_frente != "prueba")
        {
        if($pendientes->foto_nit_frente != ""){
        unlink('documentos/nit/'.$pendientes->foto_nit_frente);
        }
        $currentDate3 = Carbon::now()->toDateString();
        $imagename3 = $currentDate3.'-'. uniqid() .'.'. $nit1->getClientOriginalName();
        $nit1->move('documentos/nit/',$imagename3);
        $pendientes->foto_nit_frente = $imagename3;
        }elseif($request->foto_nit_frente == "prueba"){
        unlink('documentos/nit/'.$pendientes->foto_nit_frente);
        $pendientes->foto_nit_frente = NULL;
        }
        //nit atras
        $nit2=$request->file('foto_nit_atras');
        if (isset($nit2) && $request->foto_nit_atras != "prueba")
        {
        if($pendientes->foto_nit_atras != ""){
        unlink('documentos/nit/'.$pendientes->foto_nit_atras);
        }
        $currentDate4 = Carbon::now()->toDateString();
        $imagename4 = $currentDate4.'-'. uniqid() .'.'. $nit2->getClientOriginalName();
        $nit2->move('documentos/nit/',$imagename4);
        $pendientes->foto_nit_atras = $imagename4;
        }elseif($request->foto_nit_atras == "prueba"){
        unlink('documentos/nit/'.$pendientes->foto_nit_atras);
        $pendientes->foto_nit_atras = NULL;
        }
        //nit documento
        $documentonit=$request->file('documento_nit');
        if (isset($documentonit) && $request->documento_nit != "prueba")
        {
        if($pendientes->documento_nit != ""){
        unlink('documentos/nit/'.$pendientes->documento_nit);
        }
        $currentDate22 = Carbon::now()->toDateString();
        $imagename22 = $currentDate22.'-'. uniqid() .'.'. $documentonit->getClientOriginalName();
        $documentonit->move('documentos/nit/',$imagename22);
        $pendientes->documento_nit = $imagename22;
        }elseif($request->documento_nit == "prueba"){
        unlink('documentos/nit/'.$pendientes->documento_nit);
        $pendientes->documento_nit = NULL;
        }
        //acta
        $acta=$request->file('foto_acta');
        if (isset($acta) && $request->foto_acta != "prueba")
        {
        if($pendientes->foto_acta != ""){
        unlink('documentos/acta_denuncia/'.$pendientes->foto_acta);
        }
        $currentDate5 = Carbon::now()->toDateString();
        $imagename5 = $currentDate5.'-'. uniqid() .'.'. $acta->getClientOriginalName();
        $acta->move('documentos/acta_denuncia/',$imagename5);
        $pendientes->foto_acta = $imagename5;
        }elseif($request->foto_acta == "prueba"){
        unlink('documentos/acta_denuncia/'.$pendientes->foto_acta);
        $pendientes->foto_acta = NULL;
        }
        //acta documento
        $documentoacta=$request->file('documento_acta');
        if (isset($documentoacta) && $request->documento_acta != "prueba")
        {
        if($pendientes->documento_acta != ""){
        unlink('documentos/acta_denuncia/'.$pendientes->documento_acta);
        }
        $currentDate33 = Carbon::now()->toDateString();
        $imagename33 = $currentDate33.'-'. uniqid() .'.'. $documentoacta->getClientOriginalName();
        $documentoacta->move('documentos/acta_denuncia/',$imagename33);
        $pendientes->documento_acta = $imagename33;
        }elseif($request->documento_acta == "prueba"){
        unlink('documentos/acta_denuncia/'.$pendientes->documento_acta);
        $pendientes->documento_acta = NULL;
        }
        //denuncia
        $denuncias=$request->file('foto_denuncia');
        if (isset($denuncias) && $request->foto_denuncia != "prueba")
        {
        if($pendientes->foto_denuncia != ""){
        unlink('documentos/acta_denuncia/'.$pendientes->foto_denuncia);
        }
        $currentDate6 = Carbon::now()->toDateString();
        $imagename6 = $currentDate6.'-'. uniqid() .'.'. $denuncias->getClientOriginalName();
        $denuncias->move('documentos/acta_denuncia/',$imagename6);
        $pendientes->foto_denuncia = $imagename6;
        }elseif($request->foto_denuncia == "prueba"){
        unlink('documentos/acta_denuncia/'.$pendientes->foto_denuncia);
        $pendientes->foto_denuncia = NULL;
        }
        //denuncia documento
        $documentodenuncia=$request->file('documento_denuncia');
        if (isset($documentodenuncia) && $request->documento_denuncia != "prueba")
        {
        if($pendientes->documento_denuncia != ""){
        unlink('documentos/acta_denuncia/'.$pendientes->documento_denuncia);
        }
        $currentDate44 = Carbon::now()->toDateString();
        $imagename44 = $currentDate33.'-'. uniqid() .'.'. $documentodenuncia->getClientOriginalName();
        $documentodenuncia->move('documentos/acta_denuncia/',$imagename33);
        $pendientes->documento_denuncia = $imagename44;
        }elseif($request->documento_denuncia == "prueba"){
        unlink('documentos/acta_denuncia/'.$pendientes->documento_denuncia);
        $pendientes->documento_denuncia = NULL;
        }
        //Otros
        $otros=$request->file('foto_otro');
        if (isset($otros) && $request->foto_otro != "prueba")
        {
        if($pendientes->foto_otro != ""){
        unlink('documentos/otros/'.$pendientes->foto_otro);
        }
        $currentDate7 = Carbon::now()->toDateString();
        $imagename7 = $currentDate7.'-'. uniqid() .'.'. $otros->getClientOriginalName();
        $otros->move('documentos/otros/',$imagename7);
        $pendientes->foto_otro = $imagename7;
        }elseif($request->foto_otro == "prueba"){
        unlink('documentos/otros/'.$pendientes->foto_otro);
        $pendientes->foto_otro = NULL;
        }
        //Otros 2
        $otros2=$request->file('foto_otro2');
        if (isset($otros2) && $request->foto_otro2 != "prueba")
        {
        if($pendientes->foto_otro2 != ""){
        unlink('documentos/otros/'.$pendientes->foto_otro2);
        }
        $currentDate8 = Carbon::now()->toDateString();
        $imagename8 = $currentDate8.'-'. uniqid() .'.'. $otros2->getClientOriginalName();
        $otros2->move('documentos/otros/',$imagename8);
        $pendientes->foto_otro2 = $imagename8;
        }elseif($request->foto_otro2 == "prueba"){
        unlink('documentos/otros/'.$pendientes->foto_otro2);
        $pendientes->foto_otro2 = NULL;
        }
        //otro documento
        $documentootro=$request->file('documento_otro');
        if (isset($documentootro) && $request->documento_otro != "prueba")
        { 
        if($pendientes->documento_otro != ""){
        unlink('documentos/otros/'.$pendientes->documento_otro);
        }
        $currentDate55 = Carbon::now()->toDateString();
        $imagename55 = $currentDate55.'-'. uniqid() .'.'. $documentootro->getClientOriginalName();
        $documentootro->move('documentos/otros/',$imagename55);
        $pendientes->documento_otro = $imagename55;
        }elseif($request->foto_otro2 == "documento_otro"){
        unlink('documentos/otros/'.$pendientes->documento_otro);
        $pendientes->documento_otro = NULL;
        }
        $pendientes->save();
        Toastr::warning('Documento Modificado Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();
    }

    //Mantenimiento eliminar denuncia pendiente
    public function destroy($id)
    {
        Denuncias::find($id)->delete();
        Toastr::error('Denuncia Eliminada Con Exito :)' ,'¡Felicidades!');
        return redirect()->back();
    }
}