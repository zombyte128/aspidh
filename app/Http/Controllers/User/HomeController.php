<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Departamentos;

class HomeController extends Controller
{
    public function index()
    {
        $departamentos = Departamentos::all();
        return view('user.app', compact('departamentos'));
    }
}