<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    public $table = 'departamentos';
    
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function paises()
    {
    	return $this->belongsTo('App\Paises','pais_id');
    }

    public function municipios()
    {
        return $this->hasMany('App\Ciudades');
    }

    public function denuncias()
    {
        return $this->hasMany('App\Denuncias');
    }
}