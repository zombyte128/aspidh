<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EliminarDonante extends Command
{
    protected $signature = 'eliminar:users';

    protected $description = 'Comando que sirve para eliminar a un donante luego de 24 horas';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $date = Carbon::now()->subHours(24);
        DB::table('users')->where('role_id','4')->where('created_at','<=',$date)->delete();
        
    }
}
