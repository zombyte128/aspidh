<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        'App\Console\Commands\EliminarDonante',
        'App\Console\Commands\RecordatoriosDenunciasPendientes',
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('eliminar:users --force')->hourly();
        $schedule->command('recordatorio:denunciasatencion')->cron('0 0 15,30 * *');//se ejecuta el cron cada 15 dias de cada mes
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
