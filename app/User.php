<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Mail\CustomResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role','role_id');
    }

    public function paises()
    {
        return $this->belongsTo('App\Paises','pais_id');
    }

    public function departamentos()
    {
        return $this->belongsTo('App\Departamentos','departamento_id');
    }

    public function ciudades()
    {
        return $this->belongsTo('App\Ciudades','ciudad_id');
    }

    public function tipogenero()
    {
        return $this->belongsTo('App\TipoGenero','tipo_genero_id');
    }

    public function orientacionsexual()
    {
        return $this->belongsTo('App\OrientacionSexual','orientacion_sexual_id');
    }

    public function denuncias()
    {
        return $this->hasMany('App\Denuncias');
    }
    public function sendPasswordResetNotification($token)
    {
    $this->notify(new CustomResetPasswordNotification($token));
    }
}