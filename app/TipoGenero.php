<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoGenero extends Model
{
    public $table = 'tipo_generos';

    public function users()
    {
        return $this->hasMany('App\User');
    }
}