<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudades extends Model
{
    protected $table = 'ciudades';

    public function departamentos()
    {
    	return $this->belongsTo('App\Departamentos','departamento_id');
    }

    public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function denuncias()
    {
        return $this->hasMany('App\Denuncias','ciudad_id');
    }
}