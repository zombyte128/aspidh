<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    public $table = 'paises';
    
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function departamentos()
    {
        return $this->hasMany('App\Departamentos');
    }

    public function denuncias()
    {
        return $this->hasMany('App\Denuncias');
    }
}