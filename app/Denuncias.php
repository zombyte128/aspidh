<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denuncias extends Model
{
    public $table='denuncias';

    protected $fillable = [
        'fecha_cuando_se_reaizo_la_denuncia_formal',
    ];

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function paises()
    {
        return $this->belongsTo('App\Paises','pais_id');
    }

    public function departamentos()
    {
        return $this->belongsTo('App\Departamentos','departamento_id');
    }

    public function ciudades()
    {
        return $this->belongsTo('App\Ciudades','ciudad_id');
    }
}