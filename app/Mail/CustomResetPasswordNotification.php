<?php

namespace App\Mail;
//php artisan vendor:publish --tag=laravel-notifications
//php artisan make:notification CustomResetPasswordNotification
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CustomResetPasswordNotification extends Notification
{
   public $token;
   
   public function __construct($token)
   {
      $this->token = $token;
   }
   
   public function via($notifiable) 
   {
      return ['mail'];
   }

   public function toMail($notifiable)
   {
      return (new MailMessage)
         ->subject('Restablecer Contraseña')
         ->line('Hola, se solicitó un restablecimiento de contraseña para tu cuenta ' . $notifiable->email . ', haz clic en el botón que aparece a continuación para cambiar tu contraseña.')
         ->action('Restablecer Contraseña', url('password/reset', $this->token))
         ->line('Si tu no realizaste la solicitud de cambio de contraseña, solo ignora este mensaje.')
         ->line('Este enlace solo es válido dentro de los proximos 60 minutos');

   }
}