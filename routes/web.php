<?php

use Illuminate\Support\Facades\Route;

//::::::::::::::::::::::Rutas de Auth:::::::::::::::::::::::::::::::::://
Auth::routes();

//:::::::::::::::::::::::Middleware Para super admin::::::::::::::::::::::::://
Route::group(['as'=>'super.', 'prefix'=>'super','namespace'=>'Super','middleware'=>[
    'auth','super']], function(){        

//Dashboard superadmin vista
Route::get('dashboard','DashboardController@index')->name('dashboard');
//mantenimientos administradores
Route::resource('administradores','AdministradoresController');
//mantenimiento personas
Route::resource('personas','PersonasController');
//mantenimiento denuncias
Route::resource('denuncias','DenunciasController');
//ruta que sirve para exporta denuncia a pdf
Route::get('pdf-denuncia/{id}','DenunciasController@createPDF')->name('denuncia-pdf');
//mantenimiento denuncias pendientes
Route::resource('denuncias-pendientes','DenunciasPendientesController');
Route::Put('editar-pendientes/{id}','DenunciasPendientesController@updatedenuncias')->name('editar-pendientes');
Route::Put('editar-pendientes-documentos/{id}','DenunciasPendientesController@updatedocumentos')->name('editar-pendientes-documentos');
//mantenimiento denuncias seguimiento
Route::resource('denuncias-seguimiento','DenunciasSeguimientoController');
//mantenimiento denuncias completas
Route::resource('denuncias-completadas','DenunciasCompletadasController');
//mantenimiento denuncias finalizadas
Route::resource('denuncias-finalizadas','DenunciasFinalizadasController');
//mantenimiento denuncias baneadas
Route::resource('denuncias-baneadas','DenunciasBaneadasController');
//mantenimiento donantes
Route::resource('donantes', 'DonantesController');
//vista grafias datos generales
Route::get('datos_generales','RegistroController@general')->name('datos_generales');
Route::get('trans_general','DatosTransgeneros@general')->name('datos_generales_trans');
//vista graficas educacion
Route::get('educacion','RegistroController@educacion')->name('educacion');
Route::get('trans_estudios','DatosTransgeneros@educacion')->name('educacion_personas_trans');
//vista graficas acceso a la salud
Route::get('acesso_salud','RegistroController@salud')->name('acesso_salud');
Route::get('trans_salud','DatosTransgeneros@salud')->name('acceso_salud_trans');
//vista graficas trabajo u ocupacion
Route::get('trabajo_ocupacion','RegistroController@trabajo')->name('trabajo_ocupacion');
Route::get('trans_ocupacion', 'DatosTransgeneros@trabajo')->name('trabajo_ocupacion_trans');
//vista graficas acceso a la vivienda
Route::get('acceso_vivienda','RegistroController@vivienda')->name('acceso_vivienda');
Route::get('trans_vivienda','DatosTransgeneros@vivienda')->name('acceso_vivienda_trans');
//vista graficas politica y religion
Route::get('politica_religion','RegistroController@politica')->name('politica_religion');
Route::get('trans_religion','DatosTransgeneros@politica')->name('politica_religion_trans');
//vista graficas derechos
Route::get('derechos','RegistroController@derechos')->name('derechos');
Route::get('trans_derecho','DatosTransgeneros@derechos')->name('derechos_trans');
//mantnimiento para filtrar por año grafica
Route::post('general','RegistroController@general')->name('general');
//Filtro de ciudad por departamentos
Route::get('departamentos/{id}', 'FiltroDepartamentoController@Departament');

});

//:::::::::::::::::::::::Middleware Para admin::::::::::::::::::::::::://
Route::group(['as'=>'admin.', 'prefix'=>'admin','namespace'=>'Admin','middleware'=>[
    'auth','admin']], function(){

//Dashboard admin vista
Route::get('dashboard','DashboardController@index')->name('dashboard');
//mantenimiento personas
Route::resource('personas','PersonasController');
//mantenimiento denuncias
Route::resource('denuncias','DenunciasController');
//ruta que sirve para exporta denuncia a pdf
Route::get('pdf-denuncia/{id}','DenunciasController@createPDF')->name('denuncia-pdf');
//mantenimiento denuncias pendientes
Route::resource('denuncias-pendientes','DenunciasPendientesController');
Route::Put('editar-pendientes/{id}','DenunciasPendientesController@updatedenuncias')->name('editar-pendientes');
Route::Put('editar-pendientes-documentos/{id}','DenunciasPendientesController@updatedocumentos')->name('editar-pendientes-documentos');
//mantenimiento denuncias seguimiento
Route::resource('denuncias-seguimiento','DenunciasSeguimientoController');
Route::Put('editar-seguimiento/{id}','DenunciasSeguimientoController@updatedenuncias')->name('editar-seguimiento');
Route::Put('editar-seguimiento-documentos/{id}','DenunciasSeguimientoController@updatedocumentos')->name('editar-seguimiento-documentos');
//mantenimiento denuncias completas
Route::resource('denuncias-completadas','DenunciasCompletadasController');
//mantenimiento denuncias finalizadas
Route::resource('denuncias-finalizadas','DenunciasFinalizadasController');
//mantenimiento denuncias baneadas
Route::resource('denuncias-baneadas','DenunciasBaneadasController');
//mantenimiento donantes
Route::resource('donantes', 'DonantesController');
//vista grafias datos generales
Route::get('datos_generales','RegistroController@general')->name('datos_generales');
Route::get('trans_general','DatosTransgeneros@general')->name('datos_generales_trans');
//vista graficas educacion
Route::get('educacion','RegistroController@educacion')->name('educacion');
Route::get('trans_estudios','DatosTransgeneros@educacion')->name('educacion_personas_trans');
//vista graficas acceso a la salud
Route::get('acesso_salud','RegistroController@salud')->name('acesso_salud');
Route::get('trans_salud','DatosTransgeneros@salud')->name('acceso_salud_trans');
//vista graficas trabajo u ocupacion
Route::get('trabajo_ocupacion','RegistroController@trabajo')->name('trabajo_ocupacion');
Route::get('trans_ocupacion', 'DatosTransgeneros@trabajo')->name('trabajo_ocupacion_trans');
//vista graficas acceso a la vivienda
Route::get('acceso_vivienda','RegistroController@vivienda')->name('acceso_vivienda');
Route::get('trans_vivienda','DatosTransgeneros@vivienda')->name('acceso_vivienda_trans');
//vista graficas politica y religion
Route::get('politica_religion','RegistroController@politica')->name('politica_religion');
Route::get('trans_religion','DatosTransgeneros@politica')->name('politica_religion_trans');
//vista graficas derechos
Route::get('derechos','RegistroController@derechos')->name('derechos');
Route::get('trans_derecho','DatosTransgeneros@derechos')->name('derechos_trans');
//mantnimiento para filtrar por año grafica
Route::post('general','RegistroController@general')->name('general');
//Filtro de ciudad por departamentos
Route::get('departamentos/{id}', 'FiltroDepartamentoController@Departament');

});

//:::::::::::::::::::::::Middleware Para Donantes:::::::::::::::::::::::://
Route::group(['as'=>'donante.', 'prefix'=>'donante','namespace'=>'Donante','middleware'=>[
    'auth','donante']], function(){

//Dashboard donante vista
Route::get('dashboard','DashboardController@index')->name('dashboard');
//vista grafias datos generales
Route::get('datos_generales','RegistroController@general')->name('datos_generales');
//vista graficas educacion
Route::get('educacion','RegistroController@educacion')->name('educacion');
//vista graficas acceso a la salud
Route::get('acesso_salud','RegistroController@salud')->name('acesso_salud');
//vista graficas trabajo u ocupacion
Route::get('trabajo_ocupacion','RegistroController@trabajo')->name('trabajo_ocupacion');
//vista graficas acceso a la vivienda
Route::get('acceso_vivienda','RegistroController@vivienda')->name('acceso_vivienda');
//vista graficas politica y religion
Route::get('politica_religion','RegistroController@politica')->name('politica_religion');
//vista graficas derechos
Route::get('derechos','RegistroController@derechos')->name('derechos');
//mantnimiento para filtrar por año grafica
Route::post('general','RegistroController@general')->name('general');

});

//:::::::::::::::::::::::Middleware Para Usuarios:::::::::::::::::::::::://
Route::group(['as'=>'user.', 'prefix'=>'user','namespace'=>'User','middleware'=>[
    'auth','user']], function(){

//vista de inicio usuarios
Route::get('home', 'HomeController@index')->name('home');
//Filtro de ciudad por departamentos
Route::get('departamentos/{id}', 'FiltroDepartamentoController@Departament');
//Mantenimiento Denuncia
Route::post ('denuncias', 'DenunciasController@store')->name('denuncias');;


});

//::::::::::::::::::::::::::Middleware Sin Logeo::::::::::::::::::::::::::://
Route::group(['middleware' => ['guest']], function () {

Route::get('/', 'Frontend\HomeController@index')->name('home');
//Filtro de ciudad por departamentos
Route::get('departamentos/{id}', 'Frontend\FiltroDepartamentoController@Departament');
//Mantenimiento Registro
Route::post ('registro', 'Frontend\RegistroUsuariosController@store')->name('registro');;
//Login Usuarios
Route::post('user_login', 'Auth\UserLoginController@login')->name('user_login');

});